/**
 * 
 */
package com.ppsi.jomi.report.dto;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date May 5, 2017 1:21:29 AM
 */
public class ReportProjectStatusDto extends BaseReportDto {
	
	private String projectNumber;
	private String projectDescription;
	private String projectName;
	
	// priority
	private String lowP;
	private Integer countLowP = 0;
	private String mediumP;
	private Integer countMediumP = 0;
	private String highP;
	private Integer countHighP = 0;
	
	// complexity
	private String cosmeticC;
	private Integer countCosmeticC = 0;
	private String minorC;
	private Integer countMinorC = 0;
	private String mediumC;
	private Integer countMediumC = 0;
	private String majorC;
	private Integer countMajorC = 0;
	
	// status
	private String openS;
	private Integer countOpenS = 0;
	private String inProgressS;
	private Integer countInProgressS = 0;
	private String doneS;
	private Integer countDoneS = 0;
	private String readyToTestS;
	private Integer countReadyToTestS = 0;
	private String closedS;
	private Integer countClosedS = 0;
	
	public String getProjectNumber() {
		return projectNumber;
	}
	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}
	public String getProjectDescription() {
		return projectDescription;
	}
	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getLowP() {
		return lowP;
	}
	public void setLowP(String lowP) {
		this.lowP = lowP;
	}
	public Integer getCountLowP() {
		return countLowP;
	}
	public void setCountLowP(Integer countLowP) {
		this.countLowP = countLowP;
	}
	public String getMediumP() {
		return mediumP;
	}
	public void setMediumP(String mediumP) {
		this.mediumP = mediumP;
	}
	public Integer getCountMediumP() {
		return countMediumP;
	}
	public void setCountMediumP(Integer countMediumP) {
		this.countMediumP = countMediumP;
	}
	public String getHighP() {
		return highP;
	}
	public void setHighP(String highP) {
		this.highP = highP;
	}
	public Integer getCountHighP() {
		return countHighP;
	}
	public void setCountHighP(Integer countHighP) {
		this.countHighP = countHighP;
	}
	public String getCosmeticC() {
		return cosmeticC;
	}
	public void setCosmeticC(String cosmeticC) {
		this.cosmeticC = cosmeticC;
	}
	public Integer getCountCosmeticC() {
		return countCosmeticC;
	}
	public void setCountCosmeticC(Integer countCosmeticC) {
		this.countCosmeticC = countCosmeticC;
	}
	public String getMinorC() {
		return minorC;
	}
	public void setMinorC(String minorC) {
		this.minorC = minorC;
	}
	public Integer getCountMinorC() {
		return countMinorC;
	}
	public void setCountMinorC(Integer countMinorC) {
		this.countMinorC = countMinorC;
	}
	public String getMediumC() {
		return mediumC;
	}
	public void setMediumC(String mediumC) {
		this.mediumC = mediumC;
	}
	public Integer getCountMediumC() {
		return countMediumC;
	}
	public void setCountMediumC(Integer countMediumC) {
		this.countMediumC = countMediumC;
	}
	public String getMajorC() {
		return majorC;
	}
	public void setMajorC(String majorC) {
		this.majorC = majorC;
	}
	public Integer getCountMajorC() {
		return countMajorC;
	}
	public void setCountMajorC(Integer countMajorC) {
		this.countMajorC = countMajorC;
	}
	public String getOpenS() {
		return openS;
	}
	public void setOpenS(String openS) {
		this.openS = openS;
	}
	public Integer getCountOpenS() {
		return countOpenS;
	}
	public void setCountOpenS(Integer countOpenS) {
		this.countOpenS = countOpenS;
	}
	public String getInProgressS() {
		return inProgressS;
	}
	public void setInProgressS(String inProgressS) {
		this.inProgressS = inProgressS;
	}
	public Integer getCountInProgressS() {
		return countInProgressS;
	}
	public void setCountInProgressS(Integer countInProgressS) {
		this.countInProgressS = countInProgressS;
	}
	public String getDoneS() {
		return doneS;
	}
	public void setDoneS(String doneS) {
		this.doneS = doneS;
	}
	public Integer getCountDoneS() {
		return countDoneS;
	}
	public void setCountDoneS(Integer countDoneS) {
		this.countDoneS = countDoneS;
	}
	public String getReadyToTestS() {
		return readyToTestS;
	}
	public void setReadyToTestS(String readyToTestS) {
		this.readyToTestS = readyToTestS;
	}
	public Integer getCountReadyToTestS() {
		return countReadyToTestS;
	}
	public void setCountReadyToTestS(Integer countReadyToTestS) {
		this.countReadyToTestS = countReadyToTestS;
	}
	public String getClosedS() {
		return closedS;
	}
	public void setClosedS(String closedS) {
		this.closedS = closedS;
	}
	public Integer getCountClosedS() {
		return countClosedS;
	}
	public void setCountClosedS(Integer countClosedS) {
		this.countClosedS = countClosedS;
	}
}
