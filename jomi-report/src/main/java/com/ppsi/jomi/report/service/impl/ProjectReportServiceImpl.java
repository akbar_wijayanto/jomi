/**
 * 
 */
package com.ppsi.jomi.report.service.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppsi.jomi.persistence.model.CoreProject;
import com.ppsi.jomi.persistence.model.CoreTask;
import com.ppsi.jomi.persistence.model.CoreUser;
import com.ppsi.jomi.report.dto.ReportProjectStatusDto;
import com.ppsi.jomi.report.dto.ReportTaskDto;
import com.ppsi.jomi.report.dto.ReportUserProjectDto;
import com.ppsi.jomi.report.service.ProjectReportService;
import com.ppsi.jomi.service.CoreProjectManager;
import com.ppsi.jomi.service.CoreTaskManager;
import com.ppsi.jomi.service.CoreUserManager;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date May 7, 2017 2:05:34 PM
 */
@Service
public class ProjectReportServiceImpl implements ProjectReportService {

	private CoreProjectManager coreProjectManager;
	private CoreTaskManager coreTaskManager;
	private CoreUserManager coreUserManager;
	
	@Autowired
	public void setCoreUserManager(CoreUserManager coreUserManager) {
		this.coreUserManager = coreUserManager;
	}
	
	@Autowired
	public void setCoreTaskManager(CoreTaskManager coreTaskManager) {
		this.coreTaskManager = coreTaskManager;
	}
	
	@Autowired
	public void setCoreProjectManager(CoreProjectManager coreProjectManager) {
		this.coreProjectManager = coreProjectManager;
	}
	
	@Override
	public List<ReportTaskDto> getProjectTaskReport(Long projectId) {
		List<ReportTaskDto> dtos = new ArrayList<ReportTaskDto>();
		CoreProject coreProject = coreProjectManager.getLiveById(projectId);
		List<CoreTask> coreTasks = coreTaskManager.getByProjectId(projectId);
		for (CoreTask coreTask : coreTasks) {
			ReportTaskDto dto = new ReportTaskDto();
			dto.setComplexity(coreTask.getComplexity().getLabel());
			dto.setDescription(coreTask.getDescription());
			dto.setFeature(coreTask.getFeature());
			dto.setManHour(coreTask.getManHour());
			dto.setPriority(coreTask.getPriority().getLabel());
			dto.setTaskNumber(coreTask.getTaskNumber());
			dto.setProjectNumber(coreProject.getProjectNumber());
			dto.setProjectDescription(coreProject.getDescription());
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public List<ReportProjectStatusDto> getProjectStatusReport(Long projectId) {
		List<ReportProjectStatusDto> dtos = new ArrayList<ReportProjectStatusDto>();
		CoreProject coreProject = coreProjectManager.getLiveById(projectId);
		List<Object[]> listP = coreTaskManager.getCountAllPriorityByProject(projectId);
		List<Object[]> listC = coreTaskManager.getCountAllComplexityByProject(projectId);
		List<Object[]> listS = coreTaskManager.getCountAllStatusByProject(projectId);
		ReportProjectStatusDto dto = new ReportProjectStatusDto();
		for (Object[] objects : listP) {
			if((Integer)objects[0]+1==1){
				dto.setCountLowP(((BigInteger) objects[1]).intValue());
			} else if((Integer)objects[0]+1==2){
				dto.setCountMediumP(((BigInteger) objects[1]).intValue());
			} else if((Integer)objects[0]+1==3){
				dto.setCountHighP(((BigInteger) objects[1]).intValue());
			}
		}
		for (Object[] objects : listC) {
			if((Integer)objects[0]+1==1){
				dto.setCountCosmeticC(((BigInteger) objects[1]).intValue());
			} else if((Integer)objects[0]+1==2){
				dto.setCountMinorC(((BigInteger) objects[1]).intValue());
			} else if((Integer)objects[0]+1==3){
				dto.setCountMediumC(((BigInteger) objects[1]).intValue());
			} else if((Integer)objects[0]+1==4){
				dto.setCountMajorC(((BigInteger) objects[1]).intValue());
			}
		}
		for (Object[] objects : listS) {
			if((Integer)objects[0]+1==1){
				dto.setCountOpenS(((BigInteger) objects[1]).intValue());
			} else if((Integer)objects[0]+1==2){
				dto.setCountInProgressS(((BigInteger) objects[1]).intValue());
			} else if((Integer)objects[0]+1==3){
				dto.setCountDoneS(((BigInteger) objects[1]).intValue());
			} else if((Integer)objects[0]+1==4){
				dto.setCountReadyToTestS(((BigInteger) objects[1]).intValue());
			} else if((Integer)objects[0]+1==5){
				dto.setCountClosedS(((BigInteger) objects[1]).intValue());
			}
		}
		dto.setProjectNumber(coreProject.getProjectNumber());
		dto.setProjectName(coreProject.getName());
		dto.setProjectDescription(coreProject.getDescription());
		dtos.add(dto);
		return dtos;
	}

	@Override
	public List<ReportUserProjectDto> getUserProject(Long userId) {
		List<ReportUserProjectDto> dtos = new ArrayList<ReportUserProjectDto>();
		CoreUser coreUser = coreUserManager.get(userId);
		List<CoreProject> projects = coreProjectManager.getProjectByUsername(coreUser.getUsername());
		for (CoreProject coreProject : projects) {
			ReportUserProjectDto dto = new ReportUserProjectDto();
			dto.setProjectName(coreProject.getName());
			dto.setProjectNumber(coreProject.getProjectNumber());
			dto.setUserName(coreUser.getFirstName()+" "+coreUser.getLastName());
			dtos.add(dto);
		}
		return dtos;
	}
	
}
