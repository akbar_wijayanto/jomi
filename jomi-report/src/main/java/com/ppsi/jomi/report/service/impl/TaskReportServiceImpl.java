/**
 * 
 */
package com.ppsi.jomi.report.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppsi.jomi.persistence.model.CoreTask;
import com.ppsi.jomi.report.dto.ReportTaskDto;
import com.ppsi.jomi.report.service.TaskReportService;
import com.ppsi.jomi.service.CoreTaskManager;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date May 7, 2017 2:05:34 PM
 */
@Service
public class TaskReportServiceImpl implements TaskReportService {

	private CoreTaskManager coreTaskManager;
	
	@Autowired
	public void setCoreTaskManager(CoreTaskManager coreTaskManager) {
		this.coreTaskManager = coreTaskManager;
	}
	
	@Override
	public List<ReportTaskDto> getReportTasks(Date startDate, Date endDate, Long userId) {
		List<ReportTaskDto> dtos = new ArrayList<ReportTaskDto>();
		List<CoreTask> coreTasks = coreTaskManager.getAllLive();
		for (CoreTask coreTask : coreTasks) {
			ReportTaskDto dto = new ReportTaskDto();
			dto.setComplexity(coreTask.getComplexity().getLabel());
			dto.setDescription(coreTask.getDescription());
			dto.setFeature(coreTask.getFeature());
			dto.setManHour(coreTask.getManHour());
			dto.setPriority(coreTask.getPriority().getLabel());
			dto.setProjectNumber(coreTask.getCoreProject().getProjectNumber());
			dto.setTaskNumber(coreTask.getTaskNumber());
			dtos.add(dto);
		}
		return dtos;
	}

}
