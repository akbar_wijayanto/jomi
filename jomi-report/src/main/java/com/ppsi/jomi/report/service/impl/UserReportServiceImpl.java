/**
 * 
 */
package com.ppsi.jomi.report.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppsi.jomi.enumeration.jomi.TaskStatusEnum;
import com.ppsi.jomi.persistence.model.CoreUser;
import com.ppsi.jomi.persistence.model.NonBillableMandays;
import com.ppsi.jomi.persistence.model.TaskAssignment;
import com.ppsi.jomi.report.dto.ReportUserTimesheetDto;
import com.ppsi.jomi.report.service.UserReportService;
import com.ppsi.jomi.service.CoreUserManager;
import com.ppsi.jomi.service.NonBillableMandaysManager;
import com.ppsi.jomi.service.TaskAssignmentManager;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date May 7, 2017 2:05:34 PM
 */
@Service
public class UserReportServiceImpl implements UserReportService {

	private TaskAssignmentManager taskAssignmentManager;
	private CoreUserManager coreUserManager;
	private NonBillableMandaysManager nonBillableMandaysManager;
	
	@Autowired
	public void setNonBillableMandaysManager(NonBillableMandaysManager nonBillableMandaysManager) {
		this.nonBillableMandaysManager = nonBillableMandaysManager;
	}
	
	@Autowired
	public void setCoreUserManager(CoreUserManager coreUserManager) {
		this.coreUserManager = coreUserManager;
	}
	
	@Autowired
	public void setTaskAssignmentManager(TaskAssignmentManager taskAssignmentManager) {
		this.taskAssignmentManager = taskAssignmentManager;
	}
	
	@Override
	public List<ReportUserTimesheetDto> getuserTimesheet(Long userId) {
		List<ReportUserTimesheetDto> dtos = new ArrayList<ReportUserTimesheetDto>();
		CoreUser coreUser = coreUserManager.get(userId);
		List<TaskAssignment> assignments = taskAssignmentManager.getTaskByUsernameAndStatus(coreUser.getUsername(), TaskStatusEnum.DONE);
		List<NonBillableMandays> nonBillableMandays = nonBillableMandaysManager.getNonBillableByUserId(userId);
		for (TaskAssignment taskAssignment : assignments) {
			ReportUserTimesheetDto dto = new ReportUserTimesheetDto();
			dto.setUserName(coreUser.getFirstName()+" "+coreUser.getLastName());
			dto.setProjectName(taskAssignment.getCoreTask().getCoreProject().getName());
			dto.setProjectNumber(taskAssignment.getCoreTask().getCoreProject().getProjectNumber());
			dto.setManHour(taskAssignment.getCoreTask().getManHour());
			dto.setTaskDescription(taskAssignment.getCoreTask().getDescription());
			dtos.add(dto);
		}
		for (NonBillableMandays nbm : nonBillableMandays) {
			ReportUserTimesheetDto dto = new ReportUserTimesheetDto();
			dto.setUserName(coreUser.getFirstName()+" "+coreUser.getLastName());
			dto.setProjectName("-");
			dto.setProjectNumber("-");
			dto.setManHour(nbm.getManHour());
			dto.setTaskDescription(nbm.getNonBillableMandaysTypeEnum().getLabel()+" : "+nbm.getDescription());
			dtos.add(dto);
		}
		return dtos;
	}

}
