package com.ppsi.jomi.report.util;

/**
 * 
 */
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.ppsi.jomi.exception.JomiException;

/**
 * Create report with Spring + Hibernate list datasource
 * 
 * @author agung.kurniawan
 * Date : 14 Feb 2014
 */
public class ClientReportGenerator {

//	private static final File JRXML_RESOURCE_DIRECTORY = new File(ClientReportGenerator.class.getResource("//report").getFile());
	private static FileOutputStream fileOutputStream;
	
	/**Locate and compile jrxml path first
	 * 
	 * @param jrxmlFilePath
	 * @param list
	 * @return
	 * @throws MiniLoanException
	 */
	private static JasperPrint initializeJasperFile(String jrxmlFilePath, List list) throws JomiException{
		try {
			JRDataSource dataSource = new JRBeanCollectionDataSource(list);
			
			InputStream inputStream = (InputStream) ClientReportGenerator.class.getResourceAsStream("/report/"+jrxmlFilePath);
			System.out.println("input stream " + ClientReportGenerator.class.getResourceAsStream("/report/"+jrxmlFilePath));
			File jrxmlFile = new File(readProperty("jrxml.tempDir")+jrxmlFilePath);
			FileUtils.writeByteArrayToFile(jrxmlFile, IOUtils.toByteArray(inputStream));
			
			JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlFile.getAbsolutePath());
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, dataSource);
			return jasperPrint;
		} catch (JRException e) {
			throw new JomiException(e);
		} catch (FileNotFoundException e) {
			throw new JomiException(e);
		} catch (IOException e) {
			throw new JomiException(e);
		}
	}
	
	/**Generate compiled jrxml file to XLS format
	 * 
	 * @param jrxmlFilePath
	 * @param list
	 * @param outputXLSPath
	 * @return
	 * @throws MiniLoanException
	 */
	public static final File generateXLS(String jrxmlFilePath, List list, String outputXLSPath) throws JomiException {
		List params = fillParams(list);
		JasperPrint jasperPrint = initializeJasperFile(jrxmlFilePath, params);
		new File(outputXLSPath.concat(".xls")).getParentFile().mkdirs();
		File outputFile = new File(outputXLSPath.concat(".xls"));
		
		try {
			JasperExportManager.exportReportToPdfFile(jasperPrint, outputFile.getAbsolutePath());
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			JRXlsExporter exporterXLS = new JRXlsExporter();
	        exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
	        exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, output);
	        exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
	        exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
	        exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
	        exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
	        exporterXLS.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, Boolean.TRUE);
	        exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
	        exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
	        exporterXLS.setParameter(JRXlsExporterParameter.IS_IGNORE_GRAPHICS, Boolean.TRUE);
	        exporterXLS.setParameter(JRXlsExporterParameter.IS_FONT_SIZE_FIX_ENABLED, Boolean.FALSE);
	        exporterXLS.exportReport();
	        fileOutputStream = new FileOutputStream(outputFile);
			fileOutputStream.write(output.toByteArray());
	        
	        return outputFile;
		} catch (FileNotFoundException e) {
			throw new JomiException(e);
		} catch (IOException e) {
			throw new JomiException(e);
		} catch (JRException e) {
			throw new JomiException(e);
		}
	}
	
	/**Generate compiled jrxml file to PDF format
	 * 
	 * @param jrxmlFilePath
	 * @param list
	 * @param outputXLSPath
	 * @throws MiniLoanException
	 */
	public static final File generatePDF(String jrxmlFilePath, List list, String outputXLSPath) throws JomiException {
		List params = fillParams(list);
		JasperPrint jasperPrint = initializeJasperFile(jrxmlFilePath, params);
		System.out.println("jasper " + jasperPrint);
		new File(outputXLSPath.concat(".pdf")).getParentFile().mkdirs();
		File outputFile = new File(outputXLSPath.concat(".pdf"));
		try {
			JasperExportManager.exportReportToPdfFile(jasperPrint, outputFile.getAbsolutePath());
			return outputFile;
		} catch (JRException e) {
			throw new JomiException(e);
		}
	}
	
	/**Using reflection to fill params as field name
	 * 
	 * @param param
	 * @return
	 * @throws MiniLoanException
	 */
	private static List fillParams(List param) throws JomiException {
		List list = new ArrayList();
		for (Object object : param) {
			Map<String, Object> params = new HashMap<String, Object>();
			for (Field field : ClazzPropertyUtil.getFields(object)) {
				params.put(field.getName(), ClazzPropertyUtil.getField(object, field.getName(), null));
			}
			list.add(params);
		}
		return list;
	}
	
	public static String readProperty(String propertyName) throws JomiException {
		String result = "";
		Properties properties = new Properties();
		String propertyFileName = "report.properties";
		
		InputStream inputStream = ClientReportGenerator.class.getClassLoader().getResourceAsStream(propertyFileName);
		try {
			properties.load(inputStream);
			if (inputStream == null) {
				throw new JomiException("Report property file not found");
			}
			return properties.getProperty(propertyName);
		} catch (IOException e) {
			throw new JomiException(e);
		}
	}
}
