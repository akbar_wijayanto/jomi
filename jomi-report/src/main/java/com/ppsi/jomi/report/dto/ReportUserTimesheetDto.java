/**
 * 
 */
package com.ppsi.jomi.report.dto;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date May 5, 2017 1:21:29 AM
 */
public class ReportUserTimesheetDto extends BaseReportDto {

	private String userName;
	private String projectNumber;
    private String projectName;
	private String taskDescription;
    private Integer manHour;
	
    public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getProjectNumber() {
		return projectNumber;
	}
	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getTaskDescription() {
		return taskDescription;
	}
	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}
	public Integer getManHour() {
		return manHour;
	}
	public void setManHour(Integer manHour) {
		this.manHour = manHour;
	}
    
}
