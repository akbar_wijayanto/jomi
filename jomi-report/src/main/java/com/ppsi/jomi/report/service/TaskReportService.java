package com.ppsi.jomi.report.service;

import java.util.Date;
import java.util.List;

import com.ppsi.jomi.report.dto.ReportTaskDto;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date May 5, 2017 1:19:52 AM
 */
public interface TaskReportService {

	List<ReportTaskDto> getReportTasks(Date startDate, Date endDate, Long userId);
	
}
