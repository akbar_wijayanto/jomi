package com.ppsi.jomi.exception;

public class ChargeException extends LoanException {

	private static final long serialVersionUID = -2475913015979783650L;

	public ChargeException(String message) {
        super(message);
    }

    public ChargeException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
