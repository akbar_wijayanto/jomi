package com.ppsi.jomi.exception;

import java.util.List;

import com.ppsi.jomi.ErrorCode;

public class JomiException extends Exception {
	private static final long serialVersionUID = 4025464027622523453L;
	private List<ErrorCode> errorCodes;

	public JomiException() {
		super();
	}

	public JomiException(String message, Throwable cause) {
		super(message, cause);
	}

	public JomiException(String message) {
		super(message);
	}
	
	public JomiException(List<ErrorCode> errorCodes) {
		this.errorCodes = errorCodes;
	}
	
	public JomiException(Throwable cause) {
		super(cause);
	}
	
	public void setErrorCodes(List<ErrorCode> errorCodes ){
		this.errorCodes = errorCodes;
	}

	/**
	 * @return the errorCodes
	 */
	public List<ErrorCode> getErrorCodes() {
		return errorCodes;
	}

}
