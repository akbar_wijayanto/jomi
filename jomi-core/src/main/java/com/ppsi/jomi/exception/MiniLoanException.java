package com.ppsi.jomi.exception;

import java.util.List;

import com.ppsi.jomi.ErrorCode;


public class MiniLoanException extends Exception {
	private static final long serialVersionUID = 4025464027622523453L;
	private List<ErrorCode> errorCodes;

	public MiniLoanException() {
		super();
	}

	public MiniLoanException(String message, Throwable cause) {
		super(message, cause);
	}

	public MiniLoanException(String message) {
		super(message);
	}
	
	public MiniLoanException(List<ErrorCode> errorCodes) {
		this.errorCodes = errorCodes;
	}
	
	public MiniLoanException(Throwable cause) {
		super(cause);
	}
	
	public void setErrorCodes(List<ErrorCode> errorCodes ){
		this.errorCodes = errorCodes;
	}

	/**
	 * @return the errorCodes
	 */
	public List<ErrorCode> getErrorCodes() {
		return errorCodes;
	}

}
