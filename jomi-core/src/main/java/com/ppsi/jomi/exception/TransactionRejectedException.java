package com.ppsi.jomi.exception;

public class TransactionRejectedException extends TransactionException {

	private static final long serialVersionUID = -2475913015979783650L;

	public TransactionRejectedException(String message) {
        super(message);
    }

    public TransactionRejectedException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
