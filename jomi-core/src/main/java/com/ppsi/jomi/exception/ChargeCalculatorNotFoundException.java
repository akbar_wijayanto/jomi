package com.ppsi.jomi.exception;

public class ChargeCalculatorNotFoundException extends LoanException {

	private static final long serialVersionUID = -2475913015979783650L;

	public ChargeCalculatorNotFoundException(String message) {
        super(message);
    }

    public ChargeCalculatorNotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
