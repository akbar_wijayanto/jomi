package com.ppsi.jomi.enumeration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author imam.nurhadianto
 *
 */
public enum CategoryReportEnum {

	TRXSELLER(0, "Laporan Transaksi Penjualan"),
	SETTLEMENT(1, "Laporan Settlement"),
	BUYER(2, "Laporan Pembeli");
	
	private int value;
	private String name;
	
	/**
	 * @param value
	 * @param name
	 */
	private CategoryReportEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	private static final Map<Integer, CategoryReportEnum> lookup = new HashMap<Integer, CategoryReportEnum>();
    static {
        for (CategoryReportEnum d : CategoryReportEnum.values())
            lookup.put(d.getValue(), d);
    }
    
    public static CategoryReportEnum get(Integer id) {
        return lookup.get(id);
    }
}
