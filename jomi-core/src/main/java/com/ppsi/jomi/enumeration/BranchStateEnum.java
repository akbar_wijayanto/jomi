package com.ppsi.jomi.enumeration;

public enum BranchStateEnum {

	ONLINE(1, "Online"),
	OFFLINE(2, "Offline");
	
	private Integer stateValue;
	private String stateType;
	
	private BranchStateEnum(Integer stateValue, String stateType)
	{
		this.stateValue = stateValue;
		this.stateType = stateType;
	}

	public Integer getStateValue() {
		return stateValue;
	}

	public String getStateType() {
		return stateType;
	}
	
}
