/**
 * 
 */
package com.ppsi.jomi.enumeration.jomi;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 19, 2017 11:57:41 PM
 */
public enum ComplexityEnum {
	
	COSMETIC(1,"Cosmetic"),
	MINOR(2,"Minor"),
	MEDIUM(3,"Medium"),
	MAJOR(4,"Major");

	private Integer id;
	private String label;
	
	private ComplexityEnum( Integer id , String label){
		this.label = label;
		this.id = id;
	}	
	
	public Integer getId() {
		return id;
	}
	
	public String getLabel() {
		return label;
	}
	
	private static final Map<Integer, ComplexityEnum> lookup = new HashMap<Integer, ComplexityEnum>();
    static {
        for (ComplexityEnum d : ComplexityEnum.values())
            lookup.put(d.getId(), d);
    }
    
    public static ComplexityEnum get(Integer id) {
        return lookup.get(id);
    }
}
