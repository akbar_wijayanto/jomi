/**
 * 
 */
package com.ppsi.jomi.enumeration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dimas.sulistyono
 *
 */
public enum AccrualTypeEnum {
	BEFORE_HOLIDAY(0, "Before Holiday"),
	AFTER_HOLIDAY(1, "After Holiday");

	private Integer id;
	private String value;
	
	private AccrualTypeEnum(Integer id , String value){
		this.id = id;
		this.value = value;
	}	
	
	public Integer getId() {
		return id;
	}
	
	public String getValue() {
		return value;
	}
	
	private static final Map<Integer, AccrualFrequencyEnum> lookup = new HashMap<Integer, AccrualFrequencyEnum>();
    static {
        for (AccrualFrequencyEnum d : AccrualFrequencyEnum.values())
            lookup.put(d.getId(), d);
    }
    
    public static AccrualFrequencyEnum get(Integer id) {
        return lookup.get(id);
    }
}
