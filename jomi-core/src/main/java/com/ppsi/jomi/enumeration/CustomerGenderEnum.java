/**
 * 
 */
package com.ppsi.jomi.enumeration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author akbar.wijayanto
 * Date Oct 15, 2014 10:56:05 AM
 */
public enum CustomerGenderEnum {

	MALE(0, "Male"),
	FEMALE(1, "Female");
	
	private int value;
	private String name;
	
	/**
	 * @param value
	 * @param name
	 */
	private CustomerGenderEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	private static final Map<Integer, CustomerGenderEnum> lookup = new HashMap<Integer, CustomerGenderEnum>();
    static {
        for (CustomerGenderEnum d : CustomerGenderEnum.values())
            lookup.put(d.getValue(), d);
    }
    
    public static CustomerGenderEnum get(Integer id) {
        return lookup.get(id);
    }
	
}
