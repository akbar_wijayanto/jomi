package com.ppsi.jomi.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum ApplicationTypeEnum {
	ALL("*", "ALL (*)", "*"),
	SYSTEM("CORE","SYSTEM","SYSTEM"),
	CALENDAR("CORE","CALENDAR", "CALENDAR"),
	BRANCH("CORE","BRANCH", "BRANCH"),
	CURRENCY("CORE","CURRENCY", "CURRENCY"),
    CURRENCYRATE("CORE","CURRENCYRATE", "CURRENCYRATE"),
    /* TODO: ADD BY NANA*/
    ACCOUNTOFFICER("CORE","ACCOUNTOFFICER","ACCOUNTOFFICER"),
    DEPARTMENT("CORE","DEPARTMENT","DEPARTMENT"),
    TRANSACTIONCODE("CORE","TRANSACTIONCODE","TRANSACTIONCODE"),
    DENOMINATION("CORE","DENOMINATION","DENOMINATION"),
    VAULT("CORE","VAULT","VAULT"),
    TELLERACCOUNT("CORE","TELLERACCOUNT","TELLERACCOUNT"),
    APPLICATION("CORE","APPLICATION","APPLICATION"),
    LEVEL("CORE","LEVEL","LEVEL"),
    
    INTERESTBASIS("CORE","INTERESTBASIS","INTERESTBASIS"),
    INTERESTBASE("CORE","INTERESTBASE","INTERESTBASE"),
    INTERESTPARAMETER("CORE","INTERESTPARAMETER","INTERESTPARAMETER"),
    PRODUCT("CORE","PRODUCT","PRODUCT"),
    TAX("CORE","TAX","TAX"),
    COA("CORE","COA","COA"),
    ACCOUNTING("CORE","ACCOUNTING","ACCOUNTING"),
    ACTIVITY("CORE","ACTIVITY","ACTIVITY"),
    
    MENU("CORE","MENU", "MENU"),BATCH("CORE","BATCH", "BATCH"),
    CLOSING("CORE", "CLOSING", "CLOSING"),
    HIERARCHY("CORE","HIERARCHY","HIERARCHY"),
    SECURITY("CORE","SECURITY","SECURITY"),
    SECURITYPOLICY("CORE","SECURITYPOLICY","SECURITYPOLICY"),
    INTERESTIBT("CORE","INTERESTIBT","INTERESTIBT")

//    DEFINITION("DATA","DEFINITION","DEFINITION"),
//    MAPPING("DATA","MAPPING","MAPPING"),
//    SOURCE("DATA","SOURCE", "SOURCE"),
//    ACCOUNT("LEDGER","ACCOUNT", "ACCOUNT"),
//    ACCOUNTTEMPLATE("LEDGER","ACCOUNTTEMPLATE", "ACCOUNTTEMPLATE"),
//    BATCHJOURNAL("LEDGER","BATCHJOURNAL","BATCHJOURNAL"),
//    BUDGET("LEDGER","BUDGET","BUDGET"),
//    COSTCENTER("CORE","COSTCENTER","COSTCENTER"),
//    DIMENSION("LEDGER","DIMENSION","DIMENSION"),
//    DOUBLEENTRY("LEDGER","DOUBLEENTRY","DOUBLEENTRY"),
//    RECURRING("LEDGER","RECURRING","RECURRING"),
//    REPORT("LEDGER","REPORT","REPORT"),
//    TRANSACTION("LEDGER","TRANSACTION","TRANSACTION"),
//    TRANSACTIONCODE("LEDGER","TRANSACTIONCODE","TRANSACTIONCODE"),
//    VIRTUALMAP("LEDGER","VIRTUALMAP","VIRTUALMAP"),
//    AMORTIZATION("PSAK","AMORTIZATION","AMORTIZATION"),
//    SUMMARYAMORTIZATION("PSAK","SUMMARYAMORTIZATION","SUMMARYAMORTIZATION"),
//    COLLECTIVE("PSAK","COLLECTIVE","COLLECTIVE"),
//    SUMMARYCOLLECTIVE("PSAK","SUMMARYCOLLECTIVE","SUMMARYCOLLECTIVE"),
//    INDIVIDUAL("PSAK","INDIVIDUAL","INDIVIDUAL"),
//    SUMMARYINDIVIDUAL("PSAK","SUMMARYINDIVIDUAL","SUMMARYINDIVIDUAL");
    ;
	private String applicationValue;
	private String applicationType;
    private String applicationModule;
	
	
	private ApplicationTypeEnum(String applicationModule, String applicationType, String applicationValue) {
		this.applicationValue = applicationValue;
		this.applicationType = applicationType;
        this.applicationModule = applicationModule;
	}

	public String getApplicationValue() {
		return applicationValue;
	}
	
	public String getApplicationType() {
		return applicationType;
	}

    public String getApplicationModule() {
        return applicationModule;
    }
    
    private static final Map<String, ApplicationTypeEnum> lookup = new HashMap<String, ApplicationTypeEnum>();
    static {
        for (ApplicationTypeEnum d : ApplicationTypeEnum.values())
            lookup.put(d.getApplicationValue(), d);
    }
    
    public static ApplicationTypeEnum get(String applicationValue) {
        return lookup.get(applicationValue);
    }
}
