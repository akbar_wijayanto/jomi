/**
 * 
 */
package com.ppsi.jomi.enumeration.jomi;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 19, 2017 11:57:41 PM
 */
public enum TaskStatusEnum {
	
	OPEN(1,"Open"),
	IN_PROGRESS(2,"In Progress"),
	DONE(3,"Done"),
	READY_TO_TEST(4,"Ready To Test"),
	CLOSED(5,"Closed");

	private Integer id;
	private String label;
	
	private TaskStatusEnum( Integer id , String label){
		this.label = label;
		this.id = id;
	}	
	
	public Integer getId() {
		return id;
	}
	
	public String getLabel() {
		return label;
	}
	
	private static final Map<Integer, TaskStatusEnum> lookup = new HashMap<Integer, TaskStatusEnum>();
    static {
        for (TaskStatusEnum d : TaskStatusEnum.values())
            lookup.put(d.getId(), d);
    }
    
    public static TaskStatusEnum get(Integer id) {
        return lookup.get(id);
    }
}
