package com.ppsi.jomi.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum SettleStatusEnum {
	
    UNPAID(1,"UNPAID"),
    PAID(0,"PAID");
//    INAU(2,"INAU");
	
	private String name;
	private Integer code;
	
	private SettleStatusEnum(Integer code, String name){
	      this.name=name;
	      this.code=code;
	    }

	    public String getName() {
	        return name;
	    }
	    
	    public Integer getCode() {
			return code;
		}

		private static final Map<String, SettleStatusEnum> lookup = new HashMap<String, SettleStatusEnum>();
	    static {
	        for (SettleStatusEnum d : SettleStatusEnum.values())
	            lookup.put(d.getName(), d);
	    }
	    
	    public static SettleStatusEnum get(String id) {
	        return lookup.get(id);
	    }
}
