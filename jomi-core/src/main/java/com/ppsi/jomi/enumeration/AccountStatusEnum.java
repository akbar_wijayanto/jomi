package com.ppsi.jomi.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum AccountStatusEnum {

	ACTIVE(0, "ACTIVE"), 
	INACTIVE(1, "INACTIVE"),
	DORMANT(2, "DORMANT")
	;

	private int key;
	private String value;

	private AccountStatusEnum(int key, String value) {
		this.value = value;
		this.key = key;
	}

	public int getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	private static final Map<Integer, AccountStatusEnum> lookup = new HashMap<Integer, AccountStatusEnum>();
    static {
        for (AccountStatusEnum d : AccountStatusEnum.values())
            lookup.put(d.getKey(), d);
    }
    
    public static AccountStatusEnum get(int id) {
        return lookup.get(id);
    }


}
