package com.ppsi.jomi.enumeration.jomi;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 29, 2017 2:14:56 PM
 */
public enum ErrorCodeEnum {

	// ----------------------------------- GENERAL ERROR CODE : 1xxx -----------------------------------
	SUCCESS						("1000", "Process successful."),
	PARSING_ERROR				("1001", "Parsing error."),
	INVALID_PARAMETER			("1002", "Invalid parameter."),
	INCOMPLETE_FIELD			("1003", "Missing mandatory field."),
	MALFORMED_MESSAGE			("1004", "Malformed message format."),
	
	// ----------------------------------- BUSINESS PROCESS USER ERROR CODE : 20xx - 21xx -----------------------------------
    BAD_CREDENTIAL				("2001", "Bad credential."),
    USER_LOCKED					("2002", "User is locked."),
    USER_DISABLED				("2003", "User is disabled."),
    USER_INACTIVE				("2004", "User is inactive."),
    AUTHENTICATION_FAILED		("2005", "Authentication failed."),
    UNAUTHORIZED_ACCESS			("2006", "Unauthorized access."),
    SESSION_TIMEOUT				("2007", "Timeout has reached when accessing application."),
    MAX_LOGIN_ATTEMPT			("2008", "Max login attempt has been reached."),
    CUSTOMER_NOT_FOUND			("2009", "Customer not found."),
    DATA_NOT_FOUND				("2010", "Customer not found."),
    LIST_EMPTY					("2011", "List empty."),
    
    // ----------------------------------- UNKNOWN ERROR CODE : 9xxx -----------------------------------
    UNKNOWN_ERROR				("9999", "Unknown error has occured."),
	SYSTEM						("9998", "Unknown error has occured.");
	
	private String code;
	private String defaultMsg;
	
	private ErrorCodeEnum(String code, String defaultMsg) {
		this.code = code;
		this.defaultMsg = defaultMsg;
	}

	public final String getCode() {
		return code;
	}

	public String getDefaultMsg() {
		return defaultMsg;
	}

	public void setDefaultMsg(final String defaultMsg) {
		this.defaultMsg = defaultMsg;
	}
	
	public String getFullMsg(){
		return code + " : " + defaultMsg;
	}
	
}

