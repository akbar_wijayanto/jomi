/**
 * 
 */
package com.ppsi.jomi.enumeration.jomi;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 19, 2017 11:57:41 PM
 */
public enum NonBillableMandaysTypeEnum {
	
	LEAVE(1,"Leave"),
	SICK(2,"Sick"),
	PERMISSION(3,"Permission"),
	TRAINING(4,"Training"),
	NON_PROJECT(5,"Non Project");

	private Integer id;
	private String label;
	
	private NonBillableMandaysTypeEnum( Integer id , String label){
		this.label = label;
		this.id = id;
	}	
	
	public Integer getId() {
		return id;
	}
	
	public String getLabel() {
		return label;
	}
	
	private static final Map<Integer, NonBillableMandaysTypeEnum> lookup = new HashMap<Integer, NonBillableMandaysTypeEnum>();
    static {
        for (NonBillableMandaysTypeEnum d : NonBillableMandaysTypeEnum.values())
            lookup.put(d.getId(), d);
    }
    
    public static NonBillableMandaysTypeEnum get(Integer id) {
        return lookup.get(id);
    }
}
