package com.ppsi.jomi.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum ModuleTypeEnum {
	ALL("ALL (*)", "*"),
	CORE("CORE", "CORE"),
//	LEDGER("LEDGER", "LEDGER"),
	TRX("TRX", "TRX");
	
	private String moduleValue;
	private String moduleType;
	
	private ModuleTypeEnum(String moduleType, String moduleValue) {
		this.moduleValue = moduleValue;
		this.moduleType = moduleType;
	}

	public String getModuleValue() {
		return moduleValue;
	}
	
	public String getModuleType() {
		return moduleType;
	}
	
	private static final Map<String, ModuleTypeEnum> lookup = new HashMap<String, ModuleTypeEnum>();
    static {
        for (ModuleTypeEnum d : ModuleTypeEnum.values())
            lookup.put(d.getModuleValue(), d);
    }
    
    public static ModuleTypeEnum get(String id) {
        return lookup.get(id);
    }
}
