package com.ppsi.jomi.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum SystemManualTransactionEnum {

	SYSTEM(0, "SYSTEM"), MANUAL(1, "MANUAL");

	private int systemValue;
	private String systemType;

	private SystemManualTransactionEnum(int systemValue, String systemType) {
		this.systemType = systemType;
		this.systemValue = systemValue;
	}

	public int getSystemValue() {
		return systemValue;
	}

	public String getSystemType() {
		return systemType;
	}
	
	private static final Map<Integer, SystemManualTransactionEnum> lookup = new HashMap<Integer, SystemManualTransactionEnum>();
    static {
        for (SystemManualTransactionEnum d : SystemManualTransactionEnum.values())
            lookup.put(d.getSystemValue(), d);
    }
    
    public static SystemManualTransactionEnum get(Integer id) {
        return lookup.get(id);
    }

	

}
