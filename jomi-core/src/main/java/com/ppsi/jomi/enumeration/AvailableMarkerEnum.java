package com.ppsi.jomi.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum AvailableMarkerEnum {

	YES(1, "YES"),
	NO(2, "NO");
	
	private Integer id;
	private String lable;
	
	private AvailableMarkerEnum(Integer id, String lable){
		this.id = id;
		this.lable = lable;
	}

	public Integer getId() {
		return id;
	}

	public String getLable() {
		return lable;
	}
	
	private static final Map<Integer, AvailableMarkerEnum> lookup = new HashMap<Integer, AvailableMarkerEnum>();
    static {
        for (AvailableMarkerEnum d : AvailableMarkerEnum.values())
            lookup.put(d.getId(), d);
    }
    
    public static AvailableMarkerEnum get(Integer id) {
        return lookup.get(id);
    }
}
