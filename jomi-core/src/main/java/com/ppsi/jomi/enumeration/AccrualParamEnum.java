package com.ppsi.jomi.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum AccrualParamEnum {
	FIRST(1,"FIRST"),
	LAST(2,"LAST"),
	BOTH(3,"BOTH");
	
	private int id;
	private String  lable;
		
	private AccrualParamEnum(int id, String lable) {
		this.id = id;
		this.lable = lable;
	}

	public int getId() {
		return id;
	}

	public String getLable() {
		return lable;
	}
	
	private static final Map<Integer, AccrualParamEnum> lookup = new HashMap<Integer, AccrualParamEnum>();
    static {
        for (AccrualParamEnum d : AccrualParamEnum.values())
            lookup.put(d.getId(), d);
    }
    
    public static AccrualParamEnum get(Integer id) {
        return lookup.get(id);
    }
	
}
