/**
 * 
 */
package com.ppsi.jomi.enumeration.jomi;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 19, 2017 11:57:41 PM
 */
public enum PriorityEnum {
	
	LOW(1,"Low"),
	MEDIUM(2,"Medium"),
	HIGH(3,"High");

	private Integer id;
	private String label;
	
	private PriorityEnum( Integer id , String label){
		this.label = label;
		this.id = id;
	}	
	
	public Integer getId() {
		return id;
	}
	
	public String getLabel() {
		return label;
	}
	
	private static final Map<Integer, PriorityEnum> lookup = new HashMap<Integer, PriorityEnum>();
    static {
        for (PriorityEnum d : PriorityEnum.values())
            lookup.put(d.getId(), d);
    }
    
    public static PriorityEnum get(Integer id) {
        return lookup.get(id);
    }
}
