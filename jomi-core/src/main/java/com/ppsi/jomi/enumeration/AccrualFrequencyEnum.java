/**
 * 
 */
package com.ppsi.jomi.enumeration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author arni.sihombing
 *
 */
public enum AccrualFrequencyEnum {
	DAILY(1,"Daily"),
	MONTHLY(2,"Monthly");

	private Integer id;
	private String lable;
	
	private AccrualFrequencyEnum(Integer id , String lable){
		this.id = id;
		this.lable = lable;
	}	
	
	public Integer getId() {
		return id;
	}
	
	public String getLable() {
		return lable;
	}
	
	private static final Map<Integer, AccrualFrequencyEnum> lookup = new HashMap<Integer, AccrualFrequencyEnum>();
    static {
        for (AccrualFrequencyEnum d : AccrualFrequencyEnum.values())
            lookup.put(d.getId(), d);
    }
    
    public static AccrualFrequencyEnum get(Integer id) {
        return lookup.get(id);
    }
	
}
