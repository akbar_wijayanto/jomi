package com.ppsi.jomi.enumeration;

import java.util.HashMap;
import java.util.Map;


public enum SystemStatusEnum {
	
	OFFLINE(0, "OFFLINE"),
	ONLINE(1, "ONLINE");
	
    private SystemStatusEnum(Integer index, String value) {
		this.index = index;
		this.value = value;
	}
    
	private static final Map<Integer, SystemStatusEnum> lookup = new HashMap<Integer, SystemStatusEnum>();
    static {
        for (SystemStatusEnum d : SystemStatusEnum.values())
            lookup.put(d.getIndex(), d);
    }
    
    public static SystemStatusEnum get(int id) {
        return lookup.get(id);
    }
    
    private Integer index;
    private String value;
    
	public Integer getIndex() {
		
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}


}
