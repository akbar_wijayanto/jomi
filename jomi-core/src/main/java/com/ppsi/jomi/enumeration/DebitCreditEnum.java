package com.ppsi.jomi.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum DebitCreditEnum {
	DEBIT(1,"D"),
	CREDIT(2,"C");
	
	private int entryValue;
	private String entryType;

    private static final Map<Integer, DebitCreditEnum> lookup = new HashMap<Integer, DebitCreditEnum>();

    static {
        for (DebitCreditEnum d : DebitCreditEnum.values())
            lookup.put(d.getEntryValue(), d);
    }
	private DebitCreditEnum(int entryValue, String entryType) {
		this.entryType = entryType;
		this.entryValue = entryValue;
	}

	public int getEntryValue() {
		return entryValue;
	}

	public String getEntryType() {
		return entryType;
	}

    public static DebitCreditEnum get(Integer value) {
        return lookup.get(value);
    }
}
