/**
 * 
 */
package com.ppsi.jomi.enumeration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author arni.sihombing
 * edited by dimas
 *
 */
public enum AutoRepayEnum {
	AUTOMATIC(1,"Automatic"),
	MANUAL(2,"Manual");

	private Integer id;
	private String  lable;
	
	private AutoRepayEnum( Integer id , String lable){
		this.lable = lable;
		this.id = id;
	}	
	
	public Integer getId() {
		return id;
	}
	
	public String getLable() {
		return lable;
	}
	
	private static final Map<Integer, AutoRepayEnum> lookup = new HashMap<Integer, AutoRepayEnum>();
    static {
        for (AutoRepayEnum d : AutoRepayEnum.values())
            lookup.put(d.getId(), d);
    }
    
    public static AutoRepayEnum get(Integer id) {
        return lookup.get(id);
    }
}
