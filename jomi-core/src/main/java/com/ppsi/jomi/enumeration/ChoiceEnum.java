/**
 * 
 */
package com.ppsi.jomi.enumeration;

/**
 * @author agung.kurniawan
 * Date : 6 Nov 2014
 */
public enum ChoiceEnum {
	YES("y", "Yes"),
	NO("n", "No"),
	CANCEL("c", "Cancel");

	private String key;
	private String value;

	private ChoiceEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
