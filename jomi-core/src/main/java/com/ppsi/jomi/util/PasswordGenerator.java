package com.ppsi.jomi.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class PasswordGenerator {

	public Long generateOTP() {
		long number = (long) Math.floor(Math.random() * 900000L) + 100000L;		
		return number;
	}
	
	private static final String DEFAULT_CHARSET = "0123456789" + "abcdefghijklmnopqrstuvwxyz" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
	
	public static final String getRandomString(int length) {
        Random rand = new Random(System.currentTimeMillis());
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i <= length; i++ ) {
            int pos = rand.nextInt(DEFAULT_CHARSET.length());
            sb.append(DEFAULT_CHARSET.charAt(pos));
        }
        return sb.toString();
    }
	
	public static final String getRandomString(String charset, int length) {
        Random rand = new Random(System.currentTimeMillis());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i <= length; i++ ) {
            int pos = rand.nextInt(charset.length());
            sb.append(charset.charAt(pos));
        }
        return sb.toString();
    }
	
	public static final String generateSHA256Password(String password, String username){
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
			String salt = password + "{" + username + "}";
			md.update(salt.getBytes());

			byte byteData[] = md.digest();

			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}
}
