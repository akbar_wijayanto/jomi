package com.ppsi.jomi.util;

import java.io.File;

public class FileUtil {

	/**
	 * @author dimas.sulistyono
	 * 
	 * used to check existing file on directory
	 * 
	 * @param pathFile
	 * @return null or pathFile
	 */
	public static String checkExsist(String pathFile) {
		File file = new File(pathFile);
		if (!file.exists())
			return null;
		return pathFile;
	}
}
