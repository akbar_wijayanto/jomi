package com.ppsi.jomi.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.ppsi.jomi.exception.JomiException;

/**
 * @author agung.kurniawan
 * 
 * Created Date 03 Des 2013
 * Updated Date 29 Sept 2014
 */
public class DateUtil {

	private static SimpleDateFormat uiFullFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private static SimpleDateFormat dbFullFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static SimpleDateFormat uiSimpleFormat = new SimpleDateFormat("dd MMMM yyyy");
	private static SimpleDateFormat uiSimpleFormatWithStripe = new SimpleDateFormat("dd-MM-yyyy");
	private static SimpleDateFormat reportFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
	private static SimpleDateFormat simpleStripeDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat uiDatepickerFormat = new SimpleDateFormat("dd-MM-yyyy");
	private static SimpleDateFormat bundleDateFormat = new SimpleDateFormat("yyyyddMMHHmmssSSS");
	
	/**
	 * 
	 * @param date
	 * @return dd/MM/yyyy HH:mm:ss
	 */
	public static final String toUIFullFormat(Date date){
		return uiFullFormat.format(date);
	}
	
	/**
	 * 
	 * @param date
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static final String toDBFullFormat(Date date){
		return dbFullFormat.format(date);
	}
	
	public static final Date fromDBFullFormat(String date) throws ParseException {
		return dbFullFormat.parse(date);
	}
	
	/**
	 * 
	 * @param date
	 * @return dd MMMM yyyy
	 */
	public static final String toUISimpleFormat(Date date){
		return uiSimpleFormat.format(date);
	}
	
	/**
	 * 
	 * @param date
	 * @return only date without hour, minute, second, and milisecond
	 */
	public static final Date toDateOnly(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	
	/**Returned <code>dd-MM-yyyy</code> date format 
	 * 
	 * @param date
	 * @return
	 */
	public static final String toUISimpleFormatWithStripe(Date date){
		return uiSimpleFormatWithStripe.format(date);
	}
	
	/**Add day, <code>-dayAddition</code> to substract day
	 * 
	 * @param date
	 * @param dayAddition
	 * @return
	 */
	public static final Date addDate(Date date, int dayAddition){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, dayAddition);
		return calendar.getTime();
	}
	
	/**Add day, <code>-dayAddition</code> to substract day
         * 
         * @param date
         * @param calendarField
         * @param addition
         * @return
         */
        public static final Date addDate(Date date, int calendarField, int addition){
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(calendarField, addition);
                return calendar.getTime();
        }
	
	
	/**Add month, <code>-monthAddition</code> to substract month
	 * 
	 * @param date
	 * @param monthAddition
	 * @return date
	 */
	public static final Date addMonthDate(Date date, int monthAddition){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, monthAddition);
		return calendar.getTime();
	}

	
	
	/**Add week, <code>-weekAddition</code> to substract week
	 * 
	 * @param date
	 * @param weekAddition
	 * @return date
	 */
	public static final Date addWeekDate(Date date, int weekAddition){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.WEEK_OF_YEAR, weekAddition);
		return calendar.getTime();
	}
	
	
	
	/**Add year, <code>-yearAddition</code> to substract year
	 * 
	 * @param date
	 * @param yearAddition
	 * @return date
	 */
	public static final Date addYearDate(Date date, int yearAddition){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, yearAddition);
		return calendar.getTime();
	}

	
	
	/**Get Day Numbers, <code>DD</code> from Date
	 * 
	 * @param date
	 * @return Integer
	 */
	public static final Integer getDayNumber(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_MONTH);
	}
	
	
	
	/**Return date with formatted string pattern 'dd-MM-yyyy' 
	 * 
	 * @param ddMMMMyyyy
	 * @return
	 * @throws ParseException
	 */
	public static final Date simpleStripedFormatToDate(String ddMMyyy) throws ParseException {
		return uiSimpleFormatWithStripe.parse(ddMMyyy);
	}
	
	
	
	/**
	 * 
	 * @param date
	 * @return dd-MM-yyyy_HH-mm-ss
	 */
	public static final String toReportDateFormat(Date date) {
		return reportFormat.format(date);
	}
	
	
	
	/**Return date with formatted string pattern 'yyyyMMdd'
	 * 
	 * @param yyyyMMdd
	 * @return 
	 * @throws ParseException
	 */
	public static final Date toSimpleFormat(String yyyyMMdd) throws ParseException {
		return simpleDateFormat.parse(yyyyMMdd);
	}
	
	/**Return string pattern 'yyyyMMdd'
	 * 
	 * @param date
	 * @return
	 */
	public static final String toStringSimpleFormat(Date date) {
		return simpleDateFormat.format(date);
	}
	
	/**Return date with formatted string pattern 'yyyy-MM-dd'
	 * 
	 * @param yyyyMMdd
	 * @return
	 * @throws ParseException
	 */
	public static final Date toSimpleStripeFormat(String yyyyMMdd) throws ParseException {
		return simpleStripeDateFormat.parse(yyyyMMdd);
	}
	
	/**Return string pattern 'yyyy-MM-dd'
	 * 
	 * @param date
	 * @return
	 */
	public static final String toStringSimpleStripeFormat(Date date) {
		return simpleStripeDateFormat.format(date);
	}
	
	/**Convert XMLGregorianCalendar to Date format
	 * 
	 * @param gregorianCalendar
	 * @return Date
	 */
	public static final Date fromXMLGregorian(XMLGregorianCalendar gregorianCalendar) {
		return gregorianCalendar.toGregorianCalendar().getTime();
	}
	
	/**Convert Date to XMLGregorianCalendar
	 * 
	 * @param date
	 * @return XMLGregorianCalendar
	 * @throws PocketException
	 */
	public static final XMLGregorianCalendar toXMLGregorian(Date date) throws JomiException{
		try {
			GregorianCalendar gregory = new GregorianCalendar();
			gregory.setTime(date);
			XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);
			return calendar;
		} catch (DatatypeConfigurationException e) {
			throw new JomiException("Invalid date format");
		}
	}
	
	public static final Calendar toCalendar(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}
	
	public static final Date fromUIDatepicker(String ddMMyyyy) throws ParseException {
		return uiDatepickerFormat.parse(ddMMyyyy);
	}
	
	public static final String toBundleDateFormat(Date date) throws ParseException {
		return bundleDateFormat.format(date);
	}
}
