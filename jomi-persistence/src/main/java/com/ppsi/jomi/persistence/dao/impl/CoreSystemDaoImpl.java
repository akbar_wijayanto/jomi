package com.ppsi.jomi.persistence.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ppsi.jomi.Constants;
import com.ppsi.jomi.persistence.dao.CoreSystemDao;
import com.ppsi.jomi.persistence.model.CoreSystem;
import com.ppsi.jomi.persistence.model.CoreUser;

/**
 * author : aloysius.pradipta
 * version 1.0 15/5/2012
 */

@Repository
public class CoreSystemDaoImpl extends GenericDaoImpl<CoreSystem, String> implements CoreSystemDao {

    public CoreSystemDaoImpl() {
        super(CoreSystem.class);
    }

    @SuppressWarnings("rawtypes")
	@Override
    public Date getPreviousDate() {
        List list = getEntityManager()
                .createQuery("from CoreSystem cs where cs.id=:ID")
                .setParameter("ID", Constants.SYSTEM)
                .getResultList();
        return list.size() == 0 ? null : ((CoreSystem) list.get(0)).getPreviousDate();
    }

    @SuppressWarnings("rawtypes")
	@Override
    public Date getToday() {
        List list = getEntityManager()
                .createQuery("from CoreSystem cs where cs.id=:ID")
                .setParameter("ID", Constants.SYSTEM)
                .getResultList();
        return list.size() == 0 ? null : ((CoreSystem) list.get(0)).getTodayDate();
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<CoreSystem> getCoreSystem() {
        return getEntityManager()
                .createQuery("from CoreSystem cs where cs.id=:ID")
                .setParameter("ID", Constants.SYSTEM)
                .getResultList();

    }

    @SuppressWarnings("unchecked")
	@Override
    public List<CoreSystem> getRecordByUser(CoreUser coreUser, String status) {
        return getEntityManager()
                .createQuery("select a from CoreSystem a where (a.createdBy=:createdBy or a.updatedBy=:updatedBy) and a.status=:status")
                .setParameter("createdBy", coreUser.getUsername())
                .setParameter("updatedBy", coreUser.getUsername())
                .setParameter("status", status)
                .getResultList();
    }

	@Override
	@Transactional
	public CoreSystem save(CoreSystem object) {
		
		return super.save(object);
	}

	@Override
	@Transactional
	public void remove(String id) {
		
		super.remove(id);
	}

	@Override
	@Transactional
	public CoreSystem authorize(CoreSystem oldObject, CoreSystem newObject,
			boolean isApproved) {
		
		return super.authorize(oldObject, newObject, isApproved);
	}

    
    
}
