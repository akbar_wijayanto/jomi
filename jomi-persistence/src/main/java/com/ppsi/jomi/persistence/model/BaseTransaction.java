/**
 * 
 */
package com.ppsi.jomi.persistence.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * @author akbar.wijayanto 
 * Date Oct 16, 2015 2:59:06 PM
 */
@MappedSuperclass
public abstract class BaseTransaction extends BaseGenericObject {
	private static final long serialVersionUID = 1L;

	private String reference;

	@Column(name = "reference")
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}
	
}
