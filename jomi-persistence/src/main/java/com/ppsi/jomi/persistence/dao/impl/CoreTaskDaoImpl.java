package com.ppsi.jomi.persistence.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ppsi.jomi.persistence.dao.CoreTaskDao;
import com.ppsi.jomi.persistence.model.CoreTask;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:17:43 AM
 */
@Repository
public class CoreTaskDaoImpl extends GenericDaoImpl<CoreTask, Long> implements CoreTaskDao {

	public CoreTaskDaoImpl() {
		super(CoreTask.class);
	}

	@Override
	public List<CoreTask> getByProjectId(Long projectId) {
		List<CoreTask> list = new ArrayList<CoreTask>();
		String sql = "select t from CoreTask t where t.coreProject.id=:projectId";
		list = getEntityManager()
              .createQuery(sql)
              .setParameter("projectId", projectId)
              .getResultList();

      return list;
	}

	@Override
	public List<Object[]> getCountAllPriorityByProject(Long projectId) {
		List<Object[]> list = new ArrayList<Object[]>();
		String sql = "select t.priority, count(t.priority) from core_task t inner join core_project p on t.project_id = p.id where t.status='LIVE' and p.id = :projectId group by t.priority";
		list = getEntityManager()
              .createNativeQuery(sql)
              .setParameter("projectId", projectId)
              .getResultList();
		return list;
	}

	@Override
	public List<Object[]> getCountAllComplexityByProject(Long projectId) {
		List<Object[]> list = new ArrayList<Object[]>();
		String sql = "select t.complexity, count(t.complexity) from core_task t inner join core_project p on t.project_id = p.id where t.status='LIVE' and p.id = :projectId group by t.complexity";
		list = getEntityManager()
              .createNativeQuery(sql)
              .setParameter("projectId", projectId)
              .getResultList();
		return list;
	}

	@Override
	public List<Object[]> getCountAllStatusByProject(Long projectId) {
		List<Object[]> list = new ArrayList<Object[]>();
		String sql = "select ta.task_status, count(ta.task_status) from task_assignment ta inner join core_task t on ta.task_id = t.id "
				+ "inner join core_project p on t.project_id = p.id where p.id = :projectId group by ta.task_status";
		list = getEntityManager()
              .createNativeQuery(sql)
              .setParameter("projectId", projectId)
              .getResultList();
		return list;
	}

}
