package com.ppsi.jomi.persistence.model;

import java.io.Serializable;
import java.util.Comparator;

/**
 * @author : <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 * @version : 1.0, 10/4/12, 7:42 PM
 */
public class LabelValueStatus implements Comparable, Serializable {


    /**
     * Comparator that can be used for a case insensitive sort of
     * <code>LabelValueStatus</code> objects.
     */
    public static final Comparator CASE_INSENSITIVE_ORDER = new Comparator() {
        public int compare(Object o1, Object o2) {
            String label1 = ((LabelValueStatus) o1).getLabel();
            String label2 = ((LabelValueStatus) o2).getLabel();
            return label1.compareToIgnoreCase(label2);
        }
    };



    /**
     * Default constructor.
     */
    public LabelValueStatus() {
        super();
    }

    /**
     * Construct an instance with the supplied property values.
     *
     * @param label The label to be displayed to the user.
     * @param value The value to be returned to the server.
     * @param status Checked or not checked
     */
    public LabelValueStatus(final String label, final String value, final Boolean status) {
        this.label = label;
        this.value = value;
        this.status = status;
    }

    // ------------------------------------------------------------- Properties


    /**
     * The property which supplies the option label visible to the end user.
     */
    private String label;

    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


    /**
     * The property which supplies the value returned to the server.
     */
    private String value;

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private Boolean status;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * Compare LabelValueBeans based on the label, because that's the human
     * viewable part of the object.
     *
     * @param o LabelValueStatus object to compare to
     * @return 0 if labels match for compared objects
     * @see Comparable
     */
    public int compareTo(Object o) {
        // Implicitly tests for the correct type, throwing
        // ClassCastException as required by interface
        String otherLabel = ((LabelValueStatus) o).getLabel();

        return this.getLabel().compareTo(otherLabel);
    }

    /**
     * Return a string representation of this object.
     *
     * @return object as a string
     */
    public String toString() {
        StringBuffer sb = new StringBuffer("LabelValueStatus[");
        sb.append(this.label);
        sb.append(", ");
        sb.append(this.value);
        sb.append(", ");
        sb.append(this.status);
        sb.append("]");
        return (sb.toString());
    }

    /**
     * LabelValueBeans are equal if their values are both null or equal.
     *
     * @param obj object to compare to
     * @return true/false based on whether values match or not
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof LabelValueStatus)) {
            return false;
        }

        LabelValueStatus bean = (LabelValueStatus) obj;
        int nil = (this.getValue() == null) ? 1 : 0;
        nil += (bean.getValue() == null) ? 1 : 0;
        nil += (bean.getStatus() == null) ? 1 : 0;

        if (nil == 2) {
            return true;
        } else if (nil == 1) {
            return false;
        } else {
            return this.getValue().equals(bean.getValue());
        }

    }

    /**
     * The hash code is based on the object's value.
     *
     * @return hashCode
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return (this.getValue() == null) ? 17 : this.getValue().hashCode();
    }
}
