package com.ppsi.jomi.persistence.dao;

import com.ppsi.jomi.persistence.model.CoreSystemParameter;

public interface CoreSystemParameterDao extends GenericDao<CoreSystemParameter, String>{

}
