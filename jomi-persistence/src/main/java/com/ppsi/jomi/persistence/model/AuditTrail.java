package com.ppsi.jomi.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.ppsi.jomi.persistence.util.CustomJsonDateSerializer;
/**
 * @author : arni
 * @version : 1.0, 9/13/12, 10:59 PM
 */
@Entity
@Table(name = "audit_trail")
public class AuditTrail{

	private Long id;
	
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    private String objectName;
    
    @Column(name = "object_name", nullable = true, insertable = true, updatable = true)
    public String getObjectName(){
    	return objectName;
    }
    
    public void setObjectName(String objectName){
    	this.objectName = objectName;
    }
    
    private String objectId;
    
    @Column(name = "object_id", nullable = true, insertable = true, updatable = true)
    public String getObjectId(){
    	return objectId;
    }
    
    private String parentId;
    
    @Column(name = "parent_id", nullable = true, insertable = true, updatable = true)
    public String getParentId(){
    	return parentId;
    }
    
    public void setParentId(String parentId){
    	this.parentId = parentId;
    }
    
    public void setObjectId(String objectId){
    	this.objectId = objectId;
    }
    
    private String fieldName;
    
    @Column(name = "field_name", nullable = true, insertable = true, updatable = true)
    public String getFieldName(){
    	return fieldName;
    }
    
    public void setFieldName(String fieldName){
    	this.fieldName = fieldName;
    }
    
    private String before;
    
    @Column(name = "value_before", nullable = true, insertable = true, updatable = true, columnDefinition="text")
    public String getBefore(){
    	return before;
    }
    
    public void setBefore(String before){
    	this.before = before;
    }
    
    private String after;
    
    @Column(name = "value_after", nullable = true, insertable = true, updatable = true, columnDefinition="text")
    public String getAfter(){
    	return after;
    }
    
    public void setAfter(String after){
    	this.after = after;
    }
    
    private String actor;
    
    @Column(name = "actor", nullable = true, insertable = true, updatable = true)
    public String getActor(){
    	return actor;
    }
    
    public void setActor(String actor){
    	this.actor = actor;
    }
    
    private Date time;
    
    @JsonSerialize(using=CustomJsonDateSerializer.class)
    @Column(name = "time_info", nullable = true, insertable = true, updatable = true)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getTime(){
    	return time;
    }
    
    public void setTime(Date time){
    	this.time = time;
    }
    
    @Column(name = "action", nullable = true, insertable = true, updatable = true)
    private String action;
    
    public String getAction(){
    	return action;
    }
    
    public void setAction(String action){
    	this.action = action;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuditTrail)) return false;

        AuditTrail trail = (AuditTrail) o;

        if (objectName != null ? !objectName.equals(trail.objectName) : trail.objectName != null) return false;
        if (id != null ? !id.equals(trail.id) : trail.id != null) return false;
        if (objectId != null ? !objectId.equals(trail.objectId) : trail.objectId != null) return false;
        if (fieldName != null ? !fieldName.equals(trail.fieldName) : trail.fieldName != null) return false;
        if (parentId != null ? !parentId.equals(trail.parentId) : trail.parentId != null) return false;
        if (before != null ? !before.equals(trail.before) : trail.before != null) return false;
        if (after != null ? !after.equals(trail.after) : trail.after != null) return false;
        if (actor != null ? !actor.equals(trail.actor) : trail.actor != null) return false;
        if (time != null ? !time.equals(trail.time) : trail.time != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (objectName != null ? objectName.hashCode() : 0);
        result = 31 * result + (objectId != null ? objectId.hashCode() : 0);
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (fieldName != null ? fieldName.hashCode() : 0);
        result = 31 * result + (before != null ? before.hashCode() : 0);
        result = 31 * result + (after != null ? after.hashCode() : 0);
        result = 31 * result + (actor != null ? actor.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AuditTrail {" +
                "id='" + id + '\'' +
                ", objectName='" + objectName + '\'' +
                ", objectId='" + objectId + '\'' +
                ", parentId='" + parentId + '\'' +
                ", fieldName='" + fieldName + '\'' +
                ", before=" + before +
                ", after=" + after +
                ", actor=" + actor +
                ", time=" + time.toString() +
                '}';
    }
}
