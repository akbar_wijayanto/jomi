package com.ppsi.jomi.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.ppsi.jomi.enumeration.jomi.NonBillableMandaysTypeEnum;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 19, 2017 11:35:03 PM
 */
@Table(name = "non_billable_mandays")
@Entity
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update non_billable_mandays set status='HIST' where id=?")
public class NonBillableMandays extends BaseGenericObject {
    private static final long serialVersionUID = 3690197650654049848L;

    private Long id;
    private NonBillableMandaysTypeEnum nonBillableMandaysTypeEnum;
    private String title;
    private String description;
    private Integer manHour;
    private CoreUser coreUser;
    
    @Column(name = "id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name = "type", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public NonBillableMandaysTypeEnum getNonBillableMandaysTypeEnum() {
		return nonBillableMandaysTypeEnum;
	}

	public void setNonBillableMandaysTypeEnum(NonBillableMandaysTypeEnum nonBillableMandaysTypeEnum) {
		this.nonBillableMandaysTypeEnum = nonBillableMandaysTypeEnum;
	}

	@Column(name = "description", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "title", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "man_hour", nullable = true, insertable = true, updatable = true)
	public Integer getManHour() {
		return manHour;
	}

	public void setManHour(Integer manHour) {
		this.manHour = manHour;
	}

	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false, insertable = true, updatable = true)
	public CoreUser getCoreUser() {
		return coreUser;
	}

	public void setCoreUser(CoreUser coreUser) {
		this.coreUser = coreUser;
	}

	@Override
	public String toString() {
		return "NonBillableMandays [id=" + id + ", title=" + title + ", description=" + description + ", manHour="
				+ manHour + ", coreUser=" + coreUser + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coreUser == null) ? 0 : coreUser.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((manHour == null) ? 0 : manHour.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NonBillableMandays other = (NonBillableMandays) obj;
		if (coreUser == null) {
			if (other.coreUser != null)
				return false;
		} else if (!coreUser.equals(other.coreUser))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (manHour == null) {
			if (other.manHour != null)
				return false;
		} else if (!manHour.equals(other.manHour))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	
}
