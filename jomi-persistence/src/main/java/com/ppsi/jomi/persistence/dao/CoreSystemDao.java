package com.ppsi.jomi.persistence.dao;

import java.util.Date;
import java.util.List;

import com.ppsi.jomi.persistence.model.CoreSystem;
import com.ppsi.jomi.persistence.model.CoreUser;

/**
 * author : aloysius.pradipta
 * version 1.0 15/5/2012
 */

public interface CoreSystemDao extends GenericDao<CoreSystem, String> {

    Date getPreviousDate();

    Date getToday();

    List<CoreSystem> getCoreSystem();

    List<CoreSystem> getRecordByUser(CoreUser coreUser, String status);

}
