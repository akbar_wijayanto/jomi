package com.ppsi.jomi.persistence.dao;

import java.util.List;

import com.ppsi.jomi.persistence.model.CoreRole;

/**
 * Role Data Access Object (DAO) interface.
 *
 * @author <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 */
public interface CoreRoleDao extends GenericDao<CoreRole, Integer> {


    /**
     * Gets role information based on rolename
     *
     * @param rolename the rolename
     * @return populated role object
     */
    CoreRole getRoleByName(String rolename);

    /**
     * Removes a role from the database by name
     *
     * @param rolename the role's rolename
     */
    void removeRole(String rolename);

    List<CoreRole> getAllWithList();

    CoreRole getWithListAll(Integer roleID);
    
    CoreRole getById(Integer id);
}
