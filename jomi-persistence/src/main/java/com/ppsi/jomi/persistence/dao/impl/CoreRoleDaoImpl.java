package com.ppsi.jomi.persistence.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ppsi.jomi.persistence.dao.CoreRoleDao;
import com.ppsi.jomi.persistence.model.CoreRole;


/**
 * This class interacts with Spring's HibernateTemplate to save/delete and
 * retrieve Role objects.
 *
 * @author <a href="mailto:bwnoll@gmail.com">Bryan Noll</a>
 */
@Repository
public class CoreRoleDaoImpl extends GenericDaoImpl<CoreRole, Integer> implements CoreRoleDao {

    /**
     * Constructor to create a Generics-based version using Role as the entity
     */
    public CoreRoleDaoImpl() {
        super(CoreRole.class);
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("rawtypes")
	public CoreRole getRoleByName(String rolename) {
        List roles = getEntityManager().createQuery("from CoreRole where name=:name").setParameter("name", rolename).getResultList();
        if (roles.isEmpty()) {
            return null;
        } else {
            return (CoreRole) roles.get(0);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void removeRole(String rolename) {
        Object role = getRoleByName(rolename);
        getEntityManager().remove(role);
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<CoreRole> getAllWithList() {
        String sql = "select distinct r from CoreRole r left outer join fetch r.coreMenus m "
                + "left outer join fetch r.corePermissions p";
        List<CoreRole> listCoreRoles = getEntityManager()
                .createQuery(sql)
                .getResultList();
        System.out.println(listCoreRoles);
        return listCoreRoles.size() == 0 ? null : listCoreRoles;
    }

    @SuppressWarnings("unchecked")
	@Override
    public CoreRole getWithListAll(Integer roleID) {
        String sql = "select r from CoreRole r left outer join fetch r.coreMenus m "
                + "left outer join fetch r.corePermissions p "
                + "where r.id=:ID";
        List<CoreRole> listCoreRoles = getEntityManager()
                .createQuery(sql)
                .setParameter("ID", roleID)
                .getResultList();
        return listCoreRoles.size() == 0 ? null : (CoreRole) listCoreRoles.get(0);
    }

	@SuppressWarnings("rawtypes")
	@Override
	public CoreRole getById(Integer id) {
		List roles = getEntityManager().createQuery("from CoreRole where id=:name").setParameter("name", id).getResultList();

		if (roles.isEmpty()) {
            return null;
        } else {
            return (CoreRole) roles.get(0);
        }
	}

	/*@Override
	@Transactional
	public CoreRole save(CoreRole object) {
		return super.save(object);
	}

	@Override
	@Transactional
	public void remove(Integer id) {
		super.remove(id);
	}

	@Override
	@Transactional
	public CoreRole authorize(CoreRole oldObject, CoreRole newObject,
			boolean isApproved) {
		return super.authorize(oldObject, newObject, isApproved);
	}
    */
}
