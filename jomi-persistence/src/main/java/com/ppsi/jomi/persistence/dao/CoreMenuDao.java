package com.ppsi.jomi.persistence.dao;

import java.util.List;

import com.ppsi.jomi.persistence.model.CoreMenu;

public interface CoreMenuDao extends GenericDao<CoreMenu, Integer> {

    List<CoreMenu> getByUsername(String username);
    List<CoreMenu> getByActiveRole(Integer roleId);

    List<CoreMenu> getByOrderMenu(Integer parentId, Integer orderMenu);
    
    Integer getByPermaLink(String link);
    
    CoreMenu getByParentId(Integer parentId);
	List<CoreMenu> getAllOrderByOrderMenu();
}
