package com.ppsi.jomi.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.ppsi.jomi.enumeration.jomi.TaskStatusEnum;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 19, 2017 11:35:03 PM
 */
@Table(name = "task_assignment")
@Entity
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update task_assignment set status='HIST' where id=?")
public class TaskAssignment extends BaseGenericObject {
    private static final long serialVersionUID = 3690197650654049848L;

    private Long id;
    private CoreTask coreTask;
    private CoreUser coreUser;
    private Date startDate;
    private Date endDate;
    private TaskStatusEnum taskStatus;
    
    @Column(name = "id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "task_id", nullable = false, insertable = true, updatable = true)
	public CoreTask getCoreTask() {
		return coreTask;
	}

	public void setCoreTask(CoreTask coreTask) {
		this.coreTask = coreTask;
	}

	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false, insertable = true, updatable = true)
	public CoreUser getCoreUser() {
		return coreUser;
	}

	public void setCoreUser(CoreUser coreUser) {
		this.coreUser = coreUser;
	}

	@Column(name = "start_date", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Column(name = "end_date", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "task_status", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
	public TaskStatusEnum getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(TaskStatusEnum taskStatus) {
		this.taskStatus = taskStatus;
	}

	@Override
	public String toString() {
		return "TaskAssignment [id=" + id + ", coreTask=" + coreTask + ", coreUser=" + coreUser + ", startDate="
				+ startDate + ", endDate=" + endDate + ", taskStatus=" + taskStatus + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coreTask == null) ? 0 : coreTask.hashCode());
		result = prime * result + ((coreUser == null) ? 0 : coreUser.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((taskStatus == null) ? 0 : taskStatus.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaskAssignment other = (TaskAssignment) obj;
		if (coreTask == null) {
			if (other.coreTask != null)
				return false;
		} else if (!coreTask.equals(other.coreTask))
			return false;
		if (coreUser == null) {
			if (other.coreUser != null)
				return false;
		} else if (!coreUser.equals(other.coreUser))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (taskStatus != other.taskStatus)
			return false;
		return true;
	}
	
}
