package com.ppsi.jomi.persistence.model;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author : <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 * @version : 1.0, 1/31/12 2:11 PM
 */
@Table(name = "core_menu")
@Entity
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update core_menu set status='HIST' where id=?")
public class CoreMenu extends BaseGenericObject {
	private static final long serialVersionUID = 6224488321107663417L;
	
	private Integer id;

    @Column(name = "id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private Integer parentId;

    @NotNull
    @Column(name = "parent_id", nullable = true, insertable = true, updatable = true, length = 10, precision = 0)
    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    private String name;

    @NotEmpty
    @Column(name = "name", nullable = false, insertable = true, updatable = true, length = 50, precision = 0)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String description;

    @NotEmpty
    @Column(name = "description", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String type;

    @NotEmpty
    @Column(name = "type", nullable = false, insertable = true, updatable = true, length = 1, precision = 0)
    @NotNull
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private Boolean system;

    @Column(name = "system", nullable = false, insertable = true, updatable = true, length = 1, precision = 0)
    @NotNull
    public Boolean getSystem() {
        return system;
    }

    public void setSystem(Boolean system) {
        this.system = system;
    }

    private String permalinks;

    @NotEmpty
    @Column(name = "permalinks", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getPermalinks() {
        return permalinks;
    }

    public void setPermalinks(String permalinks) {
        this.permalinks = permalinks;
    }

    private String icon;

    @Column(name = "icon", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	private Integer orderMenu;

	@NotNull
	@Column(name = "order_menu", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
	public Integer getOrderMenu() {
		return orderMenu;
	}

	public void setOrderMenu(Integer orderMenu) {
		this.orderMenu = orderMenu;
	}
	
	private Boolean checklistStatus;

	@Transient
	public Boolean getChecklistStatus() {
		return checklistStatus;
	}

	public void setChecklistStatus(Boolean checklistStatus) {
		this.checklistStatus = checklistStatus;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CoreMenu coreMenu = (CoreMenu) o;

        if (description != null ? !description.equals(coreMenu.description) : coreMenu.description != null)
            return false;
        if (id != null ? !id.equals(coreMenu.id) : coreMenu.id != null) return false;
        if (name != null ? !name.equals(coreMenu.name) : coreMenu.name != null) return false;
        if (parentId != null ? !parentId.equals(coreMenu.parentId) : coreMenu.parentId != null) return false;
        if (permalinks != null ? !permalinks.equals(coreMenu.permalinks) : coreMenu.permalinks != null) return false;
        if (system != null ? !system.equals(coreMenu.system) : coreMenu.system != null) return false;
        if (type != null ? !type.equals(coreMenu.type) : coreMenu.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (system != null ? system.hashCode() : 0);
        result = 31 * result + (permalinks != null ? permalinks.hashCode() : 0);
        return result;
    }

    
    @Override
	public String toString() {
		return "CoreMenu [id=" + id + ", parentId=" + parentId + ", name="
				+ name + ", description=" + description + ", type=" + type
				+ ", system=" + system + ", permalinks=" + permalinks
				+ ", icon=" + icon + ", orderMenu=" + orderMenu + "]";
	}
}
