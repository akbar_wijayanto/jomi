package com.ppsi.jomi.persistence.dao;

import java.util.List;

import com.ppsi.jomi.persistence.model.CoreTask;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:15:34 AM
 */
public interface CoreTaskDao extends GenericDao<CoreTask, Long> {

	List<CoreTask> getByProjectId(Long projectId);
	List<Object[]> getCountAllPriorityByProject(Long projectId);
	List<Object[]> getCountAllComplexityByProject(Long projectId);
	List<Object[]> getCountAllStatusByProject(Long projectId);
}
