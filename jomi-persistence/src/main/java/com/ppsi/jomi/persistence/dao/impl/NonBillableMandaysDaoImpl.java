package com.ppsi.jomi.persistence.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ppsi.jomi.persistence.dao.NonBillableMandaysDao;
import com.ppsi.jomi.persistence.model.NonBillableMandays;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:17:43 AM
 */
@Repository
public class NonBillableMandaysDaoImpl extends GenericDaoImpl<NonBillableMandays, Long> 
	implements NonBillableMandaysDao {

	public NonBillableMandaysDaoImpl() {
		super(NonBillableMandays.class);
	}

	@Override
	public List<NonBillableMandays> getNonBillableByUserId(Long userId) {
		String sql = "FROM NonBillableMandays c WHERE c.coreUser.id = :userId";
		try {
			return (List<NonBillableMandays>) getEntityManager()
					.createQuery(sql)
					.setParameter("userId", userId)
					.getResultList();
		} catch (Exception e) {
			if (log.isTraceEnabled()) log.error(e,e);
			else log.error(e);
			return null;
		}
	}

}
