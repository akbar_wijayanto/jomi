package com.ppsi.jomi.persistence.model;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author : <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 * @version : 1.0, 1/31/12 3:11 PM
 */
@Entity
@Table(name="core_user")
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update core_user set status='HIST' where id=?")
@Inheritance(strategy=InheritanceType.JOINED)
public class CoreUser extends BaseGenericObject {
	private static final long serialVersionUID = 3832626162173359411L;

	private Long id;
	private String username;
	private String password;
	private String confirmPassword;
	private String passwordSalt;
	private String personnelCode;
	private String firstName;
	private String lastName;
	private String email;
	private BigDecimal limitAmount;
	private Long sessionTimeout;
	private Date lastLogon;
	private String passwordHint;
	private boolean accountEnabled;
	private boolean accountExpired;
	private boolean accountLocked;
	private boolean credentialsExpired;
	private Set<CoreRole> coreRoles = new HashSet<CoreRole>();
	private Set<CorePermission> corePermissions = new HashSet<CorePermission>();
	private CoreRole activeRole;
	private String preferredLocale;
	private Integer loginAttempt;
	private String ipAddress;
	private String session;
	private String token;
	private String phoneNumber;
	private String sva;
	private String company;
	
	private Boolean checklistStatus;
	
	public CoreUser() {
	}

	public CoreUser(final String username) {
		this.username = username;
	}

	@Column(name = "id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@Column(name = "username", nullable = false, insertable = true, updatable = true, length = 25, precision = 0, unique = true)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "password", nullable = false, insertable = true, updatable = true, length = 255, precision = 0)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Transient
	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	@Column(name = "password_salt", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
	public String getPasswordSalt() {
		return passwordSalt;
	}

	public void setPasswordSalt(String passwordSalt) {
		this.passwordSalt = passwordSalt;
	}

	@Column(name = "personnel_code", nullable = true, insertable = true, updatable = true, length = 50, precision = 0)
	public String getPersonnelCode() {
		return personnelCode;
	}

	public void setPersonnelCode(String personnelCode) {
		this.personnelCode = personnelCode;
	}

	@NotNull
	@Column(name = "first_name", nullable = true, insertable = true, updatable = true, length = 50, precision = 0)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name", nullable = true, insertable = true, updatable = true, length = 50, precision = 0)
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "email", nullable = true, insertable = true, updatable = true, length = 50, precision = 0)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "limit_amount", precision = 21, scale = 2)
	@Digits(integer = 21, fraction = 2)
	public BigDecimal getLimitAmount() {
		return limitAmount;
	}

	public void setLimitAmount(BigDecimal limitAmount) {
		this.limitAmount = limitAmount;
	}

	@Column(name = "last_logon", nullable = true, insertable = true, updatable = true)
	public Date getLastLogon() {
		return lastLogon;
	}

	public void setLastLogon(Date lastLogon) {
		this.lastLogon = lastLogon;
	}

	@Column(name = "password_hint", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
	public String getPasswordHint() {
		return passwordHint;
	}

	public void setPasswordHint(String passwordHint) {
		this.passwordHint = passwordHint;
	}

	@NotNull
	@Digits(integer = 10, fraction = 0)
	@Min(0L)
	@Column(name = "session_timeout", nullable = true, insertable = true, updatable = true, length = 10, precision = 0)
	public Long getSessionTimeout() {
		return sessionTimeout;
	}

	public void setSessionTimeout(Long sessionTimeout) {
		this.sessionTimeout = sessionTimeout;
	}

	@Column(name = "account_enabled", nullable = true, insertable = true, updatable = true, length = 1, precision = 0)
	public boolean getAccountEnabled() {
		return accountEnabled;
	}

	public void setAccountEnabled(boolean accountEnabled) {
		this.accountEnabled = accountEnabled;
	}

	@Column(name = "account_expired", nullable = true, insertable = true, updatable = true, length = 1, precision = 0)
	public boolean getAccountExpired() {
		return accountExpired;
	}

	public void setAccountExpired(boolean accountExpired) {
		this.accountExpired = accountExpired;
	}

	@Column(name = "account_locked", nullable = true, insertable = true, updatable = true, length = 1, precision = 0)
	public boolean getAccountLocked() {
		return accountLocked;
	}

	public void setAccountLocked(boolean accountLocked) {
		this.accountLocked = accountLocked;
	}

	@Column(name = "credentials_expired", nullable = true, insertable = true, updatable = true, length = 1, precision = 0)
	public boolean getCredentialsExpired() {
		return credentialsExpired;
	}

	public void setCredentialsExpired(boolean credentialsExpired) {
		this.credentialsExpired = credentialsExpired;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "core_user_role", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = @JoinColumn(name = "role_id"))
	@JsonIgnore
	@OrderBy("name")
	public Set<CoreRole> getCoreRoles() {
		return coreRoles;
	}

	public void setCoreRoles(Set<CoreRole> coreRoles) {
		this.coreRoles = coreRoles;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "core_user_permission", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = @JoinColumn(name = "permission_id"))
	@JsonIgnore
	@OrderBy("name")
	public Set<CorePermission> getCorePermissions() {
		return corePermissions;
	}

	public void setCorePermissions(Set<CorePermission> corePermissions) {
		this.corePermissions = corePermissions;
	}

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "active_role_id", nullable = true, insertable = true, updatable = true)
	public CoreRole getActiveRole() {
		return activeRole;
	}

	public void setActiveRole(CoreRole activeRole) {
		this.activeRole = activeRole;
	}

	@Column(name = "preferred_locale", nullable = true, insertable = true, updatable = true, length = 5, precision = 0)
	public String getPreferredLocale() {
		return preferredLocale;
	}

	public void setPreferredLocale(String preferredLocale) {
		this.preferredLocale = preferredLocale;
	}

	@Column(name = "login_attempt", nullable = true, insertable = true, updatable = true)
	public Integer getLoginAttempt() {
		return loginAttempt;
	}

	public void setLoginAttempt(Integer loginAttempt) {
		this.loginAttempt = loginAttempt;
	}

	@Column(name = "ip_address", nullable = true, insertable = true, updatable = true)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name = "session", nullable = true, insertable = true, updatable = true)
	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	@Column(name = "token", nullable = true, insertable = true, updatable = true)
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	@Column(name = "phone_number", nullable = true, insertable = true, updatable = true)
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Column(name = "sva", nullable = true, insertable = true, updatable = true)
	public String getSva() {
		return sva;
	}

	public void setSva(String sva) {
		this.sva = sva;
	}

	@Column(name = "company", nullable = true, insertable = true, updatable = true)
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	
	@Transient
	public Boolean getChecklistStatus() {
		return checklistStatus;
	}

	public void setChecklistStatus(Boolean checklistStatus) {
		this.checklistStatus = checklistStatus;
	}

	@Override
	public String toString() {
		return "CoreUser [id=" + id + ", username=" + username + ", password=" + password + ", confirmPassword="
				+ confirmPassword + ", passwordSalt=" + passwordSalt + ", personnelCode=" + personnelCode
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", limitAmount="
				+ limitAmount + ", sessionTimeout=" + sessionTimeout + ", lastLogon=" + lastLogon + ", passwordHint="
				+ passwordHint + ", accountEnabled=" + accountEnabled + ", accountExpired=" + accountExpired
				+ ", accountLocked=" + accountLocked + ", credentialsExpired=" + credentialsExpired + ", activeRole="
				+ activeRole + ", preferredLocale=" + preferredLocale + ", loginAttempt=" + loginAttempt
				+ ", ipAddress=" + ipAddress + ", session=" + session + ", token=" + token + ", phoneNumber="
				+ phoneNumber + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (accountEnabled ? 1231 : 1237);
		result = prime * result + (accountExpired ? 1231 : 1237);
		result = prime * result + (accountLocked ? 1231 : 1237);
//		result = prime * result + ((activeRole == null) ? 0 : activeRole.hashCode());
		result = prime * result + ((company == null) ? 0 : company.hashCode());
		result = prime * result + ((confirmPassword == null) ? 0 : confirmPassword.hashCode());
		result = prime * result + (credentialsExpired ? 1231 : 1237);
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ipAddress == null) ? 0 : ipAddress.hashCode());
		result = prime * result + ((lastLogon == null) ? 0 : lastLogon.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((limitAmount == null) ? 0 : limitAmount.hashCode());
		result = prime * result + ((loginAttempt == null) ? 0 : loginAttempt.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((passwordHint == null) ? 0 : passwordHint.hashCode());
		result = prime * result + ((passwordSalt == null) ? 0 : passwordSalt.hashCode());
		result = prime * result + ((personnelCode == null) ? 0 : personnelCode.hashCode());
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result + ((preferredLocale == null) ? 0 : preferredLocale.hashCode());
		result = prime * result + ((session == null) ? 0 : session.hashCode());
		result = prime * result + ((sessionTimeout == null) ? 0 : sessionTimeout.hashCode());
		result = prime * result + ((sva == null) ? 0 : sva.hashCode());
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoreUser other = (CoreUser) obj;
		if (accountEnabled != other.accountEnabled)
			return false;
		if (accountExpired != other.accountExpired)
			return false;
		if (accountLocked != other.accountLocked)
			return false;
//		if (activeRole == null) {
//			if (other.activeRole != null)
//				return false;
//		} else if (!activeRole.equals(other.activeRole))
//			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (confirmPassword == null) {
			if (other.confirmPassword != null)
				return false;
		} else if (!confirmPassword.equals(other.confirmPassword))
			return false;
		if (credentialsExpired != other.credentialsExpired)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ipAddress == null) {
			if (other.ipAddress != null)
				return false;
		} else if (!ipAddress.equals(other.ipAddress))
			return false;
		if (lastLogon == null) {
			if (other.lastLogon != null)
				return false;
		} else if (!lastLogon.equals(other.lastLogon))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (limitAmount == null) {
			if (other.limitAmount != null)
				return false;
		} else if (!limitAmount.equals(other.limitAmount))
			return false;
		if (loginAttempt == null) {
			if (other.loginAttempt != null)
				return false;
		} else if (!loginAttempt.equals(other.loginAttempt))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (passwordHint == null) {
			if (other.passwordHint != null)
				return false;
		} else if (!passwordHint.equals(other.passwordHint))
			return false;
		if (passwordSalt == null) {
			if (other.passwordSalt != null)
				return false;
		} else if (!passwordSalt.equals(other.passwordSalt))
			return false;
		if (personnelCode == null) {
			if (other.personnelCode != null)
				return false;
		} else if (!personnelCode.equals(other.personnelCode))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (preferredLocale == null) {
			if (other.preferredLocale != null)
				return false;
		} else if (!preferredLocale.equals(other.preferredLocale))
			return false;
		if (session == null) {
			if (other.session != null)
				return false;
		} else if (!session.equals(other.session))
			return false;
		if (sessionTimeout == null) {
			if (other.sessionTimeout != null)
				return false;
		} else if (!sessionTimeout.equals(other.sessionTimeout))
			return false;
		if (sva == null) {
			if (other.sva != null)
				return false;
		} else if (!sva.equals(other.sva))
			return false;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

}
