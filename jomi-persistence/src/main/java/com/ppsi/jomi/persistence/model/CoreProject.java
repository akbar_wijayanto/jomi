package com.ppsi.jomi.persistence.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 19, 2017 11:35:03 PM
 */
@Table(name = "core_project")
@Entity
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update core_project set status='HIST' where id=?")
public class CoreProject extends BaseGenericObject {
    private static final long serialVersionUID = 3690197650654049848L;

    private Long id;
    private String projectNumber;
    private String name;
    private String description;
    private Set<CoreUser> coreUsers = new HashSet<CoreUser>();
    
    @Column(name = "id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "project_number", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getProjectNumber() {
		return projectNumber;
	}

	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}

	@Column(name = "name", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "core_project_user",
            joinColumns = {@JoinColumn(name = "project_id")},
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    public Set<CoreUser> getCoreUsers() {
		return coreUsers;
	}

	public void setCoreUsers(Set<CoreUser> coreUsers) {
		this.coreUsers = coreUsers;
	}
    
	@Override
	public String toString() {
		return "CoreProject [id=" + id + ", projectNumber=" + projectNumber + ", name=" + name + ", description="
				+ description + ", coreUsers=" + coreUsers + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coreUsers == null) ? 0 : coreUsers.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((projectNumber == null) ? 0 : projectNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoreProject other = (CoreProject) obj;
		if (coreUsers == null) {
			if (other.coreUsers != null)
				return false;
		} else if (!coreUsers.equals(other.coreUsers))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (projectNumber == null) {
			if (other.projectNumber != null)
				return false;
		} else if (!projectNumber.equals(other.projectNumber))
			return false;
		return true;
	}
    
}
