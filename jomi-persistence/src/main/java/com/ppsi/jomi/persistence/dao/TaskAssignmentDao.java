package com.ppsi.jomi.persistence.dao;

import java.util.List;

import com.ppsi.jomi.enumeration.jomi.TaskStatusEnum;
import com.ppsi.jomi.persistence.model.TaskAssignment;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:15:34 AM
 */
public interface TaskAssignmentDao extends GenericDao<TaskAssignment, Long> {
	List<TaskAssignment> getTaskByUsername(String username);
	List<TaskAssignment> getTaskByUsernameAndStatus(String username, TaskStatusEnum taskStatusEnum);
}
