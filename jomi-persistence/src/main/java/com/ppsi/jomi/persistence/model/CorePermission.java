package com.ppsi.jomi.persistence.model;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author : <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 * @version : 1.0, 1/31/12 3:11 PM
 */
@Table(name = "core_permission")
@Entity
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update core_permission set status='HIST' where id=?")
public class CorePermission extends BaseGenericObject {
    private static final long serialVersionUID = 3690197650654042848L;

    public CorePermission() {
    }


    private Integer id;

    @Column(name = "id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private String name;

    @Column(name = "name", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String description;

    @Column(name = "description", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String moduleName;

    @Column(name = "module_name", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    private String applicationName;

    @Column(name = "application_name", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    private String actionName;

    @Column(name = "action_name", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    private String recordId;

    @Column(name = "record_id", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    private Boolean checklistStatus;

    @Transient
    public Boolean getChecklistStatus() {
		return checklistStatus;
	}

	public void setChecklistStatus(Boolean checklistStatus) {
		this.checklistStatus = checklistStatus;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CorePermission)) return false;

        CorePermission that = (CorePermission) o;

        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (recordId != null ? !recordId.equals(that.recordId) : that.recordId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (recordId != null ? recordId.hashCode() : 0);
        return result;
    }

    @Override
	public String toString() {
		return "CorePermission [id=" + id + ", name=" + name + ", description="
				+ description + ", moduleName=" + moduleName
				+ ", applicationName=" + applicationName + ", actionName="
				+ actionName + ", recordId=" + recordId + "]";
	}
}
