package com.ppsi.jomi.persistence.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ppsi.jomi.persistence.dao.CorePermissionDao;
import com.ppsi.jomi.persistence.model.CorePermission;

@Repository("corePermissionDao")
public class CorePermissionDaoImpl extends GenericDaoImpl<CorePermission, Integer> implements CorePermissionDao {

    public CorePermissionDaoImpl() {
        super(CorePermission.class);
    }

    @SuppressWarnings("unchecked")
	@Override
	public List<String> getAplication() {
		String sql = "select distinct p.applicationName from CorePermission p where applicationName<>'*'";
		 List<String> listApp = getEntityManager()
                .createQuery(sql)
                .getResultList();
		return listApp;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CorePermission> getByAplication(String applicationName) {
		String sql = "select p from CorePermission p where p.applicationName=:APPNAME";
		 List<CorePermission> permissions = getEntityManager()
               .createQuery(sql)
               .setParameter("APPNAME", applicationName)
               .getResultList();
		return permissions;
	}

	/*@Override
	@Transactional
	public CorePermission save(CorePermission object) {
		
		return super.save(object);
	}

	@Override
	@Transactional
	public void remove(Integer id) {
		
		super.remove(id);
	}

	@Override
	@Transactional
	public CorePermission authorize(CorePermission oldObject,
			CorePermission newObject, boolean isApproved) {
		
		return super.authorize(oldObject, newObject, isApproved);
	}*/
    
}
