package com.ppsi.jomi.persistence.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.ppsi.jomi.enumeration.AccrualTypeEnum;

@Table(name = "core_system")
@Entity
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update core_system set status='HIST' where id=?")
public class CoreSystem extends BaseGenericObject {

    private static final long serialVersionUID = 6822803811718890357L;
    private String id;
    private String companyName;
    private Integer systemStatus; 
//    private CoreCurrency localCurrency;
//    private CoreCurrency crossCurrency;
//    private CoreCurrency reportingCurrency;
    private Date todayDate;
    private Date nextDate;
    private Date previousDate;
    private String ldapDomain;
    private String ldapAddress;
    private String defaultHoliday;
    private Date postingDate;
    private AccrualTypeEnum holidayPosting;
    private BigDecimal serviceCash;

	@Column(name = "id", nullable = false, insertable = true, updatable = false, length = 10, precision = 0)
    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "company_name", nullable = true, insertable = true,length=255, updatable = true)
    public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Column(name = "system_status", nullable = true, insertable = true, updatable = true)
	public Integer getSystemStatus() {
		return systemStatus;
	}

	public void setSystemStatus(Integer systemStatus) {
		this.systemStatus = systemStatus;
	}
	
//    @OneToOne
//    @JoinColumn(name = "local_currency_id", nullable = true, insertable = true, updatable = true)
//    public CoreCurrency getLocalCurrency() {
//        return localCurrency;
//    }
//
//    public void setLocalCurrency(CoreCurrency localCurrency) {
//        this.localCurrency = localCurrency;
//    }
//
//    @OneToOne
//    @JoinColumn(name = "cross_currency_id", nullable = true, insertable = true, updatable = true)
//    public CoreCurrency getCrossCurrency() {
//        return crossCurrency;
//    }
//
//    public void setCrossCurrency(CoreCurrency crossCurrency) {
//        this.crossCurrency = crossCurrency;
//    }
//
//    @OneToOne
//    @JoinColumn(name = "reporting_currency_id", nullable = true, insertable = true, updatable = true)
//    public CoreCurrency getReportingCurrency() {
//        return reportingCurrency;
//    }
//
//    public void setReportingCurrency(CoreCurrency reportingCurrency) {
//        this.reportingCurrency = reportingCurrency;
//    }

    @Column(name = "today_date")
    public Date getTodayDate() {
        return todayDate;
    }

    public void setTodayDate(Date todayDate) {
        this.todayDate = todayDate;
    }

    @Column(name = "next_date")
    public Date getNextDate() {
        return nextDate;
    }

    public void setNextDate(Date nextDate) {
        this.nextDate = nextDate;
    }

    @Column(name = "previous_date")
    public Date getPreviousDate() {
        return previousDate;
    }

    public void setPreviousDate(Date previousDate) {
        this.previousDate = previousDate;
    }
    
    @Column(name = "ldap_domain")
    public String getLdapDomain() {
		return ldapDomain;
	}

	public void setLdapDomain(String ldapDomain) {
		this.ldapDomain = ldapDomain;
	}

	@Column(name = "ldap_address")
    public String getLdapAddress() {
		return ldapAddress;
	}

	public void setLdapAddress(String ldapAddress) {
		this.ldapAddress = ldapAddress;
	}

//	private String ldapPort;
//
//	@Column(name = "ldap_port")
//	public String getLdapPort() {
//		return ldapPort;
//	}
//
//	public void setLdapPort(String ldapPort) {
//		this.ldapPort = ldapPort;
//	}
//
	
  	@Column(name = "default_holiday", length=7)
	public String getDefaultHoliday() {
		return defaultHoliday;
	}

	public void setDefaultHoliday(String defaultHoliday) {
		this.defaultHoliday = defaultHoliday;
	}
	
	@Transient
    public Date getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}
	
    @Column(name="holiday_posting")
	public AccrualTypeEnum getHolidayPosting() {
		return holidayPosting;
	}

	public void setHolidayPosting(AccrualTypeEnum holidayPosting) {
		this.holidayPosting = holidayPosting;
	}
	
	@Column(name="service_cash")
	public BigDecimal getServiceCash() {
		return serviceCash;
	}

	public void setServiceCash(BigDecimal serviceCash) {
		this.serviceCash = serviceCash;
	}

	@Override
	public String toString() {
		return "CoreSystem [id=" + id + ", companyName=" + companyName
				+ ", systemStatus=" + systemStatus + ", todayDate=" + todayDate
				+ ", nextDate=" + nextDate + ", previousDate=" + previousDate
				+ ", ldapDomain=" + ldapDomain + ", ldapAddress=" + ldapAddress
				+ ", defaultHoliday=" + defaultHoliday + ", postingDate="
				+ postingDate + ", holidayPosting=" + holidayPosting
				+ ", serviceCash=" + serviceCash + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((companyName == null) ? 0 : companyName.hashCode());
		result = prime * result
				+ ((defaultHoliday == null) ? 0 : defaultHoliday.hashCode());
		result = prime * result
				+ ((holidayPosting == null) ? 0 : holidayPosting.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((ldapAddress == null) ? 0 : ldapAddress.hashCode());
		result = prime * result
				+ ((ldapDomain == null) ? 0 : ldapDomain.hashCode());
		result = prime * result
				+ ((nextDate == null) ? 0 : nextDate.hashCode());
		result = prime * result
				+ ((postingDate == null) ? 0 : postingDate.hashCode());
		result = prime * result
				+ ((previousDate == null) ? 0 : previousDate.hashCode());
		result = prime * result
				+ ((serviceCash == null) ? 0 : serviceCash.hashCode());
		result = prime * result
				+ ((systemStatus == null) ? 0 : systemStatus.hashCode());
		result = prime * result
				+ ((todayDate == null) ? 0 : todayDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoreSystem other = (CoreSystem) obj;
		if (companyName == null) {
			if (other.companyName != null)
				return false;
		} else if (!companyName.equals(other.companyName))
			return false;
		if (defaultHoliday == null) {
			if (other.defaultHoliday != null)
				return false;
		} else if (!defaultHoliday.equals(other.defaultHoliday))
			return false;
		if (holidayPosting != other.holidayPosting)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ldapAddress == null) {
			if (other.ldapAddress != null)
				return false;
		} else if (!ldapAddress.equals(other.ldapAddress))
			return false;
		if (ldapDomain == null) {
			if (other.ldapDomain != null)
				return false;
		} else if (!ldapDomain.equals(other.ldapDomain))
			return false;
		if (nextDate == null) {
			if (other.nextDate != null)
				return false;
		} else if (!nextDate.equals(other.nextDate))
			return false;
		if (postingDate == null) {
			if (other.postingDate != null)
				return false;
		} else if (!postingDate.equals(other.postingDate))
			return false;
		if (previousDate == null) {
			if (other.previousDate != null)
				return false;
		} else if (!previousDate.equals(other.previousDate))
			return false;
		if (serviceCash == null) {
			if (other.serviceCash != null)
				return false;
		} else if (!serviceCash.equals(other.serviceCash))
			return false;
		if (systemStatus == null) {
			if (other.systemStatus != null)
				return false;
		} else if (!systemStatus.equals(other.systemStatus))
			return false;
		if (todayDate == null) {
			if (other.todayDate != null)
				return false;
		} else if (!todayDate.equals(other.todayDate))
			return false;
		return true;
	}

}
