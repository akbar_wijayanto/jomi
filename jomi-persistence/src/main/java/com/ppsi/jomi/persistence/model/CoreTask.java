package com.ppsi.jomi.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.ppsi.jomi.enumeration.jomi.ComplexityEnum;
import com.ppsi.jomi.enumeration.jomi.PriorityEnum;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 19, 2017 11:35:03 PM
 */
@Table(name = "core_task")
@Entity
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update core_task set status='HIST' where id=?")
public class CoreTask extends BaseGenericObject {
    private static final long serialVersionUID = 3690197650654049848L;

    private Long id;
    private String taskNumber;
    private String feature;
    private String description;
    private PriorityEnum priority;
    private ComplexityEnum complexity;
    private Integer manHour;
    private CoreProject coreProject;
    
    @Column(name = "id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "task_number", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getTaskNumber() {
		return taskNumber;
	}

	public void setTaskNumber(String taskNumber) {
		this.taskNumber = taskNumber;
	}

	@Column(name = "description", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "feature", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
	public String getFeature() {
		return feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	@Column(name = "priority", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
	public PriorityEnum getPriority() {
		return priority;
	}

	public void setPriority(PriorityEnum priority) {
		this.priority = priority;
	}

	@Column(name = "complexity", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
	public ComplexityEnum getComplexity() {
		return complexity;
	}

	public void setComplexity(ComplexityEnum complexity) {
		this.complexity = complexity;
	}

	@Column(name = "man_hour", nullable = true, insertable = true, updatable = true)
	public Integer getManHour() {
		return manHour;
	}

	public void setManHour(Integer manHour) {
		this.manHour = manHour;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id", nullable = false, insertable = true, updatable = true)
	public CoreProject getCoreProject() {
		return coreProject;
	}

	public void setCoreProject(CoreProject coreProject) {
		this.coreProject = coreProject;
	}

	@Override
	public String toString() {
		return "CoreTask [id=" + id + ", feature=" + feature + ", description=" + description + ", priority=" + priority
				+ ", complexity=" + complexity + ", manHour=" + manHour + ", coreProject=" + coreProject + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((complexity == null) ? 0 : complexity.hashCode());
		result = prime * result + ((coreProject == null) ? 0 : coreProject.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((feature == null) ? 0 : feature.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((manHour == null) ? 0 : manHour.hashCode());
		result = prime * result + ((priority == null) ? 0 : priority.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoreTask other = (CoreTask) obj;
		if (complexity != other.complexity)
			return false;
		if (coreProject == null) {
			if (other.coreProject != null)
				return false;
		} else if (!coreProject.equals(other.coreProject))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (feature == null) {
			if (other.feature != null)
				return false;
		} else if (!feature.equals(other.feature))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (manHour == null) {
			if (other.manHour != null)
				return false;
		} else if (!manHour.equals(other.manHour))
			return false;
		if (priority != other.priority)
			return false;
		return true;
	}
}
