package com.ppsi.jomi.persistence.model;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @author : <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 * @version : 1.0, 1/31/12 3:11 PM
 */
@Table(name = "core_role")
@Entity
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update core_role set status='HIST' where id=?")
@NamedQueries({
        @NamedQuery(
                name = "findRoleByName",
                query = "select r from CoreRole r where r.name = :name "
        )
})
public class CoreRole extends BaseGenericObject {
    private static final long serialVersionUID = 3690197650654049848L;

    public CoreRole() {
    }

    public CoreRole(final String name) {
        this.name = name;
    }

    private Integer id;

    @Column(name = "id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private String name;

    @Column(name = "name", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String description;

    @Column(name = "description", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Transient
    public String getAuthority() {
        return getName();
    }

    private Set<CoreMenu> coreMenus = new HashSet<CoreMenu>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "core_role_menu",
            joinColumns = {@JoinColumn(name = "role_id")},
            inverseJoinColumns = @JoinColumn(name = "menu_id")
    )

    public Set<CoreMenu> getCoreMenus() {
        return coreMenus;
    }

    public void setCoreMenus(Set<CoreMenu> coreMenus) {
        this.coreMenus = coreMenus;
    }

    private Set<CorePermission> corePermissions = new HashSet<CorePermission>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "core_role_permission",
            joinColumns = {@JoinColumn(name = "role_id")},
            inverseJoinColumns = @JoinColumn(name = "permission_id")
    )
    public Set<CorePermission> getCorePermissions() {
        return corePermissions;
    }

    public void setCorePermissions(Set<CorePermission> corePermissions) {
        this.corePermissions = corePermissions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CoreRole)) return false;

        CoreRole coreRole = (CoreRole) o;

       
        if (description != null ? !description.equals(coreRole.description) : coreRole.description != null)
            return false;
        if (id != null ? !id.equals(coreRole.id) : coreRole.id != null) return false;
        if (name != null ? !name.equals(coreRole.name) : coreRole.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
	public String toString() {
		return "CoreRole [id=" + id + ", name=" + name + ", description="
				+ description + "]";
	}
}
