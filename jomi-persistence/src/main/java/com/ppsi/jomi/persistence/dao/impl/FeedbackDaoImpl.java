package com.ppsi.jomi.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.ppsi.jomi.persistence.dao.FeedbackDao;
import com.ppsi.jomi.persistence.model.Feedback;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:17:43 AM
 */
@Repository
public class FeedbackDaoImpl extends GenericDaoImpl<Feedback, Long> implements FeedbackDao {

	public FeedbackDaoImpl() {
		super(Feedback.class);
	}

}
