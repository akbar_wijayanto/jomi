/**
 * 
 */
package com.ppsi.jomi.persistence.dao.impl;

import java.io.Serializable;

import com.ppsi.jomi.persistence.dao.GenericTransactionDao;
import com.ppsi.jomi.persistence.model.BaseTransaction;

public abstract class GenericTransactionDaoImpl<T extends BaseTransaction, PK extends Serializable> extends GenericDaoImpl<T, PK> implements GenericTransactionDao<T, PK> {

	public GenericTransactionDaoImpl(Class<T> persistentClass) {
		super(persistentClass);
	}

	@SuppressWarnings("unchecked")
    @Override
	public T getByTransactionReference(String transactionReference) {
		try {
			return (T) getEntityManager()
					.createQuery("FROM "+getPersistentClass().getName()+" p WHERE p.transactionReference=:trxRef")
					.setParameter("trxRef", transactionReference)
					.getSingleResult();	
		} catch (Exception e) {
			log.error(e, e);
			return null;
		}
		
	}

}
