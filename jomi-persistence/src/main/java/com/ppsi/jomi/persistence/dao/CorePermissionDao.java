package com.ppsi.jomi.persistence.dao;

import java.util.List;

import com.ppsi.jomi.persistence.model.CorePermission;

public interface CorePermissionDao extends GenericDao<CorePermission, Integer> {
	List<String> getAplication();
	List<CorePermission> getByAplication(String applicationName);
}
