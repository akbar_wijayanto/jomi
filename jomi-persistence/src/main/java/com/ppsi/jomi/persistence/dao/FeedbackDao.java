package com.ppsi.jomi.persistence.dao;

import com.ppsi.jomi.persistence.model.Feedback;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:15:34 AM
 */
public interface FeedbackDao extends GenericDao<Feedback, Long> {

}
