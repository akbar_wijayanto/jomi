package com.ppsi.jomi.persistence.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.persistence.JoinColumns;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ppsi.jomi.Constants;
import com.ppsi.jomi.enumeration.RecordStatusEnum;
import com.ppsi.jomi.persistence.model.AuditTrail;
import com.ppsi.jomi.persistence.model.BaseGenericObject;


/**
 * @author : <a href="mailto:sahal.zain@anabatic.com">Sahal Zain</a>
 * @version : 1.0, 9/13/12, 10:59 PM
 */

public class ModelComparator {
//	private final Log log = LogFactory.getLog(ModelComparator.class);
	List<AuditTrail> trails = new ArrayList<AuditTrail>();
	private String userinfo;
	private List<Object> inspectedObject = new ArrayList<Object>();
	private String parentId;
	private String parentField = "";
	
	public ModelComparator(String userinfo){
		this.userinfo = userinfo;
	}
	
	public List<AuditTrail> compare(Object obj1, Object obj2){
		if(inspectedObject.contains(obj1)){
			return trails;
		}
		if((!obj1.getClass().equals(obj2.getClass()))&&
				(!(obj1 instanceof BaseGenericObject)&&!
						(obj2 instanceof BaseGenericObject))){
			
			throw new RuntimeException("Object has different class");
		}
		if((obj1==obj2)||(obj2!=null&&obj1!=null&&obj1.equals(obj2))){
			return trails;
		}
	/*	if(checkAuthorizing(obj1,obj2)){
			return extractId(obj1,Constants.AUTHORIZE_RECORD);
		}*/
		//log.debug("Inspecting object : " + obj1.toString() + " class : "+ obj1.getClass().getCanonicalName());
		inspectedObject.add(obj1);
		Class<?> clazz = obj1.getClass();
		Object objectId = null;
		Method[] methods = clazz.getDeclaredMethods();
		List<AuditTrail> tempTrail = new ArrayList<AuditTrail>();
		for(Method m:methods){
			if(m.getName().startsWith("get")&&Modifier.isPublic(m.getModifiers())&&!Modifier.isStatic(m.getModifiers())){
				try {
					//log.debug("Method name : " + m.getName());
					Object o1 = m.invoke(obj1);
					Object o2 = m.invoke(obj2);
					if(o1!=null&&(m.isAnnotationPresent(javax.persistence.Id.class)||m.isAnnotationPresent(javax.persistence.EmbeddedId.class))&&o1!=null){
						//log.debug("Identity method with id : " + o1.toString());
						objectId = o1;
					}
					if(o1!=null&&o2!=null&&o1 instanceof BaseGenericObject){
						//log.debug("Found model type data : " + o1.getClass().getCanonicalName());
						ModelComparator comp = new ModelComparator(userinfo);
						comp.setInspectedObject(inspectedObject);
						comp.setParentId(parentId);
						comp.setParentField(m.getName().substring(3));
						addTrails(comp.compare(o1, o2));
					}else{
						if((o1==o2)||(o1!=null&&o2!=null&&o1.equals(o2))){
							continue;
						}
						boolean array = false;
						Object[] objects1 = null;
						Object[] objects2 = null;
						if(o1!=null&&o1.getClass().isArray()&&BaseGenericObject.class.isAssignableFrom(o1.getClass().getComponentType())){
							//log.debug("Component clas : " + o1.getClass().getComponentType());
							array = true;
							objects1 = (Object[])o1;
							if(o2!=null)
								objects2 = (Object[])o2;
						}
						if(o1!=null&&(o1 instanceof List||o1 instanceof Set)){
							objects1 = ((Collection<?>)o1).toArray();
							if(objects1.length>0&&objects1[0] instanceof BaseGenericObject){
								array = true;
								if(o2!=null)
									objects2 = ((Collection<?>)o2).toArray();
							}
						}
						if(array){
							//log.debug("array 1 length : " + objects1.length);
							//log.debug("array 2 length : " + objects2.length);
							int len = objects1.length>=objects2.length?objects1.length:objects2.length;
							for(int i=0;i<len;i++){
								ModelComparator cmp = new ModelComparator(userinfo);
								cmp.setInspectedObject(inspectedObject);
								cmp.setParentId(parentId);
								cmp.setParentField(m.getName().substring(3));
								if(i < objects1.length && objects1[i]==null)
									addTrails(cmp.extractId(objects2[i],Constants.NEW_RECORD));
								else if(i < objects2.length && objects2[i]==null)
									addTrails(cmp.extractId(objects1[i], Constants.DELETE_RECORD));
								else
								{
									if(i >= objects1.length)
										addTrails(cmp.extractId(objects2[i],Constants.NEW_RECORD));
									else if(i >= objects2.length)
										addTrails(cmp.extractId(objects1[i], Constants.DELETE_RECORD));
									else
										addTrails(cmp.compare(objects1[i], objects2[i]));
								}
							}
						}else{
							AuditTrail cTrail = new AuditTrail();
							cTrail.setActor(userinfo);
							cTrail.setObjectName(clazz.getCanonicalName());
							if(!parentField.equals(""))
								cTrail.setFieldName(parentField+"."+m.getName().substring(3));
							else
								cTrail.setFieldName(m.getName().substring(3));
							if(parentId!=null)
								cTrail.setParentId(parentId);
							if(o1!=null)
								cTrail.setBefore(o1.toString());
							if(o2!=null)
								cTrail.setAfter(o2.toString());
							
							cTrail.setTime(Calendar.getInstance().getTime());
							cTrail.setAction(Constants.MODIFY_RECORD);
							tempTrail.add(cTrail);
						}
					}
					
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
		
		for(AuditTrail t:tempTrail){
			t.setObjectId(objectId.toString());
			addTrail(t);
		}
		return trails;
			
	}
	
	public boolean checkAuthorizing(Object oldObject,Object newObject){
		if(oldObject==null||newObject==null)
			return false;
		if(!(oldObject instanceof BaseGenericObject&&newObject instanceof BaseGenericObject))
			return false;
		
		//log.debug("Check authorizing");
		BaseGenericObject oldObj = (BaseGenericObject) oldObject;
		BaseGenericObject newObj = (BaseGenericObject) newObject;
		//log.debug("Old object status : " + oldObj.getStatus());
		//log.debug("New object status : " + newObj.getStatus());
		return oldObj.getStatus().equals(RecordStatusEnum.INAU.getName())&&newObj.getStatus().equals(RecordStatusEnum.LIVE.getName());
		
	}
	/*
	public List<AuditTrail> extractData(Object obj){
		if(inspectedObject.contains(obj)){
			return trails;
		}
		if(!(obj instanceof BaseGenericObject))
			throw new RuntimeException("Invalid object for extraction");
		
		log.debug("Extracting object : " + obj.toString() + " class : "+ obj.getClass().getCanonicalName());
		inspectedObject.add(obj);
		Class<?> clazz = obj.getClass();
		Object objectId = null;
		Method[] methods = clazz.getDeclaredMethods();
		List<AuditTrail> tempTrail = new ArrayList<AuditTrail>();
		for(Method m:methods){
			if(m.getName().startsWith("get")&&Modifier.isPublic(m.getModifiers())&&!Modifier.isStatic(m.getModifiers())){
				try {
					log.debug("Method name : " + m.getName());
					Object o = m.invoke(obj);
					if((m.isAnnotationPresent(javax.persistence.Id.class)||m.isAnnotationPresent(javax.persistence.EmbeddedId.class))&&o!=null){
						log.debug("Identity method with id : " + o.toString());
						objectId = o;
					}
					if(o instanceof BaseGenericObject){
						log.debug("Found model type data : " + o.getClass().getCanonicalName());
						ModelComparator comp = new ModelComparator(userinfo);
						comp.setInspectedObject(inspectedObject);
						addTrails(comp.extractData(o));
					}else{
						if(o==null){
							continue;
						}
						boolean array = false;
						Object[] objects = null;
						if(o.getClass().isArray()&&BaseGenericObject.class.isAssignableFrom(o.getClass().getComponentType())){
							log.debug("Component clas : " + o.getClass().getComponentType());
							objects = (Object[])o;
							if(objects.length>0)
								array = true;
						}
						if(o instanceof List||o instanceof Set){
							objects = ((Collection<?>)o).toArray();
							if(objects.length>0&&objects[0] instanceof BaseGenericObject)
								array = true;
						}
						if(array){
							for(int i=0;i<objects.length;i++){
								ModelComparator cmp = new ModelComparator(userinfo);
								cmp.setInspectedObject(inspectedObject);
								addTrails(cmp.extractData(objects[i]));
							}
						}else{
							AuditTrail cTrail = new AuditTrail();
							cTrail.setActor(userinfo);
							cTrail.setObjectName(clazz.getCanonicalName());
							cTrail.setFieldName(m.getName().substring(3));
							cTrail.setBefore("");
							cTrail.setAfter(o.toString());
							cTrail.setTime(Calendar.getInstance().getTime());
							cTrail.setAction(Constants.NEW_RECORD);
							tempTrail.add(cTrail);
						}
					}
					
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
		if(objectId!=null){
			for(AuditTrail t:tempTrail){
				t.setObjectId(objectId.toString());
				addTrail(t);
			}
		}
		return trails;
	}
	*/
	public List<AuditTrail> extractId(Object obj, String recordStatus){
		if(inspectedObject.contains(obj)){
			return trails;
		}
		if(!(obj instanceof BaseGenericObject))
			throw new RuntimeException("Invalid object for extraction");
		
		//log.debug("Extracting id of object  : " + obj.toString() + " class : "+ obj.getClass().getCanonicalName());
		inspectedObject.add(obj);
		Class<?> clazz = obj.getClass();
		Object objectId = null;
		Method[] methods = clazz.getDeclaredMethods();
		List<AuditTrail> tempTrail = new ArrayList<AuditTrail>();
		for(Method m:methods){
			if(m.getName().startsWith("get") && Modifier.isPublic(m.getModifiers())
					&& !Modifier.isStatic(m.getModifiers()) && m.getAnnotation(Transient.class)==null){
				try {
					//log.debug("Method name : " + m.getName());
					Object o = m.invoke(obj);
					if((m.isAnnotationPresent(javax.persistence.Id.class)||m.isAnnotationPresent(javax.persistence.EmbeddedId.class))&&o!=null){
						//log.debug("Identity method with id : " + o.toString());
						objectId = o;
						AuditTrail cTrail = new AuditTrail();
						cTrail.setActor(userinfo);
						cTrail.setObjectName(clazz.getCanonicalName());
						cTrail.setObjectId(objectId.toString());
						cTrail.setParentId(parentId);
						if(!parentField.equals(""))
							cTrail.setFieldName(parentField+"."+m.getName().substring(3));
						else
							cTrail.setFieldName(m.getName().substring(3));
						cTrail.setBefore("");
						cTrail.setAfter(o.toString());
						cTrail.setTime(Calendar.getInstance().getTime());
						cTrail.setAction(recordStatus);
						addTrail(cTrail);
					}
					else{
					if(o instanceof BaseGenericObject){
						//log.debug("Found model type data : " + o.getClass().getCanonicalName());
						ModelComparator comp = new ModelComparator(userinfo);
						comp.setInspectedObject(inspectedObject);
						comp.setParentId(parentId);
						comp.setParentField(m.getName().substring(3));
						addTrails(comp.extractId(o,recordStatus));
					}else{
						if(o==null || o.equals("")){
							continue;
						}
						boolean array = false;
						Object[] objects = null;
						if(o.getClass().isArray()&&BaseGenericObject.class.isAssignableFrom(o.getClass().getComponentType())){
							//log.debug("Component clas : " + o.getClass().getComponentType());
							objects = (Object[])o;
							if(objects.length>0)
								array = true;

							if(!array && objects.length == 0)
								continue;
						}
						if(o instanceof List||o instanceof Set){
							objects = ((Collection<?>)o).toArray();
							if(objects.length>0&&objects[0] instanceof BaseGenericObject)
								array = true;

							if(!array && objects.length == 0)
								continue;
						}
						if(array){
							for(int i=0;i<objects.length;i++){
								ModelComparator cmp = new ModelComparator(userinfo);
								cmp.setInspectedObject(inspectedObject);
								cmp.setParentId(parentId);
								cmp.setParentField(m.getName().substring(3));
								addTrails(cmp.extractId(objects[i],recordStatus));
							}
						}
						else
						{
//							objectId = o;
							AuditTrail cTrail = new AuditTrail();
							cTrail.setActor(userinfo);
							cTrail.setObjectName(clazz.getCanonicalName());
//							cTrail.setObjectId(objectId.toString());
							cTrail.setParentId(parentId);
							if(!parentField.equals(""))
								cTrail.setFieldName(parentField+"."+m.getName().substring(3));
							else
								cTrail.setFieldName(m.getName().substring(3));
							cTrail.setBefore("");
							cTrail.setAfter(o.toString());
							cTrail.setTime(Calendar.getInstance().getTime());
							cTrail.setAction(recordStatus);
//							addTrail(cTrail);				
							tempTrail.add(cTrail);
						}
					}
					}
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
		
		for(AuditTrail t:tempTrail){
			t.setObjectId(objectId.toString());
			addTrail(t);
		}
		
		return trails;
	}
	
	public void setInspectedObject(List<Object> objects){
		this.inspectedObject = objects;
	}
	
	private void addTrail(AuditTrail trail){
		trails.add(trail);
	}
	
	private void addTrails(List<AuditTrail> trails){
		this.trails.addAll(trails);
	}
	
	public void setParentId(String parentId){
		this.parentId = parentId;
	}
	
	public void setParentField(String parentField){
		this.parentField = parentField;
	}

	public String getParentField(){
		return parentField;
	}
	
	public List<AuditTrail> compare(Object obj1, Object obj2,String authorizeRecord){
		if(inspectedObject.contains(obj1)){
			return trails;
		}
		if((!obj1.getClass().equals(obj2.getClass()))&&
				(!(obj1 instanceof BaseGenericObject)&&!
						(obj2 instanceof BaseGenericObject))){
			
			throw new RuntimeException("Object has different class");
		}
		if((obj1==obj2)||(obj2!=null&&obj1!=null&&obj1.equals(obj2))){
			return trails;
		}
	/*	if(checkAuthorizing(obj1,obj2)){
			return extractId(obj1,Constants.AUTHORIZE_RECORD);
		}*/
		//log.debug("Inspecting object : " + obj1.toString() + " class : "+ obj1.getClass().getCanonicalName());
		inspectedObject.add(obj1);
		Class<?> clazz = obj1.getClass();
		Object objectId = null;
		Method[] methods = clazz.getDeclaredMethods();
		List<AuditTrail> tempTrail = new ArrayList<AuditTrail>();
		for(Method m:methods){
			if(m.getName().startsWith("get") && Modifier.isPublic(m.getModifiers())&&!Modifier.isStatic(m.getModifiers()) 
					&& m.getParameterTypes().length==0 && !m.isAnnotationPresent(OneToMany.class) && !m.isAnnotationPresent(JoinColumns.class)){
				try {
					//log.debug("Method name : " + m.getName());
					Object o1 = m.invoke(obj1);
					Object o2 = m.invoke(obj2);
					if(o1!=null&&(m.isAnnotationPresent(javax.persistence.Id.class)||m.isAnnotationPresent(javax.persistence.EmbeddedId.class))&&o1!=null){
						//log.debug("Identity method with id : " + o1.toString());
						objectId = o1;
					}
					if(o1!=null&&o2!=null&&o1 instanceof BaseGenericObject){
						//log.debug("Found model type data : " + o1.getClass().getCanonicalName());
						ModelComparator comp = new ModelComparator(userinfo);
						comp.setInspectedObject(inspectedObject);
						comp.setParentId(parentId);
						comp.setParentField(m.getName().substring(3));
						addTrails(comp.compare(o1, o2));
					}else{
						if((o1==o2)||(o1!=null&&o2!=null&&o1.equals(o2))){
							continue;
						}
						boolean array = false;
						Object[] objects1 = null;
						Object[] objects2 = null;
						if(o1!=null&&o1.getClass().isArray()&&BaseGenericObject.class.isAssignableFrom(o1.getClass().getComponentType())){
							//log.debug("Component clas : " + o1.getClass().getComponentType());
							array = true;
							objects1 = (Object[])o1;
							if(o2!=null)
								objects2 = (Object[])o2;
						}
						if(o1!=null&&(o1 instanceof List||o1 instanceof Set)){
							objects1 = ((Collection<?>)o1).toArray();
							if(objects1.length>0&&objects1[0] instanceof BaseGenericObject){
								array = true;
								if(o2!=null)
									objects2 = ((Collection<?>)o2).toArray();
							}
						}
						if(array){
							//log.debug("array 1 length : " + objects1.length);
							//log.debug("array 2 length : " + objects2.length);
							int len=0;
							if(objects1==null && objects2 != null) len = objects2.length;
							else if(objects1!=null && objects2 == null) len = objects1.length;
							else len = objects1.length>=objects2.length?objects1.length:objects2.length;
							for(int i=0;i<len;i++){
								ModelComparator cmp = new ModelComparator(userinfo);
								cmp.setInspectedObject(inspectedObject);
								cmp.setParentId(parentId);
								cmp.setParentField(m.getName().substring(3));
								if(i < objects1.length && objects1[i]==null)
									addTrails(cmp.extractId(objects2[i],Constants.NEW_RECORD));
								else if(i < objects2.length && objects2[i]==null)
									addTrails(cmp.extractId(objects1[i], Constants.DELETE_RECORD));
								else
								{
									if(i >= objects1.length)
										addTrails(cmp.extractId(objects2[i],Constants.NEW_RECORD));
									else if(i >= objects2.length)
										addTrails(cmp.extractId(objects1[i], Constants.DELETE_RECORD));
									else
										addTrails(cmp.compare(objects1[i], objects2[i]));
								}
							}
						}else{
							AuditTrail cTrail = new AuditTrail();
							cTrail.setActor(userinfo);
							cTrail.setObjectName(clazz.getCanonicalName());
							if(!parentField.equals(""))
								cTrail.setFieldName(parentField+"."+m.getName().substring(3));
							else
								cTrail.setFieldName(m.getName().substring(3));
							if(parentId!=null)
								cTrail.setParentId(parentId);
							if(o1!=null)
								cTrail.setBefore(o1.toString());
							if(o2!=null)
								cTrail.setAfter(o2.toString());
							
							cTrail.setTime(Calendar.getInstance().getTime());
							cTrail.setAction(authorizeRecord);
							tempTrail.add(cTrail);
						}
					}
					
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
		
		for(AuditTrail t:tempTrail){
			t.setObjectId(objectId.toString());
			addTrail(t);
		}
		return trails;
			
	}
}
