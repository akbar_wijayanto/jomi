package com.ppsi.jomi.persistence.dao;

import java.util.Date;
import java.util.List;

import com.ppsi.jomi.persistence.model.AuditTrail;

public interface AuditTrailDao {

	List<AuditTrail> getAll();

    List<AuditTrail> getAllDistinct();

    AuditTrail get(Long id);

    boolean exists(Long id);

    AuditTrail save(AuditTrail object);
    
    void remove(Long id);
    

    List<AuditTrail> getPagingResultAudit(Long firstResult, Long maxResults,String objectName, String fieldName,Date dateFrom,Date dateTo, String actor,String action);
    
    Long getAuditCount(String objectName, String fieldName,Date dateFrom ,Date dateTo, String actor,String action);
    
    List<AuditTrail> getDataAudittrail(String objectName, String fieldName,Date dateFrom,Date dateTo, String actor,String action);

	List<AuditTrail> getDataAudittrail(Long start, Long end, String objectName,
			String fieldName, Date dateFrom, Date dateTo, String actor,
			String action);
    
    
}
