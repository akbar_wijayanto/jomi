package com.ppsi.jomi.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.ppsi.jomi.persistence.dao.CoreReportDao;
import com.ppsi.jomi.persistence.model.CoreReport;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date May 9, 2017 11:05:08 AM
 */
@Repository
public class CoreReportDaoImpl extends GenericDaoImpl<CoreReport, Long> implements CoreReportDao {

	public CoreReportDaoImpl() {
		super(CoreReport.class);
	}
	
}
