package com.ppsi.jomi.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.ppsi.jomi.persistence.dao.CoreSystemParameterDao;
import com.ppsi.jomi.persistence.model.CoreSystemParameter;

@Repository("coreSystemParameterDao")
public class CoreSystemParameterDaoImpl extends
		GenericDaoImpl<CoreSystemParameter, String> implements
		CoreSystemParameterDao {

	public CoreSystemParameterDaoImpl() {
		super(CoreSystemParameter.class);
	}

}
