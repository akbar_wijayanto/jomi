package com.ppsi.jomi.persistence.dao;

import java.util.List;

import com.ppsi.jomi.persistence.model.CoreProject;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:15:34 AM
 */
public interface CoreProjectDao extends GenericDao<CoreProject, Long> {
	List<CoreProject> getProjectByUsername(String username);
}
