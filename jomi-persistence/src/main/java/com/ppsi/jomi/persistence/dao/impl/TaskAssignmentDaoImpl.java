package com.ppsi.jomi.persistence.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ppsi.jomi.enumeration.jomi.TaskStatusEnum;
import com.ppsi.jomi.persistence.dao.TaskAssignmentDao;
import com.ppsi.jomi.persistence.model.TaskAssignment;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:17:43 AM
 */
@Repository
public class TaskAssignmentDaoImpl extends GenericDaoImpl<TaskAssignment, Long> implements TaskAssignmentDao {

	public TaskAssignmentDaoImpl() {
		super(TaskAssignment.class);
	}

	@Override
	public List<TaskAssignment> getTaskByUsername(String username) {
		String sql = "FROM TaskAssignment c WHERE c.coreUser.username  = :username";
		try {
			return (List<TaskAssignment>) getEntityManager()
					.createQuery(sql)
					.setParameter("username", username)
					.getResultList();
		} catch (Exception e) {
			if (log.isTraceEnabled()) log.error(e,e);
			else log.error(e);
			return null;
		}
	}

	@Override
	public List<TaskAssignment> getTaskByUsernameAndStatus(String username, TaskStatusEnum taskStatusEnum) {
		String sql = "FROM TaskAssignment c WHERE c.coreUser.username = :username and c.taskStatus = :taskStatusEnum";
		try {
			return (List<TaskAssignment>) getEntityManager()
					.createQuery(sql)
					.setParameter("username", username)
					.setParameter("taskStatusEnum", taskStatusEnum)
					.getResultList();
		} catch (Exception e) {
			if (log.isTraceEnabled()) log.error(e,e);
			else log.error(e);
			return null;
		}
	}

}
