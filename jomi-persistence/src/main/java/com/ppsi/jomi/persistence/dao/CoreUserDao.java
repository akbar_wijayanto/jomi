package com.ppsi.jomi.persistence.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.ppsi.jomi.persistence.model.CoreUser;

/**
 * User Data Access Object (GenericDao) interface.
 *
 * @author <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 */
public interface CoreUserDao extends GenericDao<CoreUser, Long> {

    CoreUser saveCurrentUser(CoreUser object);

    @Transactional
    CoreUser loadUserByUsername(String username);

    List<CoreUser> getUsers();

    CoreUser saveUser(CoreUser coreUser);


    String getUserPassword(String username);

    CoreUser getWithListAll(Long id);
    
    List<CoreUser> getAllWithListAll();

    List<CoreUser> getBranchById(Long id);

    List<CoreUser> getCheckBranchOnUser(String id);

    List<CoreUser> getCheckRoleUsedByUser(Integer id);

    List<CoreUser> getCheckPermissionUsedByUser(Integer id);

    List<CoreUser> getUserRoleList(Integer id);

    Long getRecordCount(Integer id);

    List<CoreUser> getSearchResult(Integer id, Long firstResult, Long maxResult);

    @Transactional
    CoreUser updateLoginAttempt(CoreUser user);

    List<CoreUser> getAllOrderByUsername();

    @Override
    List<CoreUser> getPagingResults(Long firstResult, Long maxResults);
    
    CoreUser getUserSpring(String username);
    
    CoreUser change(CoreUser coreUser);
    
    CoreUser save(CoreUser coreUser);
    
    List<CoreUser> getByClientUser();
    
    CoreUser getByToken(String token);
    
    void setHistoryToSeller(Long id);
}
