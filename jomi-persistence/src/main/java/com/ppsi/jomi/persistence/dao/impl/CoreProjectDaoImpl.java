package com.ppsi.jomi.persistence.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ppsi.jomi.persistence.dao.CoreProjectDao;
import com.ppsi.jomi.persistence.model.CoreProject;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:17:43 AM
 */
@Repository
public class CoreProjectDaoImpl extends GenericDaoImpl<CoreProject, Long> implements CoreProjectDao {

	public CoreProjectDaoImpl() {
		super(CoreProject.class);
	}

	@Override
	public List<CoreProject> getProjectByUsername(String username) {
		String sql = "from CoreProject u join fetch u.coreUsers b where b.username=:username";
		try {
			return (List<CoreProject>) getEntityManager()
					.createQuery(sql)
					.setParameter("username", username)
					.getResultList();
		} catch (Exception e) {
			if (log.isTraceEnabled()) log.error(e,e);
			else log.error(e);
			return null;
		}
	}

}
