package com.ppsi.jomi.persistence.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ppsi.jomi.persistence.dao.CoreMenuDao;
import com.ppsi.jomi.persistence.model.CoreMenu;
import com.ppsi.jomi.persistence.model.CoreUser;

@Repository("coreMenuDao")
public class CoreMenuDaoImpl extends GenericDaoImpl<CoreMenu, Integer> implements CoreMenuDao {

    public CoreMenuDaoImpl() {
        super(CoreMenu.class);
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<CoreMenu> getByUsername(String username) {
        String sql = "select distinct c from CoreUser a " +
                "join a.coreRoles b " +
                "join b.coreMenus c " +
                "where a.username = :username and c.status='LIVE' order by c.orderMenu asc";
        List<CoreMenu> menus = getEntityManager().createQuery(sql).setParameter("username", username).getResultList();
        return menus;
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<CoreMenu> getByOrderMenu(Integer parentId, Integer orderMenu) {
    	return getEntityManager()
                .createQuery("From CoreMenu c where c.parentId=:parentId and orderMenu=:ORDERMENU")
                .setParameter("ORDERMENU", orderMenu)
                .setParameter("parentId", parentId)
                .getResultList();
    }

	@Override
	public Integer getByPermaLink(String link) {
		try {
			Integer id = (Integer) getEntityManager()
					.createQuery("Select parentId from CoreMenu where permalinks=:a")
					.setParameter("a", link)
					.getSingleResult();
				return id;
		} catch (NoResultException nre) {
			return 0;
		}
		
	}
	
	@Override
	public CoreMenu getByParentId(Integer parentId) {
		String sql = "from CoreMenu u where u.id=:parentId";
		try {
			return (CoreMenu) getEntityManager()
					.createQuery(sql)
					.setParameter("parentId", parentId)
					.getSingleResult();
		} catch (Exception e)  {e.printStackTrace(); return null;}
	}

	@Override
	public List<CoreMenu> getByActiveRole(Integer roleId) {
		String sql = "select distinct b from CoreRole a " +
                "join a.coreMenus b " +
                "where a.id = :roleId and b.status='LIVE' order by b.orderMenu asc";
        List<CoreMenu> menus = getEntityManager().createQuery(sql).setParameter("roleId", roleId).getResultList();
        return menus;
	}
	
	@Override
	public List<CoreMenu> getAllOrderByOrderMenu() {
		String sql = "FROM CoreMenu cm WHERE cm.status = :status ORDER BY cm.orderMenu ASC";
		List<CoreMenu> result = new ArrayList<CoreMenu>();
		
		result = getEntityManager().createQuery(sql).setParameter("status", "LIVE").getResultList();
		return result;
	}

	/*@Override
	@Transactional
	public CoreMenu save(CoreMenu object) {
		
		return super.save(object);
	}

	@Override
	@Transactional
	public void remove(Integer id) {
		
		super.remove(id);
	}

	@Override
	@Transactional
	public CoreMenu authorize(CoreMenu oldObject, CoreMenu newObject,
			boolean isApproved) {
		
		return super.authorize(oldObject, newObject, isApproved);
	}*/

}
