package com.ppsi.jomi.persistence.dao;

import com.ppsi.jomi.persistence.model.CoreReport;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date May 9, 2017 11:04:21 AM
 */
public interface CoreReportDao extends GenericDao<CoreReport, Long> {
}
