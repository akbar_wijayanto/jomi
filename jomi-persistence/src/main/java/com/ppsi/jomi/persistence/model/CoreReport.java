package com.ppsi.jomi.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 19, 2017 11:35:03 PM
 */
@Table(name = "core_report")
@Entity
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update core_report set status='HIST' where id=?")
public class CoreReport extends BaseGenericObject {
    private static final long serialVersionUID = 3690197650654049848L;

    private Long id;
    private String reportFileName;
    private String reportName;
    private String description;
    private String permalink;
    
    @Column(name = "id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "description", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "report_file_name", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
	public String getReportFileName() {
		return reportFileName;
	}

	public void setReportFileName(String reportFileName) {
		this.reportFileName = reportFileName;
	}

	@Column(name = "report_name", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	@Column(name = "permalink", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
	public String getPermalink() {
		return permalink;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	@Override
	public String toString() {
		return "CoreReport [id=" + id + ", reportFileName=" + reportFileName + ", reportName=" + reportName
				+ ", description=" + description + ", permalink=" + permalink + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((permalink == null) ? 0 : permalink.hashCode());
		result = prime * result + ((reportFileName == null) ? 0 : reportFileName.hashCode());
		result = prime * result + ((reportName == null) ? 0 : reportName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoreReport other = (CoreReport) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (permalink == null) {
			if (other.permalink != null)
				return false;
		} else if (!permalink.equals(other.permalink))
			return false;
		if (reportFileName == null) {
			if (other.reportFileName != null)
				return false;
		} else if (!reportFileName.equals(other.reportFileName))
			return false;
		if (reportName == null) {
			if (other.reportName != null)
				return false;
		} else if (!reportName.equals(other.reportName))
			return false;
		return true;
	}
    
}
