package com.ppsi.jomi.persistence.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.stereotype.Repository;

import com.ppsi.jomi.Constants;
import com.ppsi.jomi.persistence.dao.AuditTrailDao;
import com.ppsi.jomi.persistence.model.AuditTrail;



@Repository("auditTrailDao")
public class AuditTrailDaoImpl implements AuditTrailDao {

	 public static final String PERSISTENCE_UNIT_NAME = "ApplicationEntityManager";

    /**
     * Entity manager, injected by Spring using @PersistenceContext annotation on setEntityManager()
     */
    @PersistenceContext(unitName = PERSISTENCE_UNIT_NAME)
    private EntityManager entityManager;
    private Class<?> persistentClass = AuditTrail.class;
	public AuditTrailDaoImpl(){
		
	}

	public EntityManager getEntityManager() {
        return this.entityManager;
    }
	
	public void setEntityManager(EntityManager entityManager){
		this.entityManager = entityManager;
	}
	
	@SuppressWarnings("unchecked")
	public List<AuditTrail> getAll() {
		 return this.entityManager.createQuery(
	                "select obj from " + this.persistentClass.getName() + " obj")
	                .getResultList();
	}
	
	public List<AuditTrail> getAllDistinct() {
		Collection<AuditTrail> result = new LinkedHashSet<AuditTrail>(getAll());
        return new ArrayList<AuditTrail>(result);
	}


	public AuditTrail get(Long id) {
		 AuditTrail entity = (AuditTrail) this.entityManager.find(this.persistentClass, id);

	        if (entity == null) {
	            String msg = "Uh oh, '" + this.persistentClass + "' object with id '" + id + "' not found...";
	            throw new EntityNotFoundException(msg);
	        }

	        return entity;
	}

	public boolean exists(Long id) {
		AuditTrail entity = (AuditTrail) this.entityManager.find(this.persistentClass, id);
        return entity != null;
	}

	public AuditTrail save(AuditTrail object) {
        return this.entityManager.merge(object);
	}

	public void remove(Long id) {
		AuditTrail entity = get(id);
        if (entity != null) {
            this.entityManager.remove(entity);
        }
	}

	@Override
	public List<AuditTrail> getPagingResultAudit(Long firstResult,Long maxResults, String objectName, String fieldName,Date dateFrom, Date dateTo, String actor,
			String action) {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<AuditTrail> query = builder.createQuery(AuditTrail.class);

        Root<AuditTrail> cust = query.from(AuditTrail.class);
        query.select(cust);

        List<Predicate> predicateList = new ArrayList<Predicate>();

        Predicate objectNamePredicate, fieldNamePredicated,dateFromPredicated, dateToPredicated, actorPredicated, actionPredicated;

        if (!StringUtils.isEmpty(objectName)) {
        	objectNamePredicate = builder.like(builder.upper(cust.<String>get("objectName")), "%" + objectName.toUpperCase() + "%");
            predicateList.add(objectNamePredicate);

        }

        if (!StringUtils.isEmpty(fieldName)) {
        	fieldNamePredicated = builder.like(builder.upper(cust.<String>get("fieldName")), "%" + fieldName.toUpperCase() + "%");
            predicateList.add(fieldNamePredicated);
        }
        
        if (dateFrom != null && dateTo != null) {
          
        	dateFromPredicated = builder.between(cust.<Date>get("time"), dateFrom, dateTo);
            predicateList.add(dateFromPredicated);
        }
        
        
        if (!StringUtils.isEmpty(actor)) {
        	actorPredicated = builder.like(builder.upper(cust.<String>get("actor")), "%" + actor.toUpperCase() + "%");
            predicateList.add(actorPredicated);
        }

        if (!StringUtils.isEmpty(action)) {
        	actionPredicated = builder.like(builder.upper(cust.<String>get("action")), "%" + action.toUpperCase() + "%");
            predicateList.add(actionPredicated);
        }


        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        query.where(predicates);


        return getEntityManager()
                .createQuery(query)
                .setFirstResult(NumberUtils.toInt(String.valueOf(firstResult)))
                .setMaxResults(NumberUtils.toInt(String.valueOf(maxResults), Constants.PAGING_MAX_RECORD))
                .getResultList();
    }
	

	@Override
	public Long getAuditCount(String objectName, String fieldName,Date dateFrom, Date dateTo,String actor, String action) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);

        Root<AuditTrail> cust = query.from(AuditTrail.class);
        query.select(builder.count(cust.<String>get("fieldName")));

        List<Predicate> predicateList = new ArrayList<Predicate>();
        Predicate statusPredicate, objectNamePredicate, fieldNamePredicated, dateFromPredicated, dateToPredicated, actorPredicated, actionPredicated;

        if (!StringUtils.isEmpty(objectName)) {
        	objectNamePredicate = builder.like(builder.upper(cust.<String>get("objectName")), "%" + objectName.toUpperCase() + "%");
            predicateList.add(objectNamePredicate);

        }

        if (!StringUtils.isEmpty(fieldName)) {
        	fieldNamePredicated = builder.like(builder.upper(cust.<String>get("fieldName")), "%" + fieldName.toUpperCase() + "%");
            predicateList.add(fieldNamePredicated);
        }                     
        
        if (dateFrom != null && dateTo != null) {
        	dateToPredicated = builder.between(cust.<Date>get("time"),dateFrom,dateTo);
            predicateList.add(dateToPredicated);
        }
        
        if (!StringUtils.isEmpty(actor)) {
        	actorPredicated = builder.like(builder.upper(cust.<String>get("actor")), "%" + actor.toUpperCase() + "%");
            predicateList.add(actorPredicated);
        }

        if (!StringUtils.isEmpty(action)) {
        	actionPredicated = builder.like(builder.upper(cust.<String>get("action")), "%" + action.toUpperCase() + "%");
            predicateList.add(actionPredicated);
        }


        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        query.where(predicates);

        List list = getEntityManager().createQuery(query).getResultList();

        return list.size() == 0 ? null : (Long) list.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AuditTrail> getDataAudittrail(Long start, Long end, String objectName,String fieldName, Date dateFrom, Date dateTo, String actor,	String action) {

		 CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
	        CriteriaQuery<AuditTrail> query = builder.createQuery(AuditTrail.class);

	        Root<AuditTrail> cust = query.from(AuditTrail.class);
	        query.select(cust);

	        List<Predicate> predicateList = new ArrayList<Predicate>();

	        Predicate objectNamePredicate, fieldNamePredicated,dateFromPredicated, dateToPredicated, actorPredicated, actionPredicated;

	        if (!StringUtils.isEmpty(objectName)) {
	        	objectNamePredicate = builder.like(builder.upper(cust.<String>get("objectName")), "%" + objectName.toUpperCase() + "%");
	            predicateList.add(objectNamePredicate);

	        }

	        if (!StringUtils.isEmpty(fieldName)) {
	        	fieldNamePredicated = builder.like(builder.upper(cust.<String>get("fieldName")), "%" + fieldName.toUpperCase() + "%");
	            predicateList.add(fieldNamePredicated);
	        }
	        
	        if (dateFrom != null && dateTo != null) {
	          
	        	dateFromPredicated = builder.between(cust.<Date>get("time"), dateFrom, dateTo);
	            predicateList.add(dateFromPredicated);
	        }
	        
	        
	        if (!StringUtils.isEmpty(actor)) {
	        	actorPredicated = builder.like(builder.upper(cust.<String>get("actor")), "%" + actor.toUpperCase() + "%");
	            predicateList.add(actorPredicated);
	        }

	        if (!StringUtils.isEmpty(action)) {
	        	actionPredicated = builder.like(builder.upper(cust.<String>get("action")), "%" + action.toUpperCase() + "%");
	            predicateList.add(actionPredicated);
	        }


	        Predicate[] predicates = new Predicate[predicateList.size()];
	        predicateList.toArray(predicates);
	        query.where(predicates);

	        Query q = getEntityManager()
	                .createQuery(query);
	        
	        if(start != null) {
	        	q.setFirstResult(start.intValue());
	        	q.setMaxResults(end.intValue());
	        }
	        
	        return q.getResultList();
	}

	@Override
	public List<AuditTrail> getDataAudittrail(String objectName, String fieldName, Date dateFrom, Date dateTo,
			String actor, String action) {

		return getDataAudittrail(null, null, objectName, fieldName, dateFrom, dateTo, actor, action);
	}
}
