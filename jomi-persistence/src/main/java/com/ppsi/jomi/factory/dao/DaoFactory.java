package com.ppsi.jomi.factory.dao;

import com.ppsi.jomi.persistence.dao.GenericDao;

public interface DaoFactory {
	
	@SuppressWarnings("rawtypes")
	GenericDao getDao(String className);
	
}
