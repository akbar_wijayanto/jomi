package com.ppsi.jomi.factory.model;

public interface ClassFactory {

	public Object getObject(Object oldObject);
	
}
