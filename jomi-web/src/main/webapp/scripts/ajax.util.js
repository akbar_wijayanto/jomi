function ajax(url, data, callback){
	$.ajax({
		type: 'POST',
		url: url,
		data: data,
		success: callback,
		async:true
	});
}