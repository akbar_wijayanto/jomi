//var base_url = '${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, pageContext.request.contextPath)}';
var base_url = '<c:url value='/' />';
var amount = "${curr}";
var interestParameterSlabAmount = "${interestParameterRateFormat}";
var amountRate = "${currRate}";
var preferredLocale = "${preferredLocale}";
var dateformat = "dd-M-yyyy";
var dateformatList = "dd-MM-yyyy";
var dateSeparator = "-";
var todayDate = "${today}";

var tdate = todayDate.split(dateSeparator)[0];
var tmonth = todayDate.split(dateSeparator)[1] - 1;
var tyear = todayDate.split(dateSeparator)[2];
//var decimalPoint =2 ;

//$(document).ready(function() {
//    $.datepicker.setDefaults({ dateFormat:'dd-mm-yy', changeMonth:true, changeYear:true });
//});