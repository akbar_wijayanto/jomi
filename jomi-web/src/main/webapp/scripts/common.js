function is_touch_device() {
    return !!('ontouchstart' in window);
}

$(document).ready(function() {
	
	$('input:text.interestParameterSlabAmount').each(function() {
        $(this).parseNumber({format:interestParameterSlabAmount, locale:preferredLocale})
        $(this).formatNumber({format:interestParameterSlabAmount, locale:preferredLocale});
    });
	
	$('input:text.interestParameterSlabAmount').on('blur', function() {
        $('input:text.interestParameterSlabAmount').each(function() {
            $(this).parseNumber({format:interestParameterSlabAmount, locale:preferredLocale})
            $(this).formatNumber({format:interestParameterSlabAmount, locale:preferredLocale});
        });
    });
	
	$('input:text.amount').each(function() {
        $(this).parseNumber({format:amount, locale:preferredLocale})
        $(this).formatNumber({format:amount, locale:preferredLocale});
    });
	
    $('input:text.amount').on('blur', function() {
        $('input:text.amount').each(function() {
            $(this).parseNumber({format:amount, locale:preferredLocale})
            $(this).formatNumber({format:amount, locale:preferredLocale});
        });
    });

    $('input:text.rateAmount').each(function() {
        $(this).parseNumber({format:amountRate, locale:preferredLocale})
        $(this).formatNumber({format:amountRate, locale:preferredLocale});
    });
    
    $('input:text.rateAmount').on('blur', function() {
        $('input:text.rateAmount').each(function() {
            $(this).parseNumber({format:amountRate, locale:preferredLocale})
            $(this).formatNumber({format:amountRate, locale:preferredLocale});
        });
    });



    //* tooltips
    egl_tips.init();
    if(!is_touch_device()){
        //* popovers
        egl_popOver.init();
    }

    //* breadcrumbs
    egl_crumbs.init();
    //* pre block prettify
    prettyPrint();
    //* external links
    egl_external_links.init();
    //* style switcher
    egl_style.init();
    //* accordion icons
    egl_acc_icons.init();
    //* colorbox single
    if($('.cbox_single').length) {
        egl_colorbox_single.init();
    }
    //* main menu mouseover (uncoment next line to enable mouseover)
    //egl_nav_mouseover.init();


});


//* tooltips
egl_tips = {
    init: function() {
        if(!is_touch_device()){
            var shared = {
                style		: {
                    classes: 'ui-tooltip-shadow ui-tooltip-tipsy'
                },
                show		: {
                    delay: 100,
                    event: 'mouseenter focus'
                },
                hide		: { delay: 0 }
            };
            if($('.ttip_b').length) {
                $('.ttip_b').qtip( $.extend({}, shared, {
                    position	: {
                        my		: 'top center',
                        at		: 'bottom center',
                        viewport: $(window)
                    }
                }));
            };
            if($('.ttip_t').length) {
                $('.ttip_t').qtip( $.extend({}, shared, {
                    position: {
                        my		: 'bottom center',
                        at		: 'top center',
                        viewport: $(window)
                    }
                }));
            };
            if($('.ttip_l').length) {
                $('.ttip_l').qtip( $.extend({}, shared, {
                    position: {
                        my		: 'right center',
                        at		: 'left center',
                        viewport: $(window)
                    }
                }));
            };
            if($('.ttip_r').length) {
                $('.ttip_r').qtip( $.extend({}, shared, {
                    position: {
                        my		: 'left center',
                        at		: 'right center',
                        viewport: $(window)
                    }
                }));
            };
        }
    }
};

//* popovers
egl_popOver = {
    init: function() {
        $(".pop_over").popover();
    }
};

//* breadcrumbs
egl_crumbs = {
    init: function() {
        $('#jCrumbs').jBreadCrumb({
            endElementsToLeaveOpen: 0,
            beginingElementsToLeaveOpen: 0,
            timeExpansionAnimation: 500,
            timeCompressionAnimation: 500,
            timeInitialCollapse: 500,
            previewWidth: 30
        });
    }
};

//* external links
egl_external_links = {
    init: function() {
        $("a[href^='http']").not('.thumbnail>a').each(function() {
            $(this).attr('target','_blank').addClass('external_link');
        })
    }
};

//* style switcher
egl_style = {
    init: function() {
        $('.style_switcher a').click(function() {
            $('#link_theme').attr('href','css/'+$(this).attr('title')+'.css');
            $('.style_switcher a').removeClass('th_active');
            $(this).addClass('th_active');
        });
    }
};

//* accordion icons
egl_acc_icons = {
    init: function() {
        var accordions = $('#accordion1,#accordion2');

        accordions.find('.accordion-group').each(function(){
            var acc_active = $(this).find('.accordion-body').filter('.in');
            acc_active.prev('.accordion-heading').find('.accordion-toggle').addClass('acc-in');
        });
        accordions.on('show', function(option) {
            $(this).find('.accordion-toggle').removeClass('acc-in');
            $(option.target).prev('.accordion-heading').find('.accordion-toggle').addClass('acc-in');
        });
        accordions.on('hide', function(option) {
            $(option.target).prev('.accordion-heading').find('.accordion-toggle').removeClass('acc-in');
        });
    }
};

//* main menu mouseover
egl_nav_mouseover = {
    init: function() {
        $('header li.dropdown').on('mouseenter', function() {
            $(this).addClass('navHover').children('ul.dropdown-menu').show();
        }).on('mouseleave', function() {
                $(this).removeClass('navHover open').children('ul.dropdown-menu').hide();
            });
    }
};

//* single image colorbox
egl_colorbox_single = {
    init: function() {
        $('.cbox_single').colorbox({
            maxWidth	: '80%',
            maxHeight	: '80%',
            opacity		: '0.2',
            fixed		: true
        });
    }
};
/* This function is used to set cookies */
function setCookie(name,value,expires,path,domain,secure) {
    document.cookie = name + "=" + escape (value) +
        ((expires) ? "; expires=" + expires.toGMTString() : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") + ((secure) ? "; secure" : "");
}

/* This function is used to get cookies */
function getCookie(name) {
    var prefix = name + "=";
    var start = document.cookie.indexOf(prefix);

    if (start==-1) {
        return null;
    }

    var end = document.cookie.indexOf(";", start+prefix.length);
    if (end==-1) {
        end=document.cookie.length;
    }

    var value=document.cookie.substring(start+prefix.length, end);
    return unescape(value);
}

/* This function is used to delete cookies */
function deleteCookie(name,path,domain) {
    if (getCookie(name)) {
        document.cookie = name + "=" +
            ((path) ? "; path=" + path : "") +
            ((domain) ? "; domain=" + domain : "") +
            "; expires=Thu, 01-Jan-70 00:00:01 GMT";
    }
}

function format(){
	$('input:text.rateAmount').each(function() {
        $(this).parseNumber({format:amountRate, locale:preferredLocale})
        $(this).formatNumber({format:amountRate, locale:preferredLocale});
    });
	
	$('input:text.amount').each(function() {
        $(this).parseNumber({format:amount, locale:preferredLocale})
        $(this).formatNumber({format:amount, locale:preferredLocale});
    });
	
	$('input:text.interestParameterSlabAmount').each(function() {
        $(this).parseNumber({format:interestParameterSlabAmount, locale:preferredLocale})
        $(this).formatNumber({format:interestParameterSlabAmount, locale:preferredLocale});
    });
}



