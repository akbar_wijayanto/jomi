/**
 * @author -U-
 */

/*$('input:text.amount').on('change', function() {
	$(this).number(true, 2);
});	

$('input:text.rateAmount').on('change', function() {
	$(this).number(true, 2);
});*/
$(document).ready(function(){
	$('input:text.interestParameterSlabAmount').each(function() {
	    $(this).parseNumber({format:interestParameterSlabAmount, locale:preferredLocale})
	    $(this).formatNumber({format:interestParameterSlabAmount, locale:preferredLocale});
	});
	
	$('input:text.amount').each(function() {
		$(this).parseNumber({format:amount, round:false});
		$(this).formatNumber({format:amount, round:false});
	});
	
	$('input:text.rateAmount').each(function() {
		$(this).parseNumber({format:amountRate, round:false});
		$(this).formatNumber({format:amountRate, round:false});
	});
});

$('input:text.interestParameterSlabAmount').on('blur', function() {
        $('input:text.interestParameterSlabAmount').each(function() {
            $(this).parseNumber({format:interestParameterSlabAmount, locale:preferredLocale})
            $(this).formatNumber({format:interestParameterSlabAmount, locale:preferredLocale});
        });
    });

$('input:text.amount').on('blur', function() {
	$('input:text.amount').each(function() {
		$(this).parseNumber({format:amount, round:false});
		$(this).formatNumber({format:amount, round:false});
    });
});

$('input:text.rateAmount').on('blur', function() {
	$('input:text.rateAmount').each(function() {
		$(this).parseNumber({format:amountRate, round:false});
		$(this).formatNumber({format:amountRate, round:false});
    });
});
