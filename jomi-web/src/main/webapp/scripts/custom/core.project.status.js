$(document).ready(function() {
	$('#save').click(function() {
		$('#coreProject').validationEngine();
	});
	$('#cancel').click(function() {
		$('#coreProject').validationEngine('detach');
	});
	
	$('#saveTask').click(function(){
		var startDate=$('#startDate').val();
		var endDate=$('#endDate').val();
		var projectId=$('#projectId').val();
        $('#coreProject').validationEngine({promptPosition : "bottomLeft"});
		if ($('#coreProject').validationEngine('validate')) {
			$.post(base_url+'core/project/status/report',{projectId:projectId});
		}
		$('#projectId').val()=projectId;
		return false;
    });
	
	$("#taskreportdownloadxls").click(function(){
	  window.location = base_url+'core/project/status/report/download/all/xls?projectId='+$('#projectId').val();
	});
		 
	$("#taskreportdownloadpdf").click(function(){
	  window.location = base_url+'core/project/status/report/download/all/pdf?projectId='+$('#projectId').val();
	});
});
function doCopy(id, id_, idList, code, nameCode, name) {
	var value = nameCode + " - " + name;
	var value_ = code;
	$("#" + idList).modal('hide');
	$("#" + id).val(value_);
	$("#" + id_).val(value);
}