$(document).ready(function(){
	check();
	$('#save').click(function(){
    	$('#corePermission').validationEngine();
    });
    $('#cancel').click(function(){
    	$('#corePermission').validationEngine('detach');
    });
    
    $("#moduleName").change(function(){
        $("#applicationName").html("");
        var options="";
        options += "<option value=''>--- Select ---</option>";
        $.post(base_url + "core/permission/" + "getmodules", {module: $("#moduleName option:selected").val()},function(data){

            var i=0;
            $.each(data, function(i, item) {
                options += "<option value="+ item.applicationValue +">"+ item.applicationType +"</option>";
            });
            $("#applicationName").html(options);
            $("#applicationName").select2("destroy");
            $("#applicationName").select2();
            check();
        });
    });
    
    $("#applicationName").change(function(){    
    	check();
    });
    
    $("#actionName").change(function(){    
    	check();
    });
    
    $("#recordId").change(function(){    
    	check();
    });
    
    function check(){
    	$("#preview").html("");
    	
    	$("#preview").val("" + $('#moduleName option:selected').val() + ":" + $('#applicationName').val() +":" + $('#actionName').val() + ":" + $('#recordId').val());
    }


});