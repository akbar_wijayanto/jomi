jQuery(document).ready(function() {
	$.ajaxSetup({
		cache : false
	});
	
	$('#save').click(function() {
		$('#systemWide').validationEngine();
	});
	
	$('#cancel').click(function() {
		$('#systemWide').validationEngine('detach');
	});
	
	$('#save').click(function() {
		$('#jobParameter').validationEngine();
	});

	$('#cancel').click(function() {
		$('#jobParameter').validationEngine('detach');
	});

	$('#save').click(function() {
		$('#emailConfiguration').validationEngine();
	});

	$('#cancel').click(function() {
		$('#emailConfiguration').validationEngine('detach');
	});

	$('#save').click(function() {
		$('#coreNotification').validationEngine();
	});

	$('#cancel').click(function() {
		$('#coreNotification').validationEngine('detach');
	});

	$('#save').click(function() {
		$('#coreTax').validationEngine();
	});

	$('#cancel').click(function() {
		$('#coreTax').validationEngine('detach');
	});
	
	var $form = $('form'),
    origForm = $form.serialize();

	//$('#save').prop("disabled", true);
	$('form :input').on('change input', function() {
	    if($form.serialize() !== origForm) {
	    	$('#save').prop("disabled", false);
	    }
	});
	
	$('#defaultHoliday').keydown(function(e) {
		var a = [];
		var k = e.which;

		// for (i = 48; i < 58; i++)
		// a.push(i);
		//console.log(e.which);

		a.push(119, 104, 8, 87, 72,37,39,9);// enable w,h,W,H,backspace,left,right,tab

		if (!(a.indexOf(k) >= 0)) {
			e.preventDefault();
		}

		$('#defaultHoliday').keyup(function(key) {
			var inputStr = $('#defaultHoliday').val();
			$('#defaultHoliday').val(inputStr.toUpperCase());
			console.log(key.which);
		});
	});
});
