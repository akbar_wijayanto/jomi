$(document).ready(function() {
    $('#parent').hide();	
    checkChargeType();

    $('#type').change(function(){
    	var selectedType = this.value;
    	if(selectedType == "C"){
    		$('#parent').show();
    	}else{
    		$('#parent').hide();
    		$('#parentId').val("0");
    	}
    });
    
	$.ajaxSetup({
		cache : false
	});
	$('#save').click(function() {
		$('#coreCharge').validationEngine();
	});
	$('#cancel').click(function() {
		$('#coreCharge').validationEngine('detach');
	});

	checkChargeType;

	$('#chargeType').on('change', function() {
		checkChargeType();
	});

	$('#chargeCalculation').on('change', function() {
		var chargeCal = $('#chargeCalculation').val();
		if (chargeCal > 1) {
			$('#chargeRate').attr('disabled', 'disabled');
			$('#chargeAmount').attr('disabled', 'disabled');
		} else {
			$('#chargeRate').removeAttr('disabled');
			$('#chargeAmount').removeAttr('disabled');
		}

	});

	function checkChargeType() {
		var chargeType = $('#chargeType').val();
	
		if (chargeType == 1) {
			$('#chargeRate').removeAttr('disabled');
			$('#chargeAmount').attr('disabled', 'disabled');
			$('#chargeAmount').val("");
			$('#minCharge').removeAttr('disabled');
			$('#maxCharge').removeAttr('disabled');
		} else if (chargeType == 2) {
			$('#chargeAmount').removeAttr('disabled');
			$('#chargeRate').attr('disabled', 'disabled');
			$('#minCharge').attr('disabled', 'disabled');
			$('#maxCharge').attr('disabled', 'disabled');
			$('#chargeRate').val("");
			$('#minCharge').val("");
			$('#maxCharge').val("");
		} else {
			$('#chargeRate').removeAttr('disabled');
			$('#chargeAmount').removeAttr('disabled');
			$('#minCharge').removeAttr('disabled');
			$('#maxCharge').removeAttr('disabled');
		}
	}

	function checkinteresdebit() {
		var interestcreditfreq = $("#frequency").val();

		if (interestcreditfreq == "DAILY") {
			$("#capitalizationDate").attr("disabled", true);
		} else if (interestcreditfreq == "MONTHLY") {
			$("#capitalizationDate").attr("disabled", false);
		} else {
			$("#capitalizationDate").attr("disabled", true);
		}
	}

	checkinteresdebit();
	$("#frequency").on("change", function() {
		checkinteresdebit();
	});
	
/*	$('#minCharge').blur(function() {
		var value1 = $('#minCharge').val();
		var value2 = $('#maxCharge').val();
		if (value2!=0)	
		{
			if (value1 > value2) 
		{
			alert('Max Charge has to be greater than Min Charge ')
		}
			
		}
		});	
	
	$('#maxCharge').blur(function() {
		var value1 = $('#minCharge').val();
		var value2 = $('#maxCharge').val();
		if (value1!=0)	
		{
			if (value1 > value2) 
		{
			alert('Max Charge has to be greater than Min Charge ')
		}
			
		}
		});	*/

});