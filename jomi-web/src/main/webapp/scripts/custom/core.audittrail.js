$(document).ready(function(){
	$.ajaxSetup ({
		cache: false
	});
	
	$('#save').click(function(){
        $('#coreAuditTrail').validationEngine();
    });
    
    $('#cancel').click(function(){
        $('#coreAuditTrail').validationEngine('detach');
    }); 
    
//	$('#dateFrom').datepicker({
//	   format: 'yyyy-mm-dd'
//	}); 
//	
//	$("#dateTo").datepicker({
//	   format: 'yyyy-mm-dd'
//    });

//	$('#dateFrom').on('change', function(ev){
//	    $(this).datepicker('hide');
//	});
//	$('#dateTo').on('changeDate', function(ev){
//	    $(this).datepicker('hide');
//	});
    
//    function searchAuditTrail(){
//    	$.post(base_url + "core/audittrail/searchAuditTrail", 
//    			{objectName: $('#objectName').val(), fieldName: $('#fieldName').val()
//    			,actor: $('#actor').val(), action: $('#action').val()
//    			,dateFrom: $('#dateFrom').val(), dateTo: $('#dateTo').val()},function(data){
//			var table_rows = "";
//			for (var i = 0; i < data.length; i++){
//				var data_row = data[i];
//				table_rows +='<tr>'
//								+'<td>'+data_row.actor+'</td>'
//								+'<td>'+(data_row.objectName).substring(40)+'</td>'
//								+'<td>'+data_row.fieldName+'</td>'
//								+'<td>'+data_row.before+'</td>'
//								+'<td>'+data_row.after+'</td>'
//								+'<td>'+data_row.time+'</td>'
//								+'<td>'+data_row.action+'</td>'
//								+'<td>'
//								+	'<a href="/miniloan-web/core/audittrail/view/'+data_row.id+'" class="fa fa-eye" title="View"></a>'
//								+'</td>'
//							+'</tr>';
//			}
//			
//			$('#auditTable').dataTable().fnDestroy();
//			$('#auditBody').html(table_rows);
//			$('#auditTable').dataTable();
//		});	
//    }
	
    $("#searchBtn").click(function(){
    	var oTable = $('#auditTable').dataTable();
    	oTable.fnDraw();
    	oTable.fnPageChange('first');
    	return false;
    });
    
    function replaceField()
    {
        var key=$("#fieldName").val();
        key=key.replace(/ /g,"_");
        $("#fieldName").val(key);
    }

    $("#pdf").click(function(){
    	window.location = base_url+'core/audittrail/download/pdf?actor='+$('#actor').val()+'&authorizer='+$('#authorizer').val()+'&objectName='+ $('#objectName').val()+'&fieldName='+$('#fieldName').val()+'&action='+$('#action').val()+'&dateFrom='+$('#dateFrom').val()+'&dateTo='+$('#dateTo').val();
    });
    
    $("#xls").click(function(){
    	window.location = base_url+'core/audittrail/download/xls?actor='+$('#actor').val()+'&authorizer='+$('#authorizer').val()+'&objectName='+ $('#objectName').val()+'&fieldName='+$('#fieldName').val()+'&action='+$('#action').val()+'&dateFrom='+$('#dateFrom').val()+'&dateTo='+$('#dateTo').val();
    });
    
    $('#auditTable').dataTable( {
        "aoColumnDefs": [
            { "mDataProp": "actor", "aTargets": [0] },
            { "mDataProp": "authorizer", "aTargets": [1] },
            { "mDataProp": null, "mRender" : function(data, type, oObj){
            		var oName = oObj['objectName'].split(".");
            		return oName[oName.length - 1];
            	}, "aTargets": [2] },
            { "mDataProp": "fieldName", "aTargets": [3] },
            { "mDataProp": "before", "aTargets": [4] },
            { "mDataProp": "after", "aTargets": [5] },
            { "mDataProp": "time", "aTargets": [6] },
            { "mDataProp": "action", "aTargets": [7] },
            { "mDataProp": null, "mRender" : function (data, type, oObj ) {
                return '<a href=\"'+ base_url+ 'core/audittrail/view/' + oObj['id'] + '\" title=\"View\"><i class=\"fa fa-eye\"></i></a>' ;
            },
                "aTargets" : [8]}
        ],
        "bFilter": false,
        "bSort": false,
        "bProcessing": true,
        "bServerSide": true,
        "sPaginationType": "full_numbers",
        "sAjaxSource": base_url +"core/audittrail/page",
        "fnServerParams": function ( aoData ) {
            aoData.push(
                { "name":"objectName", "value":$("#objectName").val() },
                { "name":"fieldName", "value":$("#fieldName").val() },
                { "name":"dateFrom", "value":$("#dateFrom").val() },
                { "name":"dateTo", "value":$("#dateTo").val() },
                { "name":"actor", "value":$("#actor").val() },
                { "name":"authorizer", "value":$("#authorizer").val() },
                { "name":"action", "value":$("#action").val() }
            );
        },
        "sScrollX": "100%",
        "sScrollXInner": "110%",
//        "sPaginationType": "bootstrap",
        "bScrollCollapse": true

    } );

});