jQuery(document).ready(function(){
	$.ajaxSetup ({
		cache: false
	});
	
	$(function(){
		var checkAllPermission = $('input#checkAllPermission');
		var checkboxesPermission = $('input.checkALL');
		
		checkAll(checkAllPermission, checkboxesPermission);
	});
	
	$(function(){
		var checkAllPermission = $('input#checkAuthorizePermission');
		var checkboxesPermission = $('input.checkAUTHORIZE');
		
		checkAll(checkAllPermission, checkboxesPermission);
	});
	
	$(function(){
		var checkAllPermission = $('input#checkDeletePermission');
		var checkboxesPermission = $('input.checkDELETE');
		
		checkAll(checkAllPermission, checkboxesPermission);
	});
	
	$(function(){
		var checkAllPermission = $('input#checkReadPermission');
		var checkboxesPermission = $('input.checkREAD');
		
		checkAll(checkAllPermission, checkboxesPermission);
	});
	
	$(function(){
		var checkAllPermission = $('input#checkInputPermission');
		var checkboxesPermission = $('input.checkINPUT');
		
		checkAll(checkAllPermission, checkboxesPermission);
	});
	
	$(function(){
		var checkAllPermission = $('input#checkEditPermission');
		var checkboxesPermission = $('input.checkEDIT');
		
		checkAll(checkAllPermission, checkboxesPermission);
	});
	
    $('#save').click(function(){
        $('#coreRole').validationEngine();
    });
    $('#cancel').click(function(){
        $('#coreRole').validationEngine('detach');
    });
    
    $('#checkMenu').click(function () {
        $('#menus').find('input[name=coreMenus]').attr('checked', this.checked);
    });
    	    
    $('#menus').find('input[name=coreMenus]').click(function () {
        $('#checkMenu').attr('checked', false);
    });
    
    function checkAll(eCheckAll, eCheckBox) {
    	eCheckAll.on('ifChecked ifUnchecked', function(event) {        
            if (event.type == 'ifChecked') {
            	eCheckBox.iCheck('check');
            } else {
            	eCheckBox.iCheck('uncheck');
            }
        });
        
    	eCheckBox.on('ifChanged', function(event){
            if(eCheckBox.filter(':checked').length == eCheckBox.length) {
            	eCheckAll.prop('checked', 'checked');
            } else {
            	eCheckAll.removeProp('checked');
            }
            eCheckAll.iCheck('update');
        });
    }
    
//    $('input').iCheck();
    
    
     
    	    /*$('#name').blur(function(){
    			
    			var txt = $('#name').val();
    			var re = /^[ A-Za-z0-9_@./#&+-]*$/
    			
    			if (re.test(txt)) {
    			return true;
    			}
    			else {
    			alert('Field name must be string or Alphanumeric without space');
    			return false;
    			}
    			 
    		});*/
});