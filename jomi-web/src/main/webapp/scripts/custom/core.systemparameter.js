jQuery(document).ready(function() {
	$.ajaxSetup({
		cache : false
	});
	
	$("#save").click(function(){
		$("#parameter").validationEngine();
	});
	
	$("#cancel").click(function(){
		$("#parameter").validationEngine("detach");
	});
	
	$('#passwordFlag').on('ifChanged', function(event){
		if($(this).is(":checked")){
			$("#value").prop("type", "password");
		} else {
			$("#value").prop("type", "text");
		}
	});
});