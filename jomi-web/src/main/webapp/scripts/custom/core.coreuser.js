$(document).ready(function(){

    $('#save').click(function(){
        $('#coreUser').validationEngine();
    });
    $('#cancel').click(function(){
        $('#coreUser').validationEngine('detach');
    });
    
    $(function () {
        var checkAllRole = $('input#checkRole');
        var checkboxesRole = $('input.coreRoles');
        
        checkBoxCheckAll(checkAllRole, checkboxesRole);
    });
    
    $(function () {
        var checkAllBranch = $('input#checkBranch');
        var checkboxesBranch = $('input.coreBranches');
        
        checkBoxCheckAll(checkAllBranch, checkboxesBranch);
    });
    
    $(function () {
        var checkAllPermission = $('input#checkAllPermission');
        var checkboxesAllPermission = $('input.ALL_CLASS');
        
        checkBoxCheckAll(checkAllPermission, checkboxesAllPermission);
    });
    
    $(function () {
        var checkAllAuthorPermission = $('input#checkAllAuthorPermission');
        var checkboxesAllAuthorPermission = $('input.AUTHORIZE_CLASS');
        
        checkBoxCheckAll(checkAllAuthorPermission, checkboxesAllAuthorPermission);
    });
    
    $(function () {
        var checkAllDeletePermission = $('input#checkAllDeletePermission');
        var checkboxesAllDeletePermission = $('input.DELETE_CLASS');
        
        checkBoxCheckAll(checkAllDeletePermission, checkboxesAllDeletePermission);
    });
    
    $(function () {
        var checkAllReadPermission = $('input#checkAllReadPermission');
        var checkboxesAllReadPermission = $('input.READ_CLASS');
        
        checkBoxCheckAll(checkAllReadPermission, checkboxesAllReadPermission);
    });
    
    $(function () {
        var checkAllInputPermission = $('input#checkAllInputPermission');
        var checkboxesAllInputPermission = $('input.INPUT_CLASS');
        
        checkBoxCheckAll(checkAllInputPermission, checkboxesAllInputPermission);
    });
    
    $(function () {
        var checkAllEditPermission = $('input#checkAllEditPermission');
        var checkboxesAllEditPermission = $('input.EDIT_CLASS');
        
        checkBoxCheckAll(checkAllEditPermission, checkboxesAllEditPermission);
    });   

    $('#tableList2').dataTable();
    $('#tableList3').dataTable();
    $("#lastLogon").datepicker();
    
   
    function checkBoxCheckAll(eAll, eCheck) {
    	eAll.on('ifChanged', function(event) {
    		var isChecked = $(this).is(":checked");
            if (isChecked == true) {
            	eCheck.iCheck('check');
            } else {
            	eCheck.iCheck('uncheck');
            }
        });
        
    	eCheck.on('ifChanged', function(event){
            if(eCheck.filter(':checked').length == eCheck.length) {
            	eAll.prop('checked', true);
            } else {
            	eAll.prop('checked', false);
            }
            eAll.iCheck('update');
        });
    }
	
    $('#userTable').dataTable( {
        "aoColumnDefs": [
            { "mDataProp": "username", "aTargets": [0] },
            { "mDataProp": "firstName", "aTargets": [1] },
            { "mDataProp": "lastName", "aTargets": [2] },
            { "mDataProp": null, "mRender" : function (data, type, oObj ){
            	return oObj['activeRole']['description']; 
            }, "aTargets": [3] },
            { "mDataProp": null, "mRender" : function (data, type, oObj ){
            	return oObj['departmentCode']['departmentCode']; 
            }, "aTargets": [4] },
            { "mDataProp": "status", "aTargets": [5] },
            { "mDataProp": null, "mRender" : function (data, type, oObj ){
            	if(oObj['activeRole']['name'] === "ROLE_CHIEF_TELLER")
            		return "chief teller";
            	else	
            		return oObj['userType'];
            }, "aTargets": [6] },
            { "mDataProp": null, "mRender" : function (data, type, oObj ){
            	return oObj['activeBranch']['id'] + ' - ' + oObj['activeBranch']['name']; 
            }, "aTargets": [7] },
            { "mDataProp": null, "mRender" : function (data, type, oObj ){
            	var buttons = '';
            	if(canRead)
            		buttons += '<a href=\"'+ base_url+ 'core/coreuser/view/' + oObj['id'] + '\" title=\"View\"><i class=\"fa fa-eye\"></i></a>&nbsp;';
            	if(canEdit){
            		buttons += '<a href=\"'+ base_url+ 'core/coreuser/edit/' + oObj['id'] + '\" title=\"Edit\"><i class=\"fa fa-edit\"></i></a>&nbsp;';
            		buttons += '<a href=\"'+ base_url+ 'core/coreuser/detail/' + oObj['id'] + '\" title=\"Setting\"><i class=\"fa fa-cogs\"></i></a>&nbsp;';
            	}
            	
            	if(oObj['status'] === 'LIVE'){
            		if(canEdit){
            			buttons += '<a href=\"'+ base_url+ 'core/user/changeroleview/' + oObj['id'] + '\" title=\"Change Active Role\"><i class=\"fa fa-caret-square-o-right\"></i></a>&nbsp;';
                		buttons += '<a href=\"'+ base_url+ 'core/coreuser/resetpassword/' + oObj['id'] + '\" title=\"Reset Password\"><i class=\"fa fa-refresh\"></i></a>&nbsp;';
                		
                		if(isImplementTeller){
                			buttons += '<a href=\"'+ base_url+ 'core/teller/add/' + oObj['id'] + '\" title=\"Set Teller\"><i class=\"fa fa-user\"></i></a>&nbsp;';
                    		buttons += '<a href=\"'+ base_url+ 'core/staff/add/' + oObj['id'] + '\" title=\"Set Head Teller\"><i class=\"fa fa-male\"></i></a>&nbsp;';
                		}
            		}
            	}
            	else{
            		buttons += '<a href=\"javascript:void(0);\" style=\"color: #ccc;\" title=\"Change Active Role\"><i class=\"fa fa-caret-square-o-right\"></i></a>&nbsp;';
            		buttons += '<a href=\"javascript:void(0);\" style=\"color: #ccc;\" title=\"Reset Password\"><i class=\"fa fa-refresh\"></i></a>&nbsp;';
            		
            		if(isImplementTeller){
            			buttons += '<a href=\"javascript:void(0);\" style=\"color: #ccc;\" title=\"Set Teller\"><i class=\"fa fa-user\"></i></a>&nbsp;';
                		buttons += '<a href=\"javascript:void(0);\" style=\"color: #ccc;\" title=\"Set Head Teller\"><i class=\"fa fa-male\"></i></a>&nbsp;';
            		}
            	}
            	return buttons;
            }, "aTargets": [8] },
        ],
        "bFilter": true,
        "bSort": false,
        "bProcessing": true,
        "bServerSide": true,
        "sPaginationType": "full_numbers",
        "sAjaxSource": base_url +"core/coreuser/page",
        "sScrollX": "100%",
        "sScrollXInner": "110%",
//        "sPaginationType": "bootstrap",
        "bScrollCollapse": true
    });
});


