$(document).ready(function() {
	$('#save').click(function() {
		$('#coreTask').validationEngine();
	});
	$('#cancel').click(function() {
		$('#coreTask').validationEngine('detach');
	});
	
	$('#saveTask').click(function(){
		var startDate=$('#startDate').val();
		var endDate=$('#endDate').val();
		var userId=$('#userId').val();
        $('#coreTask').validationEngine({promptPosition : "bottomLeft"});
		if ($('#coreTask').validationEngine('validate')) {
			$.post(base_url+'core/task/report',{userId:userId,startDate:startDate,endDate:endDate});
		}
		$('#startDate').val()=startDate;
		$('#endDate').val()=endDate;
		$('#userId').val()=userId;
		return false;
    });
	
	$("#taskreportdownloadxls").click(function(){
		  window.location = base_url+'core/task/report/download/all/xls?userId='+$('#userId').val()+'&startDate='+$('#startDate').val()+'&endDate='+$('#endDate').val();
		});
		 
	$("#taskreportdownloadpdf").click(function(){
	  window.location = base_url+'core/task/report/download/all/pdf?userId='+$('#userId').val()+'&startDate='+$('#startDate').val()+'&endDate='+$('#endDate').val();
	});
});
function doCopy(id, id_, idList, code, nameCode, name) {
	var value = nameCode + " - " + name;
	var value_ = code;
	$("#" + idList).modal('hide');
	$("#" + id).val(value_);
	$("#" + id_).val(value);
}