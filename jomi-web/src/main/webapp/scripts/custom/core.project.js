jQuery(document).ready(function(){
	$.ajaxSetup ({
		cache: false
	});
	
    $('#save').click(function(){
        $('#coreProject').validationEngine();
    });
    $('#cancel').click(function(){
        $('#coreProject').validationEngine('detach');
    });
    
    $('#checkUser').click(function () {
        $('#users').find('input[name=coreUsers]').attr('checked', this.checked);
    });
    	    
    $('#users').find('input[name=coreUsers]').click(function () {
        $('#checkUser').attr('checked', false);
    });
    
    function checkAll(eCheckAll, eCheckBox) {
    	eCheckAll.on('ifChecked ifUnchecked', function(event) {        
            if (event.type == 'ifChecked') {
            	eCheckBox.iCheck('check');
            } else {
            	eCheckBox.iCheck('uncheck');
            }
        });
        
    	eCheckBox.on('ifChanged', function(event){
            if(eCheckBox.filter(':checked').length == eCheckBox.length) {
            	eCheckAll.prop('checked', 'checked');
            } else {
            	eCheckAll.removeProp('checked');
            }
            eCheckAll.iCheck('update');
        });
    }
});