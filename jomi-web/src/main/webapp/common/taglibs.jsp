<%@ page language="java" errorPage="/WEB-INF/pages/500.jsp" pageEncoding="UTF-8" contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<c:set var="datePattern"><fmt:message key="date.format"/></c:set>
<c:set var="dateViewPattern"><fmt:message key="date.format.view"/></c:set>
<c:set var="dateViewInput"><fmt:message key="date.formatInputView"/></c:set>
<c:set var="curr"><fmt:message key="currency.format"/></c:set>
<c:set var="currRate"><fmt:message key="currency.rate.format"/></c:set>
<c:set var="interestParameterRateFormat"><fmt:message key="interest.parameter.rate.format"/></c:set>
<fmt:setLocale value="${preferredLocale}" scope="session" />
