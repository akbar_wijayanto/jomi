<%@ include file="/common/taglibs.jsp"%>
 
<aside class="left-side sidebar-offcanvas" >
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- sidebar menu: : style can be found in sidebar.less -->

		<ul class="sidebar-menu bg-blue" id="accordion" style="padding: 0 10px; margin-top:5px; font-family: FS_Albert;">
			<c:forEach var="menus" items="${userMenus}">
					<c:choose>
						<c:when test="${menus.parentMenu.id == parentIdSession }">
							<li class="active treeview bg-blue">
						</c:when>
						<c:otherwise>
							<li class="treeview bg-blue">
						</c:otherwise>
					</c:choose>
					<a href="#" style="border-top: none;"> <i class="fa fa-dot-circle-o"></i>
						<span> <fmt:message key="${menus.parentMenu.name}" /></span> <i
						class="fa fa-angle-down pull-right"></i>
					</a>
					<ul class="treeview-menu bg-blue">
						<c:forEach var="menu" items="${menus.childMenus}">
							<c:choose>
								<c:when test="${empty menus.childSubMenus}">
									<li><a name="link" href="<c:url value='${menu.permalinks }' />">
										<i class="fa fa-caret-square-o-right"></i> 
										<fmt:message key="${menu.name }" /></a>
									</li>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${empty menu.permalinks or menu.permalinks == '#'}">
											<c:choose>
												<c:when test="${menu.id == parentSubMenuIdSession }">
													<li class="treeview active child"><a href="#"><i class="fa fa-minus-square-o"></i> <fmt:message key="${menu.name }" /></a>
												</c:when>
												<c:otherwise>
													<li class="treeview child"><a href="#"><i class="fa fa-plus-square-o"></i> <fmt:message key="${menu.name }" /></a>
												</c:otherwise>
											</c:choose>
												<ul class="treeview-menu">
													<c:forEach var="submenu" items="${menus.childSubMenus}">
														<c:if test="${submenu.parentId == menu.id }">
															<li>
																<c:choose>
																	<c:when test="${empty submenu.permalinks or submenu.permalinks == '#'}">
																		<c:choose>
																			<c:when test="${submenu.id == parentSub2MenuIdSession }">
																				<li class="treeview active subchild"><a href="#"><i class="fa fa-minus-square-o"></i> <fmt:message key="${submenu.name }" /></a>
																			</c:when>
																			<c:otherwise>
																				<li class="treeview subchild"><a href="#"><i class="fa fa-plus-square-o"></i> <fmt:message key="${submenu.name }" /></a>
																			</c:otherwise>
																		</c:choose>
																			<ul class="treeview-menu">
																				<c:forEach var="sub2menu" items="${menus.child2SubMenus}">
																					<c:if test="${sub2menu.parentId == submenu.id }">
																						<li><a name="link" href="<c:url value='${sub2menu.permalinks }' />">
																							<i class="fa fa-caret-square-o-right"></i> 
																							<fmt:message key="${sub2menu.name }" /></a>
																						</li>
																					</c:if>
																				</c:forEach>
																			</ul>
																		</li>
																	</c:when>
																	<c:otherwise>
																		<a href="<c:url value='${submenu.permalinks }' />">
																		<i class="fa fa-caret-square-o-right"></i> 
																		<fmt:message key="${submenu.name }" /></a>
																	</c:otherwise>
																</c:choose>
															</li>
														</c:if>
													</c:forEach>
												</ul>
											</li>
										</c:when>
										<c:otherwise>
											<li><a  name="link"href="<c:url value='${menu.permalinks }' />"><i class="fa fa-caret-square-o-right"></i> <fmt:message key="${menu.name }" /></a></li>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</ul>
				</li>
			</c:forEach>
		</ul>
	</section>
</aside>
