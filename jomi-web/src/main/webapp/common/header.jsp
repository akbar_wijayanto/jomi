<%@ include file="/common/taglibs.jsp"%>
<header class="header">
<div class="logo">
	<a href="<c:url value='/mainMenu' />">
    	<!-- Add the class icon to your logo image or logo icon to add the margining -->
	    <img src="<c:url value="/images/jomi-logo-white.png"/>" style="height: 50px;width: 150px;">
	</a>
</div>

<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
	    <!-- Sidebar toggle button-->
	    <a href="#" class="fa fa-bars iconNav" data-toggle="offcanvas" role="button">
	        <span class="sr-only">Toggle navigation</span>
	       <!--  <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span> -->
	    </a>
	    <div class="mobileLogo">
		    <a href="<c:url value='/mainMenu' />">
			    <img src="<c:url value="/images/casa-logo-white2.png"/>" style="height: 100%;">
			</a>
	    </div>
	    <div class="navbar-right">
	        <ul class="nav navbar-nav">
	        	<li class="dropdown">
					<a href="#" class="dropdown-toggle nav_condensed" data-toggle="dropdown" style="padding-top: 23px; padding-bottom:17px; text-transform:capitalize;">  ${fn:substring(coreUser.preferredLocale,0,2)}</a>
					<ul class="dropdown-menu">
						<li><a href="<c:url value='/core/user/changelocale/en_US' />"><i class="flag-en"></i> En</a></li>
						<li><a href="<c:url value='/core/user/changelocale/in_ID' />"><i class="flag-id"></i> Id</a></li>
					</ul>
				</li>
				<li class="mobileHide">
					<a href="#" style="padding-top: 23px; padding-bottom:17px;"><fmt:formatDate pattern="dd-MMM-yy" value="${htoday}" /></a>
				</li>
	            <!-- User Account: style can be found in dropdown.less -->
	            <li class="dropdown user">
	                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	                    <img style="height: 29px;" alt="accountIcon" src="<c:url value="/images/accountIcon.png"/>">
	                   <!--  <i class="glyphicon glyphicon-user"></i> -->
	                    <span>${huser} <i class="caret"></i></span>
	                </a>
	                <ul class="dropdown-menu">
<%--                         <li><a href="<c:url value='/core/user/changeroleview/${coreUser.id}' />"><i class="glyphicon glyphicon-cog"></i><fmt:message key="user.myrole" /></a></li> --%>
<%-- 						<li><a href="<c:url value='/core/user' />"><i class="glyphicon glyphicon-user"></i><fmt:message key="user.myprofile" /></a></li> --%>
						<li><a data-toggle="modal" href="#aboutmodal"><i class="fa fa-question-circle"></i><fmt:message key="header.help.about"/></a></li>
						<li class="divider"></li>
						<li><a href="<c:url value='/logout' />"><i class="glyphicon glyphicon-off"></i><fmt:message key="user.logout" /></a></li>
                    </ul>	                
	            </li>
	        </ul>
	    </div>
	</nav>
</header>

<style type="text/css">
    .modal-body {
        height:250px;
        overflow-y: auto;
    }
</style>

<div class="modal fade" id="aboutmodal" tabindex="-1" role="dialog" aria-labelledby="aboutModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #AA0000;padding-right:35px;">
			    <h2 class="modal-title text-center" id="aboutModalLabel">
					<font color="white"><fmt:message key="header.help.about" /></font>
				</h2>
				<p class="text-center" style="margin-bottom: -5px;"><font color="white"><fmt:message key="product.name"/></font></p>
				<p class="text-center"><font color="white"><fmt:message key="product.version"/></font></p>
			</div>
			<div class="modal-footer text-center">
			    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>