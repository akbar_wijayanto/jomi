<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="row">
	<div class="col-xs-12">
		<c:if test="${not empty errors}">
			<div class="alert alert-danger alert-dismissable" id="errorMessages">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">&times;</button>
				<c:forEach var="error" items="${errors}">
					<c:out value="${error}"/><br />
                </c:forEach>
			</div>
			<c:remove var="errors" scope="session" />
		</c:if>
		<c:if test="${warning!=null }">
			<div class="alert alert-warning alert-dismissable"
				id="warningMessages">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<c:out value="${warning}" escapeXml="false" />
				
				<c:if test="${link!=null}">
					<c:out value="<a href='${link}'>download here</a>"  escapeXml="false"/>
				</c:if>
			</div>

		</c:if>
		<c:if test="${not empty successMessages}">
			<div class="alert alert-success alert-dismissable"
				id="successMessages">
				<i class="fa fa-check"></i>
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">&times;</button>
				<c:forEach var="msg" items="${successMessages}">
					<c:out value="${msg}" />
					<br />
				</c:forEach>
			</div>
			<c:remove var="successMessages" scope="session" />
		</c:if>
		<c:if test="${not empty authorizeMessages}">
			<div class="alert alert-warning alert-dismissable"
				id="authorizeMessages">
				<i class="fa fa-check"></i>
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">&times;</button>
				<c:forEach var="msg" items="${authorizeMessages}">
					<c:out value="${msg}" />
					<br />
				</c:forEach>
			</div>
			<c:remove var="authorizeMessages" scope="session" />
		</c:if>
	</div>
</div>