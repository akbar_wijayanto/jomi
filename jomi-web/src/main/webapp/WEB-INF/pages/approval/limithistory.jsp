<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key='workflowHistory.limit.title' /></title>
<meta name="heading"
	content="<fmt:message key='workflowHistory.limit.heading'/>" />
</head>

<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='workflowHistory.limit.message.list' />
			</div>

			<div class="box-body table-responsive">
				<table id="tableList" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th><fmt:message key="workflow.taskId" /></th>
							<th><fmt:message key="workflow.taskName" /></th>
							<th><fmt:message key="workflow.assignee" /></th>
							<th><fmt:message key="workflow.taskCompleteDate" /></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="task" items="${taskDtos}">
							<tr>
								<td>${task.taskId}</td>
								<td>${task.taskName}</td>
								<td>${task.assignee}</td>
								<td>${task.taskCompleteDate}</td>
								<td><a
									href="<c:url value='/workflow/history/limit/detail/${task.taskId}'/>"><fmt:message
											key="workflowHistory.limit.detail" /></a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="box-footer">
				<div class="form-group">
					<div class="row">
						<div class="col-xs-12 ">
							<a href="<c:url value='/workflow/history/'/>"
								class="btn btn-primary"><fmt:message key="button.back" /></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>