<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key='workflowHistory.title' /></title>
<meta name="heading" content="<fmt:message key='workflowHistory.heading'/>" />
</head>

<div class="row">
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='workflowHistory.message.list'/></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><fmt:message key="workflow.executionId" /></th>
								<th><fmt:message key="workflow.loanApplicationId" /></th>
								<th><fmt:message key="workflow.startDate" /></th>
								<th><fmt:message key="workflow.status" /></th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="history" items="${historyDtos}">
								<tr>
									<td>${history.executionId}</td>
									<td>${history.objectId}</td>
									<td>${history.startDate}</td>
									<td>${history.status}</td>
									<td><a href="<c:url value='/workflow/history/detail/${history.executionId}'/>" 
										title="See Workflow History Detail"><i class="fa fa-eye"></i></a>
										<a href="<c:url value='/workflow/history/limit/${history.executionId}'/>" 
										title="See Workflow Limit History"><i class="fa fa-eye"></i></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
			</div>
		</div>
	</div>
</div>