<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key='workflowTask.title' /></title>
<meta name="heading" content="<fmt:message key='workflowTask.heading'/>" />
</head>

<div class="row">
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='workflowTask.message.list'/></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><fmt:message key="workflow.taskId" /></th>
								<th><fmt:message key="workflow.taskName" /></th>
								<th><fmt:message key="workflow.loanApplicationId" /></th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="task" items="${taskList}">
								<tr>
									<td>${task.taskId}</td>
									<td>${task.taskName}</td>
									<td>${task.loanApplicationId}</td>
									<td><a href="<c:url value='/workflow/task/${task.taskId}/${task.taskKey}/${task.taskExecutionId}'/>" >
											<fmt:message key="workflowTask.complete" /></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
			</div>
		</div>
	</div>
</div>