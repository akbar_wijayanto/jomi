<%@ include file="/common/taglibs.jsp"%>
<head>
	<title><fmt:message key="coreBranchType.title" /></title>
	<meta name="heading"
		content="<fmt:message key='coreBranchType.heading'/>" />
	<script type="text/javascript"
		src="<c:url value="/resources/assets/custom/core.branch.type.js"/>"></script>
</head>

<div class="row">
	<div class="col-xs-12">
		<div id="breadcumbTitle"><fmt:message key='coreBranchType.message.add'/></div>
		<spring:bind path="coreBranchType.*">
			<c:if test="${not empty status.errorMessages}">
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
                	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<c:forEach var="error" items="${status.errorMessages}">
						<c:out value="${error}" escapeXml="false" />
						<br />
					</c:forEach>
				</div>
			</c:if>
		</spring:bind> 
		
		<div class="box">
			
			<div class="box-body">
				<spring:url var="action" value='/task/sample/approval/${group}/${coreBranchType.id}' />
				<form:form method="post" commandName="coreBranchType"
					action="${action}" id="coreBranchType" class="form-horizontal">
					<div class="row">
						<div class="col-xs-12">
							<div class="row">
								<!-- Col 1 -->
								<div class="col-xs-6">
									
									<div class="form-group">
										<div class="col-xs-4 col-md-3">
											<form:label path="id" cssClass="control-label pull-right"
												cssErrorClass="control-label pull-right error">
												<fmt:message key="coreBranchType.name" />
											</form:label>
										</div>
										<div class="col-xs-8 col-md-6">
											<form:input path="id" id="id"
												cssClass="form-control validate[required,maxSize[3]]"
												maxlength="3"
												cssErrorClass="form-control has-error validate[required,maxSize[3]]" 
												readonly="readonly"/>
											<form:errors path="id" cssClass="has-error" />
										</div>
									</div>
									
									<div class="form-group">
										<div class="col-xs-4 col-md-3">
											<form:label path="name" cssClass="control-label pull-right"
												cssErrorClass="control-label pull-right error">
												<fmt:message key="coreBranchType.name" />
											</form:label>
										</div>
										<div class="col-xs-8 col-md-6">
											<form:input path="name" id="name"
												cssClass="form-control validate[required,maxSize[50]]"
												maxlength="50"
												cssErrorClass="form-control has-error validate[required,maxSize[50]]" 
												readonly="readonly"/>
											<form:errors path="name" cssClass="has-error" />
										</div>
									</div>
								</div>
								
								<!-- Col 2 -->
								<div class="col-xs-6">
									<div class="form-group">
										<div class="col-xs-4 col-md-3">
											<form:label path="level" cssClass="control-label pull-right"
												cssErrorClass="control-label pull-right error">
												<fmt:message key="coreBranchType.level" />
											</form:label>
										</div>
										<div class="col-xs-8 col-md-6">
											<form:select path="level" id="level" cssClass="form-control" 
											cssErrorClass="form-control has-error" readonly="readonly">
												<form:options items="${levels}" itemValue="levelValue"
													itemLabel="levelValue" />
											</form:select>
											<form:errors path="level" cssClass="has-error" />
										</div>
									</div>
								</div>
							</div>
							
							<!-- Button Group -->
							<div class="box-footer">
							<div class="row">
								<div class="col-xs-12">
									<div>
										<input type="submit" name="approve" value="<fmt:message key="button.approve"/>" class="btn btn-primary"/>
										<input type="submit" name="reject" value="<fmt:message key="button.reject"/>" class="btn btn-primary"/>
										<input type="submit" name="rollback" value="<fmt:message key="button.rollback"/>" class="btn btn-primary"/>								
									</div>
								</div>
							</div>
							</div>
						</div>
					</div>
					</form:form>
			</div>
		</div>
	</div>
</div>






