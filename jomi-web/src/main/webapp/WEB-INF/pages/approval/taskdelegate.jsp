<%@ include file="/common/taglibs.jsp"%>
<head>
	<title><fmt:message key="approval.delegate.title" /></title>
	<meta name="heading"
		content="<fmt:message key='approval.delegate.heading'/>" />
</head>

<div class="row">
	<div class="col-xs-12">
		<div id="breadcumbTitle"><fmt:message key='approval.delegate.message'/></div>
<%-- 		<spring:bind path="coreBranchType.*"> --%>
<%-- 			<c:if test="${not empty status.errorMessages}"> --%>
<!-- 				<div class="alert alert-danger alert-dismissable"> -->
<!-- 					<i class="fa fa-ban"></i> -->
<!--                 	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<%-- 					<c:forEach var="error" items="${status.errorMessages}"> --%>
<%-- 						<c:out value="${error}" escapeXml="false" /> --%>
<!-- 						<br /> -->
<%-- 					</c:forEach> --%>
<!-- 				</div> -->
<%-- 			</c:if> --%>
<%-- 		</spring:bind>  --%>
		
		<div class="box">
			
			<div class="box-body">
				<spring:url var="action" value='/workflow/task/admin/delegate/${taskDto.taskId}' />
				<form:form commandName="taskDto" method="post"
					action="${action}" id="taskDto" class="form-horizontal">
					<div class="row">
						<div class="col-xs-12">
									
									<div class="form-group">
										<div class="col-xs-4 col-md-3">
											<form:label path="taskId" cssClass="control-label pull-right"
												cssErrorClass="control-label pull-right error">
												<fmt:message key="approval.delegate.taskId" />
											</form:label>
										</div>
										<div class="col-xs-8 col-md-6">
											<form:input path="taskId" id="taskId"
												cssClass="form-control"
												readonly="readonly"
												cssErrorClass="form-control has-error" />
											<form:errors path="taskId" cssClass="has-error" />
										</div>
									</div>
									
									<div class="form-group">
										<div class="col-xs-4 col-md-3">
											<form:label path="taskName" cssClass="control-label pull-right"
												cssErrorClass="control-label pull-right error">
												<fmt:message key="approval.delegate.taskName" />
											</form:label>
										</div>
										<div class="col-xs-8 col-md-6">
											<form:input path="taskName" id="taskName"
												cssClass="form-control"
												readonly="readonly"
												cssErrorClass="form-control has-error" />
											<form:errors path="taskName" cssClass="has-error" />
										</div>
									</div>
								
								
									<div class="form-group">
										<div class="col-xs-4 col-md-3">
											<form:label path="assignee"
												for="assignee"
												class="control-label pull-right">
												<fmt:message key="approval.assignee" />
											</form:label>
										</div>
										<div class="col-xs-8 col-md-6">
											<form:select path="assignee" id="assignee" cssClass="form-control" 
											cssErrorClass="form-control has-error" name="assignee" >
												<form:options items="${delegatedUsers}" itemLabel="username" itemValue="username"/>
											</form:select>
										</div>
									</div>
							
							<!-- Button Group -->
							<div class="row">
								<div class="col-xs-12">
									<div class="pull-right">
										<input type="submit" class="btn btn-primary" name="save" id="save"
										value="<fmt:message key="button.delegate"/>" /> <input type="submit"
										class="btn btn-primary" name="cancel" id="cancel"
										value="<fmt:message key="button.cancel"/>" />
									</div>
								</div>
							</div>
							</div>
							</div>
				</form:form>
			
		</div>
	</div>
</div>
</div>