<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="workflow.limitHistory.title" /></title>
<meta name="heading"
	content="<fmt:message key='workflow.limitHistory.heading'/>" />
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='workflow.limitHistory.message.view' />
			</div>

			<div class="box-body">
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<label for="loanAppliactionId"
								class="control-label pull-right">
								<fmt:message key="trxLoanApplication.loanAppliactionId" />
							</label>
						</div>
						<div class="col-xs-8 col-md-6">
							<input type="text" id="loanAppliactionId"
								readonly="readonly" class="form-control" value="${trxLoanApplication}" />
						</div>
					</div>
					
					<div class="table-responsive">
						<table id="tableList" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th><fmt:message key='trxLimit.id' /></th>
									<th><fmt:message key='trxLimit.customer' /></th>
									<th><fmt:message key='trxLimit.currency' /></th>
									<th><fmt:message key='trxLimit.onlinelimitdate' /></th>
									<th><fmt:message key='trxLimit.expirydate' /></th>
									<th><fmt:message key='trxLimit.originallimit' /></th>
									<th><fmt:message key='trxLimit.osamount' /></th>
									<th><fmt:message key='trxLimit.availableamount' /></th>
									<th><fmt:message key='trxLimit.accountofficer' /></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="limit" items="${trxLimits}">
									<tr>
	
										<td>${limit.userDefinedId}</td>
										<td>${limit.customer.id}</td>
										<td>${limit.currency.id}</td>
										<td><fmt:formatDate pattern="${datePattern}"
												value="${limit.onlineLimitDate}" /></td>
										<td><fmt:formatDate pattern="${datePattern}"
												value="${limit.expiryDate}" /></td>
										<td><fmt:formatNumber pattern="${curr}"
												value="${limit.originalLimit}" /></td>
										<td><fmt:formatNumber pattern="${curr}"
												value="${limit.osAmount}" /></td>
										<td><fmt:formatNumber pattern="${curr}"
												value="${limit.availableAmount}" /></td>
										<td>${limit.accountOfficer.id} -
											${limit.accountOfficer.aoName}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					
				</div>

				<div class="box-footer">
					<div class="form-group">
						<div class="row">
							<div class="col-xs-12 ">
								<a href="<c:url value='/workflow/history/limit/${executionId}'/>" class="btn btn-primary"><fmt:message key="button.back"/></a>
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>