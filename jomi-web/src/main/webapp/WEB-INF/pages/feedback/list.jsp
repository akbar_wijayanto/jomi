<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key='feedback.title' /></title>
<meta name="heading" content="<fmt:message key='feedback.heading'/>" />
<script type="text/javascript"
	src="<c:url value='/resources/assets/custom/feedback.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key="feedback.message" /></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
							<th><fmt:message key='feedback.description' /></th>
							<th><fmt:message key='feedback.rate' /></th>
						</tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="feedback" items="${feedbacks}">
			            	<tr>
				            	<td>${feedback.description}</td>
				            	<td>
				            		<c:forEach var="i" begin="1" end="${feedback.rate}">
				            			<a href="#"><i class="fa fa-star"></i></a>&nbsp;
				            		</c:forEach>
				            	</td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
		</div>
    </div>
</div>