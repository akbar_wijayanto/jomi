<%@ include file="/common/taglibs.jsp"%>
<%@ page language="java" isErrorPage="true"%>

<head>
	<title><fmt:message key="500.heading" /></title>
	<meta name="heading" content="<fmt:message key='500.heading'/>" />
	<meta name="decorator" content="failure">
	<script type="text/javascript">
		function goBack(){
			window.history.back();
		}
	</script>
</head>

<!-- Error info area -->
<div class="error-page">
<!-- 	<h2 class="headline text-info">500</h2> -->
	<div class="error-content">
		<h3>
			<i class="fa fa-warning text-yellow"></i> <fmt:message key="500.title"/>
		</h3>
		<p style="margin-bottom: 55px;">
			<fmt:message key="500.message">
				<fmt:param><c:url value='/mainMenu' /></fmt:param>
			</fmt:message>
		</p>
	</div>
	<!-- /.error-content -->
	<c:if test="${not empty errors}">
		<div class="alert alert-danger">
			<i class="fa fa-ban"></i>
			<c:forEach var="error" items="${errors}">
				<c:out value="${error}" />
				<br />
			</c:forEach>
			<c:remove var="errors" scope="session" />
		</div>
	</c:if>
	
</div>
<!-- /.error-page -->
