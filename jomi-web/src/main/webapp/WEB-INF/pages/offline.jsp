<%@ include file="/common/taglibs.jsp"%>


<head>
    <title><fmt:message key="500.title"/></title>
    <meta name="heading" content="<fmt:message key='500.title'/>"/>
</head>

<!-- Error info area -->
<div class="wrapper">
    <div class="errorPage">
        <h2 class="red errorTitle"><span>Something went wrong here</span></h2>
        <h1>500</h1>
        <span class="bubbles"></span>

        <div class="backToDash">
            <c:if test="${not empty errors}">
                <c:forEach var="error" items="${errors}">
                    <c:out value="${error}"/><br />
                </c:forEach>
                <c:remove var="errors" scope="session"/>
            </c:if>
            <fmt:message key="500.message"></fmt:message>
        </div>
    </div>
</div>
