<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreTask.title" /></title>
<meta name="heading" content="<fmt:message key='coreTask.heading'/>" />
<script type="text/javascript"
	src="<c:url value="/scripts/custom/core.task.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div id="breadcumbTitle">
			<fmt:message key='coreTask.message.view' />
		</div>
		<spring:bind path="coreTask.*">
			<c:if test="${not empty status.errorMessages}">
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">&times;</button>
					<c:forEach var="error" items="${status.errorMessages}">
						<c:out value="${error}" escapeXml="false" />
						<br />
					</c:forEach>
				</div>
			</c:if>
		</spring:bind>

		<div class="box">
			<spring:url var="action" value='/core/task/detail' />
			<form:form commandName="coreTask" method="post" action="${action}"
				id="coreTask" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="taskNumber" for="taskNumber"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreTask.taskNumber" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="taskNumber" id="taskNumber" readonly="true"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="taskNumber" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="feature" for="feature"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreTask.feature" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="feature" id="feature" readonly="true"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="feature" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="description" for="description"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreTask.description" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="description" id="description" readonly="true"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="description" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="priority" path="priority" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreTask.priority" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:select path="priority" id="priority" cssClass="form-control validate[required]" disabled="true">
								<form:option value="" label="--- Select ---"></form:option>
								<c:forEach items="${priorityEnums}" var="priorityEnum">
									<form:option value="${priorityEnum}">${priorityEnum}</form:option>
								</c:forEach>
							</form:select>	
							<form:errors path="priority" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="complexity" path="complexity" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreTask.complexity" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:select path="complexity" id="complexity" cssClass="form-control validate[required]" disabled="true">
								<form:option value="" label="--- Select ---"></form:option>
								<c:forEach items="${complexityEnums}" var="complexityEnum">
									<form:option value="${complexityEnum}">${complexityEnum}</form:option>
								</c:forEach>
							</form:select>	
							<form:errors path="complexity" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								Man Hour
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="manHour" id="manHour"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" placeholder="Man Hour" readonly="true"/>
							<form:errors path="manHour" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="coreProject.name" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								Project
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="coreProject.name" id="coreProject.name"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" placeholder="Project Name" readonly="true"/>
							<form:errors path="coreProject.name" cssClass="has-error" />
						</div>
					</div>
				</div>

				<div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-12">
						<div class="pull-right">
								<a href="<c:url value='/core/task' />" class="btn btn-primary"
									title="<fmt:message key="coreTask.back"/>"><fmt:message
										key="coreTask.back" /></a>
										</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>