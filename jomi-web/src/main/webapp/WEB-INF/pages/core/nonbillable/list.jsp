<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key='nonBillable.title' /></title>
<meta name="heading" content="<fmt:message key='nonBillable.heading'/>" />
<script type="text/javascript"
	src="<c:url value='/resources/assets/custom/core.nonBillable.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key="nonBillable.message" /></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
			            	<th><fmt:message key='nonBillable.nonBillableMandaysTypeEnum' /></th>
							<th><fmt:message key='nonBillable.nonBillableTitle' /></th>
							<th><fmt:message key='nonBillable.description' /></th>
							<th><fmt:message key='nonBillable.manHour' /></th>
							<th><fmt:message key='nonBillable.coreUser' /></th>
						</tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="nonBillable" items="${nonBillables}">
			            	<tr>
								<td>${nonBillable.nonBillableMandaysTypeEnum}</td>
								<td>${nonBillable.title}</td>
				            	<td>${nonBillable.description}</td>
				            	<td>${nonBillable.manHour}</td>
				            	<td>${nonBillable.coreUser.firstName} ${nonBillable.coreUser.lastName}</td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
		</div>
    </div>
</div>