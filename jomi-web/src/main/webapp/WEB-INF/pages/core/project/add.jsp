<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreProject.title" /></title>
<meta name="heading" content="<fmt:message key='coreProject.heading'/>" />
<script type="text/javascript"
	src="<c:url value="/scripts/custom/core.project.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div id="breadcumbTitle">
			<fmt:message key='coreProject.message.add' />
		</div>
<%-- 		<spring:bind path="coreRole.*"> --%>
<%-- 			<c:if test="${not empty status.errorMessages}"> --%>
<!-- 				<div class="alert alert-danger alert-dismissable"> -->
<!-- 					<i class="fa fa-ban"></i> -->
<!-- 					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<%-- 					<c:forEach var="error" items="${status.errorMessages}"> --%>
<%-- 						<c:out value="${error}" escapeXml="false" /> --%>
<!-- 						<br /> -->
<%-- 					</c:forEach> --%>
<!-- 				</div> -->
<%-- 			</c:if> --%>
<%-- 		</spring:bind> --%>

		<div class="box">
			<spring:url var="action" value='/core/project/add' />
			<form:form commandName="coreProject" method="post" action="${action}" id="coreProject" class="form-horizontal">
				<div class="box-body">					
					<input type="hidden" name="from" value="<c:out value="${param.from}"/>" />
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<%-- <form:label path="name" for="name" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="Role.name" />
							</form:label> --%>
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								Project Number <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="projectNumber" id="projectNumber"
								cssClass="form-control validate[required] "
								cssErrorClass="form-control has-error validate[required]" placeholder="Project Number" />
							<form:errors path="projectNumber" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<%-- <form:label path="name" for="name" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="Role.name" />
							</form:label> --%>
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								Name <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="name" id="name"
								cssClass="form-control validate[required] "
								cssErrorClass="form-control has-error validate[required]" placeholder="Project Name" />
							<form:errors path="name" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<%-- <form:label path="description" for="description" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="Role.description" />
							</form:label> --%>
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								Description <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="description" id="description"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" placeholder="Project Description"/>
							<form:errors path="description" cssClass="has-error" />
						</div>
					</div>
				</div>
				
				<div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-9">
							<div class="pull-right">
								<input type="submit" class="btn btn-primary" name="save" id="save" value="<fmt:message key="button.add"/>" /> 
								<input type="submit" class="btn btn-primary" name="cancel" id="cancel" value="<fmt:message key="button.cancel"/>" />
							</div>
		            	</div>
	            	</div>
                </div>                
			</form:form>
		</div>
	</div>
</div>