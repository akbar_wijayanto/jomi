<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="projectUserReport.title" /></title>
<meta name="heading" content="<fmt:message key='projectUserReport.heading'/>" />
<script type="text/javascript"
	src="<c:url value='/scripts/custom/core.project.user.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='projectUserReport.message.list' />
			</div>

			<spring:url var="action" value='/core/project/user/report' />
			<form:form commandName="coreProject" name="coreProject" action="${action}" id="coreTask" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<label for="projectId" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="coreTask.userId" />
							</label>
						</div>
						<div class="col-xs-4 col-md-3">
							<div class="input-group">
								<input type="hidden" name="userId" id="userId" value="${coreUser.id}"/>
								<input type="text" class="form-control" readonly
									id="coreUser_"
									value="${coreUser.firstName} - ${coreUser.lastName}">
	
								<span class="input-group-btn">
									<button style="height: 30px;" class="btn btn-info btn-flat"
										type="button" id="btnCoreUser" data-toggle="modal"
										data-target="#userList">
										<i class="fa fa-search"></i>
									</button>
								</span>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-12">
								<div class="pull-right">
									<input type="submit" class="btn btn-primary" name="saveTask"
										id="saveTask" value="<fmt:message key="button.submit"/>" />
									<input type="button" class="btn btn-primary"
										name="taskreportdownloadpdf" id="taskreportdownloadpdf"
										value="<fmt:message key="button.downloadPdf"/>" /> 
									<input type="button" class="btn btn-primary"
										name="taskreportdownloadxls" id="taskreportdownloadxls"
										value="<fmt:message key="button.downloadExcel"/>" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</form:form>
			
			<div class="box-body table-responsive">
				<table id="tableList" class="table table-bordered table-striped" style="width: 100%;">
					<thead>
						<tr>
							<th><fmt:message key='coreProject.projectNumber' /></th>
							<th><fmt:message key='coreProject.name' /></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="project" items="${userProjects}">
							<tr>

								<td>${project.projectNumber}</td>
								<td>${project.projectName}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="userList" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					<fmt:message key="coreUser.list" />
				</h4>
			</div>
			<div class="modal-body">
				<table id="coreUserTabel"
					class="table dataTable table-bordered table-striped">
					<thead>
						<tr>
							<th><fmt:message key='coreUser.name' /></th>
							<th width="80px">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="user" items="${coreUsers}">
							<tr>
								<td>${user.firstName} ${user.lastName}</td>
								<td><button type="button" class="btn btn-primary"
										onclick="doCopy('userId','coreUser_','userList','${user.id}','${user.firstName}','${user.lastName}');"
										title="check" class="btn btn-info btn-flat">Choose</button></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>