<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="projectStatusReport.title" /></title>
<meta name="heading" content="<fmt:message key='projectStatusReport.heading'/>" />
<script type="text/javascript"
	src="<c:url value='/scripts/custom/core.project.status.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='projectStatusReport.message.list' />
			</div>

			<spring:url var="action" value='/core/project/status/report' />
			<form:form commandName="coreProject" name="coreProject" action="${action}" id="coreTask" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<label for="projectId" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="coreTask.projectId" />
							</label>
						</div>
						<div class="col-xs-4 col-md-3">
							<div class="input-group">
								<input type="hidden" name="projectId" id="projectId" value="${coreProject.id}"/>
								<input type="text" class="form-control" readonly
									id="coreProject_"
									value="${coreProject.projectNumber} - ${coreProject.name}">
	
								<span class="input-group-btn">
									<button style="height: 30px;" class="btn btn-info btn-flat"
										type="button" id="btnCoreProject" data-toggle="modal"
										data-target="#projectList">
										<i class="fa fa-search"></i>
									</button>
								</span>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-12">
								<div class="pull-right">
									<input type="submit" class="btn btn-primary" name="saveTask"
										id="saveTask" value="<fmt:message key="button.submit"/>" />
									<input type="button" class="btn btn-primary"
										name="taskreportdownloadpdf" id="taskreportdownloadpdf"
										value="<fmt:message key="button.downloadPdf"/>" /> 
									<input type="button" class="btn btn-primary"
										name="taskreportdownloadxls" id="taskreportdownloadxls"
										value="<fmt:message key="button.downloadExcel"/>" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</form:form>
			<div class="row">
				<div class="box-body">
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<label for="projectNumber" class="control-label"
								class="control-label"> <fmt:message
									key="coreProject.projectNumber" />
							</label>
						</div>
						<div class="col-xs-4 col-md-3">
							<label for="projectNumber" class="control-label"
								class="control-label"> ${projectStatus.projectNumber}
							</label>
						</div>
					</div>
				</div>
				<div class="box-body">
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<label for="projectName" class="control-label"
								class="control-label"> <fmt:message
									key="coreProject.name" />
							</label>
						</div>
						<div class="col-xs-4 col-md-3">
							<label for="projectName" class="control-label"
								class="control-label"> ${projectStatus.projectName}
							</label>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-4">
					<div class="box-body table-responsive">
						<h3 class="box-title">
							<fmt:message key='coreTask.priority' />
						</h3>
						<table class="table table-bordered table-striped" style="width: 100%;">
							<tr>
								<td><fmt:message key='coreTask.low' /></td>
								<td>${projectStatus.countLowP}</td>
							</tr>
							<tr>
								<td><fmt:message key='coreTask.medium' /></td>
								<td>${projectStatus.countMediumP}</td>
							</tr>
							<tr>
								<td><fmt:message key='coreTask.high' /></td>
								<td>${projectStatus.countHighP}</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="col-md-4">
					<div class="box-body table-responsive">
						<h3 class="box-title">
							<fmt:message key='coreTask.complexity' />
						</h3>
						<table class="table table-bordered table-striped" style="width: 100%;">
							<tr>
								<td><fmt:message key='coreTask.cosmetic' /></td>
								<td>${projectStatus.countCosmeticC}</td>
							</tr>
							<tr>
								<td><fmt:message key='coreTask.minor' /></td>
								<td>${projectStatus.countMinorC}</td>
							</tr>
							<tr>
								<td><fmt:message key='coreTask.medium' /></td>
								<td>${projectStatus.countMediumC}</td>
							</tr>
							<tr>
								<td><fmt:message key='coreTask.major' /></td>
								<td>${projectStatus.countMajorC}</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="col-md-4">
					<div class="box-body table-responsive">
						<h3 class="box-title">
							<fmt:message key='coreTask.status' />
						</h3>
						<table class="table table-bordered table-striped" style="width: 100%;">
							<tr>
								<td><fmt:message key='coreTask.open' /></td>
								<td>${projectStatus.countOpenS}</td>
							</tr>
							<tr>
								<td><fmt:message key='coreTask.inProgress' /></td>
								<td>${projectStatus.countInProgressS}</td>
							</tr>
							<tr>
								<td><fmt:message key='coreTask.done' /></td>
								<td>${projectStatus.countDoneS}</td>
							</tr>
							<tr>
								<td><fmt:message key='coreTask.ready' /></td>
								<td>${projectStatus.countReadyToTestS}</td>
							</tr>
							<tr>
								<td><fmt:message key='coreTask.closed' /></td>
								<td>${projectStatus.countClosedS}</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			
			<%-- <div class="box-body table-responsive">
				<table id="tableList" class="table table-bordered table-striped" style="width: 100%;">
					<thead>
						<tr>
							<th><fmt:message key='coreTask.low' /></th>
							<th><fmt:message key='coreTask.countLow' /></th>
							<th><fmt:message key='coreTask.medium' /></th>
							<th><fmt:message key='coreTask.countMedium' /></th>
							<th><fmt:message key='coreTask.high' /></th>
							<th><fmt:message key='coreTask.countHigh' /></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="task" items="${projectStatus}">
							<tr>

								<td>${task.lowP}</td>
								<td>${task.countLowP}</td>
								<td>${task.mediumP}</td>
								<td>${task.countMediumP}</td>
								<td>${task.highP}</td>
								<td>${task.countHighP}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div> --%>
		</div>
	</div>
</div>
<div class="modal fade" id="projectList" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					<fmt:message key="coreProject.list" />
				</h4>
			</div>
			<div class="modal-body">
				<table id="coreProjectTabel"
					class="table dataTable table-bordered table-striped">
					<thead>
						<tr>
							<th><fmt:message key='coreProject.name' /></th>
							<th width="80px">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="project" items="${coreProjects}">
							<tr>
								<td>${project.projectNumber} ${project.name}</td>
								<td><button type="button" class="btn btn-primary"
										onclick="doCopy('projectId','coreProject_','projectList','${project.id}','${project.projectNumber}','${project.name}');"
										title="check" class="btn btn-info btn-flat">Choose</button></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>