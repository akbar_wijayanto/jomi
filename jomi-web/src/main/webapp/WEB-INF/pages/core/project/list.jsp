<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key='coreProject.title' /></title>
<meta name="heading" content="<fmt:message key='coreProject.heading'/>" />
<script type="text/javascript"
	src="<c:url value='/resources/assets/custom/core.project.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">	 
<%-- 	    <security:authorize access="hasproject('CORE:project:INPUT:*')"> --%>
			<a href="<c:url value='/core/project/add' />" id="addConfigSystemLink" 
				class="btn btn-primary pull-right">
				<i class="fa fa-plus"></i> 
				<fmt:message key="coreProject.message.add" /></a>
<%-- 		</security:authorize> --%>
	</div>
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key="coreProject.message" /></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
<%-- 							<th><fmt:message key='coreProject.id' /></th> --%>
							<th><fmt:message key='coreProject.projectNumber' /></th>
							<th><fmt:message key='coreProject.name' /></th>
							<th><fmt:message key='coreProject.description' /></th>
							<th><fmt:message key='coreProject.status' /></th>
							<th width="20%">&nbsp;</th>
						</tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="coreProject" items="${coreProjects}">
			            	<tr>
<%-- 				            	<td>${coreProject.id}</td> --%>
								<td>${coreProject.projectNumber}</td>
								<td>${coreProject.name}</td>
				            	<td>${coreProject.description}</td>
				            	<td>${coreProject.status}</td>
				            	<td>
<%-- 								<security:authorize access="hasproject('CORE:project:READ:*')"> --%>
				            		<a href="<c:url value='/core/project/detail/${coreProject.id}' />" title="View">
				            		<i class="fa fa-eye"></i></a>&nbsp;
<%-- 				            	</security:authorize> --%>
<%-- 				            	<security:authorize access="hasproject('CORE:project:EDIT:*')"> --%>
				            		<a href="<c:url value='/core/project/edit/${coreProject.id}' />" title="Edit">
				            		<i class="fa fa-edit"></i></a>&nbsp;
<%-- 				            	</security:authorize> --%>
									<%-- <security:authorize access="hasRole('CORE:ROLE:INPUT:*')" --%>
				            		<a href="<c:url value='/core/project/assignmember/${coreProject.id}' />" title="Assign Member">
				            		<i class="fa fa-bars"></i></a>&nbsp;
				            		<%-- </security:authorize> --%>
<%-- 				            	<security:authorize access="hasproject('CORE:project:DELETE:*')"> --%>
				            		<a href="<c:url value='/core/project/delete/${coreProject.id}' />" title="Delete">
				            		<i class="fa fa-trash-o"></i></a>&nbsp;
<%-- 				            	</security:authorize> --%>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
		</div>
    </div>
</div>