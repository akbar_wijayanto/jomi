<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreProject.title" /></title>
<meta name="heading" content="<fmt:message key='coreProject.heading'/>" />
<script type="text/javascript"
	src="<c:url value="/scripts/custom/core.project.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div id="breadcumbTitle">
			<fmt:message key='coreProject.message.view' />
		</div>
		<spring:bind path="coreProject.*">
			<c:if test="${not empty status.errorMessages}">
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">&times;</button>
					<c:forEach var="error" items="${status.errorMessages}">
						<c:out value="${error}" escapeXml="false" />
						<br />
					</c:forEach>
				</div>
			</c:if>
		</spring:bind>

		<div class="box">
			<spring:url var="action" value='/core/project/detail' />
			<form:form commandName="coreProject" method="post" action="${action}"
				id="coreProject" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />

					<%-- <div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="id" for="id"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreProject.id" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="id" id="id" readonly="true"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="id" cssClass="has-error" />
						</div>
					</div> --%>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="projectNumber" for="projectNumber"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreProject.projectNumber" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="projectNumber" id="projectNumber" readonly="true"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="projectNumber" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="name" for="name"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreProject.name" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="name" id="name" readonly="true"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="name" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="description" for="description"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreProject.description" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="description" id="description" readonly="true"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="description" cssClass="has-error" />
						</div>
					</div>
				</div>

				<div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-12">
						<div class="pull-right">
								<a href="<c:url value='/core/project' />" class="btn btn-primary"
									title="<fmt:message key="coreProject.back"/>"><fmt:message
										key="coreProject.back" /></a>
										</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>