<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="coreMenu.title"/></title>
    <meta name="heading" content="<fmt:message key='coreMenu.heading'/>"/>
</head>
<div class="row">
<!--     <div class="col-xs-12">	  -->
<%-- 		<security:authorize access="hasRole('CORE:MENU:INPUT:*')"> --%>
<%-- 	       	<a href="<c:url value='/core/menu/add' />" class="btn btn-primary pull-right"> --%>
<!-- 	       		<i class="fa fa-plus"></i> -->
<%-- 	       		<fmt:message key="coreMenu.message.add" /></a> --%>
<%-- 	    </security:authorize> --%>
<!-- 	</div> -->
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='coreMenu.message.list'/></div>
	       	
	        <div class="box-body table-responsive">
        	<table id="tableList" class="table table-bordered table-striped">
                <thead>
                    <tr>
                    	<th style="vertical-align: inherit;"><fmt:message key='coreMenu.orderMenu'/></th>
                        <th style="vertical-align: inherit;"><fmt:message key='coreMenu.description'/></th>
                        <th style="vertical-align: inherit;"><fmt:message key='coreMenu.name'/></th>
                        <th style="vertical-align: inherit;"><fmt:message key='coreMenu.parentId'/></th>
                        <th style="vertical-align: inherit;"><fmt:message key='coreMenu.permalinks'/></th>
                        <th style="vertical-align: inherit;"><fmt:message key='coreMenu.type'/></th>
                       <%--  <th><fmt:message key='coreMenu.system'/></th> --%>
                        <th style="vertical-align: inherit; width: 45px;"><fmt:message key='coreMenu.status'/></th>
                        <th style="vertical-align: inherit; width:40px !important;">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="menu" items="${coreMenus}">
                        <tr>
                        	<td>${menu.orderMenu}</td>
                            <td>${menu.description}</td>
                            <td>${menu.name}</td>
                            <td>${menu.parentId}</td>
                            <td>${menu.permalinks}</td>
                            <td>${menu.type}</td>
                           <%--  <td>${menu.system}</td> --%>
                            <td>${menu.status}</td>
                            <td>
                            	<security:authorize access="hasRole('CORE:MENU:READ:*')">
                            		<a style="margin-right:9px;" href="<c:url value='/core/menu/view/${menu.id}' />" title="View"><i class="fa fa-eye"></i></a>
                            	</security:authorize>
                            	<security:authorize access="hasRole('CORE:MENU:EDIT:*')">
                            		<a href="<c:url value='/core/menu/edit/${menu.id}' />" title="Edit"><i class="fa fa-edit"></i></a>
                            	</security:authorize>
<%--                             	<a href="<c:url value='/core/menu/delete/${menu.id}' />" title="Delete"><i class="fa fa-trash-o"></i></a> --%>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        	</div>
		</div>
    </div>
</div>