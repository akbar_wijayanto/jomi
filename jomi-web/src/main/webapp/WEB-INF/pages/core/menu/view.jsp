<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreMenu.title"/></title>
    <meta name="heading" content="<fmt:message key='coreMenu.heading'/>"/>
    <script type="text/javascript" src="<c:url value="/scripts/custom/core.menu.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreMenu.message.view' />
			</div>
        <spring:url var = "action" value='/core/menu/edit/${coreMenu.id}' />
        <form:form commandName="coreMenu" method="post" action="${action}"  id="coreMenu" class="form-horizontal">
			<div class="box-body">
	            <input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>
       			
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="description" for="description" cssClass="control-label pull-right"
							cssErrorClass="control-label pull-right">
							<fmt:message key="Menu.description" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
						<form:input path="description" id="description" readonly="true"
							cssClass="form-control validate[required, maxSize[50]]" maxlength="50"
							cssErrorClass="form-control has-error validate[required, maxSize[50]]" />
						<form:errors path="description" cssClass="has-error" />
					</div>
				</div>
	
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="name" for="name" cssClass="control-label pull-right"
							cssErrorClass="control-label pull-right">
							<fmt:message key="Menu.name" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
						<form:input path="name" id="name" readonly="true"
							cssClass="form-control validate[required, maxSize[50]]" maxlength="50"
							cssErrorClass="form-control has-error validate[required, maxSize[50]]" />
						<form:errors path="name" cssClass="has-error" />
					</div>
				</div>
	
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="orderMenu" for="orderMenu" cssClass="control-label pull-right"
							cssErrorClass="control-label pull-right">
							<fmt:message key="Menu.orderMenu" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
						<form:input path="orderMenu" id="orderMenu" readonly="true"
							cssClass="form-control validate[required, maxSize[50]]" maxlength="50"
							cssErrorClass="form-control has-error validate[required, maxSize[50]]" />
						<form:errors path="orderMenu" cssClass="has-error" />
					</div>
				</div>
	            
	            <div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="type" for="type" cssClass="control-label pull-right"
							cssErrorClass="control-label pull-right">
							<fmt:message key="coreMenu.type" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
					<form:input path="type" id="type"
								cssClass="form-control"
								maxlength="255"
								cssErrorClass="has-error form-control" 
								readonly="true"/>
							<form:errors path="type" cssClass="has-error" />
	  				  <%--   <form:select path="type" disabled="disabled" id="type" items="${menus}" itemValue="menuValue" itemLabel="menuType" cssClass="form-control validate[required]" cssErrorClass="form-control  has-error validate[required]" />
	                     <form:errors path="type" cssClass="has-error"/>--%>
					</div>
				</div>
				
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="parentId" for="parentId" cssClass="control-label pull-right"
							cssErrorClass="control-label pull-right">
							<fmt:message key="coreMenu.parentId" />
						</form:label>
					</div>
					
					<div class="col-xs-8 col-md-6">
							<form:input path="parentId" id="parentId"
								cssClass="form-control"
								maxlength="255"
								cssErrorClass="has-error form-control" 
								readonly="true"/>
							<form:errors path="parentId" cssClass="has-error" />
						</div>
					
					<%-- <div class="col-xs-8 col-md-6">
	                    <form:select path="parentId" disabled="disabled" id="parentId"  cssClass="form-control validate[required]" cssErrorClass="form-control has-error validate[required]">
	                        <form:option value="0" selected="${coreMenu.getParentId() == 0 ? true : false}" label="--- Select ---"/>
	                        <form:options items="${coreMenus}" itemValue="id" itemLabel="name" />
								<c:forEach items="${coreMenus }" var="item">
									<form:option value="${item.id }">${item.id } - ${item.description }</form:option>
								</c:forEach>
	                    </form:select>
	                    <form:errors path="parentId" cssClass="has-error"/>
					</div> --%>
				</div>
				
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="permalinks" for="permalinks" cssClass="control-label pull-right"
							cssErrorClass="control-label pull-right">
							<fmt:message key="Menu.permalinks" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
						<form:input path="permalinks" id="permalinks" readonly="true"
							cssClass="form-control validate[required, maxSize[50]]" maxlength="50"
							cssErrorClass="form-control has-error validate[required, maxSize[50]]" />
						<form:errors path="permalinks" cssClass="has-error" />
					</div>
				</div>
				
<!--        			<div class="form-group"> -->
<!-- 					<div class="col-xs-4 col-md-3"> -->
<%-- 						<form:label path="system" for="system" cssClass="control-label pull-right" --%>
<%-- 							cssErrorClass="control-label pull-right"> --%>
<%-- 							<fmt:message key="coreMenu.system" /> --%>
<%-- 						</form:label> --%>
<!-- 					</div> -->
<!-- 					<div class="col-xs-8 col-md-6"> -->
<%-- 	                    <form:checkbox  path="system" id="system"  cssErrorClass="has-error" disabled="true"/> --%>
<%-- 	                    <form:errors path="system" cssClass="has-error"/> --%>
<!-- 					</div> -->
<!-- 				</div> -->
				
       			<%-- <div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="type" for="type" cssClass="control-label pull-right"
							cssErrorClass="control-label pull-right">
							<fmt:message key="coreMenu.type" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
	  				    <form:select path="type" id="type" items="${menus}" itemValue="menuValue" itemLabel="menuType" cssClass="form-control validate[required]" cssErrorClass="form-control  has-error validate[required]" />
	                    <form:errors path="type" cssClass="has-error"/>
					</div>
				</div> --%>
			</div>
			<div class="box-footer">
				<div class="form-group">
					<div class="col-xs-12 col-md-9">
						<div class="pull-right">
			                <input type="submit" class="btn btn-primary" name="cancel" id="cancel"  value="<fmt:message key="button.back"/>"/>
	            		</div>
	            	</div>
            	</div>
            </div>
        </form:form>
    </div>
</div>
</div>