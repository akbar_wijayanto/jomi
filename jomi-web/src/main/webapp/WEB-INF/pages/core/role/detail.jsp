<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreRole.title" /></title>
<meta name="heading" content="<fmt:message key='coreRole.heading'/>" />
<script type="text/javascript"
	src="<c:url value="/scripts/custom/core.role.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div id="breadcumbTitle">
			<fmt:message key='coreRole.message.view' />
		</div>
		<spring:bind path="coreRole.*">
			<c:if test="${not empty status.errorMessages}">
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">&times;</button>
					<c:forEach var="error" items="${status.errorMessages}">
						<c:out value="${error}" escapeXml="false" />
						<br />
					</c:forEach>
				</div>
			</c:if>
		</spring:bind>

		<div class="box">
			<spring:url var="action" value='/core/role/detail' />
			<form:form commandName="coreRole" method="post" action="${action}"
				id="coreRole" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="id" for="id"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreRole.id" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="id" id="id" readonly="true"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="id" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="name" for="name"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreRole.name" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="name" id="name" readonly="true"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="name" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="description" for="description"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreRole.description" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="description" id="description" readonly="true"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="description" cssClass="has-error" />
						</div>
					</div>

					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th><fmt:message key='coreRole.no' /></th>
								<th><fmt:message key='coreMenu.id' /></th>
								<th><fmt:message key='coreMenu.name' /></th>
								<th><fmt:message key='coreMenu.description' /></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="coreMenu" items="${coreRole.coreMenus}"
								varStatus="status">
								<tr>
									<td>${status.count}</td>
									<td>${coreMenu.id}</td>
									<td>${coreMenu.name}</td>
									<td>${coreMenu.description}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>

				<div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-12">
						<div class="pull-right">
								<a href="<c:url value='/core/role' />" class="btn btn-primary"
									title="<fmt:message key="coreRole.back"/>"><fmt:message
										key="coreRole.back" /></a>
										</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>