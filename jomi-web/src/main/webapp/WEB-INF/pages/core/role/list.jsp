<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key='coreRole.title' /></title>
<meta name="heading" content="<fmt:message key='coreRole.heading'/>" />
<script type="text/javascript"
	src="<c:url value='/resources/assets/custom/core.role.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">	 
	    <security:authorize access="hasRole('CORE:ROLE:INPUT:*')">
			<a href="<c:url value='/core/role/add' />" id="addConfigSystemLink" 
				class="btn btn-primary pull-right">
				<i class="fa fa-plus"></i> 
				<fmt:message key="coreRole.message.add" /></a>
		</security:authorize>
	</div>
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key="coreRole.message" /></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
<%-- 							<th><fmt:message key='coreRole.id' /></th> --%>
							<th><fmt:message key='coreRole.description' /></th>
							<th><fmt:message key='coreRole.status' /></th>
							<th width="20%">&nbsp;</th>
						</tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="coreRole" items="${coreRoles}">
			            	<tr>
<%-- 				            	<td>${coreRole.id}</td> --%>
				            	<td>${coreRole.description}</td>
				            	<td>${coreRole.status}</td>
				            	<td>
				            	<security:authorize access="hasRole('CORE:ROLE:READ:*')">
				            		<a href="<c:url value='/core/role/detail/${coreRole.id}' />" title="View">
				            		<i class="fa fa-eye"></i></a>&nbsp;
				            	</security:authorize>
				            	<security:authorize access="hasRole('CORE:ROLE:EDIT:*')">
				            		<a href="<c:url value='/core/role/edit/${coreRole.id}' />" title="Edit">
				            		<i class="fa fa-edit"></i></a>&nbsp;
				            	</security:authorize>
				            	<security:authorize access="hasRole('CORE:ROLE:DELETE:*')">
				            		<a href="<c:url value='/core/role/delete/${coreRole.id}' />" title="Delete">
				            		<i class="fa fa-trash-o"></i></a>&nbsp;
				            	</security:authorize>
				            	<security:authorize access="hasRole('CORE:ROLE:INPUT:*')">
				            		<a href="<c:url value='/core/role/managemenu/${coreRole.id}' />" title="Manage Menu">
				            		<i class="fa fa-bars"></i></a>&nbsp;
				            	</security:authorize>
				            	 <security:authorize access="hasRole('CORE:ROLE:INPUT:*')">
				            		<a href="<c:url value='/core/role/managepermission/${coreRole.id}' />" title="Manage Permission">
				            		<i class="fa fa-key"></i></a>&nbsp;
				            	</security:authorize> 
				            	<security:authorize access="hasRole('CORE:ROLE:READ:*')">
				            		<a href="<c:url value='/core/role/userrole/${coreRole.id}' />" title="User Role List">
				            		<i class="fa fa-users"></i></a></td>
				            	</security:authorize>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
		</div>
    </div>
</div>