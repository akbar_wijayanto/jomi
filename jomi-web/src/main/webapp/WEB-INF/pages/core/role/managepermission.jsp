<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreRole.title"/></title>
    <meta name="heading" content="<fmt:message key='coreRole.heading'/>"/>
    <script type="text/javascript" src="<c:url value="/scripts/custom/core.role.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreRole.message.managepermission' />
			</div>
			
        	<spring:url var = "action" value='/core/role/managepermission/${coreRole.id}' />
        	<form:form commandName="coreRole" method="post" action="${action}"  id="coreRole" class="form-horizontal">
        		<div class="box-body">
	            	<input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>
	
	            	<div class="form-group">
						<div class="col-xs-4 col-md-2">
							<form:label for="id" path="id" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreRole.id" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-8">
							<form:input path="id" id="id" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="id" cssClass="has-error" />
						</div>
					</div>
	
	            	<div class="form-group">
						<div class="col-xs-4 col-md-2">
							<form:label for="name" path="name" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreRole.name" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-8">
							<form:input path="name" id="name" cssClass="form-control"
								cssErrorClass="form-control has-error" readonly="true" />
							<form:errors path="name" cssClass="has-error" />
						</div>
					</div>
		            
		            <div class="form-group">
						<div class="col-xs-4 col-md-2">
							<form:label for="description" path="description" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreRole.description" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-8">
							<form:input path="description" id="description" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="description" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-2">
<%-- 							<form:label for="corePermissions" path="corePermissions" cssClass="control-label pull-right" --%>
<%-- 								cssErrorClass="control-label"> --%>
<%-- 								<fmt:message key="coreRole.corePermissions" /> --%>
<%-- 							</form:label> --%>
						</div>
						<div class="col-xs-8 col-md-8">
		                     <table class="table table-condensed table-striped table-bordered" id="permissions">
		                        <thead>
		                        <tr>
									<th><fmt:message key="coreRole.managepermission.applicationname"/></th>
									<th><input type="checkbox" id="checkAllPermission" /></th>
									<th><fmt:message key="coreRole.managepermission.all"/></th>
									<th><input type="checkbox" id="checkAuthorizePermission" /></th>
									<th><fmt:message key="coreRole.managepermission.authorize"/></th>
									<th><input type="checkbox" id="checkDeletePermission" /></th>
									<th><fmt:message key="coreRole.managepermission.delete"/></th>
									<th><input type="checkbox" id="checkReadPermission" /></th>
									<th><fmt:message key="coreRole.managepermission.read"/></th>
									<th><input type="checkbox" id="checkInputPermission" /></th>
									<th><fmt:message key="coreRole.managepermission.input"/></th>
									<th><input type="checkbox" id="checkEditPermission" /></th>
									<th><fmt:message key="coreRole.managepermission.edit"/></th>
								</tr>
		                        </thead>
		                        <tbody>
		                        <c:forEach var="dto" items="${permissionDtos}">
		                            <tr>
		                            	<td>${dto.applicationName}</label></td>
		                                <c:forEach var="permission" items="${dto.corePermissions}">
		                                	 <td><input type="checkbox" name="permissionIds" value="${permission.id}" class="check${permission.actionName == '*' ? 'ALL' : permission.actionName}"  ${permission.checklistStatus==true?'checked="checked"': ''}/></td>
		                                	 <td>${permission.actionName=='*'?'ALL':permission.actionName}</td>
		                                </c:forEach>
		                            </tr>
		                        </c:forEach>
		                        </tbody>
		                    </table>
						</div>
					</div>
	            </div>
				
	            <div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-12">
							<div class="pull-right">
				               <input type="submit" class="btn btn-primary" name="save" id="save"  value="<fmt:message key="button.save"/>"/>
				               <input type="submit" class="btn btn-primary" name="cancel" id="cancel"  value="<fmt:message key="button.cancel"/>"/>
		            		</div>
		            	</div>
	            	</div>
	            </div>
   	 		</form:form>
       	 </div>
    </div>
</div>