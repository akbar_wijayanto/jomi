<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key='corePermission.title' /></title>
<meta name="heading"
	content="<fmt:message key='corePermission.heading'/>" />
<script type="text/javascript"
	src="<c:url value='/resources/assets/custom/core.permission.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<security:authorize access="hasRole('CORE:PERMISSION:INPUT:*')">
			<a href="<c:url value='/core/permission/add' />"
				id="addConfigSystemLink" class="btn btn-primary pull-right"> <i
				class="fa fa-plus"></i> <fmt:message
 					key="corePermission.message.add" /></a> 
 		</security:authorize> 
	</div>
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key="corePermission.message" />
			</div>

			<div class="box-body table-responsive">
				<table id="tableList" class="table table-bordered table-striped">
					<thead>
						<tr>
<%-- 							<th><fmt:message key='corePermission.id' /></th> --%>
							<th><fmt:message key='corePermission.description' /></th>
							<th><fmt:message key='corePermission.moduleName' /></th>
							<th><fmt:message key='corePermission.applicationName' /></th>
							<th><fmt:message key='corePermission.actionName' /></th>
<%-- 							<th><fmt:message key='corePermission.recordId' /></th> --%>
							<th><fmt:message key='corePermission.status' /></th>
							<th width="11%">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="corePermissions" items="${corePermissions}">
							<tr>
<%-- 								<td>${corePermissions.id}</td> --%>
								<td>${corePermissions.description}</td>
								<td>${corePermissions.moduleName}</td>
								<td>${corePermissions.applicationName}</td>
								<td>
									<c:choose>
										<c:when test="${corePermissions.actionName == '*'}">
											ALL
										</c:when>
										<c:otherwise>
											${corePermissions.actionName}
										</c:otherwise>
									</c:choose>
								</td>
<!-- 								<td> -->
<%-- 									<c:choose> --%>
<%-- 										<c:when test="${corePermissions.recordId == '*'}"> --%>
<!-- 											ALL -->
<%-- 										</c:when> --%>
<%-- 										<c:otherwise> --%>
<%-- 											${corePermissions.recordId} --%>
<%-- 										</c:otherwise> --%>
<%-- 									</c:choose> --%>
<!-- 								</td> -->
								<td>${corePermissions.status}</td>
								<td>
								<security:authorize access="hasRole('CORE:PERMISSION:READ:*')">
									<a 
 									href="<c:url value='/core/permission/view/${corePermissions.id}' />" 
 									title="View"><i class="fa fa-eye"></i></a>
 								</security:authorize>
 								
 								<security:authorize access="hasRole('CORE:PERMISSION:INPUT:*')">
 									<a 
 									href="<c:url value='/core/permission/edit/${corePermissions.id}' />" 
 									title="Edit"> <i class="fa fa-edit"></i></a> 
 								</security:authorize>	

 								<security:authorize access="hasRole('CORE:PERMISSION:DELETE:*')">
 									<a
 									href="<c:url value='/core/permission/delete/${corePermissions.id}' />" 
 									title="Delete"> <i class="fa fa-trash-o"></i></a>
 								</security:authorize>	
 									
 									</td>
 									
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>