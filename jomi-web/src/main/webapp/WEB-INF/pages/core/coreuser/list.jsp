<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreUser.title"/></title>
    <meta name="heading" content="<fmt:message key='coreUser.heading'/>"/>
    <script type="text/javascript" src="<c:url value='/scripts/custom/core.coreuser.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">	 
		<security:authorize access="hasRole('CORE:USER:INPUT:*')">
	       	<a href="<c:url value='/core/coreuser/add' />" class="btn btn-primary pull-right">
	       		<i class="fa fa-plus"></i>
	       		<fmt:message key="coreUser.message.add" /></a>
	    </security:authorize>
	</div>
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='coreUser.message.list'/></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
			                <th><fmt:message key='coreUser.username'/></th>
			                <th><fmt:message key='coreUser.firstName'/></th>
			                <th><fmt:message key='coreUser.lastName'/></th>
			                <th><fmt:message key='coreUser.activeRole'/></th>
<%-- 			                <th><fmt:message key='coreUser.departmentCode'/></th> --%>
			                <th><fmt:message key='coreUser.status'/></th>
<%-- 			                <th><fmt:message key='coreUser.userType'/></th> --%>
<%-- 			                <th><fmt:message key='coreUser.activeBranch'/></th> --%>
			                <th width="15%">&nbsp;</th>
			            </tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="cu" items="${coreUsers}">
			            	<tr>
			            		
				            	<td>${cu.username}</td>
				            	<td>${cu.firstName}</td>
				            	<td>${cu.lastName}</td>
				            	<td>${cu.activeRole.description}</td>
<%-- 				            	<td>${cu.departmentCode.departmentCode}</td> --%>
				            	<td>${cu.status}</td>
<!-- 				            	<td> -->
<%-- 					            	<c:if test="${cu.activeRole.name == 'ROLE_CHIEF_TELLER'}"> --%>
<!-- 										chief teller -->
<%-- 					            	</c:if> --%>
<%-- 					            	<c:if test="${cu.activeRole.name != 'ROLE_CHIEF_TELLER'}"> --%>
<%-- 					            		${cu.userType} --%>
<%-- 					            	</c:if> --%>
<!-- 				            	</td> -->
<%-- 				            	<td>${cu.activeBranch.id} - ${cu.activeBranch.name}</td> --%>
				            	<td>
				            	<security:authorize access="hasRole('CORE:USER:READ:*')">
				            		<a href="<c:url value='/core/coreuser/view/${cu.id}' />" title="View"><i class="fa fa-eye"></i></a>&nbsp;
		                        </security:authorize>
		                        <security:authorize access="hasRole('CORE:USER:EDIT:*')">
		                            <a href="<c:url value='/core/coreuser/edit/${cu.id}' />" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;
		                        </security:authorize>
		                        <security:authorize access="hasRole('CORE:USER:EDIT:*')">    
		                            <a href="<c:url value='/core/coreuser/detail/${cu.id}' />" title="Setting"><i class="fa fa-cogs"></i></a>&nbsp;
		                        </security:authorize>
		                        <c:choose>
			                        <c:when test="${cu.status == 'LIVE' }">
				                        <security:authorize access="hasRole('CORE:USER:EDIT:*')">      
				                          	<a href="<c:url value='/core/user/changeroleview/${cu.id}' />" title="Change Active Role"><i class="fa fa-caret-square-o-right "></i></a>&nbsp;
				                        </security:authorize>
				                        <security:authorize access="hasRole('CORE:USER:EDIT:*')">     	
				                          	<a href="<c:url value='/core/coreuser/resetpassword/${cu.id}' />" title="Reset Password"><i class="fa fa-refresh "></i></a>&nbsp;
				                        </security:authorize>  
					            		<c:if test="${isImplementTeller }">
					            			<security:authorize access="hasRole('CORE:USER:EDIT:*')">   
						            	   		<a href="<c:url value='/core/teller/add/${cu.id}' />" title="Set Teller"><i class="fa fa-user"></i></a>&nbsp;
			                                </security:authorize>
			                                <security:authorize access="hasRole('CORE:USER:EDIT:*')">   
			                                   <a href="<c:url value='/core/staff/add/${cu.id}' />" title="Set Head Teller"><i class="fa fa-male"></i></a>&nbsp;
						            	   	</security:authorize>
					            		</c:if>
					            	</c:when>
				            		<c:otherwise>
			                          	<a href="javascript:void(0);" style="color: #ccc;" title="Change Active Role"><i class="fa fa-caret-square-o-right "></i></a>&nbsp;
			                          	<a href="javascript:void(0);" style="color: #ccc;" title="Reset Password"><i class="fa fa-refresh "></i></a>&nbsp;
					            	   	<c:if test="${isImplementTeller }">
						            	   	<a href="javascript:void(0);" style="color: #ccc;" title="Set Teller"><i class="fa fa-user"></i></a>&nbsp;
		                                   	<a href="javascript:void(0);" style="color: #ccc;" title="Set Staff"><i class="fa fa-male"></i></a>&nbsp;
				            			</c:if>
				            		</c:otherwise>
			            		</c:choose>
				            	</td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
		</div>
    </div>
</div>