<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreUser.title"/></title>
    <meta name="heading" content="<fmt:message key='coreUser.heading'/>"/>
    <script type="text/javascript" src="<c:url value="/scripts/custom/core.coreuser.js"/>"></script>
    <script type="text/javascript">
    
    </script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreUser.message.add' />
			</div>
	        <spring:url var ="action" value='/core/coreuser/add' />
	        <form:form commandName="coreUser" method="post" action="${action}"  
	            id="coreUser" class="form-horizontal">
	        	<div class="box-body">
		            <input type="hidden" name="from" 
		                value="<c:out value="${param.from}"/>" />
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<%-- <form:label for="username" path="username" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="User.username" />
							</form:label> --%>
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.username" /> <font color="red">*</font>
							</form:label>							
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="username" id="username" cssClass="form-control validate[required, maxSize[15]]"
								cssErrorClass="form-control has-error validate[required, maxSize[15]]" placeholder="Username used for login, ex: User" />
							<form:errors path="username" cssClass="has-error" />
						</div>
					</div>
						
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<%-- <form:label for="password" path="password" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="User.password" />
							</form:label> --%>
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.password" /> <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:password path="password" id="password" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" placeholder="Password used for login" />
							<form:errors path="password" cssClass="has-error" />
						</div>
					</div>	
						
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<%-- <form:label for="confirmPassword" path="confirmPassword" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="User.confirmPassword" />
							</form:label> --%>
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.confirmPassword" /> <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:password path="confirmPassword" id="confirmPassword" cssClass="form-control  validate[required]"
								cssErrorClass="form-control has-error  validate[required]" placeholder="Repeat password" />
							<form:errors path="confirmPassword" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<%-- <form:label for="email" path="email" cssClass="control-label pull-right "
								cssErrorClass="control-label pull-right">
								<fmt:message key="User.email" />
							</form:label> --%>
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.email" /> <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="email" id="email" cssClass="form-control  validate[required]"
								cssErrorClass="form-control has-error  validate[required]" placeholder="Valid e-mail, ex: user@dunia.com" />
							<form:errors path="email" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<%-- <form:label for="firstName" path="firstName" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="User.firstName" />
							</form:label> --%>
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.firstName" /> <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="firstName" id="firstName" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" placeholder="First name" />
							<form:errors path="firstName" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="lastName" path="lastName" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.lastName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="lastName" id="lastName" cssClass="form-control"
								cssErrorClass="form-control has-error" placeholder="Last name" />
							<form:errors path="lastName" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<%-- <form:label for="sessionTimeout" path="sessionTimeout" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.sessionTimeout" />
							</form:label> --%>
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.sessionTimeout" /> <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="sessionTimeout" id="sessionTimeout" maxlength="9" cssClass="form-control validate[required, min[1.0], max[86400.0], custom[number]]"
								cssErrorClass="form-control has-error validate[required, min[1.0], max[86400.0], custom[number]]" placeholder="Session Timeout (in minutes) ex: 1000, max: 86400" />
							<form:errors path="sessionTimeout" cssClass="has-error " />
						</div>
					</div>
					
					<!-- start tab    nav-tabs-custom -->
					<div class="form-group row" style="margin-top: 25px;">
						<div class="col-md-9">
							<ul id="myTab" class="nav nav-tabs">
							   <%--  <li ><a href="#managepermission" data-toggle="tab"><fmt:message key="coreuser.tab.managepermission" /></a></li>  --%>
<%-- 								<li style="float: right;" class="active"><a href="#managebranch" data-toggle="tab"><fmt:message key="coreuser.tab.managebranch" /></a></li> --%>
								<li style="float: right;"><a href="#managerole" data-toggle="tab"><fmt:message key="coreuser.tab.role" /></a></li>
							</ul>
						</div>
					</div>

					<div id="myTabContent" class="tab-content">
						<!-- start tab 3 role -->
						<div class="tab-pane fade" id="managerole">
							<div class="form-group">
								<div class="col-xs-4 col-md-3">
									<form:label for="coreRoles" path="coreRoles"
										cssClass="control-label pull-right"
										cssErrorClass="control-label">
										<fmt:message key="coreUser.coreRoles" /> <font color="red">*</font>
									</form:label>
								</div>
								<div class="col-xs-8 col-md-6">
									<table class="table table-condensed table-striped table-bordered"
										id="roles">
										<thead>
											<tr>
												<th class="table_checkbox"><input type="checkbox"
													id="checkRole"/></th>
												<th><fmt:message key='coreRole.name' /></th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="cp" items="${listCoreRoles}">
												<tr>
													<td><input type="checkbox" name="coreRoles"
														class="coreRoles" value="${cp.value}"
														${cp.status==true?'checked="checked"': ''} /></td>
													<td>${cp.label}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- end tab 3 role -->
					</div>
					<!-- end tab -->

					
		            <div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-9">
								<div class="pull-right">
									<input type="submit" class="btn btn-primary" name="save" id="save"  value="<fmt:message key="button.add"/>"/>
									<input type="submit" class="btn btn-default" name="cancel" id="cancel"  value="<fmt:message key="button.cancel"/>"/>
					            </div>
			            	</div>
		            	</div>
		            </div>
	            </div>
	        </form:form>
     	</div>
    </div>
</div>