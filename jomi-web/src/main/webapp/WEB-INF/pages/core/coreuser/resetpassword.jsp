<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreUser.title"/></title>
    <meta name="heading" content="<fmt:message key='coreUser.heading'/>"/>
	<script type="text/javascript" src="<c:url value='/scripts/custom/core.coreuser.resetpass.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreUser.message.resetpass' />
			</div>
        
	        <spring:url var = "action" value='/core/coreuser/resetpassword/${coreUser.id }' />
	        <form:form commandName="coreUser" method="post" action="${action}" id="coreUser" class="form-horizontal">
	        	<div class="box-body">
	            	<input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>
	
	            	<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="username" path="username" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.username" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="username" id="username" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="username" cssClass="has-error" />
						</div>
					</div>
	            
	            <div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="password" path="password" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="User.password" />&nbsp;<font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:password path="password" id="password" placeholder="Password used for login" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="password" cssClass="has-error" />
						</div>
					</div>	
						
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="confirmPassword" path="confirmPassword" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="User.confirmPassword" />&nbsp;<font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:password path="confirmPassword" id="confirmPassword" placeholder="Repeat password" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="confirmPassword" cssClass="has-error" />
						</div>
					</div>
				</div>
					
	            <div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-9">
							<div class="pull-right">
				               <input type="submit" class="btn btn-primary" name="save" id="save"  value="<fmt:message key="button.save"/>"/>
				               <input type="submit" class="btn btn-primary" id="cancel" name="cancel" value="<fmt:message key="button.cancel"/>"/>
		            		</div>
		            	</div>
	            	</div>
	            </div>
	        </form:form>
        </div>
    </div>
</div>