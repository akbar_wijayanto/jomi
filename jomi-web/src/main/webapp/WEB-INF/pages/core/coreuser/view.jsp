<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreUser.title"/></title>
    <meta name="heading" content="<fmt:message key='coreUser.heading'/>"/>
    <%-- <script type="text/javascript" src="<c:url value="/scripts/custom/core.coreuser.js"/>"></script> --%>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreUser.message.view' />
			</div>
	        <spring:url var ="action" value='/core/coreuser/view' />
	        <form:form commandName="coreUser" method="post" action="${action}"  
	            id="coreUser" class="form-horizontal">
	        	<div class="box-body">
		            <input type="hidden" name="from" 
		                value="<c:out value="${param.from}"/>" />
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<%-- <form:label for="username" path="username" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="User.username" />
							</form:label> --%>
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.username" /> <font color="red">*</font>
							</form:label>							
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="username" id="username" cssClass="form-control validate[required]" readonly="true"
								cssErrorClass="form-control has-error validate[required]" placeholder="Username used for login, ex: User" />
							<form:errors path="username" cssClass="has-error" />
						</div>
					</div>
				
					<%-- <div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.password" /> <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:password path="password" id="password" cssClass="form-control validate[required]" readonly="true"
								cssErrorClass="form-control has-error validate[required]" placeholder="******"/>
							<form:errors path="password" cssClass="has-error" />
						</div>
					</div>	
						
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.confirmPassword" /> <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:password path="confirmPassword" id="confirmPassword" cssClass="form-control  validate[required]" readonly="true"
								cssErrorClass="form-control has-error  validate[required]" placeholder="******" />
							<form:errors path="confirmPassword" cssClass="has-error" />
						</div>
					</div> --%>
						
					<%-- <div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.passwordHint" /> <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:password path="passwordHint" id="passwordHint" cssClass="form-control  validate[required]" readonly="true"
								cssErrorClass="form-control has-error  validate[required]" placeholder="******"/>
							<form:errors path="passwordHint" cssClass="has-error" />
						</div>
					</div> --%>
						
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<%-- <form:label for="email" path="email" cssClass="control-label pull-right "
								cssErrorClass="control-label pull-right">
								<fmt:message key="User.email" />
							</form:label> --%>
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.email" /> <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="email" id="email" cssClass="form-control  validate[required,custom[email]]" readonly="true"
								cssErrorClass="form-control has-error  validate[required,custom[email]]" placeholder="Valid e-mail, ex: user@dunia.com" />
							<form:errors path="email" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<%-- <form:label for="firstName" path="firstName" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="User.firstName" />
							</form:label> --%>
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.firstName" /> <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="firstName" id="firstName" cssClass="form-control validate[required]" readonly="true"
								cssErrorClass="form-control has-error validate[required]" placeholder="First name" />
							<form:errors path="firstName" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="lastName" path="lastName" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.lastName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="lastName" id="lastName" cssClass="form-control" readonly="true"
								cssErrorClass="form-control has-error" placeholder="Last name" />
							<form:errors path="lastName" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<%-- <form:label for="personnelCode" path="personnelCode" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.personnelCode" />
							</form:label> --%>
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.personalCode" /> <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="personnelCode" id="personnelCode" cssClass="form-control" readonly="true"
								cssErrorClass="form-control has-error" placeholder="Personnel code, ex: 1234567" />
							<form:errors path="personnelCode" cssClass="has-error"/>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="currency" for="currency"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.currency" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="currency.id" readonly="true" id="currency.id"
								cssClass="form-control validate[required]"
								items="${coreUser.currency.id}"
								cssErrorClass="has_error form-control validate[required]"
								itemLabel="currency.id" itemValue="currency.id" />
							<form:errors path="currency" cssClass="error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="limitAmount" path="limitAmount"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.limitAmount" /> <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="limitAmount" id="limitAmount"
								cssClass="form-control amount"
								cssErrorClass="form-control amount s_error" readonly="true" style="text-align:right;"/>
							<form:errors path="limitAmount" cssClass="s_error" />
						</div>
					</div>
			
				<%-- 	<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								Limit Amount <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="limitAmount" id="limitAmount" cssClass="form-control amount validate[required,maxSize[19]]" maxlength="19"
								cssErrorClass="form-control amount has-error validate[required,maxSize[19]]" style="text-align:right;" value="0" readonly="true" />
							<form:errors path="limitAmount" cssClass="has-error" />
						</div>
					</div> --%>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<%-- <form:label for="sessionTimeout" path="sessionTimeout" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.sessionTimeout" />
							</form:label> --%>
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.sessionTimeout" /> <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="sessionTimeout" id="sessionTimeout" cssClass="form-control validate[maxSize[9]]" readonly="true"
								cssErrorClass="form-control has-error validate[maxSize[9]]" placeholder="Session Timeout (in minutes) ex: 1000, max: 999999999" />
							<form:errors path="sessionTimeout" cssClass="has-error " />
						</div>
					</div>
					
				<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="departmentCode" path="departmentCode"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.departmentCode" /> <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="departmentCode.description" id="departmentCode"
								cssClass="form-control" cssErrorClass="form-control s_error"
								readonly="true" />
							<form:errors path="departmentCode" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-2 col-md-3">
							<form:label for="accountLocked" path="accountLocked" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.accountLocked" />
							</form:label>
						</div>
						<div class="col-xs-4 col-md-6">
		                    <form:checkbox  path="accountLocked" id="accountLocked" cssErrorClass="has-error" disabled="true" />
		                    <form:errors path="accountLocked" cssClass="has-error"/>
						</div>
					</div>
<!-- 		            <div class="form-group"> -->
<!-- 						<div class="col-xs-4 col-md-3"> -->
<%-- 							<form:label for="credentialsExpired" path="credentialsExpired" cssClass="control-label pull-right" --%>
<%-- 								cssErrorClass="control-label pull-right"> --%>
<%-- 								<fmt:message key="user.credentialsExpired" /> --%>
<%-- 							</form:label> --%>
<!-- 						</div> -->
<!-- 						<div class="col-xs-8 col-md-6"> -->
<%-- 		                    <form:checkbox  path="credentialsExpired" id="credentialsExpired" cssErrorClass="has-error" disabled="true"/> --%>
<%-- 		                    <form:errors path="credentialsExpired" cssClass="has-error"/> --%>
<!-- 						</div> -->
<!-- 					</div> -->
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="accountEnabled" path="accountEnabled" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.accountEnabled" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
		                    <form:checkbox  path="accountEnabled" id="accountEnabled" cssErrorClass="has-error" disabled="true"/>
		                    <form:errors path="accountEnabled" cssClass="has-error"/>
						</div>
					</div>
		            
<!-- 		            <div class="form-group"> -->
<!-- 						<div class="col-xs-4 col-md-3"> -->
<%-- 							<form:label for="accountExpired" path="accountExpired" cssClass="control-label pull-right" --%>
<%-- 								cssErrorClass="control-label pull-right"> --%>
<%-- 								<fmt:message key="coreUser.accountExpired" /> --%>
<%-- 							</form:label> --%>
<!-- 						</div> -->
<!-- 						<div class="col-xs-8 col-md-6"> -->
<%-- 		                    <form:checkbox  path="accountExpired" id="accountExpired" cssErrorClass="has-error" disabled="true"/> --%>
<%-- 		                    <form:errors path="accountExpired" cssClass="has-error"/> --%>
<!-- 						</div> -->
<!-- 					</div> -->
				</div>
					
					<!-- start tab  -->
					<div class="nav-tabs-custom">
						<ul id="myTab" class="nav nav-tabs">
							<%-- <li class="active"><a href="#managepermission"
								data-toggle="tab"><fmt:message
										key="coreuser.tab.managepermission" /></a></li> --%>
							<li class="active"><a href="#managebranch" data-toggle="tab"><fmt:message
										key="coreuser.tab.managebranch" /></a></li>
							<li><a href="#managerole" data-toggle="tab"><fmt:message
										key="coreuser.tab.role" /></a></li>

						</ul>
					</div>

					<div id="myTabContent" class="tab-content">
<%-- 						<!-- tab 1 permission -->
						<div class="tab-pane fade in active" id="managepermission">
							<div class="box-body">
								<input type="hidden" name="from"
									value="<c:out value="${param.from}"/>" />

								<div class="form-group">
									<div class="col-xs-4 col-md-3">
										<form:label for="corePermissions" path="corePermissions"
											cssClass="control-label pull-right"
											cssErrorClass="control-label">
											<fmt:message key="coreUser.corePermissions" />
										</form:label>
									</div>
									<div class="col-xs-8 col-md-6">
										<table style="width: 400px"
											class="table table-condensed table-striped table-bordered"
											id="permissions">
											<thead>
												<tr>
													<th><fmt:message key='coreUser.corePermissions' /></th>
													<th class="table_checkbox">
														<input type="checkbox" id="checkAllPermission" /></th>
													<th><fmt:message key='coreUser.select' /></th>
													<th class="table_checkbox">
														<input type="checkbox" id="checkAllAuthorPermission" /></th>
													<th><fmt:message key='coreUser.select' /></th>
													<th class="table_checkbox">
														<input type="checkbox" id="checkAllDeletePermission" /></th>
													<th><fmt:message key='coreUser.select' /></th>
													<th class="table_checkbox">
														<input type="checkbox" id="checkAllReadPermission" /></th>
													<th><fmt:message key='coreUser.select' /></th>
													<th class="table_checkbox">
														<input type="checkbox" id="checkAllInputPermission" /></th>
													<th><fmt:message key='coreUser.select' /></th>
													<th class="table_checkbox">
														<input type="checkbox" id="checkAllEditPermission" /></th>
													<th><fmt:message key='coreUser.select' /></th>
													
												</tr>
											</thead>
											<tbody>
												<c:forEach var="dto" items="${dtos}">
													<tr>
														<td>${dto.applicationName}</td>
														<c:forEach var="permission" items="${dto.corePermissions}">
															<td><input type="checkbox" name="corePermissions"
																value="${permission.id}" class="${permission.actionName=='*'?'ALL':permission.actionName}_CLASS"
																${permission.checklistStatus==true?'checked="checked"': ''} /></td>
															<td>${permission.actionName=='*'?'ALL':permission.actionName}</td>
														</c:forEach>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<!-- end tab 1 managepermission --> --%>

						<!-- start tab 2 branch -->
						<div class="tab-pane fade in active" id="managebranch">
							<div class="form-group">
								<div class="col-xs-4 col-md-3">
									<form:label for="coreBranches" path="coreBranches"
										cssClass="control-label pull-right"
										cssErrorClass="control-label">
										<fmt:message key="coreUser.coreBranches" />
									</form:label>
								</div>
								<div class="col-xs-8 col-md-6">
									<table style="width: 400px"
										class="table table-condensed table-striped table-bordered"
										id="branches">
										<thead>
											<tr>
												<th class="table_checkbox"><input type="checkbox"
													id="checkBranch" /></th>
												<th><fmt:message key='coreBranch.name' /></th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="cp" items="${branches}">
												<tr>
													<td><input type="checkbox" name="coreBranches" disabled="disabled"
														class="coreBranches" value="${cp.value}"
														${cp.status==true?'checked="checked"': ''} /></td>
													<td>${cp.label}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- end tab 2 branch -->

						<!-- start tab 3 role -->
						<div class="tab-pane fade" id="managerole">
							<div class="form-group">
								<div class="col-xs-4 col-md-3">
									<form:label for="corePermissions" path="corePermissions"
										cssClass="control-label pull-right"
										cssErrorClass="control-label">
										<fmt:message key="coreUser.coreRoles" />
									</form:label>
								</div>
								<div class="col-xs-8 col-md-6">
									<table style="width: 400px"
										class="table table-condensed table-striped table-bordered"
										id="roles">
										<thead>
											<tr>
												<th class="table_checkbox"><input type="checkbox"
													id="checkRole"/></th>
												<th><fmt:message key='coreRole.name' /></th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="cp" items="${listCoreRoles}">
												<tr>
													<td><input type="checkbox" name="coreRoles" disabled="disabled"
														class="coreRoles" value="${cp.value}"
														${cp.status==true?'checked="checked"': ''} /></td>
													<td>${cp.label}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- end tab 3 role -->
					</div>
					<!-- end tab -->
					
		            <div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-9">
								<div class="pull-right">
					              
					                <a
									href="${ctx}/core/coreuser"><input type="button"
									class="btn btn-primary" name="cancel" id="cancel"
									value="<fmt:message key="button.back"/>" /></a>
			            		</div>
			            	</div>
		            	</div>
		            </div>
	            </div>
	        </form:form>
     	</div>
    </div>
</div>