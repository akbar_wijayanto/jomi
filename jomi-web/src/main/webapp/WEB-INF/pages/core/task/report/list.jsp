<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreTask.title" /></title>
<meta name="heading" content="<fmt:message key='coreTaskReport.heading'/>" />
<script type="text/javascript"
	src="<c:url value='/scripts/custom/core.task.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreTaskReport.message.list' />
			</div>

			<spring:url var="action" value='/core/task/report' />
			<form:form commandName="coreTask" name="coreTask" action="${action}" id="coreTask" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<label for="userId" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="coreTask.userId" />
							</label>
						</div>
						<%-- <div class="col-xs-4 col-md-3">
							<input id="userId" name="userId"
								class="form-control"
								class="form-control has-error validate[required]" 
								value="${userId}"/>
						</div> --%>
						<div class="col-xs-4 col-md-3">
							<div class="input-group">
								<input type="hidden" name="userId" id="userId" value="${coreUser.id}"/>
								<input type="text" class="form-control" readonly
									id="coreUser_"
									value="${coreUser.firstName} - ${coreUser.lastName}">
	
								<span class="input-group-btn">
									<button style="height: 30px;" class="btn btn-info btn-flat"
										type="button" id="btnCoreUser" data-toggle="modal"
										data-target="#userList">
										<i class="fa fa-search"></i>
									</button>
								</span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<label for="startDate" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="coreTask.startDate" />
							</label>
						</div>
						<div class="col-xs-4 col-md-3">
							<input id="startDate" name="startDate"
								class="datepicker form-control validate[required]"
								class="datepicker form-control has-error validate[required]" 
								value="${startDate}"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<label for="endDate" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="coreTask.endDate" />
							</label>
						</div>
						<div class="col-xs-4 col-md-3">
							<input id="endDate" name="endDate"
								class="datepicker form-control validate[required]"
								class="datepicker form-control has-error validate[required]"
								value="${endDate}" />
						</div>
					</div>
					<div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-12">
								<div class="pull-right">
									<input type="submit" class="btn btn-primary" name="saveTask"
										id="saveTask" value="<fmt:message key="button.submit"/>" />
									<input type="button" class="btn btn-primary"
										name="taskreportdownloadpdf" id="taskreportdownloadpdf"
										value="<fmt:message key="button.downloadPdf"/>" /> 
									<input type="button" class="btn btn-primary"
										name="taskreportdownloadxls" id="taskreportdownloadxls"
										value="<fmt:message key="button.downloadExcel"/>" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</form:form>

			<div class="box-body table-responsive">
				<table id="tableList" class="table table-bordered table-striped" style="width: 100%;">
					<thead>
						<tr>
							<th><fmt:message key='coreTask.taskNumber' /></th>
							<th><fmt:message key='coreTask.feature' /></th>
							<th><fmt:message key='coreTask.description' /></th>
							<th><fmt:message key='coreTask.priority' /></th>
							<th><fmt:message key='coreTask.complexity' /></th>
							<th><fmt:message key='coreTask.manHour' /></th>
							<th><fmt:message key='coreTask.projectNumber' /></th>
<%-- 							<th><fmt:message key='coreTask.projectDescription' /></th> --%>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="task" items="${taskReports}">
							<tr>

								<td>${task.taskNumber}</td>
								<td>${task.feature}</td>
								<td>${task.description}</td>
								<td>${task.priority}</td>
								<td>${task.complexity}</td>
								<td>${task.manHour}</td>
								<td>${task.projectNumber}</td>
<%-- 								<td>${task.projectDescription}</td> --%>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="userList" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					<fmt:message key="coreUser.list" />
				</h4>
			</div>
			<div class="modal-body">
				<table id="coreUserTabel"
					class="table dataTable table-bordered table-striped">
					<thead>
						<tr>
							<th><fmt:message key='coreUser.name' /></th>
							<th width="80px">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="user" items="${coreUsers}">
							<tr>
								<td>${user.firstName} ${user.lastName}</td>
								<td><button type="button" class="btn btn-primary"
										onclick="doCopy('userId','coreUser_','userList','${user.id}','${user.firstName}','${user.lastName}');"
										title="check" class="btn btn-info btn-flat">Choose</button></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>