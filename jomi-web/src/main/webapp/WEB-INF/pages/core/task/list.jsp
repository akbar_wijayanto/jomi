<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key='coreTask.title' /></title>
<meta name="heading" content="<fmt:message key='coreTask.heading'/>" />
<script type="text/javascript"
	src="<c:url value='/resources/assets/custom/core.task.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">	 
<%-- 	    <security:authorize access="hastask('CORE:task:INPUT:*')"> --%>
			<a href="<c:url value='/core/task/add' />" id="addConfigSystemLink" 
				class="btn btn-primary pull-right">
				<i class="fa fa-plus"></i> 
				<fmt:message key="coreTask.message.add" /></a>
<%-- 		</security:authorize> --%>
	</div>
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key="coreTask.message" /></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
<%-- 							<th><fmt:message key='coreTask.id' /></th> --%>
							<th><fmt:message key='coreTask.taskNumber' /></th>
							<th><fmt:message key='coreTask.feature' /></th>
							<th><fmt:message key='coreTask.description' /></th>
							<th><fmt:message key='coreTask.priority' /></th>
							<th><fmt:message key='coreTask.status' /></th>
							<th width="20%">&nbsp;</th>
						</tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="coreTask" items="${coreTasks}">
			            	<tr>
<%-- 				            	<td>${coreTask.id}</td> --%>
								<td>${coreTask.taskNumber}</td>
								<td>${coreTask.feature}</td>
				            	<td>${coreTask.description}</td>
				            	<td>${coreTask.priority}</td>
				            	<td>${coreTask.status}</td>
				            	<td>
<%-- 								<security:authorize access="hastask('CORE:task:READ:*')"> --%>
				            		<a href="<c:url value='/core/task/detail/${coreTask.id}' />" title="View">
				            		<i class="fa fa-eye"></i></a>&nbsp;
<%-- 				            	</security:authorize> --%>
<%-- 				            	<security:authorize access="hastask('CORE:task:EDIT:*')"> --%>
				            		<a href="<c:url value='/core/task/edit/${coreTask.id}' />" title="Edit">
				            		<i class="fa fa-edit"></i></a>&nbsp;
<%-- 				            	</security:authorize> --%>
									<%-- <security:authorize access="hasRole('CORE:ROLE:INPUT:*')" --%>
				            		<a href="<c:url value='/core/task/assignmember/${coreTask.id}' />" title="Assign Member">
				            		<i class="fa fa-bars"></i></a>&nbsp;
				            		<%-- </security:authorize> --%>
<%-- 				            	<security:authorize access="hastask('CORE:task:DELETE:*')"> --%>
				            		<a href="<c:url value='/core/task/delete/${coreTask.id}' />" title="Delete">
				            		<i class="fa fa-trash-o"></i></a>&nbsp;
<%-- 				            	</security:authorize> --%>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
		</div>
    </div>
</div>