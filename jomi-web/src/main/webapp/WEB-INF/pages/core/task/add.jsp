<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreTask.title" /></title>
<meta name="heading" content="<fmt:message key='coreTask.heading'/>" />
<script type="text/javascript"
	src="<c:url value="/scripts/custom/core.task.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div id="breadcumbTitle">
			<fmt:message key='coreTask.message.add' />
		</div>
<%-- 		<spring:bind path="coreTask.*"> --%>
<%-- 			<c:if test="${not empty status.errorMessages}"> --%>
<!-- 				<div class="alert alert-danger alert-dismissable"> -->
<!-- 					<i class="fa fa-ban"></i> -->
<!-- 					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<%-- 					<c:forEach var="error" items="${status.errorMessages}"> --%>
<%-- 						<c:out value="${error}" escapeXml="false" /> --%>
<!-- 						<br /> -->
<%-- 					</c:forEach> --%>
<!-- 				</div> -->
<%-- 			</c:if> --%>
<%-- 		</spring:bind> --%>

		<div class="box">
			<spring:url var="action" value='/core/task/add' />
			<form:form commandName="coreTask" method="post" action="${action}" id="coreTask" class="form-horizontal">
				<div class="box-body">					
					<input type="hidden" name="from" value="<c:out value="${param.from}"/>" />
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<%-- <form:label path="name" for="name" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="Role.name" />
							</form:label> --%>
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								Task Number <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="taskNumber" id="taskNumber"
								cssClass="form-control validate[required] "
								cssErrorClass="form-control has-error validate[required]" placeholder="Task Number" />
							<form:errors path="taskNumber" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								Feature <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="feature" id="feature"
								cssClass="form-control validate[required] "
								cssErrorClass="form-control has-error validate[required]" placeholder="Task Feature" />
							<form:errors path="feature" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								Description <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="description" id="description"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" placeholder="Task Description"/>
							<form:errors path="description" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="priority" path="priority" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreTask.priority" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:select path="priority" id="priority" cssClass="form-control validate[required]" >
								<form:option value="" label="--- Select ---"></form:option>
								<c:forEach items="${priorityEnums}" var="priorityEnum">
									<form:option value="${priorityEnum}">${priorityEnum}</form:option>
								</c:forEach>
							</form:select>	
							<form:errors path="priority" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="complexity" path="complexity" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreTask.complexity" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:select path="complexity" id="complexity" cssClass="form-control validate[required]" >
								<form:option value="" label="--- Select ---"></form:option>
								<c:forEach items="${complexityEnums}" var="complexityEnum">
									<form:option value="${complexityEnum}">${complexityEnum}</form:option>
								</c:forEach>
							</form:select>	
							<form:errors path="complexity" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								Man Hour <font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="manHour" id="manHour"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" placeholder="Man Hour"/>
							<form:errors path="manHour" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<label for="coreProject" class="control-label pull-right">
								<fmt:message key="coreTask.project" />
							</label>
						</div>
						<div class="col-xs-8 col-md-6">
							<div class="input-group">
								<form:hidden path="coreProject.id" id="coreProject"
									cssClass="form-control"
									cssErrorClass=" form-control has-error validate[required]" />
								<input type="text" class="form-control" readonly
									id="coreProject_" 
									value="${coreProject.projectNumber} - ${coreProject.name}">
	
								<span class="input-group-btn">
									<button style="height: 30px;" class="btn btn-info btn-flat"
										type="button" id="btncoreProject" data-toggle="modal"
										data-target="#coreProjectList">
										<i class="fa fa-search"></i>
									</button>
								</span>
							</div>
							<form:errors path="coreProject.id" cssClass="has-error" />
						</div>
					</div>
					
				</div>
				
				<div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-9">
							<div class="pull-right">
								<input type="submit" class="btn btn-primary" name="save" id="save" value="<fmt:message key="button.add"/>" /> 
								<input type="submit" class="btn btn-primary" name="cancel" id="cancel" value="<fmt:message key="button.cancel"/>" />
							</div>
		            	</div>
	            	</div>
                </div>                
			</form:form>
		</div>
	</div>
</div>

<div class="modal fade" id="coreProjectList" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					<fmt:message key="project.list" />
				</h4>
			</div>
			<div class="modal-body">
				<table id="coreProjectTabel"
					class="table dataTable table-bordered table-striped">
					<thead>
						<tr>
							<th><fmt:message key='coreProject.projectNumber' /></th>
							<th><fmt:message key='coreProject.name' /></th>
							<th width="80px">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="project" items="${coreProjects}">
							<tr>
								<td>${project.projectNumber}</td>
								<td>${project.name}</td>
								<td><button type="button" class="btn btn-primary"
										onclick="doCopy('coreProject','coreProject_','coreProjectList','${project.id}','${project.projectNumber}','${project.name}');"
										title="check" class="btn btn-info btn-flat">Choose</button></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>