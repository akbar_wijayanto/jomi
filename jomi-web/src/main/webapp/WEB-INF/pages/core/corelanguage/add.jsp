<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="/common/taglibs.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key='coreLanguage.title' /></title>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='corelanguage.message.add' />
			</div>			
			<spring:url var="action" value='/core/corelanguage/add' />
	        <form:form commandName="coreLanguage" method="post" action="${action}" 
	         id="coreLanguage" role="form" class="form-horizontal" modelAttribute="coreLanguage">
				<div class="box-body">
		            <input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="id" path="id" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreLanguage.id" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="id" id="id" cssClass="form-control validate[required]]"
								cssErrorClass="form-control has-error validate[required]"/>
							<form:errors path="id" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="mnemonic" path="icon" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreLanguage.mnemonic" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="mnemonic" id="mnemonic" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="mnemonic" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="description" path="description" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreLanguage.description" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="description" id="description" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="description" cssClass="has-error" />
						</div>
					</div>
					
		            <div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-9">
								<div class="pull-right">
					                <input type="submit" class="btn btn-primary" name="save" id="save"  value="<fmt:message key="button.add"/>"/>
					                <input type="submit" class="btn btn-primary" name="cancel" id="cancel"  value="<fmt:message key="button.cancel"/>"/>
			            		</div>
			            	</div>
		            	</div>
		            </div>
	            </div>
	        </form:form>
		</div>
	</div>
</div>