<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="coreLanguage.title"/></title>
    <meta name="heading" content="<fmt:message key='coreLanguage.heading'/>"/>
</head>
<div class="row">
    <div class="col-xs-12">	 
<%-- 		<security:authorize access="hasRole('CORE:ACCOUNTING:INPUT:*')"> --%>
	       	<a href="<c:url value='/core/corelanguage/list' />" class="btn btn-primary pull-right">
	       		<i class="fa fa-plus"></i>
	       		<fmt:message key="coreLanguage.message.list" /></a>
<%-- 	    </security:authorize> --%>
	</div>
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='coreLanguage.message.list'/></div>
	       	
	        <div class="box-body table-responsive">
        	<table id="tableList" class="table table-bordered table-striped">
                <thead>
                    <tr>
                    	<th><fmt:message key='coreLanguage.id'/></th>
                        <th><fmt:message key='coreLanguage.mnemonic'/></th>
                        <th><fmt:message key='coreLanguage.description'/></th>
                        
                        <th width="80px">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                   <c:forEach var="corelanguage" items="${coreLanguages}">
                        <tr>
                        	<td>${corelanguage.id}</td>
                            <td>${corelanguage.mnemonic}</td>
                			<td>${corelanguage.description}</td>
                            <td>
                            	<a href="<c:url value='/core/corelanguage/edit/${corelanguage.id}' />" title="Edit"><i class="fa fa-edit"></i></a>
                            	<a href="<c:url value='/core/corelanguage/delete/${corelanguage.id}' />" title="Delete"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        	</div>
		</div>
    </div>
</div>