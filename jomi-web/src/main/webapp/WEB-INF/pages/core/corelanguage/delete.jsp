<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreLanguage.title"/></title>
    <meta name="heading" content="<fmt:message key='coreLanguage'/>"/>
    <script type="text/javascript" src="<c:url value="/scripts/custom/core.language.js"/>"></script>
</head>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div id="breadcumbTitle">
                <fmt:message key='coreLanguage.message.delete' />
            </div>
        <spring:url var = "action" value='/core/corelanguage/delete/${coreLanguage.id}' />
        <form:form commandName="coreLanguage" method="post" action="${action}"  id="coreLanguage" class="form-horizontal">
              <div class="box-body">
                <input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>
                
                <div class="form-group">
                    <div class="col-xs-8 col-md-6">
                        <form:hidden path="id" id="id" 
                            cssClass="form-control validate[required, max[50]]" maxlength="50"
                            cssErrorClass="form-control has-error validate[required, max[50]]"/>
                        <form:errors path="id" cssClass="has-error" />
                    </div>
                </div>
                
                <div class="form-group">
                     <div class="col-xs-4 col-md-3">
                            <form:label path="mnemonic" for="mnemonic" cssClass="control-label pull-right"
                                cssErrorClass="control-label pull-right">
                                <fmt:message key="coreLanguage.mnemonic" />
                            </form:label>
                     </div>
                     <div class="col-xs-8 col-md-6">
                            <form:input path="mnemonic" id="mnemonic" readonly="true"
                                cssClass="form-control validate[required]"
                                cssErrorClass="has-error form-control validate[required]" />
                                <form:errors path="mnemonic" cssClass="has-error" />
                     </div>
                </div>
                
                <div class="form-group">
                     <div class="col-xs-4 col-md-3">
                            <form:label path="description" for="description" cssClass="control-label pull-right"
                                cssErrorClass="control-label pull-right">
                                <fmt:message key="coreLanguage.description" />
                            </form:label>
                     </div>
                     <div class="col-xs-8 col-md-6">
                            <form:input path="description" id="description" readonly="true"
                                cssClass="form-control validate[required]"
                                cssErrorClass="has-error form-control validate[required]" />
                                <form:errors path="description" cssClass="has-error" />
                     </div>
                </div>
                
            </div>
            <div class="box-footer">
                <div class="form-group">
                    <div class="col-xs-12 col-md-9">
                        <div class="pull-right">
                            <input type="submit" class="btn btn-primary" name="save" id="save"  value="<fmt:message key="button.delete"/>"/>
                            <input type="submit" class="btn btn-primary" name="cancel" id="cancel"  value="<fmt:message key="button.cancel"/>"/>
                        </div>
                    </div>
                </div>
            </div>
        </form:form>
    </div>
</div>
</div>