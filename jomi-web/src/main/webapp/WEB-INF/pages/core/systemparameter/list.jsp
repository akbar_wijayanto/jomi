<%@ include file="/common/taglibs.jsp"%>
<head>
	<title><fmt:message key='coreSystemParameter.title' /></title>
	<meta name="heading" content="<fmt:message key='coreSystemParameter.title'/>" />
</head>

<div class="row">
	<div class="col-xs-12">
		<security:authorize access="hasAnyRole('CORE:SYSTEMPARAMETER:INPUT:*')">
			<a href="<c:url value='/core/systemparameter/add' />" class="btn btn-primary pull-right"> <i class="fa fa-plus"></i> <fmt:message key="coreSystemParameter.message.add" /></a>
		</security:authorize>
	</div>
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreSystemParameter.message.list' />
			</div>
			<div class="box-body table-responsive">
				<table id="tableList" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th ><fmt:message key='coreSystemParameter.key' /></th>
							<th ><fmt:message key='coreSystemParameter.description' /></th>
							<th ><fmt:message key='coreSystemParameter.value' /></th>
							<th ><fmt:message key='coreSystemParameter.status' /></th>
							<th width="10%">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${parameters }" var="parameter">
							<tr>
								<td>${parameter.id }</td>
								<td>${parameter.description }</td>
								<td>${parameter.value }</td>
								<td>${parameter.status }</td>
								<td>
								<security:authorize access="hasAnyRole('CORE:SYSTEMPARAMETER:READ:*')">
									<a href="<c:url value='/core/systemparameter/detail/${parameter.id }' />" title="View"><i class="fa fa-eye"></i></a>
									&nbsp;
								</security:authorize>
								<security:authorize access="hasAnyRole('CORE:SYSTEMPARAMETER:EDIT:*')">
									<a href="<c:url value='/core/systemparameter/edit/${parameter.id }' />" title="Edit"><i class="fa fa-edit"></i></a>
									&nbsp;
								</security:authorize>
<%-- 									<a href="<c:url value='/core/systemparameter/copy/${parameter.id }' />" title="Copy"><i class="fa fa-copy"></i></a> --%>
<!-- 									<i class="fa fa-copy"></i> 
									&nbsp; -->
<%-- 									<a href="<c:url value='/core/systemparameter/delete/${parameter.id }' />" title="Delete"><i class="fa fa-trash-o"></i></a> --%>
<!-- 									<i class="fa fa-trash-o"></i> -->
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>