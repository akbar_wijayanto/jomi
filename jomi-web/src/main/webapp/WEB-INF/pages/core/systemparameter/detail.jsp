<%@ include file="/common/taglibs.jsp"%>
<head>
	<title><fmt:message key="coreSystemParameter.detail.title" /></title>
	<meta name="heading" content="<fmt:message key='coreSystemParameter.detail.title'/>" />
</head>
<div class="row">
	<div class="col-xs-12">
		<div id="breadcumbTitle">
			<fmt:message key="coreSystemParameter.detail" />
		</div>
		<div class="box">
			<spring:url var="action" value='/core/systemparameter/add' />
			<form:form commandName="parameter" method="post" action="${action }" id="parameter" cssClass="form-horizontal" role="form">
				<div class="box-body">
					<div class="row">
						<div class="form-group">
							<div class="col-xs-4 col-md-3">
								<form:label path="id" for="id" cssClass="control-label pull-right" cssErrorClass="control-label pull-right ">
									<fmt:message key="coreSystemParameter.key"/>
								</form:label>
							</div>
							<div class="col-xs-8 col-md-6">
								<form:input path="id" cssClass="form-control" cssErrorClass="has-error form-control" readonly="true"/>
								<form:errors path="id" cssClass="has-error"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-4 col-md-3">
								<form:label path="description" for="description" cssClass="control-label pull-right" cssErrorClass="control-label pull-right ">
									<fmt:message key="coreSystemParameter.description"/>
								</form:label>
							</div>
							<div class="col-xs-8 col-md-6">
								<form:input path="description" cssClass="form-control" cssErrorClass="has-error form-control" readonly="true"/>
								<form:errors path="description" cssClass="has-error"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-4 col-md-3">
								<form:label path="passwordFlag" for="passwordFlag" cssClass="control-label pull-right" cssErrorClass="control-label pull-right">
									<fmt:message key="coreSystemParameter.passwordFlag"/>
								</form:label>
							</div>
							<div class="col-xs-8 col-md-6">
			                    <form:checkbox path="passwordFlag" id="passwordFlag" cssErrorClass="has-error" disabled="true"/>
			                    <form:errors path="passwordFlag" cssClass="has-error"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-4 col-md-3">
								<form:label path="value" for="value" cssClass="control-label pull-right" cssErrorClass="control-label pull-right ">
									<fmt:message key="coreSystemParameter.value"/>
								</form:label>
							</div>
							<div class="col-xs-8 col-md-6">
								<c:choose>
									<c:when test="${parameter.passwordFlag == false }">
										<form:input path="value" cssClass="form-control" cssErrorClass="has-error form-control" readonly="true"/>
									</c:when>
									<c:otherwise>
										<form:input type="password" path="value" cssClass="form-control" cssErrorClass="has-error form-control" readonly="true"/>
									</c:otherwise>
								</c:choose>
								<form:errors path="value" cssClass="has-error"/>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-9">
							<div class="pull-right">
								<a href="${ctx}/core/systemparameter"><input type="button" class="btn btn-primary" name="cancel" id="cancel" value="<fmt:message key="button.back"/>" /></a>
							</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>