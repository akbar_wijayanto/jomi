<%@ include file="/common/taglibs.jsp"%>
<head>
	<title><fmt:message key="coreSystemParameter.delete.title" /></title>
	<meta name="heading" content="<fmt:message key='coreSystemParameter.delete.title'/>" />
</head>
<div class="row">
	<div class="col-xs-12">
		<div id="breadcumbTitle">
			<fmt:message key="coreSystemParameter.delete" />
		</div>
		<div class="box">
			<spring:url var="action" value='/core/systemparameter/delete' />
			<form:form commandName="parameter" method="post" action="${action }" id="parameter" cssClass="form-horizontal" role="form">
				<div class="box-body">
					<div class="row">
						<div class="form-group">
							<div class="col-xs-4 col-md-3">
								<form:label path="id" for="id" cssClass="control-label pull-right" cssErrorClass="control-label pull-right ">
									<fmt:message key="coreSystemParameter.key"/>
								</form:label>
							</div>
							<div class="col-xs-8 col-md-6">
								<form:input path="id" cssClass="form-control" cssErrorClass="has-error form-control" readonly="true"/>
								<form:errors path="id" cssClass="has-error"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-4 col-md-3">
								<form:label path="description" for="description" cssClass="control-label pull-right" cssErrorClass="control-label pull-right ">
									<fmt:message key="coreSystemParameter.description"/>
								</form:label>
							</div>
							<div class="col-xs-8 col-md-6">
								<form:input path="description" cssClass="form-control" cssErrorClass="has-error form-control" readonly="true"/>
								<form:errors path="description" cssClass="has-error"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-4 col-md-3">
								<form:label path="value" for="value" cssClass="control-label pull-right" cssErrorClass="control-label pull-right ">
									<fmt:message key="coreSystemParameter.value"/>
								</form:label>
							</div>
							<div class="col-xs-8 col-md-6">
								<form:input path="value" cssClass="form-control" cssErrorClass="has-error form-control" readonly="true"/>
								<form:errors path="value" cssClass="has-error"/>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-9">
							<div class="pull-right">
								<input type="submit" class="btn btn-primary" name="save" id="save" value="<fmt:message key="button.delete"/>" />
								<a href="${ctx}/core/systemparameter"><input type="button" class="btn btn-primary" name="cancel" id="cancel" value="<fmt:message key="button.cancel"/>" /></a>
							</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>