<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreUser.title" /></title>
<meta name="heading" content="<fmt:message key='coreUser.heading'/>" />

</head>
<div class="row">
	<div class="col-xs-11">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreUser.message.editpass' />
			</div>

			<spring:url var="action" value='/core/user/editpassword' />
			<form:form commandName="coreUserDto" method="post" action="${action}"
				id="coreUserDto" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />

					<div class="form-group">
						<div class="col-xs-3 col-md-2">
							<form:label for="username" path="username"
								cssClass="control-label"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.username" />
							</form:label>
						</div>
						<div class="col-xs-6 col-md-4">
							<form:input path="username" id="username"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="username" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-3 col-md-2">
							<form:label for="oldPassword" path="oldPassword"
								cssClass="control-label "
								cssErrorClass="control-label">
								<fmt:message key="coreUser.oldpassword" />
							</form:label>&nbsp;<font color="red">*</font>
						</div>
						<div class="col-xs-6 col-md-4">
							<form:password path="oldPassword" id="oldPassword"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" placeholder="" />
							<form:errors path="oldPassword" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-3 col-md-2">
							<form:label for="newPassword" path="newPassword"
								cssClass="control-label "
								cssErrorClass="control-label">
								<fmt:message key="coreUser.newPassword" />
							</form:label>&nbsp;<font color="red">*</font>
						</div>
						<div class="col-xs-6 col-md-4">
							<form:password path="newPassword" id="newPassword"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="newPassword" cssClass="has-error" />
						</div>
					</div>
    
					<div class="form-group">
						<div class="col-xs-3 col-md-2">
							<form:label for="confirmPassword" path="confirmPassword"
								cssClass="control-label "
								cssErrorClass="control-label">
								<fmt:message key="User.confirmPassword" />&nbsp;<font color="red">*</font>
							</form:label>
						</div>
						<div class="col-xs-6 col-md-4">
							<form:password path="confirmPassword" id="confirmPassword"
								cssClass="form-control validate[required]" cssErrorClass="form-control has-error validate[required]"/>
							<form:errors path="confirmPassword" cssClass="has-error" />
						</div>
					</div>

					<%-- <div class="form-group">
						<div class="col-xs-3 col-md-2">
							<form:label for="passwordHint" path="passwordHint"
								cssClass="control-label "
								cssErrorClass="control-label">
								<fmt:message key="coreUser.passwordHint" />
							</form:label>
						</div>
						<div class="col-xs-6 col-md-4">
							<form:input path="passwordHint" id="passwordHint"
								cssClass="form-control" cssErrorClass="form-control has-error" />
							<form:errors path="passwordHint" cssClass="has-error" />
						</div>
					</div> --%>
				</div>

				<div class="box-footer">
					<div class="form-group">
						<div class="col-xs-9 col-md-6">
							<div class="pull-right">
								<input type="submit" class="btn btn-primary" name="save"
									id="save" value="<fmt:message key="button.save"/>" /> 
								<a name="link"
								href="<c:url value='/core/user' />" class="btn btn-primary"
								id="cancel" />
							<fmt:message key="button.cancel" /></a>
							</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>