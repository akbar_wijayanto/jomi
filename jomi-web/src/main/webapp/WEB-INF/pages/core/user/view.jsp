<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreUser.title" /></title>
<meta name="heading" content="<fmt:message key='coreUser.heading'/>" />
</head>
<div class="row">
	<div id="breadcumbTitle">
		<fmt:message key="coreUser.message.view" />
	</div>

	<spring:url var="action" value='/core/user/view' />
	<form:form commandName="coreUser" method="post" action="${action}"
		id="coreUser" class="form-horizontal">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="username" path="username"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.username" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="username" id="username"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="username" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="email" path="email"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.email" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="email" id="email" cssClass="form-control"
								cssErrorClass="form-control has-error" readonly="true" />
							<form:errors path="email" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="firstName" path="firstName"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.firstName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="firstName" id="firstName"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="firstName" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="lastName" path="lastName"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.lastName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="lastName" id="lastName" cssClass="form-control"
								cssErrorClass="form-control has-error" readonly="true" />
							<form:errors path="lastName" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="personnelCode" path="personnelCode"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.personnelCode" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="personnelCode" id="personnelCode"
								cssClass="form-control" cssErrorClass="form-control has-error"
								readonly="true" />
							<form:errors path="personnelCode" cssClass="has-error" />
						</div>
					</div>

					<div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-9">
								<div class="pull-right">
									<%-- <a href="<c:url value='/core/user/editprofile' />"
										class="btn btn-primary"
										title="<fmt:message key="coreUser.edit"/>">
										<fmt:message key="user.edit" />
										<div class="splashy-application_windows_edit"></div></a>  --%>
									<a href="<c:url value='/core/user/editpassword' />"
										class="btn btn-primary"
										title="<fmt:message key="coreUser.editPassword"/>">
										<fmt:message key="user.editpassword" />
										<div class="splashy-box_edit"></div></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						<fmt:message key="coreBranch.message" />
					</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table class="table table-condensed table-striped table-bordered"
						id="tableList">
						<thead>
							<tr>
								<th><fmt:message key='coreUser.no' /></th>
								<th><fmt:message key='coreBranch.id' /></th>
								<th><fmt:message key='coreBranch.name' /></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="coreBranch" items="${coreUser.coreBranches}"
								varStatus="status">
								<tr>
									<td>${status.count}</td>
									<td>${coreBranch.id}</td>
									<td>${coreBranch.name}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>

<!-- 		<div class="col-xs-12"> -->
<!-- 			<div class="box"> -->
<!-- 				<div class="box-header"> -->
<!-- 					<h3 class="box-title"> -->
<%-- 						<fmt:message key="corePermission.message" /> --%>
<!-- 					</h3> -->
<!-- 				</div> -->
<!-- 				/.box-header -->
<!-- 				<div class="box-body"> -->
<!-- 					<table class="table table-condensed table-striped table-bordered" -->
<!-- 						id="tableList2"> -->
<!-- 						<thead> -->
<!-- 							<tr> -->
<%-- 								<th><fmt:message key='coreUser.no' /></th> --%>
<%-- 								<th><fmt:message key='corePermission.id' /></th> --%>
<%-- 								<th><fmt:message key='corePermission.name' /></th> --%>
<%-- 								<th><fmt:message key='corePermission.description' /></th> --%>
<!-- 							</tr> -->
<!-- 						</thead> -->
<!-- 						<tbody> -->
<%-- 							<c:forEach var="corePermission" --%>
<%-- 								items="${coreUser.corePermissions}" varStatus="status"> --%>
<!-- 								<tr> -->
<%-- 									<td>${status.count}</td> --%>
<%-- 									<td>${corePermission.id}</td> --%>
<%-- 									<td>${corePermission.name}</td> --%>
<%-- 									<td>${corePermission.description}</td> --%>
<!-- 								</tr> -->
<%-- 							</c:forEach> --%>
<!-- 						</tbody> -->
<!-- 					</table> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 		</div> -->

		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						<fmt:message key="coreRole.message" />
					</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table class="table table-condensed table-striped table-bordered"
						id="tableList3">
						<thead>
							<tr>
								<th><fmt:message key='coreUser.no' /></th>
								<th><fmt:message key='coreRole.id' /></th>
								<th><fmt:message key='coreRole.name' /></th>
								<th><fmt:message key='coreRole.description' /></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="coreRole" items="${coreUser.coreRoles}"
								varStatus="status">
								<tr>
									<td>${status.count}</td>
									<td>${coreRole.id}</td>
									<td>${coreRole.name}</td>
									<td>${coreRole.description}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</form:form>
</div>