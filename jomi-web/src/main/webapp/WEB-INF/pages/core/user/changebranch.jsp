<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="coreUser.title" /></title>
<meta name="heading" content="<fmt:message key='coreUser.heading'/>" />
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreUserBranch.message' />
			</div>
			
			<c:if test="${not empty user}">
			<div class="box-body table-responsive">
				<table class="table table-condensed table-striped table-bordered" id="tableList">
					<thead>
						<tr>
							<th><fmt:message key='coreUser.branchId' /></th>
							<th><fmt:message key='coreUser.branchName' /></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="br" items="${user.coreBranches}">
							<tr>
								<td>${br.id}</td>
								<td>${br.name}</td>
	
								<td class="center">
								<c:if test="${system.systemStatus==1 }">
									<c:if test="${br.id != user.activeBranch.id }">
											<a href="<c:url value='/core/user/on/${br.id}' />"
												class="fa fa-play" title="Activate"></a>&nbsp;
		                            </c:if> 
		                            <c:if test="${br.id == user.activeBranch.id }">
		                                <c:choose>
		                                	<c:when test="${user.activeBranch.onlineState==1 }">Already active &nbsp;
<%-- 		                                	<a href="<c:url value='/core/user/off/${br.id}' />" class="fa fa-power-off" title="Off"></a>&nbsp; --%>
											</c:when>
		                                	<c:otherwise>Offline &nbsp;
		                                		<a href="<c:url value='/core/user/on/${br.id}' />" class="fa fa-play" title="Activate"></a>&nbsp;
		                                	</c:otherwise>
		                                </c:choose>
		                            </c:if>
	                            </c:if>
	                            </td>
							</tr>
	
						</c:forEach>
					</tbody>
				</table>
				</div>
			</c:if>
		</div>

	</div>
</div>
