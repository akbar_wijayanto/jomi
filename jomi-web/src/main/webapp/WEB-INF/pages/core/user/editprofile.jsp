<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreUser.title" /></title>
<meta name="heading" content="<fmt:message key='coreUser.heading'/>" />

</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key="coreUser.message.editprofil" />
			</div>

			<spring:url var="action" value='/core/user/editprofile' />
			<form:form commandName="coreUser" method="post" action="${action}"
				id="coreUser" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="id" path="id"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.id" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="id" id="id"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]"
								readonly="true" />
							<form:errors path="id" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="username" path="username"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.username" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="username" id="username"
								cssClass="form-control validate[required] pull-right"
								cssErrorClass="form-control pull-right has-error validate[required]" readonly="true" />
							<form:errors path="username" cssClass="has-error pull-right" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="email" path="email"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreUser.email" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="email" id="email" cssClass="form-control pull-right"
								cssErrorClass="form-control has-error pull-right" />
							<form:errors path="email" cssClass="has-error pull-right" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="firstName" path="firstName"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.firstName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="firstName" id="firstName"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="firstName" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="lastName" path="lastName"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.lastName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="lastName" id="lastName" cssClass="form-control"
								cssErrorClass="form-control has-error" />
							<form:errors path="lastName" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="personnelCode" path="personnelCode"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.personnelCode" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="personnelCode" id="personnelCode"
								cssClass="form-control" cssErrorClass="form-control has-error" />
							<form:errors path="personnelCode" cssClass="has-error" />
						</div>
					</div>
				</div>

				<div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-9">
							<div class="pull-right">
								<input type="hidden" id="sessionTimeout" name="sessionTimeout"
									value="${coreUser.sessionTimeout}" /> <input type="hidden"
									id="limitAmount" name="limitAmount"
									value="${coreUser.limitAmount}" /> <input type="submit"
									class="btn btn-primary" name="save" id="save"
									value="<fmt:message key="button.save"/>" /> <input
									type="submit" class="btn btn-primary" name="cancel" id="cancel"
									value="<fmt:message key="button.cancel"/>" />
							</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>