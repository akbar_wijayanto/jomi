<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="coreRole.title" /></title>
<meta name="heading" content="<fmt:message key='coreRole.heading'/>" />
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key="coreRole.message" />
			</div>

			<c:if test="${not empty user}">
				<div class="box-body table-responsive">
					<table class="table">
						<!-- <thead>
							<tr>
<%-- 								<th width="21%" class="head0"><fmt:message key='coreUser.roleId' /></th> --%>
								<th width="21%" class="head1"><fmt:message key='coreUser.roleName' /></th>
								<th>&nbsp;</th>
							</tr>
						</thead> -->
						<tbody>
							<tr>
								<td class="head1">Username</td>
								<td>:</td>
								<td>${user.username }</td>
							</tr>
							<tr>
								<td class="head1">Email</td>
								<td>:</td>
								<td>${user.email }</td>
							</tr>
							<tr>
								<td class="head1">First Name</td>
								<td>:</td>
								<td>${user.firstName }</td>
							</tr>
							<tr>
								<td class="head1">Last Name</td>
								<td>:</td>
								<td>${user.lastName }</td>
							</tr>
							<tr>
								<td class="head1">Personnel Code</td>
								<td>:</td>
								<td>${user.personnelCode }</td>
							</tr>
							<tr>
								<td class="head1">Currency</td>
								<td>:</td>
								<td>${user.currency.name }</td>
							</tr>
							<tr>
								<td class="head1">Limit Amount</td>
								<td>:</td>
								<td><fmt:formatNumber type="number" maxFractionDigits="3" value="${user.limitAmount }" /></td>
							</tr>
							<tr>
								<td class="head1">Session Timeout</td>
								<td>:</td>
								<td>${user.sessionTimeout }</td>
							</tr>
							<tr>
								<td class="head1">Department Code</td>
								<td>:</td>
								<td>${user.departmentCode.departmentCode }</td>
							</tr>
						</tbody></table>
			</div>
		</c:if>
	</div>
</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-body table-responsive">
				<table class="table table-bordered table-striped" id="tableList">
					<thead>
						<th width="21%" class="head1"><fmt:message key='coreUser.roleName' /></th>
						<th></th>
					</thead>
					<tbody>
						<c:forEach var="us" items="${user.coreRoles}">
						<tr>
						<td>${us.name}</td>
						<td class="center">
							<c:if test="${us.id != user.activeRole.id }">
								<a name="link" href="<c:url value='/core/user/changerole/${us.id}-${user.id}' />"
										title="Activate"><i class="fa fa-play"></i></a>&nbsp;
	                        </c:if> 
	                        <c:if test="${us.id == user.activeRole.id }">
	                        	Already active
	                        </c:if>
	                    </td>
	                    </c:forEach>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="box-footer">
				<div class="form-group">
					<div class="input-group pull-right margin width100">
						<a href="<c:url value='/core/coreuser' />"
							class="btn btn-primary"
							title="<fmt:message key="button.back"/>"><fmt:message
							key="button.back" />
						<div class="splashy-arrow_small_left"></div></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

