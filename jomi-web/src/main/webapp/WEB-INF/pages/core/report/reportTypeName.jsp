<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key='coreReport.title' /></title>
<meta name="heading"
	content="<fmt:message key='coreReport.heading'/>" />
<%-- <script type="text/javascript" src="<c:url value='/resources/assets/custom/core.report.js' />"></script> --%>
</head>
<div class="row">
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key="coreReport.message" /> ${reportType}</div>
	       	
	       	<spring:url var="action" value='/core/report/cob' />
	       	<form:form commandName="cobReportDto" method="post" action="${action}" id="cobReportDto" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from" value="<c:out value="${param.from}"/>" />
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="reportName" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreReport.name" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:select path="reportName" id="reportName"
								cssClass="form-control validate[required]"
								cssErrorClass=" form-control has-error validate[required]">
								<form:options items="${reports }" itemValue="reportNameFile" itemLabel="reportName" />
							</form:select>
							<form:errors path="reportName" cssClass="form-control has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="reportDate" path="reportDate"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreReport.reportDate" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<div class="input-group date datepicker">
								<form:input path="reportDate" id="reportDate"
									cssClass="customDate form-control validate[required]"
									cssErrorClass=" has-error customDate form-control validate[required]" />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<form:errors path="reportDate" cssClass="has-error" />
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-12 col-md-9">
							<div class="pull-right">
								<input type="submit" class="btn btn-primary" name="search"
									id="search" value="<fmt:message key="button.search"/>" />
							</div>
						</div>
				</div>
			</form:form>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
							<th><fmt:message key='coreReport.reportName' /></th>
							<th width="11%">&nbsp;</th>
						</tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="coreReports" items="${coreReports}">
			            	<tr>
				            	<td>${coreReports}</td>
				            	<td>
				            		<a href="<c:url value='/core/report/cob/download/pdf/${date}/${coreReports}' />" title="Download PDF">
				            		<i class="fa fa-download"></i>PDF</a>&nbsp;
				            		<a href="<c:url value='/core/report/cob/download/xls/${date}/${coreReports}' />" title="Download Excel">
				            		<i class="fa fa-download"></i>XLS</a></td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
		        
		        <div class="form-group">
			     	<a href="<c:url value='/core/report/cob' />" class="btn btn-primary pull-right">
			     		<fmt:message key="button.back" />
			      	</a>
				</div>
			</div>
		</div>
    </div>
</div>