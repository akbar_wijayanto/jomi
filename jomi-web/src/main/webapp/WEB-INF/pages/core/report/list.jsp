<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key='coreReport.title' /></title>
<meta name="heading"
	content="<fmt:message key='coreReport.heading'/>" />
<%-- <script type="text/javascript" src="<c:url value='/resources/assets/custom/core.report.js' />"></script> --%>
</head>
<div class="row">
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key="coreReport.message" /></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
<%-- 							<th><fmt:message key='coreReport.id' /></th> --%>
							<th><fmt:message key='coreReport.reportName' /></th>
							<th><fmt:message key='coreReport.description' /></th>
							<th width="20%">&nbsp;</th>
						</tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="coreReports" items="${coreReports}">
			            	<tr>
<%-- 				            	<td>${coreReports.id}</td> --%>
				            	<td>${coreReports.reportName}</td>
				            	<td>${coreReports.description}</td>
				            	<td align="center">
				            	<a href="<c:url value='${coreReports.permalink}' />"
										title="View"> <i class="fa fa-eye"></i></a>&nbsp;</a>
								</td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
		</div>
    </div>
</div>