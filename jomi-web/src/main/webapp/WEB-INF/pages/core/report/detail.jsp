<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key='coreReport.title' /></title>
<meta name="heading" content="<fmt:message key='coreReport'/>" />
</head>

<div class="row">
	<div class="col-xs-12">
		<div id="breadcumbTitle">
			<fmt:message key="coreReport.message.view" />
		</div>

		<div class="box">
			<spring:url var="action" value='/core/report' />
			<form:form commandName="coreReport" method="post" action="${action}"
				id="coreReport" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="reportName" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreReport.name" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="reportName" id="reportName" maxlength="100"
								cssClass="form-control validate[maxSize[100]]"
								cssErrorClass=" validate[required,maxSize[100]]" readonly="true" />
							<form:errors path="reportName" cssClass="form-control has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="reportFileName"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreReport.fileName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="reportFileName" id="reportFileName"
								maxlength="50" cssClass="form-control validate[maxSize[50]]"
								cssErrorClass=" validate[required,maxSize[50]]" readonly="true" />
							<form:errors path="reportFileName"
								cssClass="form-control has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="parameter.label"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreReport.parameter" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="parameter.label" id="parameter" maxlength="50"
								cssClass="form-control validate[maxSize[50]]"
								cssErrorClass=" validate[required,maxSize[50]]" readonly="true" />
							<form:errors path="parameter.label"
								cssClass="form-control has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="permalink" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreReport.permalink" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="permalink" id="description" maxlength="50"
								cssClass="form-control validate[maxSize[50]]"
								cssErrorClass=" validate[required,maxSize[50]]" readonly="true" />
							<form:errors path="permalink" cssClass="form-control has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="description"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreReport.description" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="description" id="description" maxlength="50"
								cssClass="form-control validate[maxSize[50]]"
								cssErrorClass=" validate[required,maxSize[50]]" readonly="true" />
							<form:errors path="description" cssClass="form-control has-error" />
						</div>
					</div>

					<div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-9">
								<div class="pull-right">
									<input type="submit" class="btn btn-primary" name="cancel"
										id="cancel" value="<fmt:message key="button.cancel"/>" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>
