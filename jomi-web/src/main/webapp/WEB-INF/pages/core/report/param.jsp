<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreReport.title" /></title>
<meta name="heading" content="<fmt:message key='coreReport.heading'/>" />
<script type="text/javascript">
	var parameterType = '${ coreReport.parameter }';
</script>
<script type="text/javascript" src="<c:url value="/scripts/custom/reportparam.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div id="breadcumbTitle">
			<fmt:message key="coreReport.message.download" /> | ${coreReport.reportName }
		</div>
		<spring:bind path="coreReport.*">
			<c:if test="${not empty status.errorMessages}">
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">&times;</button>
					<c:forEach var="error" items="${status.errorMessages}">
						<c:out value="${error}" escapeXml="false" />
						<br />
					</c:forEach>
				</div>
			</c:if>
		</spring:bind>

		<div class="box">
			<spring:url var="actions" value='/core/report/download/${type}/${id}' />
			<spring:url var="getCif" value="/core/report/cif/"/>
			<form method="post" action="${actions}" id="coreReport"
				class="form-horizontal">
				<input type="hidden" name="from"
					value="<c:out value="${param.from}"/>" />
				<div class="box-body">

					<c:choose>
						<c:when test="${ coreReport.parameter == 'BYUSER' }">
							<div class="form-group">
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.startDate" /></label>
								</div>
								<div class="col-xs-4">

									<div class="input-group date datepicker">
										<input type="text" name="startDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.endDate" /></label>
								</div>
								<div class="col-xs-4">
									<div class="input-group date datepicker">
										<input type="text" name="endDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div>
							
							<!-- cif id -->
<!-- 							<div class="form-group"> -->
<!-- 								<div class="col-xs-2"> -->
<%-- 									<form:label for="cif" path="cif" cssClass="control-label pull-right" --%>
<%-- 										cssErrorClass="control-label"> --%>
<%-- 										<fmt:message key="coreReport.cifId" /> --%>
<%-- 									</form:label> --%>
<!-- 								</div> -->
<!-- 								<div class="col-xs-4 col-md-4"> -->
<%-- 										<form:input path="cif" id="cif" cssClass="form-control validate[required]" --%>
<%-- 										cssErrorClass="form-control has-error validate[required]" readonly="true" /> --%>
<%-- 									<form:errors path="cif" cssClass="has-error" /> --%>
<!-- 								</div> -->
<!-- 							</div> -->
					
							<!-- /cif id -->
							
							<div class="form-group">
								<div class="col-xs-2">
									<label for="userId" class="control-label pull-right"><fmt:message
											key="coreReport.user" /></label> <input type="hidden" id="userId"></input>
								</div>
								<div class="col-xs-4">
									<select name="parameter" id="parameter" class="form-control">
										<option value>---Select---</option>
										<c:forEach items="${users}" var="user">
											<option value="${user.username}">${user.username}</option>
										</c:forEach>

									</select>
								</div>
							</div>
						</c:when>
					</c:choose>
					<c:choose>
						<c:when test="${ coreReport.parameter == 'BYAO' }">
							<%-- <div class="form-group">
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.startDate" /></label>
								</div>
								<div class="col-xs-4">


									<div class="input-group date datepicker">
										<input type="text" name="startDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.endDate" /></label>
								</div>
								<div class="col-xs-4">
									<div class="input-group date datepicker">
										<input type="text" name="endDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div> --%>
							<input type="hidden" name="startDate">
							<input type="hidden" name="endDate">
							<!-- cif id -->
<!-- 							<div class="form-group"> -->
<!-- 								<div class="col-xs-2"> -->
<%-- 									<form:label for="cif" path="cif" cssClass="control-label pull-right" --%>
<%-- 										cssErrorClass="control-label"> --%>
<%-- 										<fmt:message key="coreReport.cifId" /> --%>
<%-- 									</form:label> --%>
<!-- 								</div> -->
<!-- 								<div class="col-xs-4 col-md-4"> -->
<%-- 										<form:input path="cif" id="cif" cssClass="form-control validate[required]" --%>
<%-- 										cssErrorClass="form-control has-error validate[required]" readonly="true" /> --%>
<%-- 									<form:errors path="cif" cssClass="has-error" /> --%>
<!-- 								</div> -->
<!-- 							</div> -->
					
							<!-- /cif id -->
							<div class="form-group">
								<div class="col-xs-2">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.accountOfficer" /></label>
								</div>
								<div class="col-xs-4">
								
									 <select name="parameter" id="parameter" class="form-control">
										<option value>ALL</option>
										<c:forEach items="${accountOfficers}" var="accountOfficer">
											<option value="${accountOfficer}">${accountOfficer}</option>
										</c:forEach>

									</select> 
								</div>
							</div>
						</c:when>
					</c:choose>
					<c:choose>
						<c:when test="${ coreReport.parameter == 'PRODUCT' }">
							<%-- <div class="form-group">
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.startDate" /></label>
								</div>
								<div class="col-xs-4">


									<div class="input-group date datepicker">
										<input type="text" name="startDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.endDate" /></label>
								</div>
								<div class="col-xs-4">
									<div class="input-group date datepicker">
										<input type="text" name="endDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div> --%>
							<input type="hidden" name="startDate">
							<input type="hidden" name="endDate">
							<!-- cif id -->
<!-- 							<div class="form-group"> -->
<!-- 								<div class="col-xs-2"> -->
<%-- 									<form:label for="cif" path="cif" cssClass="control-label pull-right" --%>
<%-- 										cssErrorClass="control-label"> --%>
<%-- 										<fmt:message key="coreReport.cifId" /> --%>
<%-- 									</form:label> --%>
<!-- 								</div> -->
<!-- 								<div class="col-xs-4 col-md-4"> -->
<%-- 										<form:input path="cif" id="cif" cssClass="form-control validate[required]" --%>
<%-- 										cssErrorClass="form-control has-error validate[required]" readonly="true" /> --%>
<%-- 									<form:errors path="cif" cssClass="has-error" /> --%>
<!-- 								</div> -->
<!-- 							</div> -->
							<!-- cif id -->
							<div class="form-group">
								<div class="col-xs-2">
									<label for="productId" class="control-label pull-right"><fmt:message
											key="coreReport.product" /></label>
								</div>
								<div class="col-xs-4">
									<select name="parameter" id="parameter" class="form-control">
										<option value>---Select---</option>
										<c:forEach items="${productTypes}" var="proType">
											<option value="${proType.productCode}">${proType.productCode}</option>

										</c:forEach>


									</select>
								</div>
							</div>
						</c:when>
					</c:choose>
					<c:choose>
						<c:when test="${ coreReport.parameter == 'CURRENCY' }">
							<%-- <div class="form-group">
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.startDate" /></label>
								</div>
								<div class="col-xs-4">


									<div class="input-group date datepicker">
										<input type="text" name="startDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.endDate" /></label>
								</div>
								<div class="col-xs-4">
									<div class="input-group date datepicker">
										<input type="text" name="endDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div> --%>
							<input type="hidden" name="startDate">
							<input type="hidden" name="endDate">
							<!-- cif id -->
<!-- 							<div class="form-group"> -->
<!-- 								<div class="col-xs-2 col-md-3"> -->
<%-- 									<form:label for="cif" path="cif" cssClass="control-label pull-right" --%>
<%-- 										cssErrorClass="control-label"> --%>
<%-- 										<fmt:message key="coreReport.cifId" /> --%>
<%-- 									</form:label> --%>
<!-- 								</div> -->
<!-- 								<div class="col-xs-4"> -->
<%-- 										<form:input path="cif" id="cif" cssClass="form-control validate[required]" --%>
<%-- 										cssErrorClass="form-control has-error validate[required]" readonly="true" /> --%>
<%-- 									<form:errors path="cif" cssClass="has-error" /> --%>
<!-- 								</div> -->
<!-- 							</div> -->
							<!-- cif id -->
							<div class="form-group">
								<div class="col-xs-2">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.currency" /></label>
								</div>
								<div class="col-xs-4">
									<select name="parameter" id="parameter" class="form-control">
										<option value>---Select---</option>
										<c:forEach items="${currencys}" var="currency">
											<option value="${currency.id}" ${currency.id==localCurrency?'selected="selected"': ''}>${currency.id}</option>
										</c:forEach>

									</select>
								</div>
								
								<div class="col-xs-2 ">
									<label for="branch" class="control-label pull-right"><fmt:message
											key="coreReport.branch" /></label>
								</div>
								<div class="col-xs-4">
									<table  class="table table-condensed table-striped table-bordered"  id="branches">
				                      <thead>
				                      <tr>
				                          <th class="table_checkbox"><input type="checkbox" id="checkBranch" class="allBranch"/></th>
				                          <th><fmt:message key='coreBranch.name'/></th>
				                      </tr>
				                      </thead>
				                      <tbody>
				                      <c:forEach var="coreBranch" items="${branchUser}">
				                          <tr>
				                              <td><input type="checkbox" checked name="branchId" class="checkBranch" value="${coreBranch}"  /></td>
				                              <td>${coreBranch}</td>
				                          </tr>
				                      </c:forEach>
				                      </tbody>
				                  </table>
								</div>
								
							</div>
						</c:when>
					</c:choose>
					<c:choose>
						<c:when test="${ coreReport.parameter == 'ACCOUNT' }">
							<div class="form-group">
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.startDate" /></label>
								</div>
								<div class="col-xs-4">
									<div class="input-group date datepicker">
										<input type="text" name="startDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.endDate" /></label>
								</div>
								<div class="col-xs-4">
									<div class="input-group date datepicker">
										<input type="text" name="endDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div>
							<!-- cif id -->
							<div class="form-group">
								<div class="col-xs-2">
									<label for="userId" class="control-label pull-right"><fmt:message
											key="coreReport.account" /></label>
								</div>
								<div class="col-xs-4">
<!-- 									<select name="parameter" id="parameter" class="form-control validate[required]"> -->
<!-- 										<option value>---Select---</option> -->
<%-- 										<c:forEach items="${accounts}" var="account"> --%>
<%-- 											<option value="${account}">${account}</option> --%>
<%-- 										</c:forEach> --%>

<!-- 									</select> -->
									<input type="text" onkeypress="return event.keyCode != 13;"  name="parameter" id="parameter" class="form-control validate[required]" />
								</div>
							</div>
							<!-- cif id -->
							<div class="form-group">
								<div class="col-xs-2">
									<label for="cif" class="control-label pull-right"><fmt:message
											key="coreReport.cifId" /></label>
								</div>
								<div class="col-xs-4">
									 <input type="text" name="cif" id="cif" class="form-control validate[required]" readonly="false"/>
								
								</div>
							</div>
						</c:when>
					</c:choose>
					<c:choose>
						<c:when test="${ coreReport.parameter == 'DATE' }">

							<div class="form-group">
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.startDate" /></label>
								</div>
								<div class="col-xs-4">
									<input type="hidden" name="parameter">
									<div class="input-group date datepicker">
										<input type="text" name="startDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.endDate" /></label>
								</div>
								<div class="col-xs-4">
									<div class="input-group date datepicker">
										<input type="text" name="endDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<!-- cif id -->
<!-- 								<div class="form-group"> -->
<!-- 									<div class="col-xs-2"> -->
<%-- 										<form:label for="cif" path="cif" cssClass="control-label pull-right" --%>
<%-- 											cssErrorClass="control-label"> --%>
<%-- 											<fmt:message key="coreReport.cifId" /> --%>
<%-- 										</form:label> --%>
<!-- 									</div> -->
<!-- 									<div class="col-xs-4 col-md-4"> -->
<%-- 											<form:input path="cif" id="cif" cssClass="form-control validate[required]" --%>
<%-- 											cssErrorClass="form-control has-error validate[required]" readonly="true" /> --%>
<%-- 										<form:errors path="cif" cssClass="has-error" /> --%>
<!-- 									</div> -->
<!-- 								</div> -->
								<!-- cif id -->
							</div>

						</c:when>
					</c:choose>

					<c:choose>
						<c:when test="${ coreReport.parameter == 'BRANCH' }">
							<%-- <div class="form-group">
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.startDate" /></label>
								</div>
								<div class="col-xs-4">
									<div class="input-group date datepicker">
										<input type="text" name="startDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.endDate" /></label>
								</div>
								<div class="col-xs-4">
									<div class="input-group date datepicker">
										<input type="text" name="endDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div> --%>
							<input type="hidden" name="startDate">
							<input type="hidden" name="endDate">
							<!-- cif id -->
<!-- 							<div class="form-group"> -->
<!-- 								<div class="col-xs-2"> -->
<%-- 									<form:label for="cif" path="cif" cssClass="control-label pull-right" --%>
<%-- 										cssErrorClass="control-label"> --%>
<%-- 										<fmt:message key="coreReport.cifId" /> --%>
<%-- 									</form:label> --%>
<!-- 								</div> -->
<!-- 								<div class="col-xs-4 col-md-4"> -->
<%-- 										<form:input path="cif" id="cif" cssClass="form-control validate[required]" --%>
<%-- 										cssErrorClass="form-control has-error validate[required]" readonly="true" /> --%>
<%-- 									<form:errors path="cif" cssClass="has-error" /> --%>
<!-- 								</div> -->
<!-- 							</div> -->
							<!-- cif id -->
							<div class="form-group">
								<div class="col-xs-2">
									<label for="userId" class="control-label pull-right"><fmt:message
											key="coreReport.branch" /></label>
								</div>
								<div class="col-xs-4">
									<select name="parameter" id="parameter" class="form-control">
										<option value>---Select---</option>
										<c:forEach items="${branch}" var="coreBranch">
											<option value="${coreBranch.id}">${coreBranch.id}</option>
										</c:forEach>

									</select>
								</div>
							</div>
						</c:when>
					</c:choose>
					
					<c:choose>
						<c:when test="${ coreReport.parameter == 'JOURNAL' }">
							<div class="form-group">
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.startDate" /></label>
								</div>
								<div class="col-xs-4">
									<input type="hidden" name="parameter">
									<div class="input-group date datepicker">
										<input type="text" name="startDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.endDate" /></label>
								</div>
								<div class="col-xs-4">
									<div class="input-group date datepicker">
										<input type="text" name="endDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div>
	
						
						</c:when>
					</c:choose>
					
					
					<c:choose>
						<c:when test="${ coreReport.parameter == 'TRXRECORD' }">
							<div class="form-group">
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.startDate" /></label>
								</div>
								<div class="col-xs-4">
									<input type="hidden" name="parameter">
									<div class="input-group date datepicker">
										<input type="text" name="startDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.endDate" /></label>
								</div>
								<div class="col-xs-4">
									<div class="input-group date datepicker">
										<input type="text" name="endDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div>
							
							<div class="form-group">

								<div class="col-xs-2 ">
									<label for="branch" class="control-label pull-right"><fmt:message
											key="coreReport.branch" /></label>
								</div>
								<div class="col-xs-4">
									<table  class="table table-condensed table-striped table-bordered"  id="branches">
				                      <thead>
				                      <tr>
				                          <th class="table_checkbox"><input type="checkbox" id="checkBranch" class="allBranch"/></th>
				                          <th><fmt:message key='coreBranch.name'/></th>
				                      </tr>
				                      </thead>
				                      <tbody>
				                      <c:forEach var="coreBranch" items="${branchUser}">
				                          <tr>
				                              <td><input type="checkbox" checked name="branchId" class="checkBranch" value="${coreBranch}"  /></td>
				                              <td>${coreBranch}</td>
				                          </tr>
				                      </c:forEach>
				                      </tbody>
				                  </table>
								</div>
								
							</div>
	
						
						</c:when>
					</c:choose>
					
					<c:choose>
						<c:when test="${ coreReport.parameter == 'SINGLEDATE' }">
							<div class="form-group">
								<div class="col-xs-2 ">
									<label for="accountOfficer" class="control-label pull-right"><fmt:message
											key="coreReport.startDate" /></label>
								</div>
								<div class="col-xs-4">
									<input type="hidden" name="parameter">
									<input type="hidden" name="endDate">
									<div class="input-group date datepicker">
										<input type="text" name="startDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div>
	
						
						</c:when>
					</c:choose>
					
					<c:choose>
						<c:when test="${ coreReport.parameter == 'BSA_ACCOUNT' }">
							<div class="form-group">
								<div class="col-xs-2">
									<label for="accountNumber" class="control-label pull-right"><fmt:message
											key="coreReport.accountNumber" /></label>
								</div>
								<div class="col-xs-4 col-md-4">
										<input type="text" name="parameter" id="accountNumber"
											Class="form-control validate[required]" />
								</div>	
							</div>
						
							<div class="form-group">
							
								<div class="col-xs-2">
									<label for="startDate" class="control-label pull-right"><fmt:message
											key="coreReport.startDate" /></label>
								</div>
								<div class="col-xs-4">
									<div class="input-group date datepicker">
										<input type="text" name="startDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-xs-2">
									<label for="endDate" class="control-label pull-right"><fmt:message
											key="coreReport.endDate" /></label>
								</div>
								<div class="col-xs-4">
									<div class="input-group date datepicker">
										<input type="text" name="endDate"
											Class="datepicker form-control validate[required]" /> <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								
							</div>	

						</c:when>
					</c:choose>
					<c:choose>
						<c:when test="${ coreReport.parameter == 'CIF' }">
							<input type="hidden" name="startDate">
							<input type="hidden" name="endDate">
							<div class="form-group">
								<div class="col-xs-2">
									<label for="userId" class="control-label pull-right"><fmt:message
											key="coreReport.cifId" /></label>
								</div>
								<div class="col-xs-4">
									<input type="text" onchange="getCif();" class="form-control" id="cif" name="parameter">
								</div>
							</div>
						</c:when>
					</c:choose>
					<div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-12">
								<div class="pull-center" style="text-align: center">
									<input type="submit" class="btn btn-primary" name="download"
										id="save" value="<fmt:message key="button.download"/>"  /> <a
										href="<c:url value='/core/report' />" class="btn btn-primary"
										id="cancel" />
									<fmt:message key="button.cancel" />
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="CifNotFoundModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" style="margin-top:200px">
		<div class="modal-content">
			<%-- <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					<fmt:message key="account.list" />
				</h4>
			</div> --%>
			<div class="modal-body" style="height:68px">
				<div class="text-center"><fmt:message key="cif.notfound.error" /></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script>
var url='${getCif}';
$("#parameter").change(function(){
$.ajax({
	url:url+$("#parameter").val(),
	context:{}
}).done(function(data){
	$("#cif").val(data.cif);
});
	
})
</script>