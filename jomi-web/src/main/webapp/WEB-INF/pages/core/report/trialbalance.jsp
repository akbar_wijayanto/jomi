<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreReport.title" /></title>
<meta name="heading" content="<fmt:message key='coreReport.heading'/>" />
<script>
$(function () {
    var checkAllBranch = $('input.allBranch');
    var checkAllCurrency = $('input.allCurrency');
    var checkboxesBranch = $('input.checkBranch');
    var checkboxesCurrency = $('input.checkCurrency');
    
//    $('input').iCheck();
    
    checkAllBranch.on('ifChecked ifUnchecked', function(event) {        
        if (event.type == 'ifChecked') {
        	checkboxesBranch.iCheck('check');
        } else {
        	checkboxesBranch.iCheck('uncheck');
        }
    });
    
    checkboxesBranch.on('ifChanged', function(event){
        if(checkboxesBranch.filter(':checked').length == checkboxesBranch.length) {
        	checkAllBranch.prop('checked', 'checked');
        } else {
        	checkAllBranch.removeProp('checked');
        }
        checkAllBranch.iCheck('update');
    });
    
    
    
    checkAllCurrency.on('ifChecked ifUnchecked', function(event) {        
        if (event.type == 'ifChecked') {
        	checkboxesCurrency.iCheck('check');
        } else {
        	checkboxesCurrency.iCheck('uncheck');
        }
    });
    
    checkboxesBranch.on('ifChanged', function(event){
        if(checkboxesCurrency.filter(':checked').length == checkboxesCurrency.length) {
        	checkAllCurrency.prop('checked', 'checked');
        } else {
        	checkAllCurrency.removeProp('checked');
        }
        checkAllCurrency.iCheck('update');
    });
});

</script>
</head>

<div class="row">
	<div class="col-xs-12">
		<div id="breadcumbTitle">
			<fmt:message key="coreReport.message.download" /> | ${coreReport.reportName }
		</div>
		<spring:bind path="coreReport.*">
			<c:if test="${not empty status.errorMessages}">
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">&times;</button>
					<c:forEach var="error" items="${status.errorMessages}">
						<c:out value="${error}" escapeXml="false" />
						<br />
					</c:forEach>
				</div>
			</c:if>
		</spring:bind>

		<div class="box">
			<spring:url var="actions" value='/core/report/trialbalance/${type}/${id}' />
				<form method="post" action="${actions}" id="coreReport"
				class="form-horizontal">
				<input type="hidden" name="from"
					value="<c:out value="${param.from}"/>" />
					<div class="box-body">
					<div class="form-group">
						<div class="col-xs-2 ">
							<label for="effectiveDate" class="control-label pull-right"><fmt:message
									key="coreReport.effectiveDate" /></label>
						</div>
						<div class="col-xs-4">
							<div class="input-group date datepicker">
								<input type="text" name="effectiveDate"
									Class="datepicker form-control validate[required]" /> <span
									class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-2 ">
							<label for="branch" class="control-label pull-right"><fmt:message
									key="coreReport.branch" /></label>
						</div>
						<div class="col-xs-4">
							<table  class="table table-condensed table-striped table-bordered"  id="branches">
		                      <thead>
		                      <tr>
		                          <th class="table_checkbox"><input type="checkbox" id="checkBranch" class="allBranch"/></th>
		                          <th><fmt:message key='coreBranch.name'/></th>
		                      </tr>
		                      </thead>
		                      <tbody>
		                      <c:forEach var="cp" items="${branches}">
		                          <tr>
		                              <td><input type="checkbox" checked name="branchId" class="checkBranch" value="${cp.id}"  /></td>
		                              <td>${cp.id} - ${cp.name}</td>
		                          </tr>
		                      </c:forEach>
		                      </tbody>
		                  </table>
						</div>
					</div>
					
					<%-- <div class="form-group">
						<div class="col-xs-2 ">
							<label for="currency" class="control-label pull-right"><fmt:message
									key="coreReport.currency" /></label>
						</div>
						<div class="col-xs-4">
							<select name="currencyId"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]">
								<c:forEach items="${currencies}" var="cp">
									<option value="${cp.id}" ${cp.id==localCurrency?'selected="selected"': ''}>${cp.id} - ${cp.name}</option>
								</c:forEach>
							<select>
							
						</div>
					</div> --%>
					
					<div class="form-group">
						<div class="col-xs-2 ">
							<label for="currency" class="control-label pull-right"><fmt:message
									key="coreReport.currency" /></label>
						</div>
						<div class="col-xs-4">
							<table  class="table table-condensed table-striped table-bordered"  id="currencies">
		                      <thead>
		                      <tr>
		                          <th class="table_checkbox"><input type="checkbox" id="checkCurrency" class="allCurrency"/></th>
		                          <th><fmt:message key='coreCurrency.name'/></th>
		                      </tr>
		                      </thead>
		                      <tbody>
		                      <c:forEach var="cp" items="${currencies}">
		                          <tr>
		                              <td><input type="checkbox" name="currencyId" class="checkCurrency" value="${cp.id}" ${cp.id==localCurrency?'checked': ''}  /></td>
		                              <td>${cp.id} - ${cp.name}</td>
		                          </tr>
		                      </c:forEach>
		                      </tbody>
		                  </table>
						</div>
					</div>
					
					<div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-12">
								<div class="pull-center" style="text-align: center">
									<input type="submit" class="btn btn-primary" name="download"
										id="save" value="<fmt:message key="button.download"/>" />
									<button type="submit" class="btn btn-primary" name="view"
										id="save"><fmt:message key="button.view"/></button>
										 <a
										href="<c:url value='/core/report' />" class="btn btn-primary"
										id="cancel" />
									<fmt:message key="button.cancel" />
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>



<script>
var url='${getCif}';
$("#parameter").change(function(){
$.ajax({
	url:url+$("#parameter").val(),
	context:{}
}).done(function(data){
	$("#cif").val(data.cif);
});
	
})
</script>