<%@ include file="/common/taglibs.jsp"%>


<head>
    <title><fmt:message key="403.heading"/></title>
    <meta name="heading" content="<fmt:message key='403.heading'/>"/>
    <meta name="decorator" content="failure">
    <script type="text/javascript">
		function goBack(){
			window.history.back();
		}
	</script>
</head>
<!-- Error info area -->
<div class="error-page">
<!--     <h2 class="headline text-info"> 403</h2> -->
    <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> <fmt:message key="403.title"/></h3>
        <p style="margin-bottom: 35px;">
            <fmt:message key="403.message" >
                <fmt:param><c:url value='/mainMenu' /></fmt:param>
            </fmt:message>
        </p>
    </div><!-- /.error-content -->
    <div class="alert alert-danger">
    	<i class="fa fa-ban"></i>
		<c:if test="${not empty errors}">
<%-- 			<c:forEach var="error" items="${errors}"> --%>
<%-- 				<c:out value="${error}"/><br /> --%>
<%-- 			</c:forEach> --%>
			<c:remove var="errors" scope="session"/>
		</c:if>
		<fmt:message key="error.403"/><br />
	</div>
</div><!-- /.error-page -->
