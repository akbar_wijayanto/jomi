<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="authentication.title"/></title>
    <meta name="heading" content="<fmt:message key='authentication.heading'/>"/>
    <script>
		function myFunction() {
    		return confirm('Are you sure want to delete this session?');
		}
	</script>
</head>
<div class="row">
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='authentication.message.list'/></div>
	        <div class="box-body table-responsive">
        	<table id="tableList" class="table table-bordered table-striped">
                <thead>
                    <tr>
                    	<th><fmt:message key='authentication.username'/></th>
                        <th><fmt:message key='authentication.sessionId'/></th>
                        <th><fmt:message key='authentication.lastRequest'/></th>
                        <th width="80px">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                   <c:forEach var="session" items="${activeSessions}">
                        <tr>
                        	<td>${session.principal.coreUser.username}</td>
                            <td>${session.sessionId}</td>
                            <td>${session.lastRequest}</td>
                            <td>
                            	<c:set var="sessionId" value="${fn:replace(session.sessionId,'.', '_')}" />
                            	<a href="<c:url value='/authentication/expire/${sessionId}' />" title="Release" onclick="return myFunction()">Release</a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        	</div>
		</div>
    </div>
</div>