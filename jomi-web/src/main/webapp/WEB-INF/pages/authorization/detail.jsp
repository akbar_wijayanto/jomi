<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="authorization.title" /></title>
<meta name="heading"
	content="<fmt:message key='authorization.heading'/>" />
	<script type="text/javascript">
	function checkIsDate(val){
		var pattern=new RegExp(/^\d{2}-\d{2}-\d{4}/);
		if(pattern.test(val)){
			var splitSec=val.split(/\s/);
			var month=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
			var dateStr= splitSec[0].split(/-/);
			var date=new Date(dateStr[2],dateStr[1]-1,dateStr[0]);
			return date.getDate()+"-"+month[date.getMonth()]+"-"+date.getFullYear().toString().substr(2,2);
		}
		return val;
	} 
	$(document).ready(function() {
		var formText=document.getElementsByClassName("form-text");
		for(var i=0;i<formText.length;i++){
			formText[i].value=checkIsDate(formText[i].value);
			console.log(checkIsDate(formText[i].value));
		}
		
		$(".new").find("input[type=text]").each(function() {
			var objectold = $("#"+this.id+"_old").val();
			var objectnew = this.value;
			
			if(objectold == objectnew.replace(".00", "")){
				$("."+this.id+"_div_new").hide();
				$("."+this.id+"_div_old").hide();
			}else{
				$("."+this.id+"_div_new").show();
				$("."+this.id+"_div_old").show();
			}
		});

		$(".new").find("textarea").each(function() {
			var objectold = $("#"+this.id+"_old").val();
			var objectnew = this.value;
			
			if(objectold == objectnew.replace(".00", "")){
				$("."+this.id+"_div_new").hide();
				$("."+this.id+"_div_old").hide();
			}else{
				$("."+this.id+"_div_new").show();
				$("."+this.id+"_div_old").show();
			}
		});
	});
	
	</script>
</head>
<div class="row-fluid">
	<div class="span12">
		<div id="breadcumbTitle">
			<fmt:message key="authorization.message.authorize" />
		</div>

		<div class="box">
			<spring:url var="action"
				value='/authorize/detail/${group }/${newID}' />
			<form:form commandName="unauthorizeObject" method="post"
				action="${action}" id="unauthorizeObject" class="form-horizontal">
				<div class="box-body">
				<c:if test = "${type == 'UPDATE'}">
					<table style="width: 100%;">
						<tr style="text-align: center;">
							<th style="text-align: center; font-size:15px; width: 20%;"></th>
							
								<th style="text-align: left; font-size:15px; width: 30%;"><h4 align="right">
									<fmt:message key="authorization.oldObject" /> 
								</h4> </th>
							 
							 
								<th style="text-align: right; font-size:15px; width: 30%;"><h4 align="right">
									<fmt:message key="authorization.newObject" /> 
								</h4></th>	
							 					
						</tr>
						<c:forEach var="result" items="${dto.result }">
							
							<tr>
								<td style="border:none;">
									<label><fmt:message key="${classNameForMessage }.${result.key}" /> </label>
									<%-- <input type="text" class="form-control" value="<fmt:message key="${classNameForMessage }.${result.key}" />"  
										style="background-color: #FF851B; color: white; text-align: center; font-weight: 700;"/> --%>
								</td>
								<td style="border:none;">
									<c:choose>
										<c:when test="${result.value.useChildValue}">
											<div id="tblResult" style="width: 433px;height: 200px;">
											<table style="width: 100%" class="table table-bordered table-striped myTable">												
												<c:forEach var="resultChild" items="${result.value.childValues}">											
													<tr>																	
														<td>
															<b><fmt:message key="authorization.${resultChild.key}"/></b>
														</td>
														<c:forEach var="resultDetailChild" items="${resultChild.value.objA}">																																																		
																<c:choose>
																	<c:when test="${resultChild.value.isNumeric}">
																		<td style="text-align: right;">
																			${resultDetailChild }
																		</td>
																	</c:when>
																	<c:otherwise>
																		<td>
																			${resultDetailChild }
																		</td>
																	</c:otherwise>
																</c:choose>																																		
														</c:forEach>
													</tr>																																						
												</c:forEach>												
											</table>
											</div>
										</c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${result.value.objB[0]!=result.value.objA[0]}">
													<c:choose>
													<c:when test="${result.value.objA.size() <= 1}">
														<c:choose>
															<c:when test="${result.value.isNumeric}">
																<input type="text"  style="text-align: right;"
																	class="form-control different" value="${result.value.objA[0]}" readonly="readonly" />
															</c:when>
															<c:when test="${fn:contains(result.value.objA[0],'true')}">
																<input type="checkbox" class="different" checked disabled>
															</c:when>
															<c:when test="${fn:contains(result.value.objA[0],'false')}">
																<input type="checkbox" class="different"  disabled>
															</c:when>
															<c:otherwise>
																<input type="text"  class="form-control different" 
																	value="${result.value.objA[0]}" readonly="readonly" />
																
														 	</c:otherwise>
														 	
														</c:choose> 
													</c:when>
													 
													<c:otherwise>
														<input type="text"  class="form-control" 
																		value="${result.value.objA[0]}" readonly="readonly" />
													</c:otherwise>
												</c:choose>
											</c:when>
											<c:otherwise>
												<c:choose>
													<c:when test="${result.value.objA.size() <= 1}">
															<c:choose>
																<c:when test="${result.value.isNumeric}">
																	<input type="text"  style="text-align: right;"
																		class="form-control " value="${result.value.objA[0]}" readonly="readonly" />
																</c:when>
																<c:when test="${fn:contains(result.value.objA[0],'true')}">
																	<input type="checkbox"  checked disabled>
																</c:when>
																<c:when test="${fn:contains(result.value.objA[0],'false')}">
																	<input type="checkbox"   disabled>
																</c:when>
																<c:otherwise>
																	<input type="text"  class="form-control" 
																		value="${result.value.objA[0]}" readonly="readonly" />
																	
															 	</c:otherwise>
															 	
															</c:choose> 
														</c:when>
														 
														<c:otherwise>
															<input type="text"  class="form-control" 
																		value="${result.value.objA[0]}" readonly="readonly" />
														</c:otherwise>
												</c:choose>
												</c:otherwise>
											</c:choose>
											
										</c:otherwise>
									</c:choose>
								</td>
								<c:if test = "${type != 'NEW' && type != 'DELETE'}">
								<td style="border:none;">			
									<c:choose>
										<c:when test="${result.value.useChildValue}">
											<div id="tblResultNew" style="width: 433px;height: 200px;">
											<table  style="width: 100%" class="table table-bordered table-striped myTableNew">												
												<c:forEach var="resultChild" items="${result.value.childValues}">											
													<tr>																	
														<td>
															<b><fmt:message key="authorization.${resultChild.key}"/></b>
														</td>
														<c:forEach var="resultDetailChild" items="${resultChild.value.objB}">																																																		
																<c:choose>
																	<c:when test="${resultChild.value.isNumeric}">
																		<td style="text-align: right;">
																			${resultDetailChild }
																		</td>
																	</c:when>
																	<c:otherwise>
																		<td>
																			${resultDetailChild }
																		</td>
																	</c:otherwise>
																</c:choose>																																		
														</c:forEach>
													</tr>																																				
												</c:forEach>												
											</table>
											</div>
										</c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${result.value.objB[0]!=result.value.objA[0]}">
													<c:choose>
													<c:when test="${result.value.objB.size() <= 1}">
														<c:choose>
															<c:when test="${result.value.isNumeric}">
																<input type="text"  style="text-align: right;"
																	class="form-control different" value="${result.value.objB[0]}" readonly="readonly" />
															</c:when>
															<c:when test="${fn:contains(result.value.objB[0],'true')}">
																<input type="checkbox" class="different" checked disabled>
															</c:when>
															<c:when test="${fn:contains(result.value.objB[0],'false')}">
																<input type="checkbox" class="different"  disabled>
															</c:when>
															<c:otherwise>
																<input type="text"  class="form-control different" 
																	value="${result.value.objB[0]}" readonly="readonly" />
																
														 	</c:otherwise>
														 	
														</c:choose> 
													</c:when>
													 
													<c:otherwise>
														<input type="text"  class="form-control" 
																		value="${result.value.objB[0]}" readonly="readonly" />
													</c:otherwise>
												</c:choose>
											</c:when>
											<c:otherwise>
												<c:choose>
													<c:when test="${result.value.objB.size() <= 1}">
															<c:choose>
																<c:when test="${result.value.isNumeric}">
																	<input type="text"  style="text-align: right;"
																		class="form-control" value="${result.value.objB[0]}" readonly="readonly" />
																</c:when>
																<c:when test="${fn:contains(result.value.objB[0],'true')}">
																	<input type="checkbox"  checked disabled>
																</c:when>
																<c:when test="${fn:contains(result.value.objB[0],'false')}">
																	<input type="checkbox"   disabled>
																</c:when>
																<c:otherwise>
																	<input type="text"  class="form-control" 
																		value="${result.value.objB[0]}" readonly="readonly" />
																	
															 	</c:otherwise>
															 	
															</c:choose> 
														</c:when>
														 
														<c:otherwise>
															<input type="text"  class="form-control" 
																		value="${result.value.objB[0]}" readonly="readonly" />
														</c:otherwise>
												</c:choose>
												</c:otherwise>
											</c:choose>
											
										</c:otherwise>
									</c:choose>	
								</td>
								</c:if>
							</tr>
							
							
						</c:forEach>
					</table>
					</c:if>
					<c:if test = "${type == 'NEW' || type == 'DELETE'}">
					<table style="width: 70%;margin: 0px 100px 0px 100px;">
						<tr style="text-align: center;">
							<th style="text-align: center; font-size:15px; width: 30%;"></th>
							 
								<th style="text-align: right; font-size:15px;"><h4 align="right">
								</h4></th>	
							 					
						</tr>
						<c:forEach var="result" items="${dto.result }">
							<tr>
								<td style="border:none;">
									<label><fmt:message key="${classNameForMessage }.${result.key}" /></label>
									<%-- <input type="text" class="form-control" value="<fmt:message key="${classNameForMessage }.${result.key}" />"  
										style="background-color: #FF851B; color: white; text-align: center; font-weight: 700;"/> --%>
								</td>
								<td style="border:none;">
									<c:choose>
										<c:when test="${result.value.useChildValue}">
											<div id="tblResult" style="width: 100%;height: 200px;">
											<table  style="width: 100%" class="table table-bordered table-striped myTable">												
												<c:forEach var="resultChild" items="${result.value.childValues}">											
													<tr>																	
														<td>
															<b><fmt:message key="authorization.${resultChild.key}"/></b>
														</td>
														<c:forEach var="resultDetailChild" items="${resultChild.value.objA}">																																																		
																<c:choose>
																	<c:when test="${resultChild.value.isNumeric}">
																		<td style="text-align: right;">
																			${resultDetailChild }
																		</td>
																	</c:when>
																	<c:otherwise>
																		<td>
																			${resultDetailChild }
																		</td>
																	</c:otherwise>
																</c:choose>																																
														</c:forEach>
													</tr>																																						
												</c:forEach>												
											</table>
											</div>
										</c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${result.value.objA.size() <= 1}">
													<c:choose>
														<c:when test="${result.value.isNumeric}">
															<input type="text"  style="text-align: right;"
																class="form-control" value="${result.value.objA[0]}" readonly="readonly" />
														</c:when>
														<c:when test="${fn:contains(result.value.objA[0],'true')}">
															<input type="checkbox" checked disabled>
														</c:when>
														<c:when test="${fn:contains(result.value.objA[0],'false')}">
															<input type="checkbox"  disabled>
														</c:when>
														<c:otherwise>
															<input type="text"  class="form-control"
																value="${result.value.objA[0]}" readonly="readonly" />
													 	</c:otherwise>
													</c:choose> 
												</c:when>
												 
												<c:otherwise>
													<input type="text"  class="form-control" 
																		value="${result.value.objA[0]}" readonly="readonly" />
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
								</td>
								
							</tr>
						</c:forEach>
					</table>
					</c:if>
					<div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12">
								<div class="pull-right">
									<input type="button" class="btn btn-primary tigaButton"
										name="authorize" id="authorizeButton"
										value="<fmt:message key="button.approve"/>" /> <input
										type="button" class="btn btn-primary tigaButton" name="reject"
										id="rejectButton" value="<fmt:message key="button.reject"/>" />  
										<a onclick="goBack();"><input type="button" class="btn btn-primary" name="cancel" id="cancel" value="<fmt:message key="button.cancel"/>" /></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="isAuthorize" name="isAuthorize" />
				<input type="hidden" id="type" name="type" value="${type }"/>
			</form:form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$("#authorizeButton").click(function() {
		$("#isAuthorize").val("authorize");
		$("#unauthorizeObject").submit();
	});
	$("#rejectButton").click(function() {
		$("#isAuthorize").val("reject");
		$("#unauthorizeObject").submit();
	});
</script>

<script type="text/javascript">
	function goBack() {
	    window.history.go(-1);
	}
	$(".myTable").each(function() {
        var $this = $(this);
        var newrows = [];
        $this.find("tr").each(function(){
            var i = 0;
            $(this).find("td").each(function(){
                i++;
                if(newrows[i] === undefined) { newrows[i] = $("<tr></tr>"); }
                newrows[i].append($(this));
            });
        });
        $this.find("tr").remove();
        $.each(newrows, function(){
            $this.append(this);
        });
    });
	
	$(".myTableNew").each(function() {
        var $this = $(this);
        var newrows = [];
        $this.find("tr").each(function(){
            var i = 0;
            $(this).find("td").each(function(){
                i++;
                if(newrows[i] === undefined) { newrows[i] = $("<tr></tr>"); }
                newrows[i].append($(this));
            });
        });
        $this.find("tr").remove();
        $.each(newrows, function(){
            $this.append(this);
        });
    });
	
</script>

<style>
	.myTable,.myTableNew th, td{
		border: 1px solid black;
	    border-collapse: collapse;		    	   
    }
	#style{
		border: 1em;
	}
	.bootstrap-select.btn-group.show-tick .dropdown-menu li.selected a span.check-mark{
		display: none;
	}
	
	.bs-actionsbox{
		display: none;
	}
	
	.table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
		border: 1px solid #999;		
	}
	
	th {
		padding: 2px;
	}
	
	td {
		padding: 2px;
	}
	#tblResult , #tblResultNew{							
		border: 1px solid;			
		white-space:normal;
		overflow: scroll;		
		overflow-y: scroll;		
	}
	.different{
		color:#f37021;
	}
</style>