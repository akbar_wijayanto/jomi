<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key='authorization.title'/></title>
    <meta name="heading" content="<fmt:message key='authorization.heading'/>"/>
<%--     <script type="text/javascript" src="<c:url value='/scripts/custom/core.branch.js' />"></script> --%>
</head>
<div class="row">
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='authorization.message.list'/></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
			                <th><fmt:message key='authorization.tableName'/></th>
			                <th><fmt:message key='authorization.totalRecord'/></th>
			                <th>&nbsp;</th>
			            </tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="unauthorizeObject" items="${unauthorizeObjects}">
			            	<tr>
				            	<td>${unauthorizeObject.className}</td>
				            	<td>${unauthorizeObject.count}</td>
				            	<td><a href="<c:url value='/authorize/group/${unauthorizeObject.groupName}' />" title="See Detail">
				            		<i class="fa fa-arrow-circle-right"></i></a></td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
		</div>
    </div>
</div>