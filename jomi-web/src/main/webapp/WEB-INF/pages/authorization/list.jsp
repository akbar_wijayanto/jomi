<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key='authorization.title'/></title>
    <meta name="heading" content="<fmt:message key='authorization.heading'/>"/>
<%--     <script type="text/javascript" src="<c:url value='/scripts/custom/core.branch.js' />"></script> --%>
</head>
<div class="row">
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='Unauthorized List of ' />${parameterName }</div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
			                <th><fmt:message key='authorization.tableName'/></th>
			                <th><fmt:message key='authorization.record'/></th>
			                <th><fmt:message key='authorization.action'/></th>
			                <th>&nbsp;</th>
			            </tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="unauthorizeObject" items="${unauthorizeObjects}">
			            	<tr>
				            	<td>${unauthorizeObject.className}</td>
				            	<td>${unauthorizeObject.recordId}</td>
				            	<td>${unauthorizeObject.authorizationTypeEnum.label}</td>
				            	<td><a href="<c:url value='/authorize/detail/${group }/${unauthorizeObject.id}' />" title="See Detail">
				            		<i class="fa fa-eye"></i></a></td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
		        <div class="pull-right">
				    <a href="${ctx}/authorize"><input type="button" class="btn btn-primary" name="back" id="back" value="<fmt:message key="button.back"/>" /></a>
			</div>
		</div>
    </div>
</div>