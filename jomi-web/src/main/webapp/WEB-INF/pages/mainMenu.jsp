<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="mainMenu.title" /></title>
<meta name="heading" content="<fmt:message key='mainMenu.heading'/>" />
</head>

<div class="box" style="border-bottom: 5px solid #AA0000; border-right: 0px solid; border-left: 0px solid; font-family: fs_albert;">
	<div class="box-header">
		<div id="breadcumbTitle" class="box-title">
			<fmt:message key='mainMenu.welcome' />
			${coreUser.username}
		</div>
		<h3 class="box-title" style="font-family: fs_albert;">
			<fmt:message key='mainMenu.userInformation' />
		</h3>
	</div>
	
	<div class="row noMargin">
<!-- 			<div class="col-sm-12 noPadding"> -->
<!-- 				<div class="row"> -->
<%-- 					<div class="col-sm-6 marginT10"><fmt:message key='mainMenu.applicationDate' />: <span class="f20"><fmt:formatDate pattern="${format.format}" value="${htoday}" /> </span> </div> --%>
<%-- 					<div class="col-sm-6 marginT10" style="text-align:right;"><fmt:message key='mainMenu.serverDate' />: <span class="f20"><fmt:formatDate pattern="${format.format}" value="${hserverdate}" /></span></div> --%>
<!-- 				</div> -->
<!-- 			</div> -->
			
			<div class="col-sm-12 marginT10">
				<div class="row blueBox">
					<div class="col-sm-6"><fmt:message key='user.fullName' />: <span class="f20">${coreUser.firstName} ${coreUser.lastName}</span></div>
<%-- 					<div class="col-sm-6" style="text-align:right;"><fmt:message key='user.activeBranch' />: <span class="f20">${coreUser.activeBranch.id} / ${coreUser.activeBranch.name}</span></div> --%>
				</div>
			</div>
			
			
			<div class="col-sm-6 noPadding">
				<div class="row">
					<div class="col-sm-12 marginT10"><fmt:message key='user.activeRole' />: 
						<span class="f20"><c:forEach var="role" items="${coreUser.coreRoles}"> ${role.description}<br> </c:forEach></span>
					</div>
					
					<div class="col-sm-12 marginT10"> <fmt:message key='user.lastLogon' />: 
						<span class="f20"><fmt:formatDate pattern="${format.format} HH:mm:ss" value="${coreUser.lastLogon}" /> </span>
					</div> 
				</div>
			</div>
			
<!-- 			<div class="col-sm-6 noPadding marginT10" style="text-align:right;"> -->
<%-- 				<div><fmt:message key='user.limitAmount' />:</div> --%>
<%-- 				<div class="f24">${coreUser.currency.id} <fmt:formatNumber type="number" pattern="${curr}" value="${coreUser.limitAmount}" /></div> --%>
<!-- 			</div> -->
	</div>
	
</div>