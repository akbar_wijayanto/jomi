<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/common/taglibs.jsp" %>
<html xmlns="http://www.w3.org/1999/xhtml" style="overflow: hidden;">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="heading" content="<fmt:message key='login.heading'/>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><fmt:message key="login.title"/></title>
    <link rel="shortcut icon" href="<c:url value='/images/favicon.ico' />" />
	
    <link rel="stylesheet" href="<c:url value='/styles/bootstrap.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/font-awesome.css'/>" />
    <link rel="stylesheet" href="<c:url value='/styles/AdminLTE.css' />" />
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="<c:url value='/styles/ie/ie.css' />" />
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="<c:url value='/scripts/ie/html5.js' />"></script>
    <script src="<c:url value='/scripts/ie/respond.min.js' />"></script>
    <script src="<c:url value='/scripts/excanvas.min.js' />"></script>
    <![endif]-->
</head>
<body style="background: transparent;">
<div id="header">
	<div id="headerimgs">
		<div id="headerimg1" class="headerimg"></div>
		<div id="headerimg2" class="headerimg"></div>
	</div>
	

    <div class="form-box" id="login_box">
    	<div class="header">
            <img class="logo" src="<c:url value="/images/jomi-logo.png"/>"></img>
        </div>
        <%-- <spring:url var = "action" value='j_spring_security_check' /> --%>
        <form action="<c:url value="/j_spring_security_check" />" method="post" id="loginForm">
	 		<div class="body">
	 			<c:if test="${sessionScope['SPRING_SECURITY_LAST_EXCEPTION'].getClass().getSimpleName() == 'AuthenticationRequiredFieldsException'}">
					<c:set var="errorUsername"  value="${sessionScope['SPRING_SECURITY_LAST_EXCEPTION'].getErrors().getFieldError('j_username').getDefaultMessage()}"/>
					<c:set var="errorPassword"  value="${sessionScope['SPRING_SECURITY_LAST_EXCEPTION'].getErrors().getFieldError('j_password').getDefaultMessage()}"/>
				</c:if>
				<c:if test='${not empty sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}'>
					<div class="alert alert-danger alert-dismissable" style="margin-top:10px;">
						<i class="fa fa-ban"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<c:forEach var="error" items='${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}'>
							<c:out value="${error}" escapeXml="false"/><br />
						</c:forEach>
				    </div>
				    <c:remove var="errors"/>
				</c:if>
				<div class="form-group">
				    <input type="text" name="j_username" id="username" class="form-control validate[required]" placeholder="Username"/>
				</div>
				<div class="form-group">
				    <input type="password" name="j_password" id="password" class="form-control validate[required]" placeholder="Password"/>
				</div>          
	        </div>
	
	        <div class="footer">                                                               
	            <button type="submit" class="btn bg-blue-casa btn-block">Sign in</button>  
	        </div>
        </form>
    </div>
</div>
  
    <script type="text/javascript" src="<c:url value='/scripts/jquery.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/scripts/bootstrap.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/scripts/script-background.js' />"></script>

</body>
</html>