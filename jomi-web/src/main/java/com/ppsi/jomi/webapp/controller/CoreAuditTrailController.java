//package com.ppsi.jomi.webapp.controller;
//
////import org.apache.commons.lang.StringUtils;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//
//import javax.servlet.http.HttpServletRequest;
//
//import org.apache.commons.lang.math.NumberUtils;
//import org.apache.shiro.authz.annotation.RequiresPermissions;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import com.ppsi.jomi.Constants;
//import com.ppsi.jomi.dto.DataTables;
//import com.ppsi.jomi.persistence.model.AuditTrail;
//import com.ppsi.jomi.service.CoreAuditTrailManager;
//import com.ppsi.jomi.service.CoreSystemManager;
//
//
//@Controller
//@RequestMapping("/core/audittrail")
//public class CoreAuditTrailController extends BaseFormController {
//
//    private CoreAuditTrailManager coreAuditTrailManager;
//    
////    @Autowired
////    private ReportAuditTrail reportAuditTrail;
//    
//    @Autowired
//    private CoreSystemManager coreSystemManager;
//    
////    @Autowired
////    private ReportUtil reportUtil;
//
//    @Autowired
//    public void setCoreAuditTrailManager(CoreAuditTrailManager coreAuditTrailManager) {
//        this.coreAuditTrailManager = coreAuditTrailManager;
//    }
//
//
//    public CoreAuditTrailController() {
//        setCancelView("redirect:/core/audittrail");
//        setSuccessView("redirect:/core/audittrail");
//    }
//
//
////    @RequiresPermissions("CORE:AUDITTRAIL:READ:*")
//    @RequestMapping("")
//    public String viewData(Model model, HttpServletRequest request ) {
//    	SimpleDateFormat sdf = new SimpleDateFormat(getText("date.format", request.getLocale()));
////    	Date today = coreSystemManager.getCurrentSystem().getTodayDate();
//        model.addAttribute("coreAuditTrail", coreAuditTrailManager.getAll());
//        model.addAttribute("dateFrom", sdf.format(new Date()));
//        model.addAttribute("dateTo", sdf.format(new Date()));
//        return "/core/audittrail/list";
//    }
//
////    @RequiresPermissions("CORE:AUDITTRAIL:READ:*")
//    @RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
//    public String getView(@PathVariable("id") Long id, Model model) {
//        model.addAttribute("coreAuditTrail", coreAuditTrailManager.get(id));
//        return "/core/audittrail/view";
//    }
//
//    @RequestMapping(value = "/searchAuditTrail", method = RequestMethod.POST)
//    public 
//    @ResponseBody 
//    List<AuditTrail> getCustomAuditTrail(
//    		@RequestParam("objectName") String objectName, 
//    		@RequestParam("fieldName") String fieldName,
//    		@RequestParam("actor") String actor, 
//    		@RequestParam("action") String action,
//    		@RequestParam("dateFrom") String dtFrom,
//    		@RequestParam("dateTo") String dtTo, HttpServletRequest request) throws Exception{
//    	SimpleDateFormat sdf = new SimpleDateFormat(getText("date.format", request.getLocale()));
//    	Boolean passed = true;
//        Date dateFrom = null;
//        Date dateTo = null;
//
//        if (!dtFrom.isEmpty() && !dtTo.isEmpty()) {
//            try {
//               
////                dateFrom = sdf.parse(request.getParameter("dateFrom"));
////                dateTo = sdf.parse(request.getParameter("dateTo"));
//                dateFrom = sdf.parse(dtFrom);
//                dateTo = sdf.parse(dtTo);
//
//            } catch (ParseException e) {
//                passed = passed && false;
//                dateFrom = sdf.parse("01-01-1990");
//                dateTo = sdf.parse(new Date().toString());
//            }
//        }
//        List<AuditTrail> listAuditTrails =  coreAuditTrailManager.getSearchAuditTrail(objectName, fieldName, dateFrom, dateTo, actor, action);
//        
//    	return listAuditTrails;
//    }
//    
//    @RequestMapping(value = "/page", method = RequestMethod.GET)
//    public @ResponseBody DataTables<AuditTrail> page(
//    		@RequestParam("objectName") String objectName, 
//    		@RequestParam("fieldName") String fieldName,
//    		@RequestParam("actor") String actor, 
//    		@RequestParam("action") String action,
//    		@RequestParam("dateFrom") String dtFrom,
//    		@RequestParam("dateTo") String dtTo,
//    		HttpServletRequest request) throws Exception {
//        Boolean passed = true;
//        DataTables<AuditTrail> auditTrails = new DataTables<AuditTrail>();
//        SimpleDateFormat sdf = new SimpleDateFormat(getText("date.format", request.getLocale()));
//        
//        Long iDisplayStart = NumberUtils.toLong(request.getParameter("iDisplayStart"));
//        Long sEcho = NumberUtils.toLong(request.getParameter("sEcho"));
//        Long iDisplayLength = NumberUtils.toLong(request.getParameter("iDisplayLength"));
//        if (iDisplayLength == null) iDisplayLength = NumberUtils.toLong(Constants.PAGING_MAX_RECORD.toString());
//
//        Long recordCount = 0L;
//        
//        Date dateFrom = null;
//        Date dateTo = null;
//        if (!dtFrom.isEmpty() && !dtTo.isEmpty()) {
//            try {
//                dateFrom = sdf.parse(dtFrom);
//                dateTo = sdf.parse(dtTo);
//
//            } catch (ParseException e) {
//                passed = passed && false;
//                dateFrom = sdf.parse("01-01-1990");
//                dateTo = sdf.parse(new Date().toString());
//            }
//        }
//        
//        List<AuditTrail> trails = coreAuditTrailManager.getSearchAuditTrail(iDisplayStart, iDisplayLength, objectName, fieldName, dateFrom, dateTo, actor, action);
//        
//        recordCount = coreAuditTrailManager.getCountAudit(objectName, fieldName, dateFrom, dateTo, actor, action);
//        auditTrails.setAaData(trails);
//        auditTrails.setiTotalDisplayRecords(recordCount);
//        auditTrails.setiTotalRecords(recordCount);
//        auditTrails.setsEcho(sEcho);
//        
//        return auditTrails;
//    }
//}
