package com.ppsi.jomi.webapp.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ppsi.jomi.exception.JomiException;
import com.ppsi.jomi.report.dto.ReportTaskDto;
import com.ppsi.jomi.report.service.ProjectReportService;
import com.ppsi.jomi.report.util.ClientReportGenerator;
import com.ppsi.jomi.service.CoreProjectManager;
import com.ppsi.jomi.webapp.util.FileDownloader;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date May 7, 2017 2:19:02 PM
 */
@Controller
@RequestMapping("/core/project/task/report")
public class ReportProjectTaskController extends BaseFormController {

	private CoreProjectManager coreProjectManager;
	
	private ProjectReportService projectReportService;
	
	@Autowired
	public void setProjectReportService(ProjectReportService projectReportService) {
		this.projectReportService = projectReportService;
	}
	
	@Autowired
	public void setCoreProjectManager(CoreProjectManager coreProjectManager) {
		this.coreProjectManager = coreProjectManager;
	}
	
	public ReportProjectTaskController() {
		setCancelView("redirect:/core/project/task/report");
		setSuccessView("redirect:/core/project/task/report");
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String view(Model model) throws Exception {
		model.addAttribute("projectTasks", new ArrayList<ReportTaskDto>());
		model.addAttribute("coreProjects", coreProjectManager.getAllLive());
		return "/core/project/task/report/list";
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public String reportPost(@RequestParam(value = "projectId", required = false) Long projectId, HttpServletRequest request,
			Model model) throws Exception {
		
		model.addAttribute("coreProjects", coreProjectManager.getAllLive());
		try {
			model.addAttribute("projectTasks", projectReportService.getProjectTaskReport(projectId));
			return "/core/project/task/report/list";
		} catch (Exception e) {
			return "/core/project/task/report/list";
		}
	}

	@RequestMapping(value = "/download/all/{type}", method = { RequestMethod.POST, RequestMethod.GET })
	public String downloadAllData(@PathVariable("type") String type,
			@RequestParam(value = "projectId", required = false) Long projectId, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception {

		List<ReportTaskDto> projectTaskReport = new ArrayList<ReportTaskDto>();
		projectTaskReport = projectReportService.getProjectTaskReport(projectId);
		return getExport(projectTaskReport, type, response);
	}

	private <T> String getExport(List<T> dtos, String type, HttpServletResponse response) throws Exception {
		if (type.equals("pdf")) {
			response.setContentType("application/pdf");
			File downloadFile = getFile("report_project_task", "pdf", "report_project_task.jrxml", dtos);
			FileDownloader.downloadFile(downloadFile, response,
					FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
		}
		if (type.equals("xls")) {
			response.setContentType("application/vnd.xls");
			File downloadFile = getFile("report_project_task", "xls", "report_project_task.jrxml", dtos);
			FileDownloader.downloadFile(downloadFile, response,
					FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
		}
		return null;
	}

	private <T> File getFile(String fileName, String format, String jrxmlFileName, List<T> dtos) throws JomiException {
		return getData(fileName, jrxmlFileName, format, dtos);
	}

	private <T> File getData(String fileName, String jrxmlFileName, String format, List<T> list) throws JomiException {
		File downloadFile = null;
		if (format.equals("pdf")) {
			downloadFile = ClientReportGenerator.generatePDF(jrxmlFileName, list,
					ClientReportGenerator.readProperty("jrxml.tempDir") + fileName);
		}
		if (format.equals("xls")) {
			downloadFile = ClientReportGenerator.generateXLS(jrxmlFileName, list,
					ClientReportGenerator.readProperty("jrxml.tempDir") + fileName);
		}
		return downloadFile;
	}

}
