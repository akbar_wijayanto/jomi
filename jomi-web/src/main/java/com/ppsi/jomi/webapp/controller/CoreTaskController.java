package com.ppsi.jomi.webapp.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ppsi.jomi.enumeration.RecordStatusEnum;
import com.ppsi.jomi.enumeration.jomi.ComplexityEnum;
import com.ppsi.jomi.enumeration.jomi.PriorityEnum;
import com.ppsi.jomi.enumeration.jomi.TaskStatusEnum;
import com.ppsi.jomi.persistence.model.CoreTask;
import com.ppsi.jomi.persistence.model.TaskAssignment;
import com.ppsi.jomi.security.bean.UserUtil;
import com.ppsi.jomi.service.CoreProjectManager;
import com.ppsi.jomi.service.CoreTaskManager;
import com.ppsi.jomi.service.TaskAssignmentManager;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:25:42 AM
 */
@Controller
@RequestMapping(value="/core/task")
public class CoreTaskController extends BaseFormController {

	public CoreTaskController() {
		setCancelView("redirect:/core/task");
		setSuccessView("redirect:/core/task");
	}
	
	private CoreTaskManager coreTaskManager;
	private CoreProjectManager coreProjectManager;
	private TaskAssignmentManager taskAsssignmentManager;
	
	@Autowired
	public void setCoreTaskManager(CoreTaskManager coreTaskManager) {
		this.coreTaskManager = coreTaskManager;
	}
	
	@Autowired
	public void setCoreProjectManager(CoreProjectManager coreProjectManager) {
		this.coreProjectManager = coreProjectManager;
	}
	
	@Autowired
	public void setTaskAsssignmentManager(TaskAssignmentManager taskAsssignmentManager) {
		this.taskAsssignmentManager = taskAsssignmentManager;
	}
	
//	@PreAuthorize("hasAnyRole('CORE:TASK:READ:*')")
    @RequestMapping(value="")
	public String allDisplay(Model model) throws Exception{
		model.addAttribute("coreTasks", coreTaskManager.getAllLive());
		return "core/task/list";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:TASK:INPUT:*')")
    @RequestMapping(value="/add", method = RequestMethod.GET)
	public String addDisplay(Model model) throws Exception{
		model.addAttribute("coreTask", new CoreTask());
		model.addAttribute("priorityEnums", PriorityEnum.values());
		model.addAttribute("complexityEnums", ComplexityEnum.values());
		model.addAttribute("coreProjects", coreProjectManager.getAllLive());
		return "core/task/add";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:TASK:INPUT:*')")
    @RequestMapping(value="/add", method = RequestMethod.POST)
    public String addProcess(@Valid CoreTask coreTask, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
		if (errors.hasErrors() && request.getParameter("delete") == null) {
			model.addAttribute("coreTask", coreTask);
			model.addAttribute("priorityEnums", PriorityEnum.values());
			model.addAttribute("complexityEnums", ComplexityEnum.values());
			model.addAttribute("coreProjects", coreProjectManager.getAllLive());
        	return "core/task/add";
        }
		coreTaskManager.save(coreTask);
		saveMessage(request, getText("coreTask.saved", coreTask.getFeature(), request.getLocale()));

        return "redirect:/core/task";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:TASK:DELETE:*')")
    @RequestMapping(value="/delete/{id}", method = RequestMethod.GET)
	public String deleteDisplay(@PathVariable("id") Long id, Model model) throws Exception{
		model.addAttribute("coreTask", coreTaskManager.get(id));
		model.addAttribute("priorityEnums", PriorityEnum.values());
		model.addAttribute("complexityEnums", ComplexityEnum.values());
		model.addAttribute("coreProjects", coreProjectManager.getAllLive());
		return "core/task/delete";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:TASK:DELETE:*')")
    @RequestMapping(value="/delete", method = RequestMethod.POST)
	public String deleteProcess(CoreTask coreTask, Model model, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
		coreTask = coreTaskManager.get(coreTask.getId());
		coreTask.setStatus(RecordStatusEnum.HIST.getName());
		coreTask = coreTaskManager.save(coreTask);
		saveMessage(request, getText("coreTask.deleted", coreTask.getFeature(), request.getLocale()));
		return "redirect:/core/task";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:TASK:EDIT:*')")
    @RequestMapping(value="/edit/{id}", method = RequestMethod.GET)
	public String editDisplay(@PathVariable("id") Long id, Model model) throws Exception{
    	CoreTask coreTask = coreTaskManager.get(id);
		model.addAttribute("coreTask", coreTask);
		model.addAttribute("priorityEnums", PriorityEnum.values());
		model.addAttribute("complexityEnums", ComplexityEnum.values());
		model.addAttribute("coreProjects", coreProjectManager.getAllLive());
		model.addAttribute("coreProject", coreTask.getCoreProject());
		return "core/task/edit";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:TASK:EDIT:*')")
    @RequestMapping(value="/edit/{id}", method = RequestMethod.POST)
	public String editProcess(@Valid CoreTask coreTask, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
        if (errors.hasErrors() && request.getParameter("delete") == null) {
        	model.addAttribute("coreTask", coreTask);
			model.addAttribute("priorityEnums", PriorityEnum.values());
			model.addAttribute("complexityEnums", ComplexityEnum.values());
			model.addAttribute("coreProjects", coreProjectManager.getAllLive());
            return "core/task/edit";
        }
        CoreTask oldTask = coreTaskManager.get(coreTask.getId());
        coreTask.setCreatedBy(oldTask.getCreatedBy());
        coreTask.setCreatedTime(oldTask.getCreatedTime());
        coreTask.setStatus(oldTask.getStatus());
        coreTask.setUpdatedBy(UserUtil.getCurrentUsername());
        coreTask.setUpdatedTime(new Date());
        coreTask.setVersion(oldTask.getVersion());
        coreTask = coreTaskManager.save(coreTask);
        saveMessage(request, getText("coreTask.edited", coreTask.getFeature(), request.getLocale()));
        
        return "redirect:/core/task";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:TASK:READ:*')")
    @RequestMapping(value="/detail/{id}")
	public String detailDisplay(@PathVariable("id") Long id, Model model) throws Exception{
		model.addAttribute("coreTask", coreTaskManager.get(id));
		model.addAttribute("priorityEnums", PriorityEnum.values());
		model.addAttribute("complexityEnums", ComplexityEnum.values());
		model.addAttribute("coreProjects", coreProjectManager.getAllLive());
		return "core/task/detail";
	}
    
//	@PreAuthorize("hasAnyRole('CORE:TASK:EDIT:*')")
    @RequestMapping(value="/assignmember/{id}", method = RequestMethod.GET)
	public String assignMemberDisplay(@PathVariable("id") Long id, Model model) throws Exception{
    	CoreTask coreTask = coreTaskManager.get(id);
		model.addAttribute("coreTask", coreTask);
		model.addAttribute("priorityEnums", PriorityEnum.values());
		model.addAttribute("complexityEnums", ComplexityEnum.values());
		model.addAttribute("userProjects", coreTask.getCoreProject().getCoreUsers());
		return "core/task/assignmember";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:TASK:EDIT:*')")
    @RequestMapping(value="/assignmember/{id}", method = RequestMethod.POST)
	public String assignMemberProcess(@Valid CoreTask coreTask, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
        if (errors.hasErrors() && request.getParameter("delete") == null) {
        	model.addAttribute("coreTask", coreTask);
    		model.addAttribute("priorityEnums", PriorityEnum.values());
    		model.addAttribute("complexityEnums", ComplexityEnum.values());
    		model.addAttribute("userProjects", coreTask.getCoreProject().getCoreUsers());
            return "core/task/assignmember";
        }
        if (request.getParameter("userProject") != null) {
            TaskAssignment taskAssignment = new TaskAssignment();
            taskAssignment.setCoreTask(coreTask);
            taskAssignment.setCoreUser(getUserManager().get(Long.parseLong(request.getParameter("userProject"))));
            taskAssignment.setCreatedTime(new Date());
            taskAssignment.setCreatedBy(UserUtil.getCurrentUsername());
            taskAssignment.setStatus(RecordStatusEnum.LIVE.getName());
            taskAssignment.setTaskStatus(TaskStatusEnum.OPEN);
            taskAsssignmentManager.save(taskAssignment);
        }
        saveMessage(request, getText("coreTask.assign.member", coreTask.getFeature(), request.getLocale()));
        
        return "redirect:/core/task";
	}
    
}
