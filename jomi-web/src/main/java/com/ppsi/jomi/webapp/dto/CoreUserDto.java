/**
 * 
 */
package com.ppsi.jomi.webapp.dto;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 24, 2017 10:55:59 PM
 */
public class CoreUserDto {

	private Long id;
	private String firstName;
	private String lastName;
	private Boolean checklistStatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public Boolean getChecklistStatus() {
		return checklistStatus;
	}

	public void setChecklistStatus(Boolean checklistStatus) {
		this.checklistStatus = checklistStatus;
	}
	
}
