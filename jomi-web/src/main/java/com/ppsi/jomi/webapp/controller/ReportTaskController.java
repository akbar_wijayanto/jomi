package com.ppsi.jomi.webapp.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ppsi.jomi.exception.JomiException;
import com.ppsi.jomi.report.dto.ReportTaskDto;
import com.ppsi.jomi.report.service.TaskReportService;
import com.ppsi.jomi.report.util.ClientReportGenerator;
import com.ppsi.jomi.webapp.util.FileDownloader;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date May 7, 2017 2:19:02 PM
 */
@Controller
@RequestMapping("/core/task/report")
public class ReportTaskController extends BaseFormController {

	@Autowired
	private TaskReportService taskReportService;

	public ReportTaskController() {
		setCancelView("redirect:/core/task/report");
		setSuccessView("redirect:/core/task/report");
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String view(Model model) throws Exception {
		model.addAttribute("taskReports", new ArrayList<ReportTaskDto>());
		model.addAttribute("coreUsers", getUserManager().getAllLive());
		return "/core/task/report/list";
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public String reportPost(@RequestParam(value = "userId", required = false) Long userId,
			@RequestParam("startDate") String startDate,
			@RequestParam("endDate") String endDate, HttpServletRequest request,
			Model model) throws Exception {
		
		SimpleDateFormat spdf = new SimpleDateFormat("dd-MMM-yyyy");
		Date sDate = spdf.parse(startDate);
		Date eDate = spdf.parse(endDate);
		
		model.addAttribute("coreUsers", getUserManager().getAllLive());
//		model.addAttribute("startDate", new SimpleDateFormat("dd-MMM-yyyy").format(startDate));
//		model.addAttribute("endDate", new SimpleDateFormat("dd-MMM-yyyy").format(endDate));
		
		try {
			model.addAttribute("taskReports", taskReportService.getReportTasks(sDate, eDate, userId));
			return "/core/task/report/list";
		} catch (Exception e) {
			return "/core/task/report/list";
		}
	}

	@RequestMapping(value = "/download/all/{type}", method = { RequestMethod.POST, RequestMethod.GET })
	public String downloadAllData(@PathVariable("type") String type,
			@RequestParam(value = "userId", required = false) Long userId,
			@RequestParam("startDate") String startDate,
			@RequestParam("endDate") String endDate, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception {
		SimpleDateFormat spdf = new SimpleDateFormat("dd-MMM-yyyy");
		Date sDate = spdf.parse(startDate);
		Date eDate = spdf.parse(endDate);

		List<ReportTaskDto> taskReport = new ArrayList<ReportTaskDto>();
		taskReport = taskReportService.getReportTasks(sDate, eDate, userId);
		return getExport(taskReport, type, response);
	}

	private <T> String getExport(List<T> dtos, String type, HttpServletResponse response) throws Exception {
		if (type.equals("pdf")) {
			response.setContentType("application/pdf");
			File downloadFile = getFile("report_task_by_user", "pdf", "report_task_by_user.jrxml", dtos);
			FileDownloader.downloadFile(downloadFile, response,
					FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
		}
		if (type.equals("xls")) {
			response.setContentType("application/vnd.xls");
			File downloadFile = getFile("report_task_by_user", "xls", "report_task_by_user.jrxml", dtos);
			FileDownloader.downloadFile(downloadFile, response,
					FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
		}
		return null;
	}

	private <T> File getFile(String fileName, String format, String jrxmlFileName, List<T> dtos) throws JomiException {
		return getData(fileName, jrxmlFileName, format, dtos);
	}

	private <T> File getData(String fileName, String jrxmlFileName, String format, List<T> list) throws JomiException {
		File downloadFile = null;
		if (format.equals("pdf")) {
			downloadFile = ClientReportGenerator.generatePDF(jrxmlFileName, list,
					ClientReportGenerator.readProperty("jrxml.tempDir") + fileName);
		}
		if (format.equals("xls")) {
			downloadFile = ClientReportGenerator.generateXLS(jrxmlFileName, list,
					ClientReportGenerator.readProperty("jrxml.tempDir") + fileName);
		}
		return downloadFile;
	}

}
