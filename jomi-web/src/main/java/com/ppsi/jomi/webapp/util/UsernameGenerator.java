/**
 * 
 */
package com.ppsi.jomi.webapp.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * @author akbar.wijayanto
 * Date Oct 19, 2015 4:14:55 PM
 */
public class UsernameGenerator {

	public static final String generate(String nickname, Date dateOfBirth) {
		
		Calendar today = Calendar.getInstance(); 
		today.setTime(dateOfBirth);
		int month = today.get(Calendar.MONTH) + 1;
		int day = today.get(Calendar.DAY_OF_MONTH);
		String leadingZeroDay = day < 10 ? "0" + day : "" + day;
		String leadingZeroMonth = month < 10 ? "0" + month : "" + month;
		nickname = nickname.toLowerCase();
		String[] nameSplit = nickname.split(" ");
		String user = "";
		for (int i = 0; i < nameSplit.length; i++) {
			user = user.concat(nameSplit[i]);
		}
		user = user.substring(0, 5);
		Random rnd = new Random(System.currentTimeMillis());
		int random = rnd.nextInt(900) + 100;
		
		String strRand = Integer.toString(random);
		
		String username = user.concat(leadingZeroDay).concat(leadingZeroMonth).concat(strRand);
		System.out.println("Username : " + username);
		
		return username;
	}
	
}
