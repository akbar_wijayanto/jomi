package com.ppsi.jomi.webapp.security;
//package com.anabatic.kamp.webapp.security;
//
//import java.io.Serializable;
//import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Method;
//import java.lang.reflect.Modifier;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//
//import javax.persistence.JoinColumns;
//import javax.persistence.ManyToMany;
//import javax.persistence.OneToMany;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//
//import com.anabatic.kamp.Constants;
//import com.anabatic.kamp.enumeration.AuthorizationTypeEnum;
//import com.anabatic.kamp.enumeration.RecordStatusEnum;
//import com.anabatic.kamp.factory.ServiceFactory;
//import com.anabatic.kamp.persistence.dao.GenericDao;
//import com.anabatic.kamp.persistence.model.BaseGenericObject;
//import com.anabatic.kamp.persistence.model.CoreUser;
//import com.anabatic.kamp.persistence.util.DatabaseMetaDataUtil;
//import com.anabatic.kamp.security.bean.UserUtil;
//import com.anabatic.kamp.service.GenericManager;
//import com.anabatic.kamp.util.ManyToManyConverter;
//
//public class AuthorizationAspect {
//	private final Log log = LogFactory.getLog(AuthorizationAspect.class);
//
//	private UnauthorizeObjectManager unauthorizeObjectManager;
//	private UnauthorizeObjectDetailManager unauthorizeObjectDetailManager;
//	private ServiceFactory serviceFactory;
//
//	@Autowired
//	public void setUnauthorizeObjectManager(
//			UnauthorizeObjectManager unauthorizeObjectManager) {
//		this.unauthorizeObjectManager = unauthorizeObjectManager;
//	}
//
//	@Autowired
//	public void setUnauthorizeObjectDetailManager(
//			UnauthorizeObjectDetailManager unauthorizeObjectDetailManager) {
//		this.unauthorizeObjectDetailManager = unauthorizeObjectDetailManager;
//	}
//
//	@Autowired
//	@Qualifier("coreServiceFactory")
//	public void setServiceFactory(ServiceFactory serviceFactory) {
//		this.serviceFactory = serviceFactory;
//	}
//
//	@SuppressWarnings({ "rawtypes", "unchecked" })
//	public Object onSaveObject(ProceedingJoinPoint pjp) throws Throwable {
//		Object[] args = pjp.getArgs();
//		Object newObject = args[0];
//		Class<?> objectClass = newObject.getClass();
//
//		if (((BaseGenericObject) newObject).getStatus() == null
//				|| ((BaseGenericObject) newObject).getStatus() == RecordStatusEnum.LIVE.getName())
//			((BaseGenericObject) newObject).setStatus(RecordStatusEnum.INAU.getName());
//		
//		args[0] = newObject;
//		try {
//			newObject = pjp.proceed(args);
//		} catch (Throwable e) {
//			throw e;
//		}
//		
//		UnauthorizeObject unauthorizeObject = new UnauthorizeObject();
//
//		unauthorizeObject.setClassName(objectClass.getCanonicalName());
//		unauthorizeObject.setRecordId((Serializable) extractId(newObject));
//		if (((BaseGenericObject) newObject).getStatus() == RecordStatusEnum.DELAU.getName())
//			unauthorizeObject.setAuthorizationTypeEnum(AuthorizationTypeEnum.DELETE);
//		else
//			unauthorizeObject.setAuthorizationTypeEnum(AuthorizationTypeEnum.SAVE);
//		unauthorizeObject = unauthorizeObjectManager.save(unauthorizeObject);
//		try {
//			GenericManager manager = serviceFactory.getManager(objectClass.getSimpleName());
//			Object oldObject = manager.get((Serializable) extractId(newObject));
//			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_TIME);
//			String createdTime = simpleDateFormat.format(((BaseGenericObject) oldObject).getCreatedTime());
//			String updatedTime = simpleDateFormat.format(((BaseGenericObject) newObject).getUpdatedTime());
//			if (((BaseGenericObject) newObject).getStatus() != RecordStatusEnum.DELAU.getName() && !createdTime.equals(updatedTime)) {
//				((BaseGenericObject) newObject).setCreatedBy(((BaseGenericObject) oldObject).getCreatedBy());
//				((BaseGenericObject) newObject).setCreatedTime(((BaseGenericObject) oldObject).getCreatedTime());
//				((BaseGenericObject) newObject).setVersion(((BaseGenericObject) oldObject).getVersion());
//				log.debug("update record...");
//				List<Method> methods = DatabaseMetaDataUtil.getMethods(objectClass);
//
//				for (Method m : methods) {
//					if (m.getName().startsWith("get")
//							&& Modifier.isPublic(m.getModifiers())
//							&& !Modifier.isStatic(m.getModifiers())
//							&& !m.isAnnotationPresent(OneToMany.class)
//							&& !m.isAnnotationPresent(JoinColumns.class)) {
//						
//						UnauthorizeObjectDetail unauthorizeObjectDetail = new UnauthorizeObjectDetail();
//						unauthorizeObjectDetail.setUnauthorizeObject(unauthorizeObject);
//						unauthorizeObjectDetail.setFieldName(m.getName().substring(3));
//						if (m.invoke(newObject) == null) {
//							unauthorizeObjectDetail.setFieldValue(null);
//						} else {
//							try {
//								Object object = extractId(m.invoke(newObject));
//								unauthorizeObjectDetail.setFieldValue(object.toString());
//							} catch (RuntimeException e) {
//								if(m.getReturnType().equals(Date.class)){
//									unauthorizeObjectDetail.setFieldValue(simpleDateFormat.format(m.invoke(newObject)));
//								} else if (m.isAnnotationPresent(ManyToMany.class)) {
//									if(!m.invoke(newObject).toString().equals("[]"))
//										unauthorizeObjectDetail.setFieldValue(ManyToManyConverter.convertForAuthorize(m.invoke(newObject).toString()));
//								} else
//									unauthorizeObjectDetail.setFieldValue(m.invoke(newObject).toString());
//							}
//						}
//						unauthorizeObjectDetailManager.save(unauthorizeObjectDetail);
//					}
//				}
//			} else {
//				log.debug("new / delete record...");
//			}
//		} catch (Exception e) {
//			log.debug("new record...");
//		}
//		
//		return newObject;
//	}
//
//	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
//	public Object onDeleteObject(ProceedingJoinPoint pjp)
//			throws IllegalArgumentException, IllegalAccessException {
//		Object[] args = pjp.getArgs();
//		Object objectId = args[0];
//		Object result = null;
//		Object currentObject = pjp.getTarget();
//		Object removeObject = null;
//		CoreUser user = UserUtil.getCurrentUser();
//		
//		if(objectId!=null){
//            if(!(currentObject instanceof GenericDao))
//                throw new RuntimeException("Wrong pointcut");
//            if(args.length==0)
//                throw new RuntimeException("Wrong pointcut, zero argument");
//            removeObject = ((GenericDao)currentObject).get((Serializable)objectId);
//		}
////		UnauthorizeObject unauthorizeObject = new UnauthorizeObject();
////
////		unauthorizeObject.setClassName(removeObject.getClass().getCanonicalName());
////		unauthorizeObject.setRecordId((Serializable) extractId(removeObject));
////		unauthorizeObject.setAuthorizationTypeEnum(AuthorizationTypeEnum.DELETE);
////		unauthorizeObject = unauthorizeObjectManager.save(unauthorizeObject);
//
//		((BaseGenericObject) removeObject).setStatus(RecordStatusEnum.DELAU.getName());
//		try {
////			result = pjp.proceed(args);
////			result = ((GenericDao)currentObject).save((BaseGenericObject) removeObject);
//			GenericManager manager = serviceFactory.getManager(removeObject.getClass().getSimpleName());
//			result = manager.save((BaseGenericObject) removeObject);
//		} catch (Throwable e) {
//			e.printStackTrace();
//		}
//		return result;
//	}
//
//	private Object extractId(Object data) {
//		if (!(data instanceof BaseGenericObject))
//			throw new RuntimeException("Wrong data type");
//		Class<?> clazz = data.getClass();
//		Method[] methods = clazz.getMethods();
//		for (Method m : methods) {
//			if (m.isAnnotationPresent(javax.persistence.Id.class)
//					|| m.isAnnotationPresent(javax.persistence.EmbeddedId.class)) {
//				try {
//					log.debug("Found id in method : " + m.getName());
//					return m.invoke(data);
//				} catch (IllegalArgumentException e) {
//					e.printStackTrace();
//				} catch (IllegalAccessException e) {
//					e.printStackTrace();
//				} catch (InvocationTargetException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//		return null;
//	}
//}
