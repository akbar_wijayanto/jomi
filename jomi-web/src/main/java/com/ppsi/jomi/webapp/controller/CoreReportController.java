package com.ppsi.jomi.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ppsi.jomi.service.CoreReportManager;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date May 9, 2017 11:07:45 AM
 */
@Controller
@RequestMapping("/core/report")
public class CoreReportController extends BaseFormController {
	
	private CoreReportManager coreReportManager;
	
	@Autowired
	public void setCoreReportManager(CoreReportManager coreReportManager) {
		this.coreReportManager = coreReportManager;
	}
	
	public CoreReportController() {
		setCancelView("redirect:/core/report");
		setSuccessView("redirect:/core/report");
	}

//	@PreAuthorize("hasAnyRole('CORE:REPORT:READ:*')")
	@RequestMapping(value = "")
	public String reportList(Model model) throws Exception {
		model.addAttribute("coreReports", coreReportManager.getAll());
		return "core/report/list";
	}

}
