package com.ppsi.jomi.webapp.interceptor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import com.ppsi.jomi.Constants;
import com.ppsi.jomi.persistence.model.CoreSystem;
import com.ppsi.jomi.persistence.model.CoreUser;
import com.ppsi.jomi.security.bean.UserUtil;
import com.ppsi.jomi.service.CoreMenuManager;
import com.ppsi.jomi.service.CoreSystemManager;

/**
 * @author : <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 * @version : 1.0, 7/2/12, 7:32 PM
 */
public class BreadcrumbsInterceptor extends HandlerInterceptorAdapter {
    private final Log log = LogFactory.getLog(BreadcrumbsInterceptor.class);
    @Autowired
    private CoreMenuManager mgr;
    @Autowired
    private CoreSystemManager coreSystemManager;
    
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    	String uriUrl = request.getRequestURI();
    	String fixUrl = uriUrl.replace("/app", "");
    	
//    	System.out.println("URLNYA INI BROOO------>>" + fullUrl);
//    	System.out.println("TEST------>>" + fixUrl);
    	
    	if (fixUrl != null ) {
//            boolean isRedirectView = modelAndView.getView() instanceof RedirectView;
            boolean isViewObject = fixUrl == null;
            boolean viewNameStartsWithRedirect = (fixUrl == null ? true : fixUrl.startsWith(UrlBasedViewResolver.REDIRECT_URL_PREFIX));
//            String[] url = fixUrl.split("/");
//            Integer urlSize = url.length;
//            String urlTrue = "";
//            String urlTrue2 = "";
//            Integer parentId = 0;
//            Integer parentIdSize = 0;
//            List<Integer> urlTrue3 = new ArrayList<Integer>();
//            if(urlSize != 0){
//            	for(int i=0; i < urlSize; i++){
//            		parentId = 0;
//            		urlTrue = "/"+url[i];
//            		urlTrue2 += urlTrue;
//            		parentId = mgr.getByPermaLink(urlTrue2);
//            		if(parentId!=0){
//            			urlTrue3.add(parentId);
//            		}
//            	}
//            }
//            if(urlTrue3.size()!=0){
//            	parentIdSize = urlTrue3.size()-1;
//                parentId=urlTrue3.get(parentIdSize);
//            }
            if (modelAndView != null) {
            	if (modelAndView.hasView() && (
                        (isViewObject) ||
                                (!isViewObject && !viewNameStartsWithRedirect))) {
                    //log.debug("Found view " + modelAndView.getViewName());
                    request.setAttribute(Constants.BREADCRUMBS,  generateList((String) request.getAttribute("org.springframework.web.servlet.HandlerMapping.bestMatchingPattern")));
                    CoreUser coreUser = UserUtil.getCurrentUser();
                    CoreSystem coreSystem = UserUtil.getCurrentSystem(request);
                    if(coreUser !=null){
                        request.setAttribute("plocale", StringUtils.length(coreUser.getPreferredLocale())>2? coreUser.getPreferredLocale().substring(0,2):"en");
                        request.setAttribute("huser", coreUser.getFirstName() + " "+ (coreUser.getLastName() == null ? "" : coreUser.getLastName()));
                        request.setAttribute("husername", coreUser.getUsername());
                        //request.setAttribute("htoday", coreSystemManager.getCurrentSystem().getTodayDate());
                        request.setAttribute("htoday", new Date());
//                        request.setAttribute("hcurrency",coreSystem.getLocalCurrency().getId());
//                        request.setAttribute("hbranch",coreUser.getActiveBranch().getName());
                        request.setAttribute("userMenus", request.getSession().getAttribute(Constants.USER_MENU));
                    }
                }
            }
        }
    	
//        if (modelAndView != null ) {
//            boolean isRedirectView = modelAndView.getView() instanceof RedirectView;
//            boolean isViewObject = modelAndView.getView() == null;
//            boolean viewNameStartsWithRedirect = (modelAndView.getViewName() == null ? true : modelAndView.getViewName().startsWith(UrlBasedViewResolver.REDIRECT_URL_PREFIX));
//            String[] url = modelAndView.getViewName().split("/");
//            Integer urlSize = url.length;
//            String urlTrue = "";
//            String urlTrue2 = "";
//            Integer parentId = 0;
//            Integer parentIdSize = 0;
//            List<Integer> urlTrue3 = new ArrayList<Integer>();
//            if(urlSize != 0){
//            	for(int i=0; i < urlSize; i++){
//            		parentId = 0;
//            		urlTrue = "/"+url[i];
//            		urlTrue2 += urlTrue;
//            		parentId = mgr.getByPermaLink(urlTrue2);
//            		if(parentId!=0){
//            			urlTrue3.add(parentId);
//            		}
//            	}
//            }
//            if(urlTrue3.size()!=0){
//            	parentIdSize = urlTrue3.size()-1;
//                parentId=urlTrue3.get(parentIdSize);
//            }            
//            if (modelAndView.hasView() && (
//                    (isViewObject && !isRedirectView) ||
//                            (!isViewObject && !viewNameStartsWithRedirect))) {
//                //log.debug("Found view " + modelAndView.getViewName());
//                request.setAttribute(Constants.BREADCRUMBS,  generateList((String) request.getAttribute("org.springframework.web.servlet.HandlerMapping.bestMatchingPattern")));
//                CoreUser coreUser = UserUtil.getCurrentUser();
//                CoreSystem coreSystem = UserUtil.getCurrentSystem(request);
//                if(coreUser !=null){
//                    request.setAttribute("plocale", StringUtils.length(coreUser.getPreferredLocale())>2? coreUser.getPreferredLocale().substring(0,2):"en");
//                    request.setAttribute("huser", coreUser.getFirstName() + " "+ (coreUser.getLastName() == null ? "" : coreUser.getLastName()));
//                    request.setAttribute("htoday",coreSystem.getTodayDate());
//                    request.setAttribute("hbranch",coreUser.getActiveBranch().getId());
//                    request.setAttribute("userMenus", request.getSession().getAttribute(Constants.USER_MENU));
//                }
//            }
//        }
    }

    private List<String> generateList(String urlPattern) {
        // Get the request path
        List<String> listPath = new ArrayList<String>();

        // Get the URI template
        if (urlPattern != null) {
            String[] splittedUrlPattern = urlPattern.split("/");

            for (int i = 0; i < splittedUrlPattern.length; i++) {
                if (!splittedUrlPattern[i].isEmpty() && !isVariable(splittedUrlPattern[i])) { 
                    listPath.add(splittedUrlPattern[i].split("\\?")[0]);
                }
            }
        }

        return listPath;
    }

    private boolean isVariable(String uriPath) {
        // if it is a variable. e.g {id}
        if (uriPath.startsWith("{") && uriPath.endsWith("}"))
            return true;
        return false;
    }


}
