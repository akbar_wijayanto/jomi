package com.ppsi.jomi.webapp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ppsi.jomi.enumeration.ActionTypeEnum;
import com.ppsi.jomi.enumeration.ApplicationTypeEnum;
import com.ppsi.jomi.enumeration.ModuleTypeEnum;
import com.ppsi.jomi.enumeration.RecordTypeEnum;
import com.ppsi.jomi.persistence.model.CorePermission;
import com.ppsi.jomi.security.bean.UserUtil;
import com.ppsi.jomi.service.CorePermissionManager;
import com.ppsi.jomi.service.CoreUserManager;
import com.ppsi.jomi.webapp.dto.ApplicationTypeDto;

@Controller
@RequestMapping(value="/core/permission")
public class CorePermissionController extends BaseFormController {
	private CorePermissionManager corePermissionManager = null;
	private CoreUserManager coreUserManager;
	
	@Autowired
	public void setCoreUserManager(CoreUserManager coreUserManager) {
		this.coreUserManager = coreUserManager;
	}

	@Autowired
	public void setCorePermissionManager(CorePermissionManager corePermissionManager){
		this.corePermissionManager = corePermissionManager;
	}
	
	public CorePermissionController(){
		setCancelView("redirect:/core/permission");
		setSuccessView("redirect:/core/permission");
	}
	
	@PreAuthorize("hasAnyRole('CORE:PERMISSION:READ:*')")
    @RequestMapping(value="")
	public String allDisplay(Model model) throws Exception{
		model.addAttribute("corePermissions", corePermissionManager.getAll());
		return "core/permission/list";
	}
	
	@PreAuthorize("hasAnyRole('CORE:PERMISSION:INPUT:*')")
    @RequestMapping(value="/add", method = RequestMethod.GET)
	public String addDisplay(Model model) throws Exception{
		model.addAttribute("corePermission", new CorePermission());
		model.addAttribute("moduleTypeEnums", ModuleTypeEnum.values());
		model.addAttribute("applicationTypeEnums", ApplicationTypeEnum.values());
		model.addAttribute("actionTypeEnums", ActionTypeEnum.values());
		model.addAttribute("recordTypeEnums", RecordTypeEnum.values());
		return "core/permission/add";
	}
	
	@PreAuthorize("hasAnyRole('CORE:PERMISSION:INPUT:*')")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addProcess(@Valid CorePermission corePermission,BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
		
		
		if (validator != null) {
			validator.validate(corePermission, errors);
			if(errors.hasErrors()){
				model.addAttribute("moduleTypeEnums", ModuleTypeEnum.values());
        		model.addAttribute("applicationTypeEnums", ApplicationTypeEnum.values());
        		model.addAttribute("actionTypeEnums", ActionTypeEnum.values());
        		model.addAttribute("recordTypeEnums", RecordTypeEnum.values());
                return "core/permission/add";
			}
		}	
		
            if (errors.hasErrors() && request.getParameter("delete") == null) {
            	model.addAttribute("moduleTypeEnums", ModuleTypeEnum.values());
        		model.addAttribute("applicationTypeEnums", ApplicationTypeEnum.values());
        		model.addAttribute("actionTypeEnums", ActionTypeEnum.values());
        		model.addAttribute("recordTypeEnums", RecordTypeEnum.values());
                return "core/permission/add";
            }
            
        
        corePermissionManager.save(corePermission);
        saveMessage(request, getText("corePermission.saved", " "+corePermission.getDescription(), request.getLocale()));
        
        return "redirect:/core/permission";
	}
	
	/*@PreAuthorize("hasAnyRole('CORE:SECURITY:READ:*')")
    
    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public
    @ResponseBody
    DataTables<CorePermission> page(HttpServletRequest request) throws Exception {
        DataTables<CorePermission> dataTables = new DataTables<CorePermission>();

        Long iDisplayStart = NumberUtils.toLong(request.getParameter("iDisplayStart"));
        Long sEcho = NumberUtils.toLong(request.getParameter("sEcho"));
        Long iDisplayLength = NumberUtils.toLong(request.getParameter("iDisplayLength"));
        Long recordCount = this.corePermissionManager.getRecordCount();
        if (iDisplayLength == null) iDisplayLength = NumberUtils.toLong(Constants.PAGING_MAX_RECORD.toString());

        dataTables.setiTotalDisplayRecords(recordCount);
        dataTables.setiTotalRecords(recordCount);
        dataTables.setsEcho(sEcho);
        dataTables.setAaData(corePermissionManager.getPagingResults(iDisplayStart, iDisplayLength));

        return dataTables;
    }*/
	
	@PreAuthorize("hasAnyRole('CORE:PERMISSION:DELETE:*')")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String deleteDisplay(@PathVariable("id") Integer id, Model model) throws Exception{
		model.addAttribute("corePermission", corePermissionManager.get(id));
		model.addAttribute("moduleTypeEnums", ModuleTypeEnum.values());
		model.addAttribute("applicationTypeEnums", ApplicationTypeEnum.values());
		model.addAttribute("actionTypeEnums", ActionTypeEnum.values());
		model.addAttribute("recordTypeEnums", RecordTypeEnum.values());		
		return "core/permission/delete";
	}
	
	@PreAuthorize("hasAnyRole('CORE:PERMISSION:DELETE:*')")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String deleteProcess(CorePermission corePermission, Model model, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
		
//		if(!(coreUserManager.getCheckPermissionOnUser(corePermission.getId()).isEmpty())){
//			//System.out.println("lewat");
//			model.addAttribute("corePermission", corePermissionManager.get(corePermission.getId()));
//			model.addAttribute("moduleTypeEnums", ModuleTypeEnum.values());
//			model.addAttribute("applicationTypeEnums", ApplicationTypeEnum.values());
//			model.addAttribute("actionTypeEnums", ActionTypeEnum.values());
//			model.addAttribute("recordTypeEnums", RecordTypeEnum.values());	
//			saveError(request, getText("coreRole.roleUsedByUser", corePermission.getId().toString(), request.getLocale()));
//			return "core/permission/delete";
//		}
		
		corePermissionManager.remove(corePermission.getId());
		saveMessage(request, getText("corePermission.deleted", corePermission.getDescription(), request.getLocale()));
		return "redirect:/core/permission";
	}
	
	@PreAuthorize("hasAnyRole('CORE:PERMISSION:INPUT:*')")
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String editDisplay(@PathVariable("id") Integer id, Model model) throws Exception{
		model.addAttribute("corePermission", corePermissionManager.get(id));
		model.addAttribute("moduleTypeEnums", ModuleTypeEnum.values());
		model.addAttribute("applicationTypeEnums", ApplicationTypeEnum.values());
		model.addAttribute("actionTypeEnums", ActionTypeEnum.values());
		model.addAttribute("recordTypeEnums", RecordTypeEnum.values());
		return "core/permission/edit";
	}
	
	@PreAuthorize("hasAnyRole('CORE:PERMISSION:INPUT:*')")
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String editProcess(@Valid CorePermission corePermission, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }

        if (errors.hasErrors() && request.getParameter("delete") == null) {
        	model.addAttribute("moduleTypeEnums", ModuleTypeEnum.values());
    		model.addAttribute("applicationTypeEnums", ApplicationTypeEnum.values());
    		model.addAttribute("actionTypeEnums", ActionTypeEnum.values());
    		model.addAttribute("recordTypeEnums", RecordTypeEnum.values());
            return "core/permission/edit";
        }
        
        //corePermissionManager.save(corePermission);

        CorePermission oldPermission = corePermissionManager.get(corePermission.getId());
        corePermission.setCreatedBy(oldPermission.getCreatedBy());
        corePermission.setCreatedTime(oldPermission.getCreatedTime());
        corePermission.setStatus(oldPermission.getStatus());
        corePermission.setUpdatedBy(UserUtil.getCurrentUsername());
        corePermission.setUpdatedTime(new Date());
        corePermission.setVersion(oldPermission.getVersion());
        corePermissionManager.authorize(oldPermission, corePermission, true);
        
        saveMessage(request, getText("corePermission.saved", corePermission.getDescription(), request.getLocale()));
        return "redirect:/core/permission";
	}


    private List<ApplicationTypeDto> getByModule(String module){

        List<ApplicationTypeDto> applicationTypeEnums = new ArrayList<ApplicationTypeDto>();

        for(ApplicationTypeEnum e : ApplicationTypeEnum.values()){
            if(e.getApplicationModule().equals(module)){
                applicationTypeEnums.add(new ApplicationTypeDto(e.getApplicationValue(), e.getApplicationType()));
            }
        }
        return applicationTypeEnums;
    }
    
    @PreAuthorize("hasAnyRole('CORE:PERMISSION:READ:*')")
    @RequestMapping(value = "/getmodules", method = RequestMethod.POST)
    public @ResponseBody List<ApplicationTypeDto> getApplication(@RequestParam("module") String module) {
        return getByModule(module);
    }

    @PreAuthorize("hasAnyRole('CORE:PERMISSION:READ:*')")
    @RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	public String view(@PathVariable("id") Integer id, Model model) throws Exception {
		model.addAttribute("corePermission", corePermissionManager.get(id));
		model.addAttribute("moduleTypeEnums", ModuleTypeEnum.values());
		model.addAttribute("applicationTypeEnums", ApplicationTypeEnum.values());
		model.addAttribute("actionTypeEnums", ActionTypeEnum.values());
		model.addAttribute("recordTypeEnums", RecordTypeEnum.values());
		return "core/permission/view";
	}
    
}
