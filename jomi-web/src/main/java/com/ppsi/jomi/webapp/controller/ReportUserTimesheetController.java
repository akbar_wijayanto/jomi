package com.ppsi.jomi.webapp.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ppsi.jomi.exception.JomiException;
import com.ppsi.jomi.report.dto.ReportUserTimesheetDto;
import com.ppsi.jomi.report.service.UserReportService;
import com.ppsi.jomi.report.util.ClientReportGenerator;
import com.ppsi.jomi.webapp.util.FileDownloader;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date May 7, 2017 2:19:02 PM
 */
@Controller
@RequestMapping("/core/user/timesheet/report")
public class ReportUserTimesheetController extends BaseFormController {

	private UserReportService userReportService;
	
	@Autowired
	public void setUserReportService(UserReportService userReportService) {
		this.userReportService = userReportService;
	}
	
	public ReportUserTimesheetController() {
		setCancelView("redirect:/core/user/timesheet/report");
		setSuccessView("redirect:/core/user/timesheet/report");
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String view(Model model) throws Exception {
		model.addAttribute("userTimesheets", new ArrayList<ReportUserTimesheetDto>());
		model.addAttribute("coreUsers", getUserManager().getAllLive());
		return "/core/user/timesheet/report/list";
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public String reportPost(@RequestParam(value = "userId", required = false) Long userId, HttpServletRequest request,
			Model model) throws Exception {
		
		model.addAttribute("coreUsers", getUserManager().getAllLive());
		try {
			model.addAttribute("userTimesheets", userReportService.getuserTimesheet(userId));
			return "/core/user/timesheet/report/list";
		} catch (Exception e) {
			return "/core/user/timesheet/report/list";
		}
	}

	@RequestMapping(value = "/download/all/{type}", method = { RequestMethod.POST, RequestMethod.GET })
	public String downloadAllData(@PathVariable("type") String type,
			@RequestParam(value = "userId", required = false) Long userId, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception {

		List<ReportUserTimesheetDto> userTimesheetReport = new ArrayList<ReportUserTimesheetDto>();
		userTimesheetReport = userReportService.getuserTimesheet(userId);
		return getExport(userTimesheetReport, type, response);
	}

	private <T> String getExport(List<T> dtos, String type, HttpServletResponse response) throws Exception {
		if (type.equals("pdf")) {
			response.setContentType("application/pdf");
			File downloadFile = getFile("report_user_timesheet", "pdf", "report_user_timesheet.jrxml", dtos);
			FileDownloader.downloadFile(downloadFile, response,
					FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
		}
		if (type.equals("xls")) {
			response.setContentType("application/vnd.xls");
			File downloadFile = getFile("report_user_timesheet", "xls", "report_user_timesheet.jrxml", dtos);
			FileDownloader.downloadFile(downloadFile, response,
					FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
		}
		return null;
	}

	private <T> File getFile(String fileName, String format, String jrxmlFileName, List<T> dtos) throws JomiException {
		return getData(fileName, jrxmlFileName, format, dtos);
	}

	private <T> File getData(String fileName, String jrxmlFileName, String format, List<T> list) throws JomiException {
		File downloadFile = null;
		if (format.equals("pdf")) {
			downloadFile = ClientReportGenerator.generatePDF(jrxmlFileName, list,
					ClientReportGenerator.readProperty("jrxml.tempDir") + fileName);
		}
		if (format.equals("xls")) {
			downloadFile = ClientReportGenerator.generateXLS(jrxmlFileName, list,
					ClientReportGenerator.readProperty("jrxml.tempDir") + fileName);
		}
		return downloadFile;
	}

}
