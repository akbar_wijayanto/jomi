package com.ppsi.jomi.webapp.dto;

import com.ppsi.jomi.enumeration.CoreMenuEnum;
import com.ppsi.jomi.persistence.model.CoreMenu;

/**
 * @author : <a href="septiyani@anabatic.com">Septiyani</a>
 */

public class CoreMenuDto  {
	
	private Integer id;

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

    private String parentId;

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private Boolean system;

    public Boolean getSystem() {
        return system;
    }

    public void setSystem(Boolean system) {
        this.system = system;
    }

    private String permalinks;

    public String getPermalinks() {
        return permalinks;
    }

    public void setPermalinks(String permalinks) {
        this.permalinks = permalinks;
    }

    private String icon;

    public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	private Integer orderMenu;

	public Integer getOrderMenu() {
		return orderMenu;
	}

	public void setOrderMenu(Integer orderMenu) {
		this.orderMenu = orderMenu;
	}
	
	private Boolean checklistStatus;

	public String status;

	public Boolean getChecklistStatus() {
		return checklistStatus;
	}

	public void setChecklistStatus(Boolean checklistStatus) {
		this.checklistStatus = checklistStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public CoreMenuDto(){
		
	}
	
	public CoreMenuDto (CoreMenu coreMenu){
		this.checklistStatus = coreMenu.getChecklistStatus();
		this.description = coreMenu.getDescription();
		this.id = coreMenu.getId();
		this.name = coreMenu.getName();
		this.orderMenu = coreMenu.getOrderMenu();
		this.permalinks = coreMenu.getPermalinks();
		this.system = coreMenu.getSystem();
		this.type = CoreMenuEnum.get(coreMenu.getType()).getMenuType();
		this.status = coreMenu.getStatus();
	}
}
