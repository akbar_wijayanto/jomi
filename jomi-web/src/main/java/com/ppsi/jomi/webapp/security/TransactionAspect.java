package com.ppsi.jomi.webapp.security;
//package com.anabatic.kamp.webapp.security;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//
//import com.anabatic.kamp.enumeration.RecordStatusEnum;
//import com.anabatic.kamp.factory.ServiceFactory;
//import com.anabatic.kamp.persistence.model.BaseGenericObject;
//import com.anabatic.kamp.service.CoreSystemManager;
//import com.anabatic.kamp.service.GenericManager;
//
//public class TransactionAspect {
//	private final Log log = LogFactory.getLog(TransactionAspect.class);
//
////	private DaoFactory daoFactory;
//	private CoreSystemManager coreSystemManager;
//	private UnauthorizeObjectManager unauthorizeObjectManager;
//	private ServiceFactory serviceFactory;
//	//FIXME stack overflow error
////	private ClassFactory classFactory = new TempTransactionClassFactory();
//	
//	@Autowired
//	@Qualifier("coreServiceFactory")
//	public void setServiceFactory(ServiceFactory serviceFactory) {
//		this.serviceFactory = serviceFactory;
//	}
//
////	@Autowired
////	@Qualifier("tempTransactionDaoFactory")
////	public void setDaoFactory(DaoFactory daoFactory) {
////		this.daoFactory = daoFactory;
////	}
//	
//	@Autowired
//	public void setUnauthorizeObjectManager(
//			UnauthorizeObjectManager unauthorizeObjectManager) {
//		this.unauthorizeObjectManager = unauthorizeObjectManager;
//	}
//
//	@Autowired
//	public void setCoreSystemManager(CoreSystemManager coreSystemManager) {
//		this.coreSystemManager = coreSystemManager;
//	}
//
//	@SuppressWarnings({ "rawtypes", "unchecked" })
//	public Object tempTransactionSave(ProceedingJoinPoint pjp) throws Throwable {
//		Object[] args = pjp.getArgs();
//		Object transObject = args[0];
//		Object result = null;
//		Object newTransObject = null;
///*		if (transObject instanceof BaseGenericObject) {
//			newTransObject = classFactory.getObject(transObject);
//		}
//		String signature = pjp.getSignature().getName();
//		if (coreSystemManager.getCurrentSystem().getStatus().equals(SystemStatusEnum.Online.getValue())) {
//			log.debug("save to original table...");
//			pjp.proceed(args);
//			result = transObject;
//		} else {
//			log.debug("cob is running...");
//			if (transObject instanceof BaseGenericObject && newTransObject != null) {
//				GenericDao dao = daoFactory.getDao(transObject.getClass()
//						.getSimpleName());
//				if ("save".equals(signature))
//					((BaseTemporaryObject) newTransObject)
//							.setTempActionTypeEnum(TempActionTypeEnum.SAVE);
//				else if ("remove".equals(signature))
//					((BaseTemporaryObject) newTransObject)
//							.setTempActionTypeEnum(TempActionTypeEnum.DELETE);
//				log.debug("save to temporary table...");
//				dao.save((BaseTemporaryObject) newTransObject);
//				result = newTransObject;
//			}
//		}*/
//		return result;
//	}
//	
//	/**
//	 * 
//	 * @param jp
//	 * @return
//	 * @throws Throwable
//	 * 
//	 * Call after authorize method, useful when we want to routine something after transaction
//	 * always call preauthorize
//	 * 
//	 */
//	public void preAuthorize(JoinPoint jp) throws Throwable {
//		Object[] args 						= jp.getArgs();
//		Boolean approveReject				= (Boolean)args[2];	 
//		if(approveReject){
//			Object object  					= (Object)args[1];	
//			GenericManager<?, ?> generic 	= serviceFactory.getManager(object.getClass().getSimpleName());
//			
//			if(generic != null && args[0] == null){
//				String status = ((BaseGenericObject) object).getStatus();
//				if (status.equals(RecordStatusEnum.LIVE.getName())){
//					generic.preAuthorize(object);
//				} else if (status.equals(RecordStatusEnum.HIST.getName())){
//					generic.deleteAuthorize(object);
//				}
//			}
//		}
//	}
//}
