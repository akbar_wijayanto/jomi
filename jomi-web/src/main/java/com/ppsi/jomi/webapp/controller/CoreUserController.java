package com.ppsi.jomi.webapp.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ppsi.jomi.Constants;
import com.ppsi.jomi.enumeration.SupportedLocaleEnum;
import com.ppsi.jomi.exception.JomiException;
import com.ppsi.jomi.persistence.model.CoreRole;
import com.ppsi.jomi.persistence.model.CoreUser;
import com.ppsi.jomi.report.util.ClientReportGenerator;
import com.ppsi.jomi.security.bean.UserUtil;
import com.ppsi.jomi.service.CoreMenuManager;
import com.ppsi.jomi.service.CoreRoleManager;
import com.ppsi.jomi.service.CoreSystemManager;
import com.ppsi.jomi.service.CoreUserManager;
import com.ppsi.jomi.webapp.util.FileDownloader;

@Controller
@RequestMapping("/core/user")
public class CoreUserController extends BaseFormController{
	private CoreUserManager coreUserManager;
	private static String currentPassword;
	private CoreRoleManager roleManager;
	private CoreSystemManager systemManager;
	private CoreMenuManager coreMenuManager;

	public CoreUserController() {
		setCancelView("redirect:/core/user");
		setSuccessView("redirect:/core/user");
	}

	@Autowired
	public void setRoleManager(CoreRoleManager roleManager) {
		this.roleManager = roleManager;
	}

	@Autowired    
	public void setCoreMenuManager(CoreMenuManager coreMenuManager) {
		this.coreMenuManager = coreMenuManager;
	}

	@Autowired
	public void setSystemManager(CoreSystemManager systemManager) {
		this.systemManager = systemManager;
	}

	@Autowired
	public void setCoreUserManager(CoreUserManager coreUserManager) {
		this.coreUserManager = coreUserManager;
	}

	//    @PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "")
	public String userProfileDisplay(Model model) throws Exception {
		model.addAttribute("coreUser", coreUserManager.getUserByUsername(UserUtil.getCurrentUsername()));
		return "/core/user/view";
	}

	//    @PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/editprofile", method = RequestMethod.GET)
	public String editProfileDisplay(Model model) throws Exception {
		model.addAttribute("coreUser", coreUserManager.getUserByUsername(UserUtil.getCurrentUsername()));
		return "core/user/editprofile";
	}

	//    @PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/editprofile", method = RequestMethod.POST)
	public String editProfileProcess(@Valid CoreUser coreUser, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}

		CoreUser user = coreUserManager.getUserByUsername(UserUtil.getCurrentUsername());
		user.setEmail(coreUser.getEmail());
		user.setFirstName(coreUser.getFirstName());
		user.setLastName(coreUser.getLastName());
		user.setPersonnelCode(coreUser.getPersonnelCode());

		if (errors.hasErrors() && request.getParameter("delete") == null) {
			return "core/user/editprofile";
		}
		coreUserManager.save(user);
		saveMessage(request, getText("coreUser.saved", coreUser.getUsername(), request.getLocale()));
		return "redirect:/core/user";
	}

	//    @PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/editpassword", method = RequestMethod.GET)
	public String editPasswordDisplay(Model model) throws Exception {
		CoreUser coreUser = coreUserManager.getUserByUsername(UserUtil.getCurrentUsername());

		currentPassword = coreUser.getPassword();

		model.addAttribute("coreUser", coreUser);
		return "core/user/editpassword";
	}

	//    @PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/editpassword", method = RequestMethod.POST)
	public String editPasswordProcess(@ModelAttribute("coreUser") CoreUser coreUser, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}
		CoreUser user = coreUserManager.getUserByUsername(UserUtil.getCurrentUsername());
		user.setPassword(coreUser.getPassword());
		user.setConfirmPassword(coreUser.getConfirmPassword());
		user.setPasswordHint(coreUser.getPasswordHint());

		coreUserManager.save(user);
		saveMessage(request, getText("coreUser.saved", coreUser.getUsername(), request.getLocale()));
		return "redirect:/core/user";
	}

	//    @PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/changelocale/{locale}", method = RequestMethod.GET)
	public String changeLocale(@PathVariable(value = "locale") String locale, HttpServletRequest request) {
		if(SupportedLocaleEnum.get(locale)!=null){
			String username = UserUtil.getCurrentUsername();

			if (!StringUtils.isEmpty(username)) {
				CoreUser user = coreUserManager.getUserByUsername(username);
				if (user != null) {
					user.setPreferredLocale(locale);
					// prevent user from change password
					CoreUser saved = coreUserManager.change(user);
					//Update session
					//                    SecurityUtils.getSubject().getSession().setAttribute(Constants.CORE_USER, saved);
					HttpSession session = request.getSession();
					session.setAttribute(Constants.CORE_USER, saved);
					UserUtil.getCurrentUser().setPreferredLocale(saved.getPreferredLocale());
					request.getSession(false).setAttribute(Constants.PREFERRED_LOCALE_KEY, new Locale(saved.getPreferredLocale().substring(0,2),saved.getPreferredLocale().substring(2,2)));
				}
			}
		}
		return "redirect:/mainMenu";
	}

	//    @PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value="/changerole", method=RequestMethod.GET)
	public String showUser(Model model) throws Exception{
		model.addAttribute("user", UserUtil.getCurrentUser());
		return "core/user/changerole";
	}

	//    @PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value="/changerole/{role}",method=RequestMethod.GET)
	public String activateUser(@PathVariable(value="role") Integer role, HttpServletRequest request){

		String username = UserUtil.getCurrentUsername();
		//TODO: List order By id
		if (!StringUtils.isEmpty(username)) {
			CoreUser user = coreUserManager.getUserByUsername(username);


			if (user != null) {
				CoreRole activeRole = roleManager.get(role);

				user.setActiveRole(activeRole);


				// prevent user from change password
				CoreUser saved = coreUserManager.save(user);
				//Update session
				//                 SecurityUtils.getSubject().getSession().setAttribute(Constants.CORE_USER, saved);
				//                 request.getSession().setAttribute(Constants.CORE_USER, saved);

				HttpSession session = request.getSession();
				session.setAttribute(Constants.CORE_USER, saved);
				replaceSession(session,
						Constants.USER_MENU,coreMenuManager.getUserMenusByActive(saved.getActiveRole().getId()));

				UserUtil.getCurrentUser().setActiveRole(activeRole);

			}
		}
		return "redirect:/core/user/changerole";

	}

	@RequestMapping(value = "/buyer", method=RequestMethod.GET)
	public String reportView3(Model model) throws Exception {

		return "/core/user/buyer/list";
	}

	private void replaceSession (HttpSession session, String key, Object value){
		session.removeAttribute(key);
		session.setAttribute(key, value);
	}

	private <T> String getExport(List<T> dtos, String type, HttpServletResponse response) throws Exception {
		String eDate = "";
		SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		eDate = spdf.format(date);
		if (type.equals("pdf")) {
			response.setContentType("application/pdf");
			File downloadFile = getFile("trxBuyer_"+eDate, "pdf", "trxBuyer_all.jrxml", dtos);
			FileDownloader.downloadFile(downloadFile, response, FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
		}
		if (type.equals("xls")) {
			response.setContentType("application/vnd.xls");
			File downloadFile = getFile("trxBuyer_"+eDate, "xls", "trxBuyer_all.jrxml", dtos);
			FileDownloader.downloadFile(downloadFile, response, FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
		}
		return null;
	}

	private <T> File getFile(String fileName, String format, String jrxmlFileName, List<T> dtos) throws JomiException {
		return getData(fileName, jrxmlFileName, format, dtos);
	}

	private <T> File getData(String fileName, String jrxmlFileName, String format, List<T> list) throws JomiException {
		File downloadFile = null;
		if (format.equals("pdf")) {
			downloadFile = ClientReportGenerator.generatePDF(jrxmlFileName, list, ClientReportGenerator.readProperty("jrxml.tempDir")+fileName);
		}
		if (format.equals("xls")) {
			downloadFile = ClientReportGenerator.generateXLS(jrxmlFileName, list, ClientReportGenerator.readProperty("jrxml.tempDir")+fileName);
		}
		return downloadFile;
	}
}
