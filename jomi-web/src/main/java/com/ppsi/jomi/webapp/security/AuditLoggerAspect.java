package com.ppsi.jomi.webapp.security;

import java.util.Calendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

import com.ppsi.jomi.Constants;
import com.ppsi.jomi.persistence.model.BaseGenericObject;
import com.ppsi.jomi.persistence.model.CoreUser;

/**
 * @author : <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 * @version : 1.0, 7/19/12, 4:59 PM
 */
public class AuditLoggerAspect {
    private final Log log = LogFactory.getLog(AuditLoggerAspect.class);
    private CoreUser coreUser;

    public void addAudit(Object o) {
        //log.debug("Entering audit record.. ");
        Subject subject = SecurityUtils.getSubject();
        coreUser = getCurrentUser(subject);
        if (coreUser != null && o instanceof BaseGenericObject) {

            if (((BaseGenericObject) o).getCreatedBy() == null) {
                ((BaseGenericObject) o).setCreatedBy(coreUser.getUsername());
                ((BaseGenericObject) o).setCreatedTime(Calendar.getInstance().getTime());
            }
            ((BaseGenericObject) o).setUpdatedBy(coreUser.getUsername());
            ((BaseGenericObject) o).setUpdatedTime(Calendar.getInstance().getTime());

        }
    }
    
    public void addAuthAudit(Object o)
    {
    	Subject subject = SecurityUtils.getSubject();
        coreUser = getCurrentUser(subject);
        if (coreUser != null && o instanceof BaseGenericObject) {

            ((BaseGenericObject) o).setAuthoriser(coreUser.getUsername());
            ((BaseGenericObject) o).setAuthorizeTime(Calendar.getInstance().getTime());
        }
    }

    protected CoreUser getCurrentUser(Subject subject) {
        Session session = (subject == null) ? null : subject.getSession();
        CoreUser coreUser = (CoreUser) session.getAttribute(Constants.CORE_USER);
        return coreUser;
    }

}
