package com.ppsi.jomi.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ppsi.jomi.service.FeedbackManager;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:25:42 AM
 */
@Controller
@RequestMapping(value="/feedback")
public class FeedbackController extends BaseFormController {

	public FeedbackController() {
		setCancelView("redirect:/feedback");
		setSuccessView("redirect:/feedback");
	}
	
	private FeedbackManager feedbackManager;
	
	@Autowired
	public void setFeedbackManager(FeedbackManager feedbackManager) {
		this.feedbackManager = feedbackManager;
	}
	
//	@PreAuthorize("hasAnyRole('CORE:FEEDBACK:READ:*')")
    @RequestMapping(value="")
	public String allDisplay(Model model) throws Exception{
		model.addAttribute("feedbacks", feedbackManager.getAllLive());
		return "feedback/list";
	}
}
