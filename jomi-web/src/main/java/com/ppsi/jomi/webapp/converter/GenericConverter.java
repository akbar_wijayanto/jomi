package com.ppsi.jomi.webapp.converter;

import java.util.Locale;

public interface GenericConverter<R,P> {
	
	public R convert (P beforeModel) ;
	
	public R convert (P beforeModel, Locale locale) ;
	
}
