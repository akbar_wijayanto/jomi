package com.ppsi.jomi.webapp.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ppsi.jomi.exception.JomiException;
import com.ppsi.jomi.report.dto.ReportUserProjectDto;
import com.ppsi.jomi.report.service.ProjectReportService;
import com.ppsi.jomi.report.util.ClientReportGenerator;
import com.ppsi.jomi.webapp.util.FileDownloader;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date May 7, 2017 2:19:02 PM
 */
@Controller
@RequestMapping("/core/project/user/report")
public class ReportProjectUserController extends BaseFormController {

	private ProjectReportService projectReportService;
	
	@Autowired
	public void setProjectReportService(ProjectReportService projectReportService) {
		this.projectReportService = projectReportService;
	}
	
	public ReportProjectUserController() {
		setCancelView("redirect:/core/project/user/report");
		setSuccessView("redirect:/core/project/user/report");
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String view(Model model) throws Exception {
		model.addAttribute("userProjects", new ArrayList<ReportUserProjectDto>());
		model.addAttribute("coreUsers", getUserManager().getAllLive());
		return "/core/project/user/report/list";
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public String reportPost(@RequestParam(value = "userId", required = false) Long userId, HttpServletRequest request,
			Model model) throws Exception {
		
		model.addAttribute("coreUsers", getUserManager().getAllLive());
		try {
			model.addAttribute("userProjects", projectReportService.getUserProject(userId));
			return "/core/project/user/report/list";
		} catch (Exception e) {
			return "/core/project/user/report/list";
		}
	}

	@RequestMapping(value = "/download/all/{type}", method = { RequestMethod.POST, RequestMethod.GET })
	public String downloadAllData(@PathVariable("type") String type,
			@RequestParam(value = "userId", required = false) Long userId, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception {

		List<ReportUserProjectDto> projectUserReport = new ArrayList<ReportUserProjectDto>();
		projectUserReport = projectReportService.getUserProject(userId);
		return getExport(projectUserReport, type, response);
	}

	private <T> String getExport(List<T> dtos, String type, HttpServletResponse response) throws Exception {
		if (type.equals("pdf")) {
			response.setContentType("application/pdf");
			File downloadFile = getFile("report_user_project", "pdf", "report_user_project.jrxml", dtos);
			FileDownloader.downloadFile(downloadFile, response,
					FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
		}
		if (type.equals("xls")) {
			response.setContentType("application/vnd.xls");
			File downloadFile = getFile("report_user_project", "xls", "report_user_project.jrxml", dtos);
			FileDownloader.downloadFile(downloadFile, response,
					FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
		}
		return null;
	}

	private <T> File getFile(String fileName, String format, String jrxmlFileName, List<T> dtos) throws JomiException {
		return getData(fileName, jrxmlFileName, format, dtos);
	}

	private <T> File getData(String fileName, String jrxmlFileName, String format, List<T> list) throws JomiException {
		File downloadFile = null;
		if (format.equals("pdf")) {
			downloadFile = ClientReportGenerator.generatePDF(jrxmlFileName, list,
					ClientReportGenerator.readProperty("jrxml.tempDir") + fileName);
		}
		if (format.equals("xls")) {
			downloadFile = ClientReportGenerator.generateXLS(jrxmlFileName, list,
					ClientReportGenerator.readProperty("jrxml.tempDir") + fileName);
		}
		return downloadFile;
	}

}
