package com.ppsi.jomi.webapp.security;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AuthorizationServiceException;

import com.ppsi.jomi.Constants;
import com.ppsi.jomi.enumeration.RecordStatusEnum;
import com.ppsi.jomi.persistence.dao.AuditTrailDao;
import com.ppsi.jomi.persistence.dao.GenericDao;
import com.ppsi.jomi.persistence.model.AuditTrail;
import com.ppsi.jomi.persistence.model.BaseGenericObject;
import com.ppsi.jomi.persistence.model.CoreRole;
import com.ppsi.jomi.persistence.model.CoreUser;
import com.ppsi.jomi.persistence.util.ModelComparator;
import com.ppsi.jomi.security.bean.UserUtil;
import com.ppsi.jomi.service.CoreRoleManager;

/**
 * @author : <a href="mailto:sahal.zain@anabatic.com">Sahal Zain</a>
 * @version : 1.0, 9/13/12, 10:59 PM
 */

public class AuditTrailAspect {
	private final Log log = LogFactory.getLog(AuditTrailAspect.class);

	private AuditTrailDao auditTrailDao;
	private List<AuditTrail> trails;
	private CoreUser currentUser = null;
	private static List<String> excludeRoles;
	private static List<CoreRole> roles;
	private CoreRoleManager coreRoleManager;
	private static List<String> authorizationRoles;
	private static List<CoreRole> includeRoles;
	private boolean allowSameUser = true;

	@Autowired
	public void setAuditTrailDao(AuditTrailDao auditTrailDao) {
		this.auditTrailDao = auditTrailDao;
	}

	@Autowired
	public void setCoreRoleManager(CoreRoleManager coreRoleManager) {
		this.coreRoleManager = coreRoleManager;
	}

	public void setExcludeRoles(List<String> excludeRoles) {
		AuditTrailAspect.excludeRoles = excludeRoles;
	}

	public List<String> getExcludeRoles() {
		return excludeRoles;
	}

	public void setAuthorizationRoles(List<String> authorizationRoles) {
		AuditTrailAspect.authorizationRoles = authorizationRoles;
	}

	public List<String> getAuthorizationRoles() {
		return authorizationRoles;
	}

	public void setAllowSameUser(boolean same) {
		this.allowSameUser = same;
	}

	public boolean getAllowSameUser() {
		return allowSameUser;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object onSave(ProceedingJoinPoint pjp) {
		trails = null;
		Object[] args = pjp.getArgs();
		Object newObject = args[0];
		if (newObject == null || args.length > 1) {
			newObject = args[1];
		}
		Object result = null;
		Object currentObject = pjp.getTarget();

		// log.debug("Current User : " + currentUser.getUsername());
		boolean isExclude = checkRoles();
		Signature signature = pjp.getSignature();
		boolean isAuthorize = "authorize".equals(signature.getName());
		boolean isApproved = true;
		if (isAuthorize) {
			isApproved = (Boolean) args[2];
		}

		Object temp = null;

		CoreUser user = UserUtil.getCurrentUser();

		// save from file set username to SYSTEM
		if (user == null) {
			user = new CoreUser();
			user.setUsername(Constants.SYSTEM);
		}
		if (isAuthorize && user != null
				&& newObject instanceof BaseGenericObject) {
			((BaseGenericObject) newObject).setAuthoriser(user.getUsername());
			((BaseGenericObject) newObject).setAuthorizeTime(Calendar.getInstance().getTime());
		}
		try {
			if (args[0] != null && args.length < 2) {
				args[0] = newObject;
			} else {
				args[1] = newObject;
			}
		} catch (IndexOutOfBoundsException e) {
			args[0] = newObject;
		}

		boolean changeRecord = false;
		// log.debug("Current object : " +
		// currentObject.getClass().getCanonicalName());
		// log.debug("Current method : " + pjp.getSignature().getName());

		// Exclude roles should not authorize himself

		if ((isAuthorize || (!isAuthorize && !isExclude))
				&& !(currentObject instanceof AuditTrailDao) && isApproved) {

			currentUser = user;

			if (!(currentObject instanceof GenericDao))
				throw new RuntimeException("Wrong pointcut");
			if (args.length == 0)
				throw new RuntimeException("Wrong pointcut, zero argument");
			if (newObject == null)
				throw new RuntimeException("Object argument cannot null");
			if (!(newObject instanceof BaseGenericObject))
				throw new RuntimeException(
						"Wrong pointcut, invalid argument type");

			Object objectId = extractId(newObject);
			Object oldObject = null;
			if (objectId != null && ((GenericDao) currentObject).exists((Serializable) objectId)) {
				// log.debug("Object id : " + objectId.toString());
				oldObject = ((GenericDao) currentObject).get((Serializable) objectId);
				// log.debug("Old object : " +oldObject.toString());
			}
			
//			if (oldObject != null && newObject.equals(args[0])) {
//				if (((BaseGenericObject) newObject).getStatus().equals(RecordStatusEnum.DELAU.getName()))
//					((BaseGenericObject) oldObject).setStatus(RecordStatusEnum.DELAU.getName());
//				else
//					((BaseGenericObject) oldObject).setStatus(RecordStatusEnum.INAU.getName());
//				args[0] = oldObject;
//			}

			if (oldObject != null) {
				// log.debug("Old object exist");
				ModelComparator comparator = new ModelComparator(currentUser.getUsername());
				comparator.setParentId(newObject.getClass().getCanonicalName() + ":" + objectId.toString());
				// if(isAuthorizing(oldObject, newObject))
				if ("authorize".equals(((MethodSignature) pjp.getSignature()).getMethod().getName()) && isAuthorizing(oldObject, newObject)) {

					// if (((BaseGenericObject)oldObject).getNewValue()!=null){
					// if (temp == null) temp = oldObject;
					// trails = comparator.compare(oldObject,
					// temp,Constants.AUTHORIZE_RECORD);
					// }
					// else
					if (((BaseGenericObject) oldObject).getStatus().equals(
							RecordStatusEnum.DELAU.getName())) {
						try {
							temp = oldObject.getClass().newInstance();
							trails = comparator.compare(oldObject, temp, Constants.AUTHORIZE_RECORD);
						} catch (InstantiationException e) {
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						}
					} else
						trails = comparator.extractId(oldObject, Constants.AUTHORIZE_RECORD);
				} else {
					if (temp == null) {
						if (((BaseGenericObject) newObject).getStatus() == RecordStatusEnum.DELAU.getName()) {
							try {
								temp = oldObject.getClass().newInstance();
								trails = comparator.compare(oldObject, temp, Constants.DELETE_RECORD);
							} catch (InstantiationException e) {
								e.printStackTrace();
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							}
						} else {
							try {
								temp = oldObject.getClass().newInstance();
								trails = comparator.compare(oldObject, temp, Constants.MODIFY_RECORD);
							} catch (InstantiationException e) {
								e.printStackTrace();
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							}
						}
					} else
						trails = comparator.compare(oldObject, temp, Constants.MODIFY_RECORD);
				}
				// saveTrails(trails);
				changeRecord = true;
			}

		}
		try {
			log.debug("Executing method...");

			result = pjp.proceed(args);
			if (!changeRecord && currentUser != null && result != null && result instanceof BaseGenericObject) {
				ModelComparator comparator = new ModelComparator(currentUser.getUsername());
				// trails = comparator.extractData(newObject);
				comparator.setParentId(result.getClass().getCanonicalName() + ":" + extractId(result).toString());
				trails = comparator.extractId(result, Constants.NEW_RECORD);
			}
			// if(!changeRecord&&currentUser!=null&&newObject!=null&&newObject
			// instanceof BaseGenericObject){
			// ModelComparator comparator = new
			// ModelComparator(currentUser.getUsername());
			// //trails = comparator.extractData(newObject);
			// if(result!=null)
			// comparator.setParentId(result.getClass().getCanonicalName() + ":"
			// + extractId(result).toString());
			// else
			// comparator.setParentId(newObject.getClass().getCanonicalName() +
			// ":" + extractId(newObject).toString());
			// trails = comparator.extractId(newObject, Constants.NEW_RECORD);
			// }
			if (trails != null)
				saveTrails(trails);

			// } catch (AuthorizationException ex){
		} catch (AuthorizationServiceException ex) {
			throw ex;
		} catch (Throwable e) {
			e.printStackTrace();
		}

		if (extractId(result) != null && extractId(newObject) == null) {
			return result;
		} else {
			return newObject;
		}
	}

	public Object onDelete(ProceedingJoinPoint pjp) {
		trails = null;
		Object[] args = pjp.getArgs();
		Object objectId = args[0];
		Object result = null;
		Object currentObject = pjp.getTarget();
		Object removeObject = null;
		// log.debug("Current object : " +
		// currentObject.getClass().getCanonicalName());
		// log.debug("Object id to remove : " + objectId);
		if (!checkRoles() && !(currentObject instanceof AuditTrailDao)
				&& objectId != null) {
			if (!(currentObject instanceof GenericDao))
				throw new RuntimeException("Wrong pointcut");
			if (args.length == 0)
				throw new RuntimeException("Wrong pointcut, zero argument");
			currentUser = UserUtil.getCurrentUser();
			removeObject = ((GenericDao) currentObject).get((Serializable) objectId);
			// log.debug("Current User : " + currentUser.getUsername());
			if (removeObject == null)
				throw new RuntimeException("Argument object cannot be null");
			if (!(removeObject instanceof BaseGenericObject))
				throw new RuntimeException("Wrong pointcut, invalid argument type");

			ModelComparator comparator = new ModelComparator(currentUser.getUsername());
			comparator.setParentId(removeObject.getClass().getCanonicalName() + ":" + extractId(removeObject).toString());
			trails = comparator.extractId(removeObject, Constants.DELETE_RECORD);
		}

		try {
			// log.debug("Executing method...");
			result = pjp.proceed(args);
			if (trails != null) {
				log.debug("Saving audit trail...");
				saveTrails(trails);
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return result;
	}

	private Object extractId(Object data) {
		if (data != null) {
			if (!(data instanceof BaseGenericObject))
				throw new RuntimeException("Wrong data type");
			Class<?> clazz = data.getClass();
			Method[] methods = clazz.getMethods();
			for (Method m : methods) {
				if (m.isAnnotationPresent(javax.persistence.Id.class)
						|| m.isAnnotationPresent(javax.persistence.EmbeddedId.class)) {
					try {
						// log.debug("Found id in method : " + m.getName());
						return m.invoke(data);
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}
		}

		return null;
	}

	private void saveTrails(List<AuditTrail> trails) {
		for (AuditTrail trail : trails) {
			// log.debug("saving trail : " + trail.toString());
			auditTrailDao.save(trail);
		}
	}

	private boolean checkRoles() {
		// Subject subject = SecurityUtils.getSubject();
		if (excludeRoles == null || excludeRoles.isEmpty()) {
			return false;
		}
		if (roles == null) {
			roles = new ArrayList<CoreRole>();
			for (String role : excludeRoles) {
				CoreRole r = coreRoleManager.getRole(role);
				if (r != null) {
					roles.add(r);
				}
			}
		}

		// need for file input in camel
		if (UserUtil.getCurrentUser() == null) {
			return false;
		}

		Set<CoreRole> userRoles = UserUtil.getCurrentUser().getCoreRoles();
		if (userRoles.isEmpty()) {
			return false;
		}
		for (CoreRole r : userRoles) {
			if (roles.contains(r)) {
				// log.debug("Contain non auditable roles..");
				return true;
			}
		}
		return false;
	}

	public boolean isAuthorizing(Object oldObject, Object newObject) {
		if (oldObject == null || newObject == null)
			return false;
		if (!(oldObject instanceof BaseGenericObject && newObject instanceof BaseGenericObject))
			return false;

		// log.debug("Check authorizing");
		// BaseGenericObject oldObj = (BaseGenericObject) oldObject;
		// BaseGenericObject newObj = (BaseGenericObject) newObject;
		// log.debug("Old object status : " + oldObj.getStatus());
		// log.debug("New object status : " + newObj.getStatus());

		// TODO: Change implementations
		// boolean authorizing =
		// RecordStatusEnum.INAU.getName().equals(oldObj.getStatus())&&RecordStatusEnum.LIVE.getName().equals(newObj.getStatus());
		// if(authorizing){
		// log.debug("Authorization mode..");
		if (!checkAuthorizationRoles()) {
			// log.debug("No role for authorization");
			// throw new
			// AuthorizationException("Invalid Business Rule BaseGenericObject : not enough privilege to authorize record");
			throw new AuthorizationServiceException("authorization.error.authorizerPrivilege");
		} else {
			if (!allowSameUser) {
				// log.debug("Authorizing with same user is forbidden");
				String updater = ((BaseGenericObject) newObject).getUpdatedBy();
				String authorizer = UserUtil.getCurrentUsername();
				// log.debug("Inputter : " + updater);
				// log.debug("Authorizer : " + authorizer);
				if (authorizer.equals(updater)) {
					// log.debug("Inputer is the same user as the authorizer");
					// throw new
					// AuthorizationException("Invalid Business Rule BaseGenericObject : authorizer user must be different user than inputter");
					throw new AuthorizationServiceException("authorization.error.authorizerUser");
				}

			}
		}
		// }
		// return authorizing;
		return true;

	}

	private boolean checkAuthorizationRoles() {
		// log.debug("Check authorization roles..");
		// Subject subject = SecurityUtils.getSubject();
		// Object object =
		// SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (authorizationRoles == null || authorizationRoles.isEmpty()) {
			return true;
		}
		if (includeRoles == null) {
			includeRoles = new ArrayList<CoreRole>();
			for (String role : authorizationRoles) {
				CoreRole r = coreRoleManager.getRole(role);
				if (r != null) {
					// log.debug("Included authorization role : " +
					// r.toString());
					includeRoles.add(r);
				}
			}
		}
		Set<CoreRole> userRoles = UserUtil.getCurrentUser().getCoreRoles();
		if (userRoles.isEmpty()) {
			return false;
		}
		for (CoreRole r : userRoles) {
			// log.debug("User role : " + r.toString());
			if (includeRoles.contains(r)) {
				// log.debug("Contain authorized roles..");
				return true;
			}
		}
		return false;
	}

}