package com.ppsi.jomi.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ppsi.jomi.enumeration.AccrualTypeEnum;
import com.ppsi.jomi.enumeration.SystemStatusEnum;
import com.ppsi.jomi.persistence.model.CoreSystem;
import com.ppsi.jomi.security.bean.UserUtil;
import com.ppsi.jomi.service.CoreSystemManager;

@Controller
@RequestMapping("/core/system")
public class CoreSystemController extends BaseFormController {

    private CoreSystemManager mgr;

    public CoreSystemController() {
        setCancelView("redirect:/core/system");
        setSuccessView("redirect:/core/system");
    }

    @Autowired
    public void setManager(CoreSystemManager mgr) {
        this.mgr = mgr;
    }

	@PreAuthorize("hasAnyRole('CORE:SYSTEM:READ:*')")
    @RequestMapping("")
    public String handle(Model model) {
    	CoreSystem systemWide = mgr.getCurrentSystem();
        model.addAttribute("systemWide", systemWide);
        return "redirect:/core/system/edit";
    }

	@PreAuthorize("hasAnyRole('CORE:SYSTEM:INPUT:*')")
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String edit(Model model) throws Exception {
        CoreSystem systemWide = mgr.getCurrentSystem();
        model.addAttribute("systemWide", systemWide);
        model.addAttribute("status", SystemStatusEnum.ONLINE.getIndex());
        model.addAttribute("holidayPosting", AccrualTypeEnum.values());
    	return "core/system/edit";
    }

	@PreAuthorize("hasAnyRole('CORE:SYSTEM:INPUT:*')")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    protected String update(@ModelAttribute("systemWide") CoreSystem systemWide, BindingResult errors, HttpServletRequest request,
                            HttpServletResponse response) throws Exception {
        if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }

        if (validator != null) {
            validator.validate(systemWide, errors);
            if (errors.hasErrors() && request.getParameter("edit") == null) {

                return "core/system/edit";
            }
        }
        
        CoreSystem oldSystem = mgr.getCurrentSystem();
        
        systemWide.setId(oldSystem.getId());
        systemWide.setTodayDate(oldSystem.getTodayDate());
        systemWide.setPreviousDate(oldSystem.getPreviousDate());
        systemWide.setNextDate(oldSystem.getNextDate());
        systemWide.setCreatedBy(UserUtil.getCurrentUsername());
        systemWide.setCreatedTime(UserUtil.getCurrentSystem(request).getCreatedTime());
        systemWide.setSystemStatus(oldSystem.getSystemStatus());
        systemWide.setUpdatedBy(UserUtil.getCurrentUsername());
        systemWide.setUpdatedTime(UserUtil.getCurrentSystem(request).getCreatedTime());
        systemWide.setVersion(oldSystem.getVersion());
        systemWide.setDefaultHoliday(systemWide.getDefaultHoliday().toUpperCase());
        
        mgr.updateCurrentSystem(systemWide);   	    
    	saveMessage(request, getText("systemWide.saved", "System Wide", request.getLocale()));             
    	return "redirect:/core/system/edit";

    }

	@PreAuthorize("hasAnyRole('CORE:SYSTEM:READ:*')")
    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public String viewSystem(Model model, HttpServletRequest request) throws Exception {
    	CoreSystem systemWide = mgr.getCurrentSystem();
    	model.addAttribute("systemWide", systemWide);
	    model.addAttribute("status", SystemStatusEnum.ONLINE.getIndex());
//	    model.addAttribute("defaultHoliday", "WWWWWHH");
	    return "core/system/view";
    }
    
    @RequestMapping(value = "/param", method = RequestMethod.GET)
    public String paramSettle(Model model, HttpServletRequest request) throws Exception {
    	CoreSystem coreSystem = mgr.getCurrentSystem();
    	model.addAttribute("systemWide", coreSystem);
	    return "core/system/param";
    }

    @RequestMapping(value = "/param", method = RequestMethod.POST)
    protected String Settle(@ModelAttribute("systemWide") CoreSystem systemWide, BindingResult errors, HttpServletRequest request,
                            HttpServletResponse response) throws Exception {
    	//BigDecimal serviceamt = systemWide.getServiceCash();
    	CoreSystem coreSystem = mgr.getCurrentSystem();
    	coreSystem.setServiceCash(systemWide.getServiceCash());
    	mgr.save(coreSystem);
    	saveMessage(request, "Success saved");
    	return "redirect:/mainMenu";
    }


}
