package com.ppsi.jomi.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author : <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 * @version : 1.0, 12/19/12, 6:31 PM
 */
@Controller
public class MainController {
    protected final transient Log log = LogFactory.getLog(MainController.class);

    
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    @RequestMapping(value = "/403")
    public String handle403(HttpServletRequest request, HttpServletResponse response) {
        return "403";
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @RequestMapping(value = "/404")
    public String handle404(HttpServletRequest request) {
        return "404";
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @RequestMapping(value = "/500")
    public String handle500(HttpServletRequest request) {
        return "500";
    }
    
    @RequestMapping(value = "/error/{errorCode}", method = RequestMethod.GET)
    public String handleError(@PathVariable("errorCode") Long errorCode, HttpServletRequest request) {
        return "redirect:/"+errorCode;
    }
    

    @RequestMapping(value = "/core")
    public String listViewCore(HttpServletRequest request) {
        return "/core/view";
    }

    @RequestMapping(value = "core/batch")
    public String viewCoreBatch(HttpServletRequest request) {
        return "/core/view";
    }

    @RequestMapping(value = "core/batch/job")
    public String viewCoreBatchJob(HttpServletRequest request) {
        return "/core/view";
    }


    @RequestMapping(value = "core/reporting")
    public String viewCoreReporting(HttpServletRequest request) {
        return "/core/view";
    }

    //ledger

    @RequestMapping(value = "/ledger")
    public String listViewLedger(HttpServletRequest request) {
        return "/ledger/view";
    }

    @RequestMapping(value = "ledger/reconciliation")
    public String viewLedgerReconciliation(HttpServletRequest request) {
        return "/ledger/view";
    }

    @RequestMapping(value = "ledger/transaction/upload")
    public String viewLedgerTransactionUpload(HttpServletRequest request) {
        return "/ledger/view";
    }


    //data

    @RequestMapping(value = "/data")
    public String listViewData(HttpServletRequest request) {
        return "/data/view";
    }

    @RequestMapping(value = "/ifrs")
    public String ifrs(HttpServletRequest request) {
        return "/ifrs/view";
    }
    @RequestMapping(value = "/psak")
    public String psak(HttpServletRequest request) {
        return "/psak/view";
    }
    @RequestMapping(value = "/data/source")
    public String listViewDataSource(HttpServletRequest request) {
        return "/data/view";
    }


    @RequestMapping(value = "data/definition")
    public String viewLedgerDataDefinition(HttpServletRequest request) {
        return "/data/view";
    }

    
    @RequestMapping(value = "/activationSuccess")
    public String viewActivationSuccess(HttpServletRequest request) {
        return "activationSuccess";
    }
}
