package com.ppsi.jomi.webapp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ppsi.jomi.enumeration.RecordStatusEnum;
import com.ppsi.jomi.persistence.model.CoreProject;
import com.ppsi.jomi.persistence.model.CoreUser;
import com.ppsi.jomi.security.bean.UserUtil;
import com.ppsi.jomi.service.CoreProjectManager;
import com.ppsi.jomi.webapp.dto.CoreUserDto;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:25:42 AM
 */
@Controller
@RequestMapping(value="/core/project")
public class CoreProjectController extends BaseFormController {

	public CoreProjectController() {
		setCancelView("redirect:/core/project");
		setSuccessView("redirect:/core/project");
	}
	
	private CoreProjectManager coreProjectManager;
	
	@Autowired
	public void setCoreProjectManager(CoreProjectManager coreProjectManager) {
		this.coreProjectManager = coreProjectManager;
	}
	
//	@PreAuthorize("hasAnyRole('CORE:PROJECT:READ:*')")
    @RequestMapping(value="")
	public String allDisplay(Model model) throws Exception{
		model.addAttribute("coreProjects", coreProjectManager.getAllLive());
		return "core/project/list";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:PROJECT:INPUT:*')")
    @RequestMapping(value="/add", method = RequestMethod.GET)
	public String addDisplay(Model model) throws Exception{
		model.addAttribute("coreProject", new CoreProject());
		return "core/project/add";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:PROJECT:INPUT:*')")
    @RequestMapping(value="/add", method = RequestMethod.POST)
    public String addProcess(@Valid CoreProject coreProject, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
		if (errors.hasErrors() && request.getParameter("delete") == null) {
        	return "core/project/add";
        }
		coreProjectManager.save(coreProject);
		saveMessage(request, getText("coreProject.saved", coreProject.getName(), request.getLocale()));

        return "redirect:/core/project";
	}
//	public String addProcess(@Valid CoreProject coreProject, BindingResult errors, HttpServletRequest request,
//			HttpServletResponse response, Model model) throws Exception{
//		if (request.getParameter("cancel") != null) {
//            if (!StringUtils.equals(request.getParameter("from"), "list")) {
//                return getCancelView();
//            } else {
//                return getSuccessView();
//            }
//        }
//		if (errors.hasErrors() && request.getParameter("delete") == null) {
//        	return "core/project/add";
//        }
//		coreProjectManager.save(coreProject);
//		saveMessage(request, getText("coreProject.saved", coreProject.getName(), request.getLocale()));
//
//        return "redirect:/core/project";
//	}
	
//	@PreAuthorize("hasAnyRole('CORE:PROJECT:DELETE:*')")
    @RequestMapping(value="/delete/{id}", method = RequestMethod.GET)
	public String deleteDisplay(@PathVariable("id") Long id, Model model) throws Exception{
		model.addAttribute("coreProject", coreProjectManager.get(id));
		return "core/project/delete";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:PROJECT:DELETE:*')")
    @RequestMapping(value="/delete", method = RequestMethod.POST)
	public String deleteProcess(CoreProject coreProject, Model model, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
		coreProject = coreProjectManager.get(coreProject.getId());
		coreProject.setStatus(RecordStatusEnum.HIST.getName());
		coreProject = coreProjectManager.save(coreProject);
		saveMessage(request, getText("coreProject.deleted", coreProject.getName(), request.getLocale()));
		return "redirect:/core/project";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:PROJECT:EDIT:*')")
    @RequestMapping(value="/edit/{id}", method = RequestMethod.GET)
	public String editDisplay(@PathVariable("id") Long id, Model model) throws Exception{
		model.addAttribute("coreProject", coreProjectManager.get(id));
		return "core/project/edit";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:PROJECT:EDIT:*')")
    @RequestMapping(value="/edit/{id}", method = RequestMethod.POST)
	public String editProcess(@Valid CoreProject coreProject, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
        if (errors.hasErrors() && request.getParameter("delete") == null) {
            return "core/project/edit";
        }
        CoreProject oldProject = coreProjectManager.get(coreProject.getId());
        coreProject.setCreatedBy(oldProject.getCreatedBy());
        coreProject.setCreatedTime(oldProject.getCreatedTime());
        coreProject.setStatus(oldProject.getStatus());
        coreProject.setUpdatedBy(UserUtil.getCurrentUsername());
        coreProject.setUpdatedTime(new Date());
        coreProject.setVersion(oldProject.getVersion());
        coreProjectManager.save(coreProject);
        saveMessage(request, getText("coreProject.edited", coreProject.getName(), request.getLocale()));
        
        return "redirect:/core/project";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:PROJECT:READ:*')")
    @RequestMapping(value="/detail/{id}")
	public String detailDisplay(@PathVariable("id") Long id, Model model) throws Exception{
		model.addAttribute("coreProject", coreProjectManager.get(id));
		return "core/project/detail";
	}
    
//    @PreAuthorize("hasAnyRole('CORE:PROJECT:INPUT:*')")
    @RequestMapping(value="/assignmember/{id}", method = RequestMethod.GET)
	public String assignMemberDisplay(@PathVariable("id") Long id, Model model){
		CoreProject coreProject = coreProjectManager.get(id);
		
		Set<CoreUser> projectUsers = coreProject.getCoreUsers();
		List<CoreUserDto> userDtos = new ArrayList<CoreUserDto>();
		List<CoreUser> coreUsers = getUserManager().getAllLive();
		
		for (CoreUser coreUser : coreUsers) {
			Boolean status =false;
			if(projectUsers.contains(coreUser)){
				status = true;
				coreUser.setChecklistStatus(status);
			}
			CoreUserDto dto = new CoreUserDto();
			dto.setChecklistStatus(coreUser.getChecklistStatus());
			dto.setFirstName(coreUser.getFirstName());
			dto.setLastName(coreUser.getLastName());
			dto.setId(coreUser.getId());
			userDtos.add(dto);
		}
		
		model.addAttribute("coreUsers", userDtos);
		model.addAttribute("coreProject", coreProject);
		
		return "core/project/assignmember";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:PROJECT:INPUT:*')")
    @RequestMapping(value="/assignmember/{id}", method = RequestMethod.POST)
	public String assignMemberProcess(@PathVariable("id")Long id, @RequestParam(value="coreUsers", required=false)Integer[] userIds, HttpServletRequest request, HttpServletResponse response, Model model){
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
		} 	
		
		CoreProject coreProject = coreProjectManager.get(id);
		
		if(userIds==null){
			saveError(request, getText("userproject.error", coreProject.getDescription(), request.getLocale()));
			return "redirect:/core/project/assignmember/"+id;
		}
		
		
		Set<CoreUser> selectedUsers = new HashSet<CoreUser>();
		for(Integer userId : userIds){
			selectedUsers.add(getUserManager().get(userId.longValue()));
		}
		
		coreProject.setCoreUsers(selectedUsers);
		coreProjectManager.save(coreProject);
		
		saveMessage(request, getText("userproject.saved", coreProject.getDescription(), request.getLocale()));
		return "redirect:/core/project";
	}
    
}
