package com.ppsi.jomi.webapp.dto;

import java.io.Serializable;

/**
 * 
 * @author dimas.sulistyono
 * 
 *         Aug 9, 2016
 */
public class ApplicationTypeDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9050309975516234248L;
	private String applicationValue;
	private String applicationType;

	public String getApplicationValue() {
		return applicationValue;
	}

	public void setApplicationValue(String applicationValue) {
		this.applicationValue = applicationValue;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	@Override
	public String toString() {
		return "ApplicationTypeDto [applicationValue=" + applicationValue
				+ ", applicationType=" + applicationType + "]";
	}

	public ApplicationTypeDto() {
		super();
	}

	public ApplicationTypeDto(String applicationValue, String applicationType) {
		super();
		this.applicationValue = applicationValue;
		this.applicationType = applicationType;
	}

}
