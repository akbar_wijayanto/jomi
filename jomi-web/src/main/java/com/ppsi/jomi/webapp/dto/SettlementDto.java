/**
 * 
 */
package com.ppsi.jomi.webapp.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author akbar.wijayanto Date Dec 20, 2015 2:43:30 PM
 */
public class SettlementDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String sellerId;
	private String sellerName;
	private Date orderDate;
	private String itemId;
	private String itemName;
	private Integer quantity;
	private BigDecimal sellingPrice;
	private BigDecimal total;
	private BigDecimal grandTotal;
	private BigDecimal percentage;
	private BigDecimal serviceCash;
	private BigDecimal trxTotal;
	private BigDecimal settlementTotal;
	private BigDecimal feeServiceCash;
	private String accountNumber;

	public String getSellerId() {
		return sellerId;
	}

	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

	public BigDecimal getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}

	public BigDecimal getServiceCash() {
		return serviceCash;
	}

	public void setServiceCash(BigDecimal serviceCash) {
		this.serviceCash = serviceCash;
	}

	public BigDecimal getTrxTotal() {
		return trxTotal;
	}

	public void setTrxTotal(BigDecimal trxTotal) {
		this.trxTotal = trxTotal;
	}

	public BigDecimal getSettlementTotal() {
		return settlementTotal;
	}

	public void setSettlementTotal(BigDecimal settlementTotal) {
		this.settlementTotal = settlementTotal;
	}

	public BigDecimal getFeeServiceCash() {
		return feeServiceCash;
	}

	public void setFeeServiceCash(BigDecimal feeServiceCash) {
		this.feeServiceCash = feeServiceCash;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
}
