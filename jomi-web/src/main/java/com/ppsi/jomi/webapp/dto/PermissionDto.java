package com.ppsi.jomi.webapp.dto;

import java.util.List;

import com.ppsi.jomi.persistence.model.CorePermission;


public class PermissionDto {

	private String applicationName;
	private List<CorePermission> corePermissions;
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public List<CorePermission> getCorePermissions() {
		return corePermissions;
	}
	public void setCorePermissions(List<CorePermission> corePermissions) {
		this.corePermissions = corePermissions;
	}


}

