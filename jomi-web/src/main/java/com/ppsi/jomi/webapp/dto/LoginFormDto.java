/**
 * 
 */
package com.ppsi.jomi.webapp.dto;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author rangga.putra
 *
 */
public class LoginFormDto {

	private String j_username;
	private String j_password;
	
	@NotEmpty
	public String getJ_username() {
		return j_username;
	}
	public void setJ_username(String j_username) {
		this.j_username = j_username;
	}
	
	@NotEmpty
	public String getJ_password() {
		return j_password;
	}
	public void setJ_password(String j_password) {
		this.j_password = j_password;
	}

}
