package com.ppsi.jomi.webapp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ppsi.jomi.enumeration.RecordStatusEnum;
import com.ppsi.jomi.persistence.model.CoreMenu;
import com.ppsi.jomi.persistence.model.CorePermission;
import com.ppsi.jomi.persistence.model.CoreRole;
import com.ppsi.jomi.security.bean.UserUtil;
import com.ppsi.jomi.service.CoreMenuManager;
import com.ppsi.jomi.service.CorePermissionManager;
import com.ppsi.jomi.service.CoreRoleManager;
import com.ppsi.jomi.service.CoreUserManager;
import com.ppsi.jomi.webapp.dto.CoreMenuDto;
import com.ppsi.jomi.webapp.dto.PermissionDto;

@Controller
@RequestMapping(value="/core/role")
public class CoreRoleController extends BaseFormController {
	private CoreRoleManager coreRoleManager = null;
	private CoreMenuManager coreMenuManager;
	private CoreUserManager coreUserManager;
	private CorePermissionManager corePermissionManager;
	
	public CoreRoleController(){
		setCancelView("redirect:/core/role");
		setSuccessView("redirect:/core/role");
	}

	@Autowired
	public void setCoreUserManager(CoreUserManager coreUserManager) {
		this.coreUserManager = coreUserManager;
	}

	@Autowired
	public void setCoreRoleManager(CoreRoleManager coreRoleManager){
		this.coreRoleManager = coreRoleManager;
	}
	
	@Autowired
	public void setCoreMenuManager(CoreMenuManager coreMenuManager){
		this.coreMenuManager = coreMenuManager;
	}
	
	@Autowired
	public void setCorePermissionManager(CorePermissionManager corePermissionManager){
		this.corePermissionManager = corePermissionManager;
	}
	
	@PreAuthorize("hasAnyRole('CORE:ROLE:READ:*')")
    @RequestMapping(value="")
	public String allDisplay(Model model) throws Exception{
		model.addAttribute("coreRoles", coreRoleManager.getAll());
		return "core/role/list";
	}
	
	@PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
    @RequestMapping(value="/add", method = RequestMethod.GET)
	public String addDisplay(Model model) throws Exception{
		model.addAttribute("coreRole", new CoreRole());
		return "core/role/add";
	}
	
	@PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
    @RequestMapping(value="/add", method = RequestMethod.POST)
	public String addProcess(@Valid CoreRole coreRole, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }

        if (errors.hasErrors() && request.getParameter("delete") == null) {
        	return "core/role/add";
        }
        CoreRole existing = this.coreRoleManager.getRole(coreRole.getName());
        if (existing != null) {
            saveError(request, getText("errors.exists", coreRole.getName(), request.getLocale()));
            return "core/role/add";
        }
        
//        try {
//        	if(coreRole.getId() == null){
//        		coreRole.setId(Integer.MIN_VALUE);
//        	}
//            coreRoleManager.get(coreRole.getId());
//            saveError(request, getText("errors.exists", String.valueOf(coreRole.getId()), request.getLocale()));
//            return "core/role/add";
//
//        } catch (ObjectRetrievalFailureException ex) {
        	coreRole = coreRoleManager.save(coreRole);
            coreRoleManager.authorize(null, coreRole, true);
            saveMessage(request, getText("coreRole.saved", coreRole.getName(), request.getLocale()));
//        }
        
        return "redirect:/core/role";
	}
	
	@PreAuthorize("hasAnyRole('CORE:ROLE:DELETE:*')")
    @RequestMapping(value="/delete/{id}", method = RequestMethod.GET)
	public String deleteDisplay(@PathVariable("id") Integer id, Model model) throws Exception{
		model.addAttribute("coreRole", coreRoleManager.getWithListAll(id));
		return "core/role/delete";
	}
	
	@PreAuthorize("hasAnyRole('CORE:ROLE:DELETE:*')")
    @RequestMapping(value="/delete", method = RequestMethod.POST)
	public String deleteProcess(CoreRole coreRole, Model model, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }

		if(!coreUserManager.getCheckRoleOnUser(coreRole.getId()).isEmpty()){
			saveError(request, getText("coreRole.roleUsedByUser", coreRole.getId().toString(), request.getLocale()));
			return "core/role/delete";
		}
		
		coreRole = coreRoleManager.get(coreRole.getId());
		coreRole.setStatus(RecordStatusEnum.HIST.getName());
		coreRoleManager.authorize(null, coreRole, true);
		saveMessage(request, getText("coreRole.deleted", coreRole.getName(), request.getLocale()));
		return "redirect:/core/role";
	}
	
	@PreAuthorize("hasAnyRole('CORE:ROLE:EDIT:*')")
    @RequestMapping(value="/edit/{id}", method = RequestMethod.GET)
	public String editDisplay(@PathVariable("id") Integer id, Model model) throws Exception{
		model.addAttribute("coreRole", coreRoleManager.getWithListAll(id));
		return "core/role/edit";
	}
	
	@PreAuthorize("hasAnyRole('CORE:ROLE:EDIT:*')")
    @RequestMapping(value="/edit/{id}", method = RequestMethod.POST)
	public String editProcess(@Valid CoreRole coreRole, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
        if (errors.hasErrors() && request.getParameter("delete") == null) {
            return "core/role/edit";
        }
        //coreRoleManager.save(coreRole);
        CoreRole oldRole = coreRoleManager.getWithListAll(coreRole.getId());
        coreRole.setCreatedBy(oldRole.getCreatedBy());
        coreRole.setCreatedTime(oldRole.getCreatedTime());
        coreRole.setStatus(oldRole.getStatus());
        coreRole.setUpdatedBy(UserUtil.getCurrentUsername());
        coreRole.setUpdatedTime(new Date());
        coreRole.setVersion(oldRole.getVersion());
        coreRole.setCoreMenus(oldRole.getCoreMenus());
        coreRole.setCorePermissions(oldRole.getCorePermissions());
        coreRoleManager.authorize(oldRole, coreRole, true);
        
        saveMessage(request, getText("coreRole.edited", coreRole.getName(), request.getLocale()));
        return "redirect:/core/role";
	}
	
	@PreAuthorize("hasAnyRole('CORE:ROLE:READ:*')")
    @RequestMapping(value="/detail/{id}")
	public String detailDisplay(@PathVariable("id") Integer id, Model model) throws Exception{
		model.addAttribute("coreRole", coreRoleManager.getWithListAll(id));
		return "core/role/detail";
	}
	
	@PreAuthorize("hasAnyRole('CORE:ROLE:READ:*')")
    @RequestMapping(value="/managemenu/{id}", method = RequestMethod.GET)
	public String addMenuDisplay(@PathVariable("id") Integer id, Model model) throws Exception{
		 
		 CoreRole coreRole = coreRoleManager.getWithListAll(id);
		 List<CoreMenuDto> dtos = new ArrayList<CoreMenuDto>();
		 List<CoreMenu> cps = coreMenuManager.getAllOrderByOrderMenu();
	     Set<CoreMenu> ucps = coreRole.getCoreMenus();
//	     List<LabelValueStatus> lvs = new ArrayList<LabelValueStatus>();
	     for (CoreMenu cp : cps) {
	          Boolean status =false;
	          if (ucps.contains(cp)) {
	              status = true;
	          }
	          cp.setChecklistStatus(status);
	          CoreMenuDto dto = new CoreMenuDto(cp);
	          if(cp.getParentId().compareTo(0) != 0)
	        	  dto.setParentId(coreMenuManager.get(cp.getParentId()).getDescription());
	          else
	        	  dto.setParentId("-");
	          dtos.add(dto);
//	          lvs.add(new LabelValueStatus(cp.getDescription(), cp.getId().toString(), status));
	     }
	     model.addAttribute("coreRole", coreRole);
	     model.addAttribute("coreMenus",dtos );
		return "core/role/managemenu";
	}
	
	@PreAuthorize("hasAnyRole('CORE:ROLE:READ:*')")
    @RequestMapping(value="/managemenu/{id}", method = RequestMethod.POST)
	public String saveMenuDisplay(@PathVariable("id") Integer id, @RequestParam(value="coreMenus",required=false) Integer[] menuIDs, 
			HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }

		if(menuIDs==null){
			List<CoreMenuDto> dtos = new ArrayList<CoreMenuDto>();
			CoreRole coreRole = coreRoleManager.getWithListAll(id);
			List<CoreMenu> cps = coreMenuManager.getAll();
		    Set<CoreMenu> ucps = coreRole.getCoreMenus();
		    for (CoreMenu cp : cps) {
		          Boolean status =false;
		          if (ucps.contains(cp)) {
		              status = true;
		          }
		          cp.setChecklistStatus(status);
		          CoreMenuDto dto = new CoreMenuDto(cp);
		          if(cp.getParentId().compareTo(0) != 0)
		        	  dto.setParentId(coreMenuManager.get(cp.getParentId()).getDescription());
		          else
		        	  dto.setParentId("-");
		          dtos.add(dto);
		     }
		     model.addAttribute("coreRole", coreRole);
		     model.addAttribute("coreMenus",cps );
		     model.addAttribute("coreMenus",dtos );
			 model.addAttribute("errors", "Please select menu parent, child or submenu first");
			 return "core/role/managemenu";
		}		
            
		CoreRole oldRole = coreRoleManager.getWithListAll(id);
		CoreRole coreRole = coreRoleManager.getWithListAll(id);
		
		Set<CoreMenu> coreMenus = new HashSet<CoreMenu>();
		for (Integer menuID : menuIDs) {
			coreMenus.add(coreMenuManager.get(menuID));
		}
		
		List<CoreMenu> cps = coreMenuManager.getAll();

    	for (int i = 0;i<cps.size();i++){
    		CoreMenu cp = cps.get(i);
			if (coreMenus.contains(cp)) {
	              if (cp.getType()!="P"){
	        		  for (CoreMenu cp1 : cps){
	            		  if (cp.getParentId()==cp1.getId()){
	            			  if (cp1.getChecklistStatus()==null || cp1.getChecklistStatus()==false){
	            				  cp.setChecklistStatus(true);
	            				  cp1.setChecklistStatus(true);
	            				  CoreMenu coreMenuTemp = coreMenuManager.get(cp1.getId());	            				  
	            				
	            				  coreMenuTemp.setChecklistStatus(true);

	            				  coreMenus.add(coreMenuTemp);
	            				  i=-1;
	            				  break;
	            			  }	            			
	            		  }
	            	  }      	  
	              }
	          }
	    }       	
    	
	   /* for (CoreMenu cp : cps) {
	          if (ucps.contains(cp)) {
	              if (cp.getType()!="P"){
	            	  if (cps.size()>0){
	            		  for (int j=cps.size()-1;j>=0;j--){
		            		  CoreMenu cp1=cps.get(j);
		            		  if (cp.getParentId()==cp1.getId()){
		            			  if (cp1.getChecklistStatus()==null || cp1.getChecklistStatus()==false){
		            				  cp.setChecklistStatus(true);
		            				  coreMenus.add(coreMenuManager.get(cp1.getId()));
		            			  }	            			
		            		  }
		            	  } 
	            	  }	            	  
	              }
	          }
	     }*/
		
		
		coreRole.setCoreMenus(coreMenus);
//		coreRoleManager.saveWithoutAuthorize(coreRole);
		coreRoleManager.authorize(oldRole, coreRole, true);
		saveMessage(request, getText("managemenu.saved", coreRole.getName(), request.getLocale()));
		return "redirect:/core/role/detail/" + id;
	}
	
	@PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
    @RequestMapping(value="/managepermission/{id}", method = RequestMethod.GET)
	public String managePermission(@PathVariable("id")Integer id, Model model){
		CoreRole coreRole = coreRoleManager.getWithListAll(id);
		
		Set<CorePermission> rolepermissions = coreRole.getCorePermissions();
		List<PermissionDto> permissionsDtos = new ArrayList<PermissionDto>();
		
		for(String appName : corePermissionManager.getAplication()){
			PermissionDto dto = new PermissionDto();
			dto.setApplicationName(appName);
			
			List<CorePermission> appPermissions = corePermissionManager.getByAplication(appName);
			for(CorePermission corePermission : appPermissions){
				if(rolepermissions.contains(corePermission)) 
					corePermission.setChecklistStatus(true);
				else 
					corePermission.setChecklistStatus(false);
			}
			dto.setCorePermissions(appPermissions);
			permissionsDtos.add(dto);
		}
		
		model.addAttribute("permissionDtos", permissionsDtos);
		model.addAttribute("coreRole", coreRole);
		
		return "core/role/managepermission";
	}
	
	@PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
    @RequestMapping(value="/managepermission/{id}", method = RequestMethod.POST)
	public String managePermission(@PathVariable("id")Integer id, @RequestParam(value="permissionIds", required=false)Integer[] permissionIds, HttpServletRequest request, HttpServletResponse response, Model model){
		
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
		} 	
		
		CoreRole coreRole = coreRoleManager.getWithListAll(id);
		
		if(permissionIds==null){
			saveError(request, getText("rolepermission.error", coreRole.getDescription(), request.getLocale()));
			return "redirect:/core/role/managepermission/"+id;
		}
		
		
		Set<CorePermission> selectedPermissions = new HashSet<CorePermission>();
		for(Integer permissionId : permissionIds){
			selectedPermissions.add(corePermissionManager.get(permissionId));
		}
		
		coreRole.setCorePermissions(selectedPermissions);
		coreRoleManager.save(coreRole);
		
		saveMessage(request, getText("rolepermission.saved", coreRole.getDescription(), request.getLocale()));
		return "redirect:/core/role";
	}
	
	
	@PreAuthorize("hasAnyRole('CORE:ROLE:READ:*')")
    @RequestMapping(value="/userrole/{id}")
	public String userRoleDisplay(@PathVariable("id") Integer id, Model model) throws Exception{
		CoreRole coreRole = coreRoleManager.getWithListAll(id);	
		model.addAttribute("coreRole", coreRole);
		model.addAttribute("coreUserRole", coreUserManager.getUserRoleList(id));
		return "core/role/userrole";
	}
	
	
}
