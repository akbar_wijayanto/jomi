package com.ppsi.jomi.webapp.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ppsi.jomi.enumeration.RecordStatusEnum;
import com.ppsi.jomi.enumeration.jomi.NonBillableMandaysTypeEnum;
import com.ppsi.jomi.persistence.model.NonBillableMandays;
import com.ppsi.jomi.security.bean.UserUtil;
import com.ppsi.jomi.service.NonBillableMandaysManager;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:25:42 AM
 */
@Controller
@RequestMapping(value="/core/nonBillable")
public class NonBillableMandaysController extends BaseFormController {

	public NonBillableMandaysController() {
		setCancelView("redirect:/core/nonBillable");
		setSuccessView("redirect:/core/nonBillable");
	}
	
	private NonBillableMandaysManager nonBillableMandaysManager;
	
	@Autowired
	public void setNonBillableMandaysManager(NonBillableMandaysManager nonBillableMandaysManager) {
		this.nonBillableMandaysManager = nonBillableMandaysManager;
	}
	
//	@PreAuthorize("hasAnyRole('CORE:NONBILLABLE:READ:*')")
    @RequestMapping(value="")
	public String allDisplay(Model model) throws Exception{
		model.addAttribute("nonBillables", nonBillableMandaysManager.getAllLive());
		return "core/nonbillable/list";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:NONBILLABLE:INPUT:*')")
    @RequestMapping(value="/add", method = RequestMethod.GET)
	public String addDisplay(Model model) throws Exception{
		model.addAttribute("nonBillable", new NonBillableMandays());
		model.addAttribute("nonBillableType", NonBillableMandaysTypeEnum.values());
		return "core/nonbillable/add";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:NONBILLABLE:INPUT:*')")
    @RequestMapping(value="/add", method = RequestMethod.POST)
    public String addProcess(@Valid NonBillableMandays nonBillable, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
		if (errors.hasErrors() && request.getParameter("delete") == null) {
			model.addAttribute("nonBillable", nonBillable);
			model.addAttribute("nonBillableType", NonBillableMandaysTypeEnum.values());
        	return "core/nonbillable/add";
        }
		nonBillableMandaysManager.save(nonBillable);
		saveMessage(request, getText("nonBillable.saved", nonBillable.getNonBillableMandaysTypeEnum().getLabel(), request.getLocale()));

        return "redirect:/core/nonbillable";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:NONBILLABLE:DELETE:*')")
    @RequestMapping(value="/delete/{id}", method = RequestMethod.GET)
	public String deleteDisplay(@PathVariable("id") Long id, Model model) throws Exception{
		model.addAttribute("nonBillable", nonBillableMandaysManager.get(id));
		model.addAttribute("nonBillableType", NonBillableMandaysTypeEnum.values());
		return "core/nonbillable/delete";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:NONBILLABLE:DELETE:*')")
    @RequestMapping(value="/delete", method = RequestMethod.POST)
	public String deleteProcess(NonBillableMandays nonBillable, Model model, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
		nonBillable = nonBillableMandaysManager.get(nonBillable.getId());
		nonBillable.setStatus(RecordStatusEnum.HIST.getName());
		nonBillable = nonBillableMandaysManager.save(nonBillable);
		saveMessage(request, getText("nonBillable.deleted", nonBillable.getNonBillableMandaysTypeEnum().getLabel(), request.getLocale()));
		return "redirect:/core/nonbillable";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:NONBILLABLE:EDIT:*')")
    @RequestMapping(value="/edit/{id}", method = RequestMethod.GET)
	public String editDisplay(@PathVariable("id") Long id, Model model) throws Exception{
    	NonBillableMandays nonBillable = nonBillableMandaysManager.get(id);
		model.addAttribute("nonBillable", nonBillable);
		model.addAttribute("nonBillableType", NonBillableMandaysTypeEnum.values());
		return "core/nonbillable/edit";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:NONBILLABLE:EDIT:*')")
    @RequestMapping(value="/edit/{id}", method = RequestMethod.POST)
	public String editProcess(@Valid NonBillableMandays nonBillable, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
        if (errors.hasErrors() && request.getParameter("delete") == null) {
        	model.addAttribute("nonBillable", nonBillable);
        	model.addAttribute("nonBillableType", NonBillableMandaysTypeEnum.values());
            return "core/nonbillable/edit";
        }
        NonBillableMandays oldTask = nonBillableMandaysManager.get(nonBillable.getId());
        nonBillable.setCreatedBy(oldTask.getCreatedBy());
        nonBillable.setCreatedTime(oldTask.getCreatedTime());
        nonBillable.setStatus(oldTask.getStatus());
        nonBillable.setUpdatedBy(UserUtil.getCurrentUsername());
        nonBillable.setUpdatedTime(new Date());
        nonBillable.setVersion(oldTask.getVersion());
        nonBillable = nonBillableMandaysManager.save(nonBillable);
        saveMessage(request, getText("nonBillable.edited", nonBillable.getNonBillableMandaysTypeEnum().getLabel(), request.getLocale()));
        
        return "redirect:/core/nonbillable";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:NONBILLABLE:READ:*')")
    @RequestMapping(value="/detail/{id}")
	public String detailDisplay(@PathVariable("id") Long id, Model model) throws Exception{
		model.addAttribute("nonBillable", nonBillableMandaysManager.get(id));
		model.addAttribute("nonBillableType", NonBillableMandaysTypeEnum.values());
		return "core/nonbillable/detail";
	}
    
}
