package com.ppsi.jomi.service;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

public interface UploadFileManager {
	public String saveFile(CommonsMultipartFile file, String subfolders, String fileName);
	public String saveFile2(MultipartFile file, String saveDirectory,String subfolders, String fileName);
}
