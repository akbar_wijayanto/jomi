package com.ppsi.jomi.service;


import java.util.Date;
import java.util.List;

import com.ppsi.jomi.persistence.model.AuditTrail;

public interface CoreAuditTrailManager {

    List<AuditTrail> getAll();

    AuditTrail get(Long id);

//    List<AuditTrail> getPagingAudit(Long firstResult, Long maxResults, String objectName, String fieldName, Date dateFrom, Date dateTo, String actor, String action);
//
    Long getCountAudit(String objectName, String fieldName, Date dateFrom, Date dateTo, String actor, String action);
    List<AuditTrail> getSearchAuditTrail(String objectName, String fieldName, Date dateFrom, Date dateTo, String actor, String action);
    List<AuditTrail> getSearchAuditTrail(Long start, Long end, String objectName, String fieldName, Date dateFrom, Date dateTo, String actor, String action);

}
