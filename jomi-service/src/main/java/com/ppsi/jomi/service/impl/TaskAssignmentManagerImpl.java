package com.ppsi.jomi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppsi.jomi.enumeration.jomi.TaskStatusEnum;
import com.ppsi.jomi.persistence.dao.TaskAssignmentDao;
import com.ppsi.jomi.persistence.model.TaskAssignment;
import com.ppsi.jomi.service.TaskAssignmentManager;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:20:53 AM
 */
@Service("taskAssignmentManager")
public class TaskAssignmentManagerImpl extends GenericManagerImpl<TaskAssignment, Long>
		implements TaskAssignmentManager {
	
	private TaskAssignmentDao taskAssignmentDao;
	
	@Autowired
	public void setTaskAssignmentDao(TaskAssignmentDao taskAssignmentDao) {
		this.taskAssignmentDao = taskAssignmentDao;
		this.dao = taskAssignmentDao;
	}

	@Override
	public List<TaskAssignment> getTaskByUsername(String username) {
		return taskAssignmentDao.getTaskByUsername(username);
	}

	@Override
	public List<TaskAssignment> getTaskByUsernameAndStatus(String username, TaskStatusEnum taskStatusEnum) {
		return taskAssignmentDao.getTaskByUsernameAndStatus(username, taskStatusEnum);
	}
	
}