package com.ppsi.jomi.service;

import java.util.List;

import com.ppsi.jomi.persistence.model.CorePermission;

public interface CorePermissionManager extends GenericManager<CorePermission, Integer> {
	List<String> getAplication();
	List<CorePermission> getByAplication(String applicationName);

}
