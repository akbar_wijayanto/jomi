package com.ppsi.jomi.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Transactional;

import com.ppsi.jomi.persistence.dao.GenericDao;
import com.ppsi.jomi.persistence.model.BaseGenericObject;
import com.ppsi.jomi.service.GenericManager;

import java.io.Serializable;
import java.util.List;

/**
 * This class serves as the Base class for all other Managers - namely to hold
 * common CRUD methods that they might all use. You should only need to extend
 * this class when your require custom CRUD logic.
 *
 * @param <T>  a type variable
 * @param <PK> the primary key for that type
 * @author <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 */
public class GenericManagerImpl<T extends BaseGenericObject, PK extends Serializable> implements GenericManager<T, PK> {
    /**
     * Log variable for all child classes. Uses LogFactory.getLog(getClass()) from Commons Logging
     */
    protected final Log log = LogFactory.getLog(getClass());

    /**
     * GenericDao instance, set by constructor of child classes
     */
    protected GenericDao<T, PK> dao;


    public GenericManagerImpl() {
    }

    public GenericManagerImpl(GenericDao<T, PK> genericDao) {
        this.dao = genericDao;
    }

    /**
     * {@inheritDoc}
     */
    public List<T> getAll() {
        return dao.getAll();
    }

    /**
     * {@inheritDoc}
     */
    public T get(PK id) {
        return dao.get(id);
    }

    /**
     * {@inheritDoc}
     */
    public boolean exists(PK id) {
        return dao.exists(id);
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    public T save(T object) {
    	
        return dao.save(object);
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    public void remove(PK id) {
        dao.remove(id);
    }

    @Override
    public List<T> getPagingResults(Long firstResult, Long maxResults) {
        return dao.getPagingResults(firstResult, maxResults);
    }

    @Override
    public Long getRecordCount() {
        return dao.getRecordCount();
    }

    /**
     * {@inheritDoc}
     */
//    public T authorize(T object, boolean isApproved) {
//        return dao.authorize(object, isApproved);
//    }


	@Override
	public List<T> getUnauthList() {
		return dao.getUnauthList();
	}
	
	@Override
	public List<T> getAllLive() {
		return dao.getAllLive();
	}

	@Override
	public T getLiveById(PK id) {
		return dao.getLiveById(id);
	}

	@Override
	@Transactional
	public T authorize(T oldObject, T newObject, boolean isApproved) {
		return dao.authorize(oldObject, newObject, isApproved);
	}

	@Override
	public void removeAll() {
		dao.removeAll();
	}

	@Override
	public void preAuthorize(Object object) {
	}

	@Override
	@Transactional
	public T saveWithAuthorize(T object) {
		return dao.saveWithAuthorize(object);
	}

	@Override
	public void deleteAuthorize(Object object) {
	}

}
