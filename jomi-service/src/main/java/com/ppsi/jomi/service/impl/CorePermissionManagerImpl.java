package com.ppsi.jomi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppsi.jomi.persistence.dao.CorePermissionDao;
import com.ppsi.jomi.persistence.model.CorePermission;
import com.ppsi.jomi.service.CorePermissionManager;

@Service("corePermissionManager")
public class CorePermissionManagerImpl extends GenericManagerImpl<CorePermission, Integer> implements CorePermissionManager {
	private CorePermissionDao corePermissionDao;
	
	@Autowired
	public void setCorePermissionDao(CorePermissionDao corePermissionDao){
		this.dao = corePermissionDao;
		this.corePermissionDao = corePermissionDao;
	}

	@Override
	public List<CorePermission> getPagingResults (Long firstResult, Long maxResults){
		return corePermissionDao.getPagingResults(firstResult, maxResults);
	}

	@Override
	public List<String> getAplication() {
		return corePermissionDao.getAplication();
	}

	@Override
	public List<CorePermission> getByAplication(String applicationName) {
		return corePermissionDao.getByAplication(applicationName);
	}
}
