package com.ppsi.jomi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppsi.jomi.persistence.dao.NonBillableMandaysDao;
import com.ppsi.jomi.persistence.model.NonBillableMandays;
import com.ppsi.jomi.service.NonBillableMandaysManager;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:20:53 AM
 */
@Service("nonBillableMandaysManager")
public class NonBillableMandaysManagerImpl extends GenericManagerImpl<NonBillableMandays, Long>
		implements NonBillableMandaysManager {

	private NonBillableMandaysDao nonBillableMandaysDao;
	
	@Autowired
	public void setNonBillableMandaysDao(NonBillableMandaysDao nonBillableMandaysDao) {
		this.nonBillableMandaysDao = nonBillableMandaysDao;
		this.dao = nonBillableMandaysDao;
	}

	@Override
	public List<NonBillableMandays> getNonBillableByUserId(Long userId) {
		return nonBillableMandaysDao.getNonBillableByUserId(userId);
	}
	
}