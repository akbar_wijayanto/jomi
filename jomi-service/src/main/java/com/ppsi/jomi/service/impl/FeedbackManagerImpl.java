package com.ppsi.jomi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppsi.jomi.persistence.dao.FeedbackDao;
import com.ppsi.jomi.persistence.model.Feedback;
import com.ppsi.jomi.service.FeedbackManager;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:20:53 AM
 */
@Service("eedbackManager")
public class FeedbackManagerImpl extends GenericManagerImpl<Feedback, Long>
		implements FeedbackManager {

	private FeedbackDao feedbackDao;
	
	@Autowired
	public void setFeedbackDao(FeedbackDao feedbackDao) {
		this.feedbackDao = feedbackDao;
		this.dao = feedbackDao;
	}
	
}