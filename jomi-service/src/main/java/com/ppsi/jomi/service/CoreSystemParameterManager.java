package com.ppsi.jomi.service;

import com.ppsi.jomi.persistence.model.CoreSystemParameter;

public interface CoreSystemParameterManager extends GenericManager<CoreSystemParameter, String>{

}
