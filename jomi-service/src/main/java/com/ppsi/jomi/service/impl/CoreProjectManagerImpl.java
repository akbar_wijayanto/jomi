package com.ppsi.jomi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppsi.jomi.persistence.dao.CoreProjectDao;
import com.ppsi.jomi.persistence.model.CoreProject;
import com.ppsi.jomi.service.CoreProjectManager;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:20:53 AM
 */
@Service("coreProjectManager")
public class CoreProjectManagerImpl extends GenericManagerImpl<CoreProject, Long>
		implements CoreProjectManager {

	private CoreProjectDao coreProjectDao;
	
	@Autowired
	public void setCoreProjectDao(CoreProjectDao coreProjectDao) {
		this.coreProjectDao = coreProjectDao;
		this.dao = coreProjectDao;
	}

	@Override
	public List<CoreProject> getProjectByUsername(String username) {
		return coreProjectDao.getProjectByUsername(username);
	}
	
}