package com.ppsi.jomi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppsi.jomi.persistence.dao.CoreSystemParameterDao;
import com.ppsi.jomi.persistence.model.CoreSystemParameter;
import com.ppsi.jomi.service.CoreSystemParameterManager;

@Service("coreSystemParameterManager")
public class CoreSystemParameterManagerImpl extends GenericManagerImpl<CoreSystemParameter, String> implements CoreSystemParameterManager{

	private CoreSystemParameterDao coreSystemParameterDao;

	@Autowired
	public void setCoreSystemParameterDao(
			CoreSystemParameterDao coreSystemParameterDao) {
		this.coreSystemParameterDao = coreSystemParameterDao;
		this.dao = coreSystemParameterDao;
	}
	
}
