package com.ppsi.jomi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppsi.jomi.persistence.dao.CoreTaskDao;
import com.ppsi.jomi.persistence.model.CoreTask;
import com.ppsi.jomi.service.CoreTaskManager;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:20:53 AM
 */
@Service("coreTaskManager")
public class CoreTaskManagerImpl extends GenericManagerImpl<CoreTask, Long>
		implements CoreTaskManager {

	private CoreTaskDao coreTaskDao;
	
	@Autowired
	public void setCoreTaskDao(CoreTaskDao coreTaskDao) {
		this.coreTaskDao = coreTaskDao;
		this.dao = coreTaskDao;
	}

	@Override
	public List<CoreTask> getByProjectId(Long projectId) {
		return coreTaskDao.getByProjectId(projectId);
	}

	@Override
	public List<Object[]> getCountAllPriorityByProject(Long projectId) {
		return coreTaskDao.getCountAllPriorityByProject(projectId);
	}

	@Override
	public List<Object[]> getCountAllComplexityByProject(Long projectId) {
		return coreTaskDao.getCountAllComplexityByProject(projectId);
	}

	@Override
	public List<Object[]> getCountAllStatusByProject(Long projectId) {
		return coreTaskDao.getCountAllStatusByProject(projectId);
	}
	
}