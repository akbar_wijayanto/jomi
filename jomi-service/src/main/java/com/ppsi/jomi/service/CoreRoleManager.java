package com.ppsi.jomi.service;

import java.util.List;

import com.ppsi.jomi.persistence.model.CoreRole;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer.
 *
 * @author <a href="mailto:dan@getrolling.com">Dan Kibler </a>
 */
public interface CoreRoleManager extends GenericManager<CoreRole, Integer> {
    /**
     * {@inheritDoc}
     */
    List getRoles(CoreRole coreRole);

    /**
     * {@inheritDoc}
     */
    CoreRole getRole(String rolename);

    /**
     * {@inheritDoc}
     */
    CoreRole saveRole(CoreRole coreRole);

    /**
     * {@inheritDoc}
     */
    void removeRole(String rolename);

	List<CoreRole> getAllWithList();

	CoreRole getWithListAll(Integer roleID);
	CoreRole getById(Integer id);

}
