package com.ppsi.jomi.service;

import java.util.List;

import com.ppsi.jomi.persistence.model.NonBillableMandays;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:19:41 AM
 */
public interface NonBillableMandaysManager extends GenericManager<NonBillableMandays, Long> {

	List<NonBillableMandays> getNonBillableByUserId(Long userId);
	
}
