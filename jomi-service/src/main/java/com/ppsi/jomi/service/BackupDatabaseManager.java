package com.ppsi.jomi.service;

public interface BackupDatabaseManager {
	void backupDatabase();
	boolean isExist(String path);
}
