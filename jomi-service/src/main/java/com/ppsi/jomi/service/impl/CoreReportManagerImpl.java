package com.ppsi.jomi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppsi.jomi.persistence.dao.CoreReportDao;
import com.ppsi.jomi.persistence.model.CoreReport;
import com.ppsi.jomi.service.CoreReportManager;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:20:53 AM
 */
@Service("coreReportManager")
public class CoreReportManagerImpl extends GenericManagerImpl<CoreReport, Long>
		implements CoreReportManager {

	private CoreReportDao coreReportDao;
	
	@Autowired
	public void setCoreReportDao(CoreReportDao coreReportDao) {
		this.coreReportDao = coreReportDao;
		this.dao = coreReportDao;
	}
	
}