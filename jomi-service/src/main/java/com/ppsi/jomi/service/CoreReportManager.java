package com.ppsi.jomi.service;

import com.ppsi.jomi.persistence.model.CoreReport;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:19:41 AM
 */
public interface CoreReportManager extends GenericManager<CoreReport, Long> {

}
