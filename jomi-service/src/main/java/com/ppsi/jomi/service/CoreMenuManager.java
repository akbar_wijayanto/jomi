package com.ppsi.jomi.service;

import java.util.List;

import com.ppsi.jomi.dto.MenuDto;
import com.ppsi.jomi.persistence.model.CoreMenu;

public interface CoreMenuManager extends GenericManager<CoreMenu, Integer> {

    List<MenuDto> getUserMenus(String username);
    List<MenuDto> getUserMenusByActive(Integer roleId);

    List<CoreMenu> getByOrderMenu(Integer parentId, Integer orderMenu);
    List<CoreMenu> getAllOrderByOrderMenu();
    Integer getByPermaLink(String link);
}
