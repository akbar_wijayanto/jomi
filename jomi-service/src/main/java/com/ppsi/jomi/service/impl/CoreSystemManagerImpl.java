package com.ppsi.jomi.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppsi.jomi.Constants;
import com.ppsi.jomi.enumeration.SystemStatusEnum;
import com.ppsi.jomi.persistence.dao.CoreSystemDao;
import com.ppsi.jomi.persistence.model.CoreSystem;
import com.ppsi.jomi.persistence.model.CoreUser;
import com.ppsi.jomi.service.CoreSystemManager;

/**author : aloysius.pradipta
 * version 1.0 15/5/2012
 */
@Service("coreSystemManager")
public class CoreSystemManagerImpl extends GenericManagerImpl<CoreSystem, String> implements CoreSystemManager{
	
	private CoreSystemDao coreSystemDao;
	private CoreSystem coreSystem;
	
	@Autowired
	public void setDao(CoreSystemDao dao) {
		this.dao = dao;
		this.coreSystemDao = dao;
	}

	@Override
	public List<CoreSystem> getCoreSystem() {
		return coreSystemDao.getCoreSystem();
	}

	@Override
	public List<CoreSystem> getRecordByUser(CoreUser coreUser, String status) {
		return coreSystemDao.getRecordByUser(coreUser, status);
	}

	@Override
	public void setStatus(SystemStatusEnum status) {
//		CoreSystem coreSystem = coreSystemDao.get(Constants.SYSTEM);
		this.coreSystem.setSystemStatus(status.getIndex());
//		coreSystemDao.save(coreSystem);
	}

	@Override
	public boolean isEndOfMonth() {
		CoreSystem coreSystem = coreSystemDao.get(Constants.SYSTEM);
		Calendar today = Calendar.getInstance();
		today.setTime(coreSystem.getTodayDate());
		
		Calendar tomorrow = Calendar.getInstance();
		tomorrow.setTime(coreSystem.getPreviousDate());
		
		if(today.get(Calendar.MONTH) != tomorrow.get(Calendar.MONTH))
			return true;
		return false;
	}

	@Override
	public boolean isEndOfYear() {
		CoreSystem coreSystem = coreSystemDao.get(Constants.SYSTEM);
		Calendar today = Calendar.getInstance();
		today.setTime(coreSystem.getTodayDate());
		
		Calendar tomorrow = Calendar.getInstance();
		tomorrow.setTime(coreSystem.getPreviousDate());
		
		if(today.get(Calendar.YEAR) != tomorrow.get(Calendar.YEAR))
			return true;
		return false;
	}

	@Override
	public boolean isBeginningOfMonth() {
		CoreSystem coreSystem = coreSystemDao.get(Constants.SYSTEM);
		Calendar today = Calendar.getInstance();
		today.setTime(coreSystem.getTodayDate());
		
		Calendar yesterday = Calendar.getInstance();
		yesterday.setTime(coreSystem.getPreviousDate());
		
		if(today.get(Calendar.MONTH) != yesterday.get(Calendar.MONTH))
			return true;
		return false;
	}

	@Override
	public boolean isBeginningOfYear(Date date) {
		Calendar today = Calendar.getInstance();
		today.setTime(date);
		


        // Ubah dikit ya Josh
        /*
        Calendar yesterday = Calendar.getInstance();
		yesterday.setTime(coreSystem.getPreviousDate());
		if(today.get(Calendar.YEAR) != yesterday.get(Calendar.YEAR))
			return true;
		*/
        if(today.get(Calendar.MONTH) == 0)
            return true;

        return false;
	}

	@Override
	public CoreSystem getCurrentSystem() {
		if (this.coreSystem == null) this.coreSystem = coreSystemDao.get(Constants.SYSTEM);
		
		Calendar today = Calendar.getInstance();
		today.setTime(this.coreSystem.getTodayDate());
		
		Calendar posting = Calendar.getInstance();
		posting.set(Calendar.DATE, today.get(Calendar.DATE));
		posting.set(Calendar.MONTH, today.get(Calendar.MONTH));
		posting.set(Calendar.YEAR, today.get(Calendar.YEAR));
		
		this.coreSystem.setPostingDate(posting.getTime());
		return this.coreSystem;
	}

	@Override
	public CoreSystem updateCurrentSystem(CoreSystem coreSystem) {
		this.coreSystem = coreSystemDao.save(coreSystem);
		return this.coreSystem;
	}

	@Override
	public void resetCurrentSystem() {
		this.coreSystem = null;
	}
}
