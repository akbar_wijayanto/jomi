package com.ppsi.jomi.service;

import java.util.List;

import com.ppsi.jomi.persistence.model.CoreTask;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:19:41 AM
 */
public interface CoreTaskManager extends GenericManager<CoreTask, Long> {

	List<CoreTask> getByProjectId(Long projectId);
	List<Object[]> getCountAllPriorityByProject(Long projectId);
	List<Object[]> getCountAllComplexityByProject(Long projectId);
	List<Object[]> getCountAllStatusByProject(Long projectId);
}
