package com.ppsi.jomi.service;

import java.util.List;

import com.ppsi.jomi.enumeration.jomi.TaskStatusEnum;
import com.ppsi.jomi.persistence.model.TaskAssignment;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 20, 2017 12:19:41 AM
 */
public interface TaskAssignmentManager extends GenericManager<TaskAssignment, Long> {
	List<TaskAssignment> getTaskByUsername(String username);
	List<TaskAssignment> getTaskByUsernameAndStatus(String username, TaskStatusEnum taskStatusEnum);
}
