package com.ppsi.jomi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppsi.jomi.persistence.dao.CoreRoleDao;
import com.ppsi.jomi.persistence.model.CoreRole;
import com.ppsi.jomi.service.CoreRoleManager;
import com.ppsi.jomi.service.CoreUserManager;

/**
 * Implementation of CoreRoleManager interface.
 *
 * @author <a href="mailto:dan@getrolling.com">Dan Kibler</a>
 */
@Service("coreRoleManager")
public class CoreRoleManagerImpl extends GenericManagerImpl<CoreRole, Integer>
		implements CoreRoleManager {
	private CoreRoleDao coreRoleDao;

	@Autowired
	private CoreUserManager coreUserManager;
	
	@Autowired
	public void setCoreRoleDao(CoreRoleDao coreRoleDao) {
		this.dao = coreRoleDao;
		this.coreRoleDao = coreRoleDao;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<CoreRole> getRoles(CoreRole coreRole) {
		return dao.getAll();
	}

	/**
	 * {@inheritDoc}
	 */
	public CoreRole getRole(String rolename) {
		return coreRoleDao.getRoleByName(rolename);
	}

	/**
	 * {@inheritDoc}
	 */
	public CoreRole saveRole(CoreRole coreRole) {
		return dao.save(coreRole);
	}

	/**
	 * {@inheritDoc}
	 */
	public void removeRole(String rolename) {
		coreRoleDao.removeRole(rolename);
	}

	@Override
	public List<CoreRole> getPagingResults(Long firstResult, Long maxResults) {
		return coreRoleDao.getPagingResults(firstResult, maxResults);
	}

	@Override
	public List<CoreRole> getAllWithList() {
		return coreRoleDao.getAllWithList();
	}

	@Override
	public CoreRole getWithListAll(Integer roleID) {
		return coreRoleDao.getWithListAll(roleID);
	}

	@Override
	public CoreRole getById(Integer id) {
		return coreRoleDao.getById(id);
	}

	

}