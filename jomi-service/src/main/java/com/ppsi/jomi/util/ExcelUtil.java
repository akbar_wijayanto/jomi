package com.ppsi.jomi.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {

	private XSSFWorkbook workbook;
	private XSSFSheet activeSheet;
	
	public ExcelUtil(String path) throws IOException
	{
		FileInputStream fileInputStream = new FileInputStream(path);
    	workbook = new XSSFWorkbook(fileInputStream);
    	activeSheet = workbook.getSheetAt(0);
	}
	
	public ExcelUtil(InputStream inputStream) throws IOException
	{
    	workbook = new XSSFWorkbook(inputStream);
    	activeSheet = workbook.getSheetAt(0);
	}
	
	public void setActiveSheet(String sheetName)
	{
		activeSheet = workbook.getSheet(sheetName);
	}
	
	public void setActiveSheet(int sheetIndex)
	{
		activeSheet = workbook.getSheetAt(sheetIndex);
	}
	
	public List<XSSFSheet> getSheets()
	{
		List<XSSFSheet> sheets = new ArrayList<XSSFSheet>();
		
    	for(int i=0; i<workbook.getNumberOfSheets();i++)
    	{
    		XSSFSheet sheet = workbook.getSheetAt(i);
    		sheets.add(sheet);
    	}
    	return sheets;
	}
	
	public List<String> getSheetNames()
	{
		List<String> sheetNames = new ArrayList<String>();
		for (XSSFSheet sheet : getSheets()) {
			sheetNames.add(sheet.getSheetName());
		}
		return sheetNames;
	}
	
	public List<String> getHeaders(int sheetIndex)
	{
		List<String> headers = new ArrayList<String>();
		XSSFSheet sheet = workbook.getSheetAt(sheetIndex);
		XSSFRow row = sheet.getRow(0);
		for(Cell cell : row)
		{
			headers.add(cell.getStringCellValue());
		}

		return headers;
	}
	
	public List<String> getHeaders(String sheetName)
	{
		return getHeaders(workbook.getSheetIndex(sheetName));
	}
	
	public List<String> getHeaders()
	{
		return getHeaders(activeSheet.getSheetName());
	}
	
	public Iterator<Row> getRowIterator(int sheetIndex)
	{
		XSSFSheet sheet = workbook.getSheetAt(sheetIndex);
		return sheet.rowIterator();
	}
	
	public Iterator<Row> getRowIterator()
	{
		return activeSheet.rowIterator();
	}
	
	public Object[] getRow(int row)
	{
		return getRow(activeSheet.getRow(row));
	}
	
	public Object[] getRow(Row row)
	{
		Object[] objects = new Object[row.getLastCellNum()];
		for (Cell cell : row) {
			objects[cell.getColumnIndex()] = getCellValue(cell);
		} 
		return objects;
	}
	
	private Object getCellValue(Cell cell)
	{
		Object value = null;
		switch(cell.getCellType())
		{
			case Cell.CELL_TYPE_BLANK:value = null;break;
			case Cell.CELL_TYPE_BOOLEAN:value = cell.getBooleanCellValue();break;
			case Cell.CELL_TYPE_NUMERIC:value = new BigDecimal(cell.getNumericCellValue());break;
			case Cell.CELL_TYPE_STRING:value = cell.getStringCellValue();break;
			case Cell.CELL_TYPE_FORMULA:
				switch(cell.getCachedFormulaResultType())
				{
					case Cell.CELL_TYPE_BLANK:value = null;break;
					case Cell.CELL_TYPE_BOOLEAN:value = cell.getBooleanCellValue();break;
					case Cell.CELL_TYPE_NUMERIC:value = new BigDecimal(cell.getNumericCellValue());break;
					case Cell.CELL_TYPE_STRING:value = cell.getStringCellValue();break;
				}
		}
		return value;
	}
	
	public List<Object> getColumn(int column)
	{
		List<Object> result = new ArrayList<Object>();
		
		for(Row row : activeSheet)
		{
			result.add(getCellValue(row.getCell(column)));
		}
		
		return result;
	}
	
	public Object getCellValue(int row, int column)
	{
		try {
			return getCellValue(activeSheet.getRow(row).getCell(column));
		} catch (NullPointerException e)
		{
			return null;
		}
	}
	
	public int getNumberOfColumn(Row row)
	{
		return activeSheet.getRow(row.getRowNum()).getLastCellNum();
	}
}
