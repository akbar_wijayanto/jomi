/**
 * 
 */
package com.ppsi.jomi.util;

import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.crypto.keygen.KeyGenerators;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a> 
 * Date Apr 22, 2017 11:40:25 PM
 */
public class JomiPasswordGenerator {

	public static String generate(String key, String credential) {
		ShaPasswordEncoder encoder = new ShaPasswordEncoder(512);
		encoder.setEncodeHashAsBase64(true);
		encoder.setIterations(1024);
		String newSalt = KeyGenerators.string().generateKey();
		String salt = key.concat(newSalt);
		String hash = encoder.encodePassword(credential, salt);
		return hash;
	}
	
	public static void main(String[] args) {
		String oldPass = "aGnop8QWoMLYx8UluRgNoVclbP1U72yfNl5z2j5qZFcQS/1LTxIVoamNWPqaL5wmM2ptuRIEuIZFS/n4AbQlnQ==";
		String newPass = JomiPasswordGenerator.generate("Sheila", "123456");
		
		System.out.println(oldPass);
		System.out.println(newPass);
		System.out.println(oldPass.equalsIgnoreCase(newPass));
	}

}
