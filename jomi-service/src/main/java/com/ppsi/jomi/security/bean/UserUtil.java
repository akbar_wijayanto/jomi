package com.ppsi.jomi.security.bean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;

import com.ppsi.jomi.Constants;
import com.ppsi.jomi.persistence.model.CoreSystem;
import com.ppsi.jomi.persistence.model.CoreUser;


/**
 * @author : <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 * @version : 1.0, 2/2/12 9:58 AM
 */
public final class UserUtil {


	public synchronized static String getCurrentUsername() {

		Object object = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(object instanceof UserId){
			return ((UserId) object).getUsername();
		} else if(object instanceof LdapUserDetailsImpl){
			return ((LdapUserDetailsImpl) object).getUsername();
		} else{
			return null;
		}

	}

//	public synchronized static CoreBranch getCurrentUserBranchId() {
//
//		UserId subject = (UserId)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		if(subject.getCoreUser().getActiveBranch().getId()!=null){
//			return subject.getCoreUser().getActiveBranch();
//		}else{
//			return null;
//		}
//
//	}

	public synchronized static CoreUser getCurrentUser() {
		if(SecurityContextHolder.getContext().getAuthentication() == null){
			return null;
		}
		Object object = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(object instanceof UserId)
			return   ((UserId) object).getCoreUser();
		else
			return null;
	}

	public synchronized static CoreSystem getCurrentSystem(HttpServletRequest request) {

		Object object = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(object != null){
			HttpSession session = request.getSession();
			CoreSystem coreSystem= (CoreSystem) session.getAttribute(Constants.SYSTEM);
			return coreSystem;
		}else{
			return null;
		}

	}
	public synchronized static UserId getPrincipal(){
		UserId subject = (UserId)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(subject!=null){
			return subject;
		}else{
			return null;
		}
	}

	public synchronized static Boolean checkAuthorities(String permission){
		UserId subject = (UserId) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Boolean check = false;
		for(GrantedAuthority authority : subject.getAuthorities()){
			if(authority.getAuthority().toString().equals(permission)){
				check =true;
			}
		}
		if(check){
			return true;
		}else{
			return false;
		}
	}
}

