/**
 * 
 */
package com.ppsi.jomi.security.impl;

import org.springframework.security.core.AuthenticationException;

/**Custom class for {@link ActiveDirectoryAuthenticationException}
 * @author : agung.kurniawan
 * Date : 15 Feb 2015
 */
@SuppressWarnings("serial")
public class CustomLDAPAuthenticationException extends AuthenticationException {
    private final String dataCode;

    CustomLDAPAuthenticationException(String dataCode, String message, Throwable cause) {
        super(message, cause);
        this.dataCode = dataCode;
    }

    public String getDataCode() {
        return dataCode;
    }

}
