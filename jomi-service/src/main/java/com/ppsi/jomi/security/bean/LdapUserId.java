package com.ppsi.jomi.security.bean;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;

import com.ppsi.jomi.persistence.model.CoreUser;

/**
 * Models core user information retrieved by a {@link LdapUserDetailsImpl}
 * 
 * @author akbar.wijayanto
 * Date Oct 27, 2015 4:14:18 PM
 */
public class LdapUserId extends LdapUserDetailsImpl {
    
    private static final long serialVersionUID = -8275492272371421013L;
   
	private CoreUser coreUser;
	private String salt;

    public LdapUserId(String salt, CoreUser user, String username, String password, boolean enabled,
            boolean accountNonExpired, boolean credentialsNonExpired,
            boolean accountNonLocked, Collection<GrantedAuthority> authorities)
            throws IllegalArgumentException {
		this.coreUser = user;
		this.salt = salt;
    }
    
	public String getSalt() {
		return this.salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public CoreUser getCoreUser() {
		return this.coreUser;
	}

	public void setCoreUser(CoreUser coreUser) {
		this.coreUser = coreUser;
	}

}