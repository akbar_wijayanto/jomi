/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ppsi.jomi.security.bean;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.ppsi.jomi.persistence.model.CoreUser;

/**
 *extending user
 *
 * @author muchamad.girinata
 */
public class UserId extends User {
    
    private static final long serialVersionUID = -8275492272371421013L;
   
	private CoreUser coreUser;
	private String salt;

    public UserId( String salt, CoreUser user, String username, String password, boolean enabled,
            boolean accountNonExpired, boolean credentialsNonExpired,
            boolean accountNonLocked, Collection<GrantedAuthority> authorities)
            throws IllegalArgumentException {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired,
                accountNonLocked, authorities);   
		this.coreUser =user;
		this.salt = salt;
    }
    
	public String getSalt() {
		return this.salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public CoreUser getCoreUser() {
		return this.coreUser;
	}

	public void setCoreUser(CoreUser coreUser) {
		this.coreUser = coreUser;
	}
    
    
    
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(super.toString()).append(": ");
        return sb.toString();
    }
    

}