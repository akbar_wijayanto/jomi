/**
 * 
 */
package com.ppsi.jomi.security.bean;

/**Custom interface that should be implemented by LDAP authentication provider class to notify changes
 * 
 * @author : agung.kurniawan
 * Date : 15 Feb 2015
 */
public interface ICustomLDAPNotification {
	
	public void notify(String domain, String url);

}
