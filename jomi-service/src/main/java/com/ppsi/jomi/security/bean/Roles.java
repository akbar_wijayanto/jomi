package com.ppsi.jomi.security.bean;

public class Roles {

	public static final String ADMIN = "ROLE_ADMIN";
	public static final String USER = "ROLE_USER";
	public static final String OPERATOR = "ROLE_OPERATOR";
	public static final String CUSTOMER = "ROLE_CUSTOMER";
	public static final String AGENT = "ROLE_AGENT";
}
