package com.ppsi.jomi.dto;

import java.io.Serializable;
import java.util.Arrays;

public class FileDto implements Serializable {

	private static final long serialVersionUID = 4090315537689484502L;
	private String header;

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	private String[] details;

	public String[] getDetails() {
		return details;
	}

	public void setDetails(String[] details) {
		this.details = details;
	}

	private String footer;
	
	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	private String[] additionalData;

	public String[] getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(String[] additionalData) {
		this.additionalData = additionalData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(additionalData);
		result = prime * result + Arrays.hashCode(details);
		result = prime * result + ((footer == null) ? 0 : footer.hashCode());
		result = prime * result + ((header == null) ? 0 : header.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FileDto other = (FileDto) obj;
		if (!Arrays.equals(additionalData, other.additionalData))
			return false;
		if (!Arrays.equals(details, other.details))
			return false;
		if (footer == null) {
			if (other.footer != null)
				return false;
		} else if (!footer.equals(other.footer))
			return false;
		if (header == null) {
			if (other.header != null)
				return false;
		} else if (!header.equals(other.header))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SknFileDto [header=" + header + ", details="
				+ Arrays.toString(details) + ", footer=" + footer
				+ ", additionalData=" + Arrays.toString(additionalData) + "]";
	}
	
}
