package com.ppsi.jomi.dto;

import java.io.Serializable;

public class BatchJobDto implements Serializable {

	private static final long serialVersionUID = -6943831991117389131L;

	private Long jobInstanceInfoId;
	private String jobInstanceName;
	private Integer jobExecutionCount;
	private Long lastJobExecutionId;
	private String lastJobExecutionStatus;	
	private String jobParameters;

	public Long getJobInstanceInfoId() {
		return jobInstanceInfoId;
	}

	public void setJobInstanceInfoId(Long jobInstanceInfoId) {
		this.jobInstanceInfoId = jobInstanceInfoId;
	}

	public String getJobInstanceName() {
		return jobInstanceName;
	}

	public void setJobInstanceName(String jobInstanceName) {
		this.jobInstanceName = jobInstanceName;
	}

	public Integer getJobExecutionCount() {
		return jobExecutionCount;
	}

	public void setJobExecutionCount(Integer jobExecutionCount) {
		this.jobExecutionCount = jobExecutionCount;
	}

	public Long getLastJobExecutionId() {
		return lastJobExecutionId;
	}

	public void setLastJobExecutionId(Long lastJobExecutionId) {
		this.lastJobExecutionId = lastJobExecutionId;
	}

	public String getLastJobExecutionStatus() {
		return lastJobExecutionStatus;
	}

	public void setLastJobExecutionStatus(String lastJobExecutionStatus) {
		this.lastJobExecutionStatus = lastJobExecutionStatus;
	}

	public String getJobParameters() {
		return jobParameters;
	}

	public void setJobParameters(String jobParameters) {
		this.jobParameters = jobParameters;
	}

	@Override
	public String toString() {
		return "BatchJobDto [jobInstanceInfoId=" + jobInstanceInfoId
				+ ", jobInstanceName=" + jobInstanceName
				+ ", jobExecutionCount=" + jobExecutionCount
				+ ", lastJobExecutionId=" + lastJobExecutionId
				+ ", lastJobExecutionStatus=" + lastJobExecutionStatus
				+ ", jobParameters=" + jobParameters + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((jobExecutionCount == null) ? 0 : jobExecutionCount
						.hashCode());
		result = prime
				* result
				+ ((jobInstanceInfoId == null) ? 0 : jobInstanceInfoId
						.hashCode());
		result = prime * result
				+ ((jobInstanceName == null) ? 0 : jobInstanceName.hashCode());
		result = prime * result
				+ ((jobParameters == null) ? 0 : jobParameters.hashCode());
		result = prime
				* result
				+ ((lastJobExecutionId == null) ? 0 : lastJobExecutionId
						.hashCode());
		result = prime
				* result
				+ ((lastJobExecutionStatus == null) ? 0
						: lastJobExecutionStatus.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BatchJobDto other = (BatchJobDto) obj;
		if (jobExecutionCount == null) {
			if (other.jobExecutionCount != null)
				return false;
		} else if (!jobExecutionCount.equals(other.jobExecutionCount))
			return false;
		if (jobInstanceInfoId == null) {
			if (other.jobInstanceInfoId != null)
				return false;
		} else if (!jobInstanceInfoId.equals(other.jobInstanceInfoId))
			return false;
		if (jobInstanceName == null) {
			if (other.jobInstanceName != null)
				return false;
		} else if (!jobInstanceName.equals(other.jobInstanceName))
			return false;
		if (jobParameters == null) {
			if (other.jobParameters != null)
				return false;
		} else if (!jobParameters.equals(other.jobParameters))
			return false;
		if (lastJobExecutionId == null) {
			if (other.lastJobExecutionId != null)
				return false;
		} else if (!lastJobExecutionId.equals(other.lastJobExecutionId))
			return false;
		if (lastJobExecutionStatus == null) {
			if (other.lastJobExecutionStatus != null)
				return false;
		} else if (!lastJobExecutionStatus.equals(other.lastJobExecutionStatus))
			return false;
		return true;
	}
}
