package com.ppsi.jomi.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author arni.sihombing
 *
 */
public class TrxRequestMoneyDto {

  private Long id;
  private String sender;
  private String receiver;
  private BigDecimal totalAmount;
  private String currency;
  private Date requestDate;
  private Date approveDate;
  private Date confirmDate;
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  public String getSender() {
    return sender;
  }
  public void setSender(String sender) {
    this.sender = sender;
  }
  public String getReceiver() {
    return receiver;
  }
  public void setReceiver(String receiver) {
    this.receiver = receiver;
  }
  public BigDecimal getTotalAmount() {
    return totalAmount;
  }
  public void setTotalAmount(BigDecimal totalAmount) {
    this.totalAmount = totalAmount;
  }
  public String getCurrency() {
    return currency;
  }
  public void setCurrency(String currency) {
    this.currency = currency;
  }
  public Date getRequestDate() {
    return requestDate;
  }
  public void setRequestDate(Date requestDate) {
    this.requestDate = requestDate;
  }
  public Date getApproveDate() {
    return approveDate;
  }
  public void setApproveDate(Date approveDate) {
    this.approveDate = approveDate;
  }
  public Date getConfirmDate() {
    return confirmDate;
  }
  public void setConfirmDate(Date confirmDate) {
    this.confirmDate = confirmDate;
  }

}
