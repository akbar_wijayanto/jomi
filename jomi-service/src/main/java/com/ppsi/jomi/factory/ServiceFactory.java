package com.ppsi.jomi.factory;

import com.ppsi.jomi.service.GenericManager;

public interface ServiceFactory {

	@SuppressWarnings("rawtypes")
	GenericManager getManager(String className);
	
}
