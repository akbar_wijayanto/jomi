del jomi-business-logic\.classpath
del jomi-business-logic\.project
rmdir /S /Q jomi-business-logic\.settings
rmdir /S /Q jomi-business-logic\target
rmdir /S /Q jomi-business-logic\bin

del jomi-core\.classpath
del jomi-core\.project
rmdir /S /Q jomi-core\.settings
rmdir /S /Q jomi-core\target
rmdir /S /Q jomi-core\bin

del jomi-persistence\.classpath
del jomi-persistence\.project
rmdir /S /Q jomi-persistence\.settings
rmdir /S /Q jomi-persistence\target
rmdir /S /Q jomi-persistence\bin

del jomi-service\.classpath
del jomi-service\.project
rmdir /S /Q jomi-service\.settings
rmdir /S /Q jomi-service\target
rmdir /S /Q jomi-service\bin

del jomi-web\.classpath
del jomi-web\.project
rmdir /S /Q jomi-web\.settings
rmdir /S /Q jomi-web\target
rmdir /S /Q jomi-web\bin

del jomi-jobs\.classpath
del jomi-jobs\.project
rmdir /S /Q jomi-jobs\.settings
rmdir /S /Q jomi-jobs\target
rmdir /S /Q jomi-jobs\bin

del jomi-report\.classpath
del jomi-report\.project
rmdir /S /Q jomi-report\.settings
rmdir /S /Q jomi-report\target
rmdir /S /Q jomi-report\bin

del jomi-integration\.classpath
del jomi-integration\.project
rmdir /S /Q jomi-integration\.settings
rmdir /S /Q jomi-integration\target
rmdir /S /Q jomi-integration\bin

del jomi-rest\.classpath
del jomi-rest\.project
rmdir /S /Q jomi-rest\.settings
rmdir /S /Q jomi-rest\target
rmdir /S /Q jomi-rest\bin