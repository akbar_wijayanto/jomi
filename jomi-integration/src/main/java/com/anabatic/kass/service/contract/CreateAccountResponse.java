
package com.anabatic.kass.service.contract;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.anabatic.kass.service.contract.bean.VirtualAccount;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vitualAccount" type="{http://kass.anabatic.com/service/contract/bean}VirtualAccount"/>
 *         &lt;element name="asMember" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vitualAccount",
    "asMember"
})
@XmlRootElement(name = "CreateAccountResponse")
public class CreateAccountResponse {

    @XmlElement(required = true)
    protected VirtualAccount vitualAccount;
    protected boolean asMember;

    /**
     * Gets the value of the vitualAccount property.
     * 
     * @return
     *     possible object is
     *     {@link VirtualAccount }
     *     
     */
    public VirtualAccount getVitualAccount() {
        return vitualAccount;
    }

    /**
     * Sets the value of the vitualAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link VirtualAccount }
     *     
     */
    public void setVitualAccount(VirtualAccount value) {
        this.vitualAccount = value;
    }

    /**
     * Gets the value of the asMember property.
     * 
     */
    public boolean isAsMember() {
        return asMember;
    }

    /**
     * Sets the value of the asMember property.
     * 
     */
    public void setAsMember(boolean value) {
        this.asMember = value;
    }

}
