/**
 * 
 */
package com.anabatic.retail.adapter.api;

import com.anabatic.retail.dto.PaymentDto;
import com.ppsi.jomi.exception.JomiException;

/**
 * @author akbar.wijayanto
 * Date Nov 2, 2015 10:09:18 PM
 */
public interface PaymentAdapter {
	
	PaymentDto createTransaction(PaymentDto paymentDto) throws JomiException;
	PaymentDto refundTransaction(String paymentReference) throws JomiException;
}
