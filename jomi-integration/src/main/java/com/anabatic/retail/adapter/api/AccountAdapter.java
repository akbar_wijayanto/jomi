/**
 * 
 */
package com.anabatic.retail.adapter.api;

import com.anabatic.retail.dto.AccountDto;
import com.ppsi.jomi.exception.JomiException;

/**
 * @author akbar.wijayanto
 * Date Oct 27, 2015 11:20:01 PM
 */
public interface AccountAdapter {
	
	AccountDto balanceInquiry(String accountNumber) throws JomiException;
	AccountDto createUserAccount(String username) throws JomiException;
	AccountDto updateLimitPayroll(AccountDto dto) throws JomiException;
	AccountDto getLimitPayroll(AccountDto dto) throws JomiException;
}
