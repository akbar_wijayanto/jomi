/**
 * 
 */
package com.anabatic.retail.adapter.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.anabatic.kass.service.contract.KassService;
import com.anabatic.kass.service.contract.KassServices;
import com.ppsi.jomi.exception.JomiException;

/**
 * @author akbar.wijayanto
 * Date Oct 27, 2015 11:43:13 PM
 */
public abstract class BaseKKASAdapterImpl {

    protected final Log log = LogFactory.getLog(BaseKKASAdapterImpl.class);
    
//    private NonFinService nonFinService;
//    private TrxService trxService;
    private KassService kassService;

	public BaseKKASAdapterImpl() {
		new Thread(new KKASConnector()).start();
	}
	
	private class KKASConnector implements Runnable {

		@Override
		public void run() {
			try {
				kassService = new KassServices().getKassServiceSoap11();
				log.debug("KKAS Connected on class "+getClassName());
			} catch (Exception e) {
				log.debug("KKAS failed to connect on "+getClassName()+", reason "+e.getMessage());
			}
		}
		
	}
	
	private final String getClassName() {
		return getClass().getSimpleName();
	}
	
//	protected final NonFinService getNonFinService() throws RetailException {
//		if (nonFinService == null) 
//			nonFinService = new NonFinServices().getNonFinServiceSoap11();
//		return nonFinService;
//	}
//	
//	protected final TrxService getTrxService() throws RetailException {
//		if (trxService == null) 
//			trxService = new TrxServices().getTrxServiceSoap11();
//		return trxService;
//	}
	
	protected final KassService getKassService() throws JomiException {
		if (kassService == null) 
			kassService = new KassServices().getKassServiceSoap11();
		return kassService;
	}
}
