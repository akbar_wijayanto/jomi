package com.ppsi.jomi.businesslogic.exception;

public class InterestException extends Exception {

	private static final long serialVersionUID = -2475913015979783650L;

	public InterestException(String message) {
        super(message);
    }

    public InterestException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
