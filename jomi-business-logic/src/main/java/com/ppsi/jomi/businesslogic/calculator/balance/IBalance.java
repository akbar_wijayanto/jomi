/**
 * 
 */
package com.ppsi.jomi.businesslogic.calculator.balance;

import java.math.BigDecimal;
import java.util.List;

/**
 * 
 * @version 1.0, Aug 26, 2015 2:35:27 PM
 */
public interface IBalance {
	BigDecimal calculate(List<BigDecimal> balances);
}
