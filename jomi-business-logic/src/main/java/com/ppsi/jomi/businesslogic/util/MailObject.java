package com.ppsi.jomi.businesslogic.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

/**
 * @author akbar.wijayanto
 * Date 4 Feb 2014
 */
public class MailObject {

	private JavaMailSender mailSender;
	private SimpleMailMessage simpleMailMessage;
	private File mailTemplateFile;

	public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage) {
		this.simpleMailMessage = simpleMailMessage;
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	/**
	 * used to write and send messages
	 * 
	 * @param from
	 * @param to
	 * @param cc
	 * @param subject
	 * @param content
	 */
	public void sendMail(String from, String to, String cc, String subject,
			String content) {

		MimeMessage message = mailSender.createMimeMessage();

		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			helper.setFrom(String.format(simpleMailMessage.getFrom(), from));
			helper.setTo(String.format(to, simpleMailMessage.getTo(), to));
			if (cc!=null) helper.setCc(String.format(cc, simpleMailMessage.getCc(), cc));
			helper.setSubject(String.format(simpleMailMessage.getSubject(),
					subject));
			helper.setText(String.format(simpleMailMessage.getText(), content), true);

			// TODO : Attachment
//			 FileSystemResource file = new FileSystemResource("C:");
//			
//			 helper.addAttachment(files, file);

		} catch (MessagingException e) {
			throw new MailParseException(e);
		}
		mailSender.send(message);
		System.out.println("messages sent");

	}

	/**
	 * Send multiple receiver with attachment
	 * 
	 * @param from
	 * @param to
	 * @param cc
	 * @param subject
	 * @param content
	 * @param attachment
	 */
	public void sendMail(String from, String[] to, String[] cc, String subject,
			String content, File attachment){
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			helper.setFrom(String.format(simpleMailMessage.getFrom(), from));
			if (to!=null)helper.setTo(to);
			if (cc!=null) helper.setCc(cc);
			helper.setSubject(String.format(simpleMailMessage.getSubject(),subject));
			helper.setText(String.format(simpleMailMessage.getText(), content));
			if (attachment!=null) helper.addAttachment(attachment.getName(), attachment);
			
			mailSender.send(message);
		} catch (MessagingException e) {
			throw new MailParseException(e);
		}
	}
	
	/**
	 * send mail using HTML tags
	 * 
	 * @param from
	 * @param to
	 * @param cc
	 * @param subject
	 * @param content
	 * @param attachment
	 * @param isHTML
	 */
	public void sendMail(String from, String[] to, String[] cc, String subject,
			String content, File attachment, boolean isHTML){
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			helper.setFrom(String.format(simpleMailMessage.getFrom(), from));
			if (to!=null)helper.setTo(to);
			if (cc!=null) helper.setCc(cc);
			helper.setSubject(String.format(simpleMailMessage.getSubject(),subject));
			helper.setText(String.format(simpleMailMessage.getText(), content), isHTML);
			if (attachment!=null) helper.addAttachment(attachment.getName(), attachment);
			
			mailSender.send(message);
		} catch (MessagingException e) {
			throw new MailParseException(e);
		}
	}
	
}
