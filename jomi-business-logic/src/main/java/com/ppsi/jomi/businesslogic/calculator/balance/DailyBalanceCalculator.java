/**
 * 
 */
package com.ppsi.jomi.businesslogic.calculator.balance;

import java.math.BigDecimal;
import java.util.List;


/**
 * 
 * @version 1.0, Sep 17, 2015 3:01:51 PM
 */
public class DailyBalanceCalculator implements IBalance {

	@Override
	public BigDecimal calculate(List<BigDecimal> balances) {
		return balances.get(balances.size()-1);
	}

}
