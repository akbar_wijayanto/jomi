///**
// * 
// */
//package com.ppsi.jomi.businesslogic.util;
//
//import java.util.Date;
//
//import com.anabatic.asa.calculator.util.date.ActualDayDiffCalculator;
//
///**
// * 
// * @version 1.0, Aug 19, 2015 4:25:47 PM
// */
//public class DateCalculatorUtil {
//
//	/**
//	 * Get difference day between two date
//	 * 
//	 * @param Date firstDate
//	 * @param Date secondDate
//	 * @return Integer
//	 */
//	public static Integer getDifferenceDay(Date prevDate, Date date) {
//		ActualDayDiffCalculator calculator = new ActualDayDiffCalculator();
//		return calculator.calculate(prevDate, date).intValue();
//	}
//}
