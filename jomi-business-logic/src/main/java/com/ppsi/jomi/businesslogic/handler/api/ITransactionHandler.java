/**
 * 
 */
package com.ppsi.jomi.businesslogic.handler.api;

import com.ppsi.jomi.exception.JomiException;

/**
 * @author akbar.wijayanto
 * Date Nov 2, 2015 9:45:47 PM
 */
public interface ITransactionHandler<T extends Object> {
	
	public T process(T t) throws JomiException;

}
