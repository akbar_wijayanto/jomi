/**
 * 
 */
package com.anabatic.kamp.scheduler;

import java.text.ParseException;
import java.util.List;

import org.quartz.CronTrigger;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.anabatic.kamp.persistence.model.SchedulerCron;
import com.anabatic.kamp.service.SchedulerCronManager;

/**
 * @author dimas.sulistyono
 *
 */
@Component
public class CronScheduler {
	
	@Autowired
	private SchedulerCronManager schedulerCronManager;
	
	@Autowired
	private Scheduler scheduler;
	
	public void setNewCron(CronTrigger cronTrigger){
		List<SchedulerCron> schedulerCrons = schedulerCronManager.getAll();
		String cron = "0 0 " + schedulerCrons.get(0).getHour() + "," + schedulerCrons.get(1).getHour() + " * * ?";
		try {
			cronTrigger.setCronExpression(cron);
			scheduler.rescheduleJob(cronTrigger.getName(), cronTrigger.getGroup(), cronTrigger);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	
	public void printMe(){
		System.out.println("Jalan broooooo!!!!");
	}
}
