package com.anabatic.kamp.batch;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;

import com.anabatic.kamp.persistence.model.CoreSystem;
import com.anabatic.kamp.service.CoreSystemManager;

public class OpeningClosingSystemWriter implements ItemWriter<CoreSystem>{
	
	private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        if (jdbcTemplate == null) {
            this.jdbcTemplate = new JdbcTemplate(dataSource);
        }
    }
    
    @Autowired
    private CoreSystemManager mgr;
    
	@Override
	public void write(final List<? extends CoreSystem> items) throws Exception {
		if(items.size()>0)
		{
            String sql = "update core_system set system_status=? where id=?";
            jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                	CoreSystem coreSystem = items.get(i);
                    ps.setInt(1, coreSystem.getSystemStatus());
                    ps.setString(2, coreSystem.getId());
                }

                public int getBatchSize() {
                    return items.size();
                }
            });
            
            mgr.resetCurrentSystem();
		}
	}

}
