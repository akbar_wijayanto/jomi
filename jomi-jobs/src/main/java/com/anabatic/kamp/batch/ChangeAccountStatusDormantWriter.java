package com.anabatic.kamp.batch;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.batch.item.ItemWriter;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;

import com.anabatic.kamp.batch.dto.AccountDormantDto;

/**
 * 
 * @version 1.0, Aug 19, 2015 9:56:44 PM
 */
public class ChangeAccountStatusDormantWriter implements ItemWriter<AccountDormantDto>{
	
	private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        if (jdbcTemplate == null) {
            this.jdbcTemplate = new JdbcTemplate(dataSource);
        }
    }
    
	@Override
	public void write(final List<? extends AccountDormantDto> items) throws Exception {
		if(items.size()>0)
		{
			for (AccountDormantDto item : items) {
				if(item.getStatusAccount() != null) {
					jdbcTemplate.batchUpdate("update customer_account_opening set status_account=? where id=?", new BatchPreparedStatementSetter() {
		            	
		                public void setValues(PreparedStatement ps, int i) throws SQLException {
		                	AccountDormantDto dto = items.get(i);
		                	ps.setInt(1, dto.getStatusAccount().getKey());
		                	ps.setLong(2, dto.getId());
		                }
	
		                public int getBatchSize() {
		                    return items.size();
		                }
		            });
				}
			}
		}
	}

}
