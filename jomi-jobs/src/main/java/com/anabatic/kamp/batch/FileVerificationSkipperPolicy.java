package com.anabatic.kamp.batch;

import java.util.List;

import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;

public class FileVerificationSkipperPolicy implements SkipPolicy {

	private List<Class<? extends Exception>> exceptionClassToSkip;

	public FileVerificationSkipperPolicy(
			List<Class<? extends Exception>> exceptionClassToSkip) {
		super();
		this.exceptionClassToSkip = exceptionClassToSkip;
	}

	@Override
	public boolean shouldSkip(Throwable t, int skipCount)
			throws SkipLimitExceededException {
		
		for (Class<? extends Exception> iterable : exceptionClassToSkip) {
			if (iterable.isAssignableFrom(t.getClass()))
			{
				return true;				
			}

		}
		
		return false;
	}

}
