package com.anabatic.kamp.batch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.anabatic.kamp.persistence.model.CoreBranch;
import com.anabatic.kamp.persistence.model.CoreCurrency;
import com.anabatic.kamp.persistence.model.CustomerAccountOpening;
import com.anabatic.kamp.persistence.model.Product;

/**
 * 
 * @version 1.0, Aug 6, 2015 9:20:26 PM
 */
public class CalculationInterestRowMapper implements RowMapper<CustomerAccountOpening> {

	@Override
	public CustomerAccountOpening mapRow(ResultSet rs, int rowNum) throws SQLException {
		CustomerAccountOpening account = new CustomerAccountOpening();
		account.setId(rs.getLong("id"));
		account.setCodeAccount(rs.getString("code_account"));
		
		Product product = new Product();
		product.setId(rs.getLong("product_id"));
		account.setProductId(product);
		
//		CustomerCif customerCif = new CustomerCif();
//		customerCif.setId(rs.getLong("id_cif"));
//		account.setIdCif(customerCif);
		
		CoreCurrency currency = new CoreCurrency();
		currency.setId(rs.getString("currency"));
		account.setCurrency(currency);
//		account.setPrintedName(rs.getString("printed_name"));
//		account.setRepresentativeName(rs.getString("representative_name"));
//		account.setPurposeOfAccountOpening(rs.getString("purpose_of_account"));
//		account.setAccountOfficer(rs.getString("account_officer"));
//		account.setPrintedPassbook(rs.getBoolean("printed_passbook"));
//		
//		CoreTax coreTax = new CoreTax();
//		coreTax.setId(rs.getLong("tax_id"));
//		account.setTax(coreTax);
//		
//		account.setDateOpenAccount(rs.getDate("date_open_account"));
		account.setOnlineBalance(rs.getBigDecimal("online_balance"));
		account.setWorkingBalance(rs.getBigDecimal("working_balance"));
		account.setAvailableBalance(rs.getBigDecimal("available_balance"));
		
		account.setOverdraft(rs.getBoolean("overdraft"));
		account.setLimitOverdraft(rs.getBigDecimal("limit_overdraft"));
		
		CoreBranch coreBranch = new CoreBranch();
		coreBranch.setId(rs.getString("branch_open"));
		
		account.setBranchOpen(coreBranch);
		return account;
	}

}
