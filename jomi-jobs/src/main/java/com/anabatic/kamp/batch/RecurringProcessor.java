package com.anabatic.kamp.batch;

import java.math.BigDecimal;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.anabatic.kamp.enumeration.TransactionTypeEnum;
import com.anabatic.kamp.persistence.model.CoreCurrencyRate;
import com.anabatic.kamp.persistence.model.TrxRecord;
import com.anabatic.kamp.persistence.model.TrxRecurring;
import com.anabatic.kamp.security.bean.UserUtil;
import com.anabatic.kamp.service.CoreCurrencyRateManager;
import com.anabatic.kamp.service.CoreSystemManager;
import com.anabatic.kamp.service.TransactionCodeManager;

/**
 * 
 * @version 1.0, Sep 16, 2015 1:45:12 AM
 */
public class RecurringProcessor implements ItemProcessor<TrxRecurring, TrxRecord> {
	
	@Autowired 
	private CoreSystemManager coreSystemManager;
	
	@Autowired
	private CoreCurrencyRateManager coreCurrencyRateManager; 
	
	@Autowired 
	private TransactionCodeManager transactionCodeManager;
	
	@Override
	public TrxRecord process(TrxRecurring item) throws Exception {
		TrxRecord record = new TrxRecord();
		record.setBookingDate(coreSystemManager.getCurrentSystem().getTodayDate());
		record.setPostingDate(coreSystemManager.getCurrentSystem().getPostingDate());
		CoreCurrencyRate rate = coreCurrencyRateManager.getByCurrencies(item.getCcyAccountFrom(), item.getCcyAccountTo());
		record.setRate(rate.getTtMidRate().setScale(2, BigDecimal.ROUND_UNNECESSARY));
		record.setAccountFrom(item.getAccountFrom());
		record.setAccountTo(item.getAccountTo());
		record.setAmount(item.getAmount());
		record.setDescription("Recurring");
		record.setCoreBranch(UserUtil.getCurrentUserBranchId().getId());
		record.setCoreCurrency(item.getCcyAccountFrom());
		record.setPairedCurrency(item.getCcyAccountTo());
		record.setTeller("TL002");
		record.setTrxCode(transactionCodeManager.get(TransactionTypeEnum.INTERNAL_TRANSFER.getTransactionType()));
		
    	return record;
	}

}
