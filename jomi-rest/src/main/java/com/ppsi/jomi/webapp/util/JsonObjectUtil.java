package com.ppsi.jomi.webapp.util;

import java.io.IOException;
import java.util.Arrays;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.JavaType;

/**
 * @author Harits Fahreza Christyonotoputra<br><br>
 *
 * Class to create JSON String and map JSON String to the JSON Object
 *
 */
public class JsonObjectUtil<T> {

	private ObjectMapper mapper = new ObjectMapper();

	private Class<?>[] objectClasses;

	public JsonObjectUtil(Class<?>... classes) {
		this.objectClasses = Arrays.copyOf(classes, classes.length);
	}

	/**
	 * Generate JSON Object from JSON String. The object field has to represent
	 * all attribute in the JSON String.
	 * 
	 * @param JSON
	 *            String
	 * @return Object T
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public T generateJsonObject(String json) throws JsonParseException, JsonMappingException, IOException {
		if (objectClasses.length > 1) {
			Class<?>[] genericObjectClasses = Arrays.copyOfRange(objectClasses, 1, objectClasses.length);
			JavaType javaType = mapper.getTypeFactory().constructParametricType(objectClasses[0], genericObjectClasses);
			return (T) mapper.readValue(json, javaType);
		} else {
			return (T) mapper.readValue(json, objectClasses[0]);
		}
		
	}

	/**
	 * Generate JSON String from JSON Object. The attributes of the JSON String
	 * will be represent the JSON Object fields.
	 * 
	 * @param Object
	 *            T
	 * @return JSON String
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public String generateJson(T jsonObject) throws JsonGenerationException, JsonMappingException, IOException {
		return mapper.writeValueAsString(jsonObject);
	}

}
