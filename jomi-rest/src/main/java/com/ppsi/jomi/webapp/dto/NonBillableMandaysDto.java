/**
 * 
 */
package com.ppsi.jomi.webapp.dto;

import java.util.ArrayList;
import java.util.List;

import com.ppsi.jomi.persistence.model.NonBillableMandays;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 16, 2017 9:01:45 PM
 */
public class NonBillableMandaysDto extends BaseRestParamDto{
	
	private String username;
	private List<NonBillableMandays> nonBillableMandays = new ArrayList<NonBillableMandays>();
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<NonBillableMandays> getNonBillableMandays() {
		return nonBillableMandays;
	}

	public void setNonBillableMandays(List<NonBillableMandays> nonBillableMandays) {
		this.nonBillableMandays = nonBillableMandays;
	}
	
}
