/**
 * 
 */
package com.ppsi.jomi.webapp.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 24, 2017 9:12:31 AM
 */
/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 24, 2017 9:18:51 AM
 */
public class ProjectTaskDto {
	
	private String projectName;
	private List<TaskDto> taskDtos = new ArrayList<TaskDto>();
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public List<TaskDto> getTaskDtos() {
		return taskDtos;
	}
	public void setTaskDtos(List<TaskDto> taskDtos) {
		this.taskDtos = taskDtos;
	}
	
}
