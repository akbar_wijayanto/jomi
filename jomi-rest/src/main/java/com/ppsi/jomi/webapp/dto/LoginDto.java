/**
 * 
 */
package com.ppsi.jomi.webapp.dto;

import com.ppsi.jomi.persistence.model.CoreUser;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 16, 2017 8:03:05 PM
 */
public class LoginDto extends BaseRestParamDto{
	
	private String username;
	private String password;
	private CoreUser coreUser;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public CoreUser getCoreUser() {
		return coreUser;
	}
	public void setCoreUser(CoreUser coreUser) {
		this.coreUser = coreUser;
	}
}
