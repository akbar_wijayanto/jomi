/**
 * 
 */
package com.ppsi.jomi.webapp.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ppsi.jomi.enumeration.jomi.ErrorCodeEnum;
import com.ppsi.jomi.enumeration.jomi.NonBillableMandaysTypeEnum;
import com.ppsi.jomi.persistence.model.NonBillableMandays;
import com.ppsi.jomi.service.CoreUserManager;
import com.ppsi.jomi.service.NonBillableMandaysManager;
import com.ppsi.jomi.webapp.dto.NonBillableMandaysDto;
import com.ppsi.jomi.webapp.dto.LeaveDto;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 16, 2017 9:00:41 PM
 */
@Controller
@RequestMapping("/app/nonbillablemandays")
public class NonBillableMandaysController {
	
	protected final Log log = LogFactory.getLog(NonBillableMandaysController.class);
	
	private NonBillableMandaysManager nonBillableMandaysManager;
	private CoreUserManager coreUserManager;
	
	@Autowired
	public void setCoreUserManager(CoreUserManager coreUserManager) {
		this.coreUserManager = coreUserManager;
	}
	
	@Autowired
	public void setNonBillableMandaysManager(NonBillableMandaysManager nonBillableMandaysManager) {
		this.nonBillableMandaysManager = nonBillableMandaysManager;
	}
	
	@RequestMapping(value = "/getNonBillableMandaysByUsername",  method = RequestMethod.POST, consumes = "application/json")
	ResponseEntity<NonBillableMandaysDto> getNonBillableMandaysByUsername(@RequestBody NonBillableMandaysDto request) throws Exception {
		if(request==null) throw new Exception("Invalid parameter");
		try {
			List<NonBillableMandays> nonBillableMandays = nonBillableMandaysManager.getAllLive();
			request.setNonBillableMandays(nonBillableMandays);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<NonBillableMandaysDto>(request, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<NonBillableMandaysDto>(request, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addNonBillableMandays",  method = RequestMethod.POST, consumes = "application/json")
	ResponseEntity<LeaveDto> addNonBillableMandays(@RequestBody LeaveDto request) throws Exception {
		if(request==null) throw new Exception("Invalid parameter");
		try {
			NonBillableMandays nonBillableMandays = new NonBillableMandays();
			NonBillableMandaysTypeEnum type = NonBillableMandaysTypeEnum.get(request.getIdLeaveType());
			nonBillableMandays.setCoreUser(coreUserManager.getUserByUsername(request.getUsername()));
			nonBillableMandays.setNonBillableMandaysTypeEnum(type);
			nonBillableMandays.setDescription(request.getDescription());
			nonBillableMandays = nonBillableMandaysManager.save(nonBillableMandays);
			request.setErrorCode(ErrorCodeEnum.SUCCESS.getCode());
			request.setMessageCode(ErrorCodeEnum.SUCCESS.getDefaultMsg());
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<LeaveDto>(request, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<LeaveDto>(request, HttpStatus.OK);
	}
	
}
