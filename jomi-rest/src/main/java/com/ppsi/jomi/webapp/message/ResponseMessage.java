package com.ppsi.jomi.webapp.message;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ResponseMessage {

	private String field;
	private String message;
	
	public ResponseMessage(String field, String message){
		this.field = field;
		this.message = message;
	}
	
	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "ResponseMessage [field=" + field + ", message=" + message + "]";
	}

}
