package com.ppsi.jomi.webapp.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ppsi.jomi.ErrorCode;
import com.ppsi.jomi.enumeration.jomi.ErrorCodeEnum;
import com.ppsi.jomi.persistence.model.CoreUser;
import com.ppsi.jomi.service.CoreUserManager;
import com.ppsi.jomi.webapp.dto.LoginDto;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 16, 2017 7:59:39 PM
 */
@Controller
@RequestMapping("/app")
public class LoginController {

	protected final Log log = LogFactory.getLog(LoginController.class);
	
	private CoreUserManager coreUserManager;
	private AuthenticationManager authenticationManager;
	
	@Autowired
	public void setCoreUserManager(CoreUserManager coreUserManager) {
		this.coreUserManager = coreUserManager;
	}
	
	@Autowired
	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}
	
	@RequestMapping(value = "/login",  method = RequestMethod.POST, consumes = "application/json")
	ResponseEntity<LoginDto> login(@RequestBody LoginDto request) throws Exception {
		if(request==null) throw new Exception("Invalid parameter");
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					request.getUsername(), request.getPassword()));
			
			CoreUser coreUser = coreUserManager.getUserByUsername(request.getUsername());
			if(coreUser!=null){
//				request.setCoreUser(coreUser);
				request.setErrorCode(ErrorCodeEnum.SUCCESS.getCode());
				request.setMessageCode(ErrorCodeEnum.SUCCESS.getDefaultMsg());
			} else {
				request.setErrorCode(ErrorCodeEnum.CUSTOMER_NOT_FOUND.getCode());
				request.setMessageCode(ErrorCodeEnum.CUSTOMER_NOT_FOUND.getDefaultMsg());
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			request.setErrorCode(ErrorCodeEnum.AUTHENTICATION_FAILED.getCode());
			request.setMessageCode(e.getMessage());
			return new ResponseEntity<LoginDto>(request, HttpStatus.OK);
		}
		return new ResponseEntity<LoginDto>(request, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/logout",  method = RequestMethod.POST, consumes = "application/json")
	ResponseEntity<LoginDto> logout(@RequestBody LoginDto request) throws Exception {
		if(request==null) throw new Exception("Invalid parameter");
		try {
			CoreUser coreUser = coreUserManager.getUserByUsername(request.getUsername());
			if(coreUser!=null){
				coreUser.setSession(null);
				coreUser = coreUserManager.save(coreUser);
				request.setCoreUser(coreUser);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseEntity<LoginDto>(request, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<LoginDto>(request, HttpStatus.OK);
	}
	
}
