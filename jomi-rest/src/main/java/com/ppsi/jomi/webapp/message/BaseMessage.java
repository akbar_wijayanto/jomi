package com.ppsi.jomi.webapp.message;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class BaseMessage<T> {

	private String messageType;
	private String messageId;
	private String moduleCode;
	private String processingCode;
	private String clientId;
	private String datetime;
	private T request;

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public T getRequest() {
		return request;
	}

	public void setRequest(T request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "BaseMessage [messageType=" + messageType + ", messageId=" + messageId + ", moduleCode=" + moduleCode
				+ ", processingCode=" + processingCode + ", clientId=" + clientId + ", datetime=" + datetime
				+ ", request=" + request + "]";
	}

}
