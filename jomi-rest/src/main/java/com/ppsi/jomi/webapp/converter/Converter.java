package com.ppsi.jomi.webapp.converter;

import java.util.List;

import com.ppsi.jomi.exception.JomiException;

public interface Converter<T, P>{
	T fromContract(P convertedObject) throws JomiException;
	P toContract(T convertedObject);
	List<T> fromContracts(List<P> convertedObjects);
	List<P> toContracts(List<T> convertedObjects);
}
