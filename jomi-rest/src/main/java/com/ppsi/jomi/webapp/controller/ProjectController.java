/**
 * 
 */
package com.ppsi.jomi.webapp.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ppsi.jomi.enumeration.jomi.ErrorCodeEnum;
import com.ppsi.jomi.persistence.model.CoreProject;
import com.ppsi.jomi.service.CoreProjectManager;
import com.ppsi.jomi.webapp.dto.ProjectDto;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 16, 2017 9:00:41 PM
 */
@Controller
@RequestMapping("/app/project")
public class ProjectController {
	
	protected final Log log = LogFactory.getLog(ProjectController.class);
	
	private CoreProjectManager coreProjectManager;
	
	@Autowired
	public void setCoreProjectManager(CoreProjectManager coreProjectManager) {
		this.coreProjectManager = coreProjectManager;
	}
	
	@RequestMapping(value = "/getCustomerProjects",  method = RequestMethod.POST, consumes = "application/json")
	ResponseEntity<ProjectDto> getCustomerProjects(@RequestBody ProjectDto request) throws Exception {
		if(request==null) throw new Exception("Invalid parameter");
		try {
			List<CoreProject> projects = coreProjectManager.getProjectByUsername(request.getUsername());
			if(projects.size()==0){
				request.setHasProject(false);
				request.setErrorCode(ErrorCodeEnum.SUCCESS.getCode());
				request.setMessageCode(ErrorCodeEnum.SUCCESS.getDefaultMsg());
			}else{
				for(CoreProject project : projects){
					CoreProject model = new CoreProject();
					model.setId(project.getId());
					model.setName(project.getName());
					model.setProjectNumber(project.getProjectNumber());
					model.setDescription(project.getDescription());
					model.setStatus(project.getStatus());
					request.getCoreProject().add(model);
					request.setHasProject(true);
					request.setErrorCode(ErrorCodeEnum.SUCCESS.getCode());
					request.setMessageCode(ErrorCodeEnum.SUCCESS.getDefaultMsg());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ProjectDto>(request, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<ProjectDto>(request, HttpStatus.OK);
	}
	
}
