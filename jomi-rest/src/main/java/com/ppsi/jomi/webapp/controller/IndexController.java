package com.ppsi.jomi.webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class IndexController {
	
	@RequestMapping("")
	public String getIndex(){
		return "Hai";
	}
	
	@RequestMapping("/hallo")
	public @ResponseBody String getHallo(){
		return "Hai";
	}
}
