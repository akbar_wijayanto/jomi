package com.ppsi.jomi.webapp.message;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class BaseResponseMessage<T, P> extends BaseMessage<T> {

	private String responseCode;
	private P response;
	private List<ResponseMessage> responseMessages;
	
	public BaseResponseMessage() {
	}
	
	public BaseResponseMessage(BaseMessage<T> baseMessage) {
		this.setClientId(baseMessage.getClientId());
		this.setDatetime(baseMessage.getDatetime());
		this.setMessageId(baseMessage.getMessageId());
		this.setModuleCode(baseMessage.getModuleCode());
		this.setProcessingCode(baseMessage.getProcessingCode());
		this.setRequest(baseMessage.getRequest());
	}
	
	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public P getResponse() {
		return response;
	}

	public void setResponse(P response) {
		this.response = response;
	}

	public List<ResponseMessage> getResponseMessages() {
		return responseMessages;
	}

	public void setResponseMessages(List<ResponseMessage> responseMessages) {
		this.responseMessages = responseMessages;
	}

	public void addResponseMessage(ResponseMessage responseMessage){
		if(this.responseMessages == null)
			this.responseMessages = new ArrayList<ResponseMessage>();
		this.responseMessages.add(responseMessage);
	}
	
	@Override
	public String toString() {
		return "BaseResponseMessage [responseCode=" + responseCode + ", response=" + response + ", responseMessages="
				+ responseMessages + ", getMessageType()=" + getMessageType() + ", getMessageId()=" + getMessageId()
				+ ", getModuleCode()=" + getModuleCode() + ", getProcessingCode()=" + getProcessingCode()
				+ ", getClientId()=" + getClientId() + ", getDatetime()=" + getDatetime() + ", getRequest()="
				+ getRequest() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + "]";
	}

}
