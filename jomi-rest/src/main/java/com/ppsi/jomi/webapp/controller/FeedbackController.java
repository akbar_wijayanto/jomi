/**
 * 
 */
package com.ppsi.jomi.webapp.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ppsi.jomi.enumeration.jomi.ErrorCodeEnum;
import com.ppsi.jomi.persistence.model.Feedback;
import com.ppsi.jomi.service.FeedbackManager;
import com.ppsi.jomi.webapp.dto.FeedbackDto;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 16, 2017 9:05:06 PM
 */
@Controller
@RequestMapping("/app/feedback")
public class FeedbackController {
	
	protected final Log log = LogFactory.getLog(FeedbackController.class);
	
	private FeedbackManager feedbackManager;
	
	@Autowired
	public void setFeedbackManager(FeedbackManager feedbackManager) {
		this.feedbackManager = feedbackManager;
	}
	
	@RequestMapping(value = "/add",  method = RequestMethod.POST, consumes = "application/json")
	ResponseEntity<FeedbackDto> addFeedback(@RequestBody FeedbackDto request) throws Exception {
		if(request==null) throw new Exception("Invalid parameter");
		try {
			Feedback feedback = new Feedback();
			feedback.setDescription(request.getDescription());
			feedback = feedbackManager.save(feedback);
			request.setErrorCode(ErrorCodeEnum.SUCCESS.getCode());
			request.setMessageCode(ErrorCodeEnum.SUCCESS.getDefaultMsg());
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<FeedbackDto>(request, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<FeedbackDto>(request, HttpStatus.OK);
	}
	
}
