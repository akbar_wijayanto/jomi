/**
 * 
 */
package com.ppsi.jomi.webapp.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ppsi.jomi.persistence.model.CoreUser;
import com.ppsi.jomi.service.CoreUserManager;
import com.ppsi.jomi.util.JomiPasswordGenerator;
import com.ppsi.jomi.webapp.dto.ChangePasswordDto;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 16, 2017 9:05:06 PM
 */
@Controller
@RequestMapping("/app/setting")
public class SettingController {
	
	protected final Log log = LogFactory.getLog(SettingController.class);

	private CoreUserManager coreUserManager;

	@Autowired
	public void setCoreUserManager(CoreUserManager coreUserManager) {
		this.coreUserManager = coreUserManager;
	}
	
	@RequestMapping(value = "/changePassword",  method = RequestMethod.POST, consumes = "application/json")
	ResponseEntity<ChangePasswordDto> changePassword(@RequestBody ChangePasswordDto request) throws Exception {
		if(request==null) throw new Exception("Invalid parameter");
		try {
			CoreUser coreUser = coreUserManager.getUserByUsername(request.getUsername());
			if(coreUser!=null){
				String oldPassword = JomiPasswordGenerator.generate(request.getUsername(), request.getOldPassword());
				if(oldPassword.equalsIgnoreCase(coreUser.getPassword())){
					coreUser.setPassword(request.getNewPassword());
					coreUser = coreUserManager.save(coreUser);
					request.setErrorCode("0000");
					request.setMessageCode("Success");
				} else {
					request.setErrorCode("5001");
					request.setMessageCode("Old password doesn't match");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ChangePasswordDto>(request, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<ChangePasswordDto>(request, HttpStatus.OK);
	}
	
}
