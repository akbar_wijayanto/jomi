/**
 * 
 */
package com.ppsi.jomi.webapp.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a> Date
 *         Apr 16, 2017 9:06:07 PM
 */
public class TaskDto extends BaseRestParamDto {

	private String username;
	private Long taskId;
	private String taskName;
	private String taskDesc;
	private String priority;
	private String complexity;
	private Integer manHour;
	private Integer taskStatus;
	private boolean hasTask;
	private List<TaskDto> dtos = new ArrayList<TaskDto>();
	
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getTaskDesc() {
		return taskDesc;
	}
	public void setTaskDesc(String taskDesc) {
		this.taskDesc = taskDesc;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public Integer getTaskStatus() {
		return taskStatus;
	}
	public void setTaskStatus(Integer taskStatus) {
		this.taskStatus = taskStatus;
	}
	public String getComplexity() {
		return complexity;
	}
	public void setComplexity(String complexity) {
		this.complexity = complexity;
	}
	public Integer getManHour() {
		return manHour;
	}
	public void setManHour(Integer manHour) {
		this.manHour = manHour;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public List<TaskDto> getDtos() {
		return dtos;
	}
	public void setDtos(List<TaskDto> dtos) {
		this.dtos = dtos;
	}
	public boolean isHasTask() {
		return hasTask;
	}
	public void setHasTask(boolean hasTask) {
		this.hasTask = hasTask;
	}
}
