/**
 * 
 */
package com.ppsi.jomi.webapp.dto;

import java.util.ArrayList;
import java.util.List;

import com.ppsi.jomi.persistence.model.CoreProject;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 16, 2017 9:01:45 PM
 */
public class ProjectDto extends BaseRestParamDto{
	
	private String username;
	private List<CoreProject> coreProject = new ArrayList<CoreProject>();
	private boolean hasProject;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<CoreProject> getCoreProject() {
		return coreProject;
	}

	public void setCoreProject(List<CoreProject> coreProject) {
		this.coreProject = coreProject;
	}

	public boolean isHasProject() {
		return hasProject;
	}

	public void setHasProject(boolean hasProject) {
		this.hasProject = hasProject;
	}
}
