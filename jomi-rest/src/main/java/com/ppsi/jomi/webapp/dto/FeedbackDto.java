/**
 * 
 */
package com.ppsi.jomi.webapp.dto;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 29, 2017 3:53:34 PM
 */
public class FeedbackDto extends BaseRestParamDto{

	private String username;
	private String description;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
