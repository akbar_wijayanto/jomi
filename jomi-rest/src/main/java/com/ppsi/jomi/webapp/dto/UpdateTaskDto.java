/**
 * 
 */
package com.ppsi.jomi.webapp.dto;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 16, 2017 9:06:07 PM
 */
public class UpdateTaskDto extends BaseRestParamDto{

	private String username;
	private String taskAssignmentId;
	private String status;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTaskAssignmentId() {
		return taskAssignmentId;
	}

	public void setTaskAssignmentId(String taskAssignmentId) {
		this.taskAssignmentId = taskAssignmentId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
