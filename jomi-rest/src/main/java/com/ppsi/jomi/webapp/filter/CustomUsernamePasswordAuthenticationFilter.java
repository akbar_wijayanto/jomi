package com.ppsi.jomi.webapp.filter;

import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.ppsi.jomi.Constants;
import com.ppsi.jomi.persistence.model.CoreRole;
import com.ppsi.jomi.persistence.model.CoreSystem;
import com.ppsi.jomi.persistence.model.CoreUser;
import com.ppsi.jomi.service.CoreMenuManager;
import com.ppsi.jomi.service.CoreSystemManager;
import com.ppsi.jomi.service.CoreUserManager;
import com.ppsi.jomi.webapp.security.AuthenticationRequiredFieldsException;
import com.ppsi.jomi.webapp.security.LoginFormDto;

/**
 * extending UsernamePasswordAuthentication
 * 
 * @author muchamad.girinata
 * 
 */
public class CustomUsernamePasswordAuthenticationFilter extends
UsernamePasswordAuthenticationFilter {
	@Autowired
	private CoreMenuManager coreMenuManager;
	@Autowired
	CoreSystemManager coreSystemManager;
	@Autowired
	CoreUserManager coreUserManager;
	/*	@Autowired
	EntityModelManager entityModelManager;
	 */
	private int trialLogin = 0;
	@Autowired
	private LocalValidatorFactoryBean validator;
	private MessageSourceAccessor messages;
	
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
		LoginFormDto loginFormDto = new LoginFormDto();
		loginFormDto.setJ_username(request.getParameter(getUsernameParameter()));
		loginFormDto.setJ_password(request.getParameter(getPasswordParameter()));

		Errors errors = new BeanPropertyBindingResult(loginFormDto, "login");
		validator.validate(loginFormDto, errors);

		//	 DataBinder binder = new DataBinder(loginFormDto);
		//	 binder.setValidator(validator);
		//	 binder.validate();
		//	 BindingResult result = binder.getBindingResult();

		//	 System.out.println(errors.getFieldError(SPRING_SECURITY_FORM_USERNAME_KEY).getDefaultMessage());


		if (errors.hasErrors()) {
			throw new AuthenticationRequiredFieldsException( messages.getMessage("login.error.message", request.getLocale()));
		}

		Authentication authResult = super.attemptAuthentication(request, response);

		return authResult;
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request,
			HttpServletResponse response, Authentication authResult)
					throws IOException, ServletException {
		
		String username = authResult.getName();
		
		CoreUser coreUser = coreUserManager.getUserSpring(username);
		Calendar cal = Calendar.getInstance();
		super.successfulAuthentication(request, response, authResult);
		// Check active locale
		if (StringUtils.isEmpty(coreUser.getPreferredLocale())) {
			coreUser.setPreferredLocale("en_US");
		}
		// Set active branch
//		if (coreUser.getActiveBranch() == null
//				&& coreUser.getCoreBranches().size() > 0) {
//			coreUser.setActiveBranch((CoreBranch) coreUser.getCoreBranches()
//					.toArray()[0]);
//		}

		// Set active role
		if (coreUser.getActiveRole() == null
				&& coreUser.getCoreRoles().size() > 0) {
			coreUser.setActiveRole((CoreRole) coreUser.getCoreRoles().toArray()[0]);
		}
		setSession(request, coreUser, cal);

	}

	private void setSession(HttpServletRequest request, CoreUser coreUser,
			Calendar cal) {
		HttpSession session = request.getSession();
		session.setAttribute("username", coreUser.getUsername());
		CoreSystem coreSystem = coreSystemManager.getCurrentSystem();
		coreSystem.setTodayDate(coreSystem.getTodayDate());
		session.setAttribute(Constants.SYSTEM,
				coreSystem);
		session.setAttribute(Constants.USER_MENU,
				coreMenuManager.getUserMenusByActive(coreUser.getActiveRole().getId()));
		session.setAttribute(Constants.CORE_USER, coreUser);
		session.setAttribute(Constants.PREFERRED_LOCALE_KEY, new Locale(coreUser.getPreferredLocale().substring(0, 2), coreUser.getPreferredLocale().substring(2, 2)));
		coreUser.setLoginAttempt(0);
		coreUser.setLastLogon(cal.getTime());
		coreUser.setSession(request.getSession().getId());
		coreUserManager.updateLoginAttempt(coreUser);
		session.setMaxInactiveInterval(coreUser.getSessionTimeout().intValue()*60);
	}

	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException failed)
					throws IOException, ServletException {
		super.unsuccessfulAuthentication(request, response, failed);
		String username = request.getParameter(getUsernameParameter());
		CoreUser coreUser = coreUserManager.getUserByUsername(username);
		System.out.println("login failed");
		if (coreUser != null) {
			if (coreUser.getLoginAttempt() != null) {
				trialLogin = coreUser.getLoginAttempt();
				coreUser.setLoginAttempt(++trialLogin);
			} else
				coreUser.setLoginAttempt(1);

			if (trialLogin >= Constants.LOGIN_MAX_ATTEMPTS) {
				coreUser.setAccountLocked(true);
				coreUser.setAccountEnabled(false);
				System.out.println("account is locked");
			}
			coreUserManager.updateLoginAttempt(coreUser);
		}
	}

	public LocalValidatorFactoryBean getValidator() {
		return validator;
	}

	public void setValidator(LocalValidatorFactoryBean validator) {
		this.validator = validator;
	}
	
	@Autowired
    public void setMessages(MessageSource messageSource) {
        messages = new MessageSourceAccessor(messageSource);
    }

}
