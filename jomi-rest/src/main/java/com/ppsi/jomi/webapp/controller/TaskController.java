/**
 * 
 */
package com.ppsi.jomi.webapp.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ppsi.jomi.enumeration.jomi.ErrorCodeEnum;
import com.ppsi.jomi.enumeration.jomi.TaskStatusEnum;
import com.ppsi.jomi.persistence.model.TaskAssignment;
import com.ppsi.jomi.service.TaskAssignmentManager;
import com.ppsi.jomi.webapp.dto.TaskDto;
import com.ppsi.jomi.webapp.dto.UpdateTaskDto;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Apr 16, 2017 9:05:06 PM
 */
@Controller
@RequestMapping("/app/task")
public class TaskController {

	protected final Log log = LogFactory.getLog(TaskController.class);
	
	private TaskAssignmentManager taskAssignmentManager;
	
	@Autowired
	public void setTaskAssignmentManager(TaskAssignmentManager taskAssignmentManager) {
		this.taskAssignmentManager = taskAssignmentManager;
	}
	
	@RequestMapping(value = "/getCustomerTasks",  method = RequestMethod.POST, consumes = "application/json")
	ResponseEntity<TaskDto> getCustomerTasks(@RequestBody TaskDto request) throws Exception {
		if(request==null) throw new Exception("Invalid parameter");
		try {
			List<TaskAssignment> taskAssignments = taskAssignmentManager.getTaskByUsername(request.getUsername());
			if(taskAssignments.size()==0){
				request.setHasTask(false);
				request.setErrorCode(ErrorCodeEnum.SUCCESS.getCode());
				request.setMessageCode(ErrorCodeEnum.SUCCESS.getDefaultMsg());
			}else{
				for (TaskAssignment taskAssignment : taskAssignments) {
					TaskDto taskDto = new TaskDto();
					taskDto.setTaskId(taskAssignment.getId());
					taskDto.setTaskName(taskAssignment.getCoreTask().getFeature());
					taskDto.setTaskDesc(taskAssignment.getCoreTask().getDescription());
					taskDto.setPriority(taskAssignment.getCoreTask().getPriority().getLabel());
					taskDto.setComplexity(taskAssignment.getCoreTask().getComplexity().getLabel());
					taskDto.setManHour(taskAssignment.getCoreTask().getManHour());
					taskDto.setTaskStatus(taskAssignment.getTaskStatus().getId());
					request.getDtos().add(taskDto);
					request.setHasTask(true);
					request.setErrorCode(ErrorCodeEnum.SUCCESS.getCode());
					request.setMessageCode(ErrorCodeEnum.SUCCESS.getDefaultMsg());
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseEntity<TaskDto>(request, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<TaskDto>(request, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updateTask",  method = RequestMethod.POST, consumes = "application/json")
	ResponseEntity<UpdateTaskDto> updateTask(@RequestBody UpdateTaskDto request) throws Exception {
		if(request==null) throw new Exception("Invalid parameter");
		try {
			TaskAssignment taskAssignment = taskAssignmentManager.get(new Long(request.getTaskAssignmentId()));
			taskAssignment.setTaskStatus(TaskStatusEnum.get(new Integer(request.getStatus())));
			taskAssignment = taskAssignmentManager.save(taskAssignment);
			// return data
			request.setTaskAssignmentId(taskAssignment.getId().toString());
			request.setStatus(taskAssignment.getTaskStatus().getLabel());
			request.setErrorCode(ErrorCodeEnum.SUCCESS.getCode());
			request.setMessageCode(ErrorCodeEnum.SUCCESS.getDefaultMsg());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseEntity<UpdateTaskDto>(request, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<UpdateTaskDto>(request, HttpStatus.OK);
	}
	
}
